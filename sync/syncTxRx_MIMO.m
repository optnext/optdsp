function [txSignal,delay] = syncTxRx_MIMO(A,EQ)

% Last Update: 07/05/2017


%% Input Parser
if isfield(EQ,'syncMethod')
    SYNC.method = EQ.syncMethod;
else
    SYNC.method = 'complexField';
end
if isfield(EQ,'debugPlots')
    SYNC.debugPlots = EQ.debugPlots;
else
    SYNC.debugPlots = [];
end
if ~isfield(EQ,'nPol')
    if size(A,1) > 1
        EQ.nPol = 2;
    else
        EQ.nPol = 1;
    end
end
if EQ.nPol == 2 && ~any(EQ.txSignal(1,:)-EQ.txSignal(2,:))
    polMuxEmulator = true;
else
    polMuxEmulator = false;
end

%% Synchronization
for k = 1:size(A,1)
    if EQ.nPol == 2 && strcmp(SYNC.method,'abs')
        if ~polMuxEmulator
            if mod(k,2)
                TX = EQ.txSignal(k,:);
                [txXX,Dxx,Gxx] = syncSignals(A(k,:),TX,SYNC);
                TX = EQ.txSignal(k+1,:);
                [txXY,Dxy,Gxy] = syncSignals(A(k,:),TX,SYNC);
                if Gxx > Gxy
                    txSignal(k,:)   = txXX;
                    delay(k)        = Dxx;
                else
                    txSignal(k,:)   = txXY;
                    delay(k)        = Dxy;
                end
            else
                if Gxx > Gxy
                    TX = EQ.txSignal(k,:);
                    [txSignal(k,:),delay(k)] = syncSignals(A(k,:),TX,SYNC);
                else
                    TX = EQ.txSignal(k-1,:);
                    [txSignal(k,:),delay(k)] = syncSignals(A(k,:),TX,SYNC);
                end
            end
        else
            TX = EQ.txSignal(1,:);
            if mod(k,2)
                [txSignal(k,:),delay(k)] = syncSignals(A(k,:),TX,SYNC);
            else
                [txSignal(k,:),delay(k)] = syncSignals(A(k,:),TX,SYNC);
                if abs(delay(k) - delay(k-1)) < 3
                    SYNC.ignoreFirstMax = true;
                    [txSignal(k,:),delay(k)] = syncSignals(A(k,:),TX,SYNC);
                    SYNC.ignoreFirstMax = false;
                end
            end
        end            
            
    elseif EQ.nPol == 2 && strcmp(SYNC.method,'complexField')
        if mod(k,2)
            TX = EQ.txSignal(k,:);
            [txXX_IQ,Dxx_IQ,Gxx_IQ] = syncSignals(A(k,:),TX,SYNC);
            TX = imag(EQ.txSignal(k,:)) + 1j*real(EQ.txSignal(k,:));
            [txXX_QI,Dxx_QI,Gxx_QI] = syncSignals(A(k,:),TX,SYNC);
            TX = EQ.txSignal(k+1,:);
            [txXY_IQ,Dxy_IQ,Gxy_IQ] = syncSignals(A(k,:),TX,SYNC);
            TX = imag(EQ.txSignal(k+1,:)) + 1j*real(EQ.txSignal(k+1,:));
            [txXY_QI,Dxy_QI,Gxy_QI] = syncSignals(A(k,:),TX,SYNC);
            [~,ind] = max([Gxx_IQ Gxx_QI Gxy_IQ Gxy_QI]);
            switch ind
                case 1
                    txSignal(k,:)   = txXX_IQ;
                    delay(k)        = Dxx_IQ;
                case 2
                    txSignal(k,:)   = txXX_QI;
                    delay(k)        = Dxx_QI;
                case 3
                    txSignal(k,:)   = txXY_IQ;
                    delay(k)        = Dxy_IQ;
                case 4
                    txSignal(k,:)   = txXY_QI;
                    delay(k)        = Dxy_QI;
            end
        else
            switch ind
                case {1,2}
                    TX = EQ.txSignal(k,:);
                    [tx_IQ,D_IQ,G_IQ] = syncSignals(A(k,:),TX,SYNC);
                    TX = imag(EQ.txSignal(k,:)) + 1j*real(EQ.txSignal(k,:));
                    [tx_QI,D_QI,G_QI] = syncSignals(A(k,:),TX,SYNC);
                case {3,4}
                    TX = EQ.txSignal(k-1,:);
                    [tx_IQ,D_IQ,G_IQ] = syncSignals(A(k,:),TX,SYNC);
                    TX = imag(EQ.txSignal(k-1,:)) + 1j*real(EQ.txSignal(k-1,:));
                    [tx_QI,D_QI,G_QI] = syncSignals(A(k,:),TX,SYNC);
            end
            if G_IQ > G_QI
                txSignal(k,:)   = tx_IQ;
                delay(k)        = D_IQ;
            else
                txSignal(k,:)   = tx_QI;
                delay(k)        = D_QI;
            end
        end
    elseif nPol == 1
        TX = EQ.txSignal(k,:);
        [txSignal(k,:),delay(k)] = syncSignals(A(k,:),TX,SYNC);
    end
end

