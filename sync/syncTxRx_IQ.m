function [txSignal,delay] = syncTxRx_IQ(TX,RX,SYNC)
%syncTxRx_IQ
% Function that synchronizes the transmitted and received signals by
% independently analyzing and aligning their real (I) and imaginary (Q)
% parts. 
% Last Update: 30/10/2016


%% Input Parser
if ~exist('SYNC','var')
    SYNC.method = 'complexField';
end

%% Input Parameters
nRX = size(TX,1);       % number of received signals
nTX = size(RX,1);       % number of different transmitted signals

%% Synchronization
for k = 1:nRX
    for n = 1:nTX
        % Try to synchronize the real parts:
        [TX_I(n,:),D_I(n),G_I(n)] = syncSignals(real(RX(k,:)),...
            real(TX(n,:)),SYNC);
        % Try to synchronize the real parts:
        [TX_Q(n,:),D_Q(n),G_Q(n)] = syncSignals(imag(RX(k,:)),...
            imag(TX(n,:)),SYNC);
    end
    % Find the best correspondence between TX and RX signals:
    [~,sync_I] = max(G_I);
    [~,sync_Q] = max(G_Q);
    % Save the synchronized TX signal:
    TX_sync_I(k,:) = real(TX_I(sync_I,:));
    TX_sync_Q(k,:) = real(TX_Q(sync_Q,:));
    % Save the delays:
    D_sync_I(k) = D_I(sync_I);
    D_sync_Q(k) = D_Q(sync_Q);
end

%% Output Parameters
txSignal    = TX_sync_I + 1j*TX_sync_Q;
delay       = [D_sync_I; D_sync_Q];

