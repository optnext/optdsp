function [Stx_SYNC] = syncTxRx_SCM(Srx,Stx,SYNC)

% Last Update: 10/04/2017


%% Input Parser
if ~exist('SYNC','var')
    SYNC.syncMethod = 'complexField';
    SYNC.nPol = 2;
else
    if ~isfield(SYNC,'syncMethod')
        SYNC.syncMethod = 'complexField';
    end
    if ~isfield(SYNC,'nPol')
        SYNC.nPol = 2;
    end
end
nSC = numel(Srx);

%% Synchronize Tx and Rx Signals
for k = 1:nSC
    SYNC.txSignal = Stx{k};
    Stx_SYNC{k} = syncTxRx_MIMO(Srx{k},SYNC);
end
