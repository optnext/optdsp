function [TX_sync,SYNC] = syncSignals_2x2(RX,TX,SYNC)

% Last Update: 26/07/2017

global PROG;


%% Input Parser
if ~exist('SYNC','var')
    SYNC.method = 'complexField';
    showPlots = false;
else
    if ~isfield(SYNC,'method')
        SYNC.method = 'complexField';
    end
    if ~isfield(SYNC,'debugPlots')
        showPlots = false;
    else
        showPlots = any(strcmpi(SYNC.debugPlots,{'xCorr','all'}));
        debugPlots = SYNC.debugPlots;
        SYNC = rmfield(SYNC,'debugPlots');
    end
end
if ~any(TX(1,:)-TX(2,:)) || ~any(TX(1,:)+TX(2,:))
    polMuxEmulator = true;
    TX = TX(1,:);
else
    polMuxEmulator = false;
end

%% Synchronization
if ~polMuxEmulator
    [txXX,Dxx,Gxx,Rxx] = syncSignals(RX(1,:),TX(1,:),SYNC);
    [txXY,Dxy,Gxy,Rxy] = syncSignals(RX(1,:),TX(2,:),SYNC);
    if Gxx > Gxy
        [txYY,Dyy,Gyy,Ryy] = syncSignals(RX(2,:),TX(2,:),SYNC);
        TX_sync(1,:) = txXX;
        TX_sync(2,:) = txYY;
        SYNC.Gxx = Gxx;
        SYNC.Gxy = Gxy;
        SYNC.Gyy = Gyy;
        SYNC.Dxx = Dxx;
        SYNC.Dyy = Dyy;
        SYNC.polDelay = Dyy - Dxx;
        SYNC.rotXX = Rxx;
        SYNC.rotYY = Ryy;
        SYNC.syncOrder = 'Tx[X]->Rx[X] | Tx[Y]->Rx[Y]';
        
        % Debug plots:
        if showPlots
            nSamples = min(length(RX),length(TX));
            PLOT.showPlot = true;
            PLOT.figNumber = PROG.currentFig + 1;
            PROG.currentFig = PLOT.figNumber;
            PLOT.Color = 'b';
            PLOT.LineStyle = '-';
            switch SYNC.method
                case 'complexField'
                    xCorrPlot(RX(1,1:nSamples),TX(1,1:nSamples),Dxx,PLOT);
                    hold on;
                    PLOT.Color = 'r';
                    PLOT.LineStyle = '--';
                    xCorrPlot(RX(2,1:nSamples),TX(2,1:nSamples),Dyy,PLOT);
                    legend('Tx[X]->Rx[X]','Tx[Y]->Rx[Y]');
                case 'abs'
                    A = abs(RX(1,1:nSamples)) - mean(abs(RX(1,1:nSamples)));
                    B = abs(TX(1,1:nSamples)) - mean(abs(TX(1,1:nSamples)));
                    xCorrPlot(A,B,Dxx,PLOT);
                    hold on;
                    PLOT.Color = 'r';
                    PLOT.LineStyle = '--';
                    A = abs(RX(2,1:nSamples)) - mean(abs(RX(2,1:nSamples)));
                    B = abs(TX(2,1:nSamples)) - mean(abs(TX(2,1:nSamples)));
                    xCorrPlot(A,B,Dyy,PLOT);
            end
        end
    else
        [txYX,Dyx,Gyx,Ryx] = syncSignals(RX(2,:),TX(1,:),SYNC);
        TX_sync(1,:) = txXY;
        TX_sync(2,:) = txYX;
        SYNC.Gxy = Gxy;
        SYNC.Gyx = Gyx;
        SYNC.Gxx = Gxx;
        SYNC.Dxy = Dxy;
        SYNC.Dyx = Dyx;
        SYNC.polDelay = Dyx - Dxy;
        SYNC.rotXY = Rxy;
        SYNC.rotYX = Ryx;
        SYNC.syncOrder = 'Tx[X]->Rx[Y] | Tx[Y]->Rx[X]';
        
        % Debug plots:
        if showPlots
            nSamples = min(length(RX),length(TX));
            PLOT.showPlot = true;
            PLOT.figNumber = PROG.currentFig + 1;
            PROG.currentFig = PLOT.figNumber;
            PLOT.Color = 'b';
            PLOT.LineStyle = '-';
            switch SYNC.method
                case 'complexField'
                    xCorrPlot(RX(2,1:nSamples),TX(1,1:nSamples),Dyx,PLOT);
                    hold on;
                    PLOT.Color = 'r';
                    PLOT.LineStyle = '--';
                    xCorrPlot(RX(1,1:nSamples),TX(2,1:nSamples),Dyx,PLOT);
                case 'abs'
                    A = abs(RX(2,1:nSamples)) - mean(abs(RX(2,1:nSamples)));
                    B = abs(TX(1,1:nSamples)) - mean(abs(TX(1,1:nSamples)));
                    xCorrPlot(A,B,Dyx,PLOT);
                    hold on;
                    PLOT.Color = 'r';
                    PLOT.LineStyle = '--';
                    A = abs(RX(1,1:nSamples)) - mean(abs(RX(1,1:nSamples)));
                    B = abs(TX(2,1:nSamples)) - mean(abs(TX(2,1:nSamples)));
                    xCorrPlot(A,B,Dyx,PLOT);
            end
            legend('Tx[X]->Rx[Y]','Tx[Y]->Rx[X]');
        end
    end
else
    [TX_sync(1,:),Dxx,Gxx,Rxx] = syncSignals(RX(1,:),TX,SYNC);
    [TX_sync(2,:),Dyx,Gyx,Ryx] = syncSignals(RX(2,:),TX,SYNC);
    SYNC.syncOrder = 'Tx[X]->Rx[X] | Tx[X]->Rx[Y]';
    SYNC.Gxx = Gxx;
    SYNC.Gyx = Gyx;
    SYNC.Dxx = Dxx;
    SYNC.Dyx = Dyx;
    SYNC.polDelay = Dyx - Dxx;
    SYNC.rotXX = Rxx;
    SYNC.rotYX = Ryx;

    % Debug plots:
    if showPlots
        nSamples = min(length(RX),length(TX));
        PLOT.showPlot = true;
        PLOT.figNumber = PROG.currentFig + 1;
        PROG.currentFig = PLOT.figNumber;
        PLOT.Color = 'b';
        PLOT.LineStyle = '-';
        switch SYNC.method
            case 'complexField'
                [~,~,~,hPlot(1)] = xCorrPlot(RX(1,1:nSamples),TX(1,1:nSamples),...
                    Dxx,PLOT);
                hold on;
                PLOT.Color = 'r';
                PLOT.LineStyle = '--';
                [~,~,~,hPlot(2)] = xCorrPlot(RX(2,1:nSamples),TX(1,1:nSamples),...
                    Dyx,PLOT);
            case 'abs'
                A = abs(RX(1,1:nSamples)) - mean(abs(RX(1,1:nSamples)));
                B = abs(TX(1,1:nSamples)) - mean(abs(TX(1,1:nSamples)));
                [~,~,~,hPlot] = xCorrPlot(A,B,Dxx,PLOT);
                hold on;
                PLOT.Color = 'r';
                PLOT.LineStyle = '--';
                A = abs(RX(2,1:nSamples)) - mean(abs(RX(2,1:nSamples)));
                B = abs(TX(1,1:nSamples)) - mean(abs(TX(1,1:nSamples)));
                xCorrPlot(A,B,Dyx,PLOT);
        end
        legend(hPlot,{'Tx[X]->Rx[X]','Tx[X]->Rx[Y]'});
    end    
end

%% Outputs
if exist('debugPlots','var')
    SYNC.debugPlots = debugPlots;
end



%     elseif TX.nPol == 2 && strcmp(SYNC.method,'complexField')
%         if mod(k,2)
%             TX = TX.txSignal(k,:);
%             [txXX_IQ,Dxx_IQ,Gxx_IQ] = syncSignals(RX(k,:),TX,SYNC);
%             TX = imag(TX.txSignal(k,:)) + 1j*real(TX.txSignal(k,:));
%             [txXX_QI,Dxx_QI,Gxx_QI] = syncSignals(RX(k,:),TX,SYNC);
%             TX = TX.txSignal(k+1,:);
%             [txXY_IQ,Dxy_IQ,Gxy_IQ] = syncSignals(RX(k,:),TX,SYNC);
%             TX = imag(TX.txSignal(k+1,:)) + 1j*real(TX.txSignal(k+1,:));
%             [txXY_QI,Dxy_QI,Gxy_QI] = syncSignals(RX(k,:),TX,SYNC);
%             [~,ind] = max([Gxx_IQ Gxx_QI Gxy_IQ Gxy_QI]);
%             switch ind
%                 case 1,
%                     TX_sync(k,:)   = txXX_IQ;
%                     delay(k)        = Dxx_IQ;
%                 case 2,
%                     TX_sync(k,:)   = txXX_QI;
%                     delay(k)        = Dxx_QI;
%                 case 3,
%                     TX_sync(k,:)   = txXY_IQ;
%                     delay(k)        = Dxy_IQ;
%                 case 4,
%                     TX_sync(k,:)   = txXY_QI;
%                     delay(k)        = Dxy_QI;
%             end
%         else
%             switch ind
%                 case {1,2},
%                     TX = TX.txSignal(k,:);
%                     [tx_IQ,D_IQ,G_IQ] = syncSignals(RX(k,:),TX,SYNC);
%                     TX = imag(TX.txSignal(k,:)) + 1j*real(TX.txSignal(k,:));
%                     [tx_QI,D_QI,G_QI] = syncSignals(RX(k,:),TX,SYNC);
%                 case {3,4},
%                     TX = TX.txSignal(k-1,:);
%                     [tx_IQ,D_IQ,G_IQ] = syncSignals(RX(k,:),TX,SYNC);
%                     TX = imag(TX.txSignal(k-1,:)) + 1j*real(TX.txSignal(k-1,:));
%                     [tx_QI,D_QI,G_QI] = syncSignals(RX(k,:),TX,SYNC);
%             end
%             if G_IQ > G_QI
%                 TX_sync(k,:)   = tx_IQ;
%                 delay(k)        = D_IQ;
%             else
%                 TX_sync(k,:)   = tx_QI;
%                 delay(k)        = D_QI;
%             end
%         end
%     elseif nPol == 1
%         TX = TX.txSignal(k,:);
%         [TX_sync(k,:),delay(k)] = syncSignals(RX(k,:),TX,SYNC);
%     end
% end
% 
