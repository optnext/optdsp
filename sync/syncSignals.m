function [OUT,delay,gain,rot] = syncSignals(A,B,SYNC)
%
% Change Log:
% 2016-06-01: Introduced a functionality to ignore the first maximum in the
% correlation, in order to work with dual-polarization signals in which the
% two polarizations share the same PRBS.
% 2016-06-03: Corrected a bug on the detection and removal of the first
% maximum of correlation, in case of dual-polarization signals in which the
% two polarizations share the same PRBS.
% 2016-06-07: Added option "syncOrder" (SYNC.syncOrder) that allows to
% synchronize either B w.r.t. A (the default option, 'B->A') or A w.r.t. B
% ('A->B').
% 2016-12-03: Added output variable "rot" containing the rotation applied
% to the synchronized signal.
%
% Last Update: 24/07/2017


%% Input Parser
if ~exist('SYNC','var')
    SYNC.method = 'complexField';
%     SYNC.debugPlots = {};
    SYNC.debugPlots = {'xCorr'};
else
    if ~isfield(SYNC,'method')
        SYNC.method = 'complexField';
    end
end
if isfield(SYNC,'presetDelay')
    delay = SYNC.presetDelay;
end
if ~isfield(SYNC,'nPeakIgnore')
    SYNC.nPeakIgnore = 0;
end
if ~isfield(SYNC,'syncOrder')
    SYNC.syncOrder = 'B->A';
end

%% Input Parameters
nSamplesA = length(A);
nSamplesB = length(B);
A_all = A;
if nSamplesA > nSamplesB
    A = A(1:nSamplesB);
end

%% Calculate Cross Correlation Between A and B
if ~exist('delay','var')
    if strcmp(SYNC.method,'abs')
        [AB,lags] = xcorr(abs(A) - mean(abs(A)),abs(B) - mean(abs(B)));
    else
        [AB,lags] = xcorr(A,B);
    end
end

%% Calculate SyncPoint
if ~exist('delay','var')
    meanAB = mean(abs(AB));
    AB_pos = AB;
    AB_neg = AB;
    AB_pos(lags<0) = 0;
    AB_neg(lags>=0) = 0;
    [maxAB_pos,maxABind_pos] = max(abs(AB_pos));
    [maxAB_neg,maxABind_neg] = max(abs(AB_neg));
    % If first maximum is to be ignored, proceed to calculate the second
    % maximum:
    for n = 1:SYNC.nPeakIgnore
        gainAB_pos = 10*log10(maxAB_pos/meanAB);
        gainAB_neg = 10*log10(maxAB_neg/meanAB);
        if gainAB_pos > gainAB_neg + 1
            AB_pos(max(1,maxABind_pos-10):...
                min(numel(AB_pos),maxABind_pos+10)) = 0;
            [maxAB_pos,maxABind_pos] = max(abs(AB_pos));
        elseif gainAB_neg > gainAB_pos + 1
            AB_neg(max(1,maxABind_neg-10):...
                min(numel(AB_neg),maxABind_neg+10)) = 0;
            [maxAB_neg,maxABind_neg] = max(abs(AB_neg));
        else
            [~,ind] = min(abs([lags(maxABind_pos) lags(maxABind_neg)]));
            if ind == 1
                AB_pos(max(1,maxABind_pos-10):...
                    min(numel(AB_pos),maxABind_pos+10)) = 0;
                [maxAB_pos,maxABind_pos] = max(abs(AB_pos));
            else
                AB_neg(max(1,maxABind_neg-10):...
                    min(numel(AB_neg),maxABind_neg+10)) = 0;
                [maxAB_neg,maxABind_neg] = max(abs(AB_neg));
            end
        end
    end
    gainAB_pos = 10*log10(maxAB_pos/meanAB);
    gainAB_neg = 10*log10(maxAB_neg/meanAB);
    posLag = lags(maxABind_pos);
    negLag = lags(maxABind_neg);
    if gainAB_pos > gainAB_neg + 1
        delay = posLag;
        maxABind = maxABind_pos;
        gain = gainAB_pos;
    elseif gainAB_neg > gainAB_pos + 1
        delay = negLag;
        maxABind = maxABind_neg;
        gain = gainAB_neg;
    else
        [delay,ind] = min(abs([posLag negLag]));
        if ind == 2
            maxABind = maxABind_neg;
            delay = -delay;
            gain = gainAB_neg;
        else
            maxABind = maxABind_pos;
            gain = gainAB_pos;
        end
    end
end

%% Synchronize B relatively to A
if strcmp(SYNC.syncOrder,'B->A')
    if delay >= 0
        Bhead = B(end-delay+1:end);
        Btail = B(1:mod(nSamplesA-delay,nSamplesB));
        OUT = [Bhead repmat(B,1,floor((nSamplesA-delay)/nSamplesB)) Btail];
    else
        Bhead = B(abs(delay)+1:end);
        Btail = B(1:mod(nSamplesA-length(Bhead),nSamplesB));
        OUT = [Bhead repmat(B,1,floor((nSamplesA-abs(delay))/nSamplesB))...
            Btail];
    end
elseif strcmp(SYNC.syncOrder,'A->B')
    if delay >= 0
        d = nSamplesA - delay;
        Bhead = A_all(end-d+1:end);
        Btail = A_all(1:nSamplesA-d);
    else
        Bhead = A_all(abs(delay)+1:end);
        Btail = A_all(1:end-length(Bhead));
    end
    OUT = [Bhead Btail];
end
    
    
%% Truncate B if lenght(B)>length(A)
if length(OUT) > nSamplesA
    OUT = OUT(1:nSamplesA);
end

%% Find Rotation
if exist('maxABind','var')
    if abs(imag(AB(maxABind))) > abs(real(AB(maxABind)))
        if imag(AB(maxABind)) < 0
            rot = -pi/2;
        else
            rot = pi/2;
        end
    else
        if real(AB(maxABind))<0
            rot = -pi;
        else
            rot = 0;
        end
    end
else
    rot = 0;
end
OUT = OUT*exp(1j*rot);

%% Debug
if isfield(SYNC,'debugPlots')
    for m = 1:length(SYNC.debugPlots)
        if any(strcmpi(SYNC.debugPlots{m},{'timeComparison','all'}))
            figure();
            nSp = 1000;
            plot(1:nSp,normalizeSignal(real(OUT(1:nSp))),'-b');
            hold on;
            plot(1:nSp,normalizeSignal(real(A_all(1:nSp))),'-r');
        end
        if any(strcmpi(SYNC.debugPlots{m},{'xCorr','all'}))
            xCorrPlot(lags,AB,delay);
        end
    end
end



