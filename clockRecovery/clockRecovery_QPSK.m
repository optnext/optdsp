function Sout=clockRecovery_QPSK(Sin,CLK)

% Last Update: 18/11/2014


%% Input Parameters
[nPol,nSamples]=size(Sin);
Interpolation=CLK.interp;
k1=CLK.k1;
k2=CLK.k2;
I=real(Sin);
Q=imag(Sin);

%%
X1=0;                                       % Integrator constant for Loop Filter
% X(1)=0;                                     % X = out vector of loop filter

U=0;                                     % U = out vector of NCO

Atraso=0;                                   % mod(U, 1)... variable used for update the taps of the interpolation filter
Discard=0;                                  % Flag to stop the system: Discard = 1 - Atraso

indexAfter = 2;                             % First compensated sample (so the first sample is not compensated)
if Interpolation==1
    indexBefore = 5;                        % indexBefore = Interpolation FIR filter memory + 1
elseif Interpolation==2
    indexBefore = 3;                        % indexBefore = Interpolation FIR filter memory + 1
end
U_plot=[0 0 0];                             % Just for plot U

while indexBefore <= nSamples
    
    % Interpolador
    if Interpolation==1
        C_2 = Atraso^3/6-Atraso/6;
        C_1 = -Atraso^3/2+Atraso^2/2+Atraso;
        C0 = Atraso^3/2-Atraso^2-Atraso/2+1;
        C1 = -Atraso^3/6+Atraso^2/2-Atraso/3;
        Iout(indexAfter)=C_2*I(indexBefore-1)+C_1*I(indexBefore-2)+C0*I(indexBefore-3)+C1*I(indexBefore-4); 
        Iout(indexAfter+1)=C_2*I(indexBefore)+C_1*I(indexBefore-1)+C0*I(indexBefore-2)+C1*I(indexBefore-3);
        Qout(indexAfter)=C_2*Q(indexBefore-1)+C_1*Q(indexBefore-2)+C0*Q(indexBefore-3)+C1*Q(indexBefore-4); 
        Qout(indexAfter+1)=C_2*Q(indexBefore)+C_1*Q(indexBefore-1)+C0*Q(indexBefore-2)+C1*Q(indexBefore-3);
    elseif Interpolation==2
        C_1 = 1-Atraso;
        C0 = Atraso;
        Iout(indexAfter)=C_1*I(indexBefore-1)+C0*I(indexBefore-2); 
        Iout(indexAfter+1)=C_1*I(indexBefore)+C0*I(indexBefore-1);
        Qout(indexAfter)=C_1*Q(indexBefore-1)+C0*Q(indexBefore-2); 
        Qout(indexAfter+1)=C_1*Q(indexBefore)+C0*Q(indexBefore-1);
    end

    % TED
    if Discard==1
        Erro = 0 ;
    else
        Erro=(Iout(indexAfter)*(Iout(indexAfter+1)-Iout(indexAfter-1))+Qout(indexAfter)*(Qout(indexAfter+1)-Qout(indexAfter-1)));
    end

    % Loop filter
    X1 = X1 + k2 * Erro;
    X = X1 + k1 * Erro;
    
    % NCO
    c = U + X;
    U = rem(c, 1);
    Atraso = U;
    if abs(c) >= 1      % Return to zero if (|U| >= 1)
        Discard = 1;
        indexAfter = indexAfter - 3;            % Stop system... return to the previuos symbol
        indexBefore = indexBefore - 3;          % Stop system... return to the previuos symbol
        
        U_plot = U_plot(1:end-3); % Just for plot
    else
        Discard = 0;
        indexAfter = indexAfter + 2;            % Estimates just symbol by symbol (2 samples per symbol)
        indexBefore = indexBefore + 2;          % Estimates just symbol by symbol (2 samples per symbol)
        
        U_plot = [U_plot U U]; % Just for plot
    end
    
    indexBefore
end


