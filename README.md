# General Description of the OptDSP Library #

The OptDSP library includes a set of m-files to implement **receiver-side digital signal processing (DSP) for coherent optical communication systems**.  
Currently supported DSP subsystems include: 

+ matched filtering (SRRC);  
+ clock recovery (only for QPSK so far);  
+ optical frontend compensation (deskew, orthonormalization, DC removal, ...);  
+ adaptive linear equalization (CMA, LMS, RDE, ...);  
+ frequency offset estimation and removal;  
+ carrier phase estimation and removal (Viterbi&Viterbi, blind-phase search, maximum likelihood, decision-directed, data-aided, ...);  
+ signal demodulation and decision (BER, SER, EVM, MSE, MI, ...);


For simpler use of the implemented DSP subsystems, several DSP sets are provided, ready for use in: 

+ optical B2B applications (simulation and experimental);  
+ fiber propagation applications (simulation and experimental);  
+ ideal simulation setups.

In addition, the library also includes a set of functions to perform general purpose DSP operations such as:  

+ low-pass/band-pass filtering (frequency-domain);  
+ FFT block processing (overlap-and-save, overlap-and-add);  
+ digital resampling;  
+ synchronization of Tx and Rx signals;  
+ digital monitoring of signal impairments (estimation of OSNR, chromatic dispersion, PMD ...).



### How to Use the OptDSP Library ###
The OptDSP library is part of the OptWorks bundle, a set of libraries developed for the simulation of optical communication systems in MATLAB.  
Other libraries provided by OptWorks include:  

+ OptMEX (composed of pre-compiled .mex files for faster processing of some OptDSP functions);  
+ OptTRX (composed of m-file routines for the simulation of Tx and Rx components);  
+ OptTOOLS (composed of general purpose m-file routines useful for the simulation of optical communication systems);  
+ OptNLC (composed of m-file routines for the mitigation of nonlinear fiber impairments).  



### Acknowledgment ###

This library and its included routines were developed by Fernando Guiomar and supported by the European Commission (EC) within the framework of a Marie Skłodowska-Curie Individual Fellowship, project Flex-ON: Flexible Optical Networks – Time Domain Hybrid QAM: DSP and Physical Layer Modelling, with grant agreement number 653412.  
[http://cordis.europa.eu/project/rcn/194861_en.html](http://cordis.europa.eu/project/rcn/194861_en.html)


