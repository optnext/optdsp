function [Aout,phi] = CPE_phaseRotation(Ain,QAM,CPE)

% Last Update: 10/04/2017


%% Input Parser
if ~isfield(CPE,'mQAM')
    CPE.mQAM = QAM.modFormat;
end
if ~any(strcmpi(CPE.mQAM,{'QPSK','DP-QPSK','PM-QPSK'}))
    if ~isfield(CPE,'QAM_classes')
        CPE.QAM_classes = 'A';
    end
    if ~isfield(CPE,'p')
        CPE.p = 1;
    end
    if ~isfield(CPE,'demodQAM')
        CPE.demodQAM = 'QPSKpartition';
    end
end

%% Input Parameters
[nPol,nSamples] = size(Ain);
phi = zeros(nPol,1);

%% Fixed Phase Rotation
for n = 1:nPol
    if ~any(strcmpi(CPE.mQAM,{'QPSK','DP-QPSK','PM-QPSK'}))
        Ap = ringPartitionQAM(Ain(n,:),CPE,QAM);
        A = classPartitionQAM(Ap,CPE);
        A = demodQAM_4thPower(A,CPE);
        A = A.A1(~isnan(A.A1));
    else
        A = Ain(n,:).^4;
    end
    A = mean(A);
    phi(n) = atan2(imag(A),real(A));
    phi(n) = unwrap(phi(n)')'/4-pi/4;
end

%% Correct Carrier Phase
Aout = zeros(nPol,nSamples);
for n = 1:nPol
    Aout(n,:) = Ain(n,:)*exp(-1j*phi(n));
end
