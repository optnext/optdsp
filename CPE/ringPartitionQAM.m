function [A,bounds] = ringPartitionQAM(S,CPE,QAM)

% Last Update: 10/04/2017


%% Input Parser
if isstruct(CPE)
    mQAM = CPE.mQAM;
else
    mQAM = CPE;
end
if isstruct(QAM)
    meanQAMpower = QAM.meanConstPower;
else
    meanQAMpower = QAM;
end

%% QAM Ring Partitioning
meanP   = mean(abs(S).^2);
radii   = sort(getQAMradii(mQAM),'ascend')*sqrt(meanP)/sqrt(meanQAMpower);
bounds  = radii(1:end-1) + diff(radii)/2;
nRadius = length(radii);

P = abs(S);
A = repmat(S,nRadius,1);
A(1,P>=bounds(1)) = 0;
for n = 2:nRadius-1
    A(n,P<bounds(n-1) | P>=bounds(n)) = 0;
end
A(end,P<bounds(end)) = 0;
