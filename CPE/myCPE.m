function [Aout,PARAM,CPE] = myCPE(Ain,nSpS,PARAM,QAM,CPE)

% Last Update: 25/07/2017


global PROG;

%% Input Parser
if exist('CPE','var')
    nCPE = length(CPE);
    if nCPE == 1 && ~iscell(CPE)
        tmp = CPE;
        clear CPE;
        CPE{1} = tmp;
    end
else
    nCPE = 1;
end
for n = 1:nCPE
    if exist('CPE','var')
        CPE{n} = presetCPE('CPE',CPE{n});
    else
        CPE{n} = presetCPE();
    end
end

%% Entrance Message
entranceMsg(PROG.MSG.CPE);
tic

%% Phase Estimation
for n = 1:nCPE
    % Select and Apply CPE Method:
    switch CPE{n}.method
        case {'Viterbi','Viterbi&Viterbi','V&V','VV'}
            [Aout,CPE{n}.phi] = CPE_Viterbi(Ain,nSpS,QAM,CPE{n});
        case {'MaximumLikelihood','Maximum-Likelihood','maxLike','ML','MLPE'}
            [Aout,CPE{n}.phi] = CPE_ML(Ain,QAM,CPE{n});
        case {'DA-ML-CPE:optimized'}
            [Aout,CPE{n}] = CPE_ML_DA_opt(Ain,QAM,CPE{n});
        case {'DD-ML-CPE:optimized'}
            [Aout,CPE{n}] = CPE_ML_DD_opt(Ain,QAM,CPE{n});
        case {'PN','PN receiver'}
            [Aout,CPE{n}.phi] = CPE_PN_receiver(Ain,QAM,CPE{n});
        case {'decision-directed','DecisionDirected','DD'}
            [Aout,CPE{n}.phi] = CPE_decisionDirected(Ain,nSpS,QAM,CPE{n});
        case {'blind phase-search','blindPhaseSearch','BPS'}
            [Aout,CPE{n}.phi] = CPE_BPS(Ain,nSpS,QAM,CPE{n});
        case 'phaseRotation'
            [Aout,CPE{n}.phi] = CPE_phaseRotation(Ain,QAM,CPE{n});
        case 'none'
            Aout = Ain;
            CPE{n}.phi = 0;
        otherwise
            error('Unsupported CPE method!');
    end

    % Remove Edge Samples:
    if ~any(strcmpi(CPE{n}.method,{'phaseRotation','none'}))
        CPE_tmp = CPE{n};
        CPE_tmp.convMethod = 'vector';
        [Aout,PARAM] = rmvEdgeSamplesFIR(Aout,PARAM,CPE_tmp,nSpS);
    end

    % Print Parameters:
    if PROG.showMessagesLevel >= 2
        fprintf('------------------------------------------------------------------');
        fprintf('\nPhase Estimation PARAMETERS:\n');
        fprintf('Phase Estimation Method (CPE.method):              %s\n',CPE{n}.method);
        if isfield(CPE{n},'nTaps')
            fprintf('Number of Taps (CPE.nTaps):                        %2.0f \n',CPE{n}.nTaps);
        end
        fprintf('------------------------------------------------------------------\n');
    end
    elapsedTime = toc;
    myMessages(['Phase Estimation - Elapsed Time: ',...
        num2str(elapsedTime,'%1.4f'),' [s]\n'],1);

    Ain = Aout;
end

%% Output
if nCPE == 1
    CPE = CPE{1};
end
