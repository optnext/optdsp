function [Aout] = demodQAM_DD(S,QAM)

% Last Update: 10/04/2017


%% Input Parser
txSignal = symbol2signal(signal2symbol(S,QAM),QAM);
phi = angle(txSignal);

%% Data-Aided Demodulation
Aout = S.*exp(-1j*phi);

%%
% d = abs(txSignal-S).^2;
% Aout(d>2e-2) = NaN;

