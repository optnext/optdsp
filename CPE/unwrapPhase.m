function [phi] = unwrapPhase(phi,CPE)

% Last Update: 07/10/2016


%% Input Parser
if ~isfield(CPE,'demodQAM')
    demodQAM = 'QPSKpartition';
else
    demodQAM = CPE.demodQAM;
end

%% Unwrap Phase
switch demodQAM
    case {'QPSKpartition','data-aided (4th-power)',...
            'decision-directed (4th-power)','QPSK'},
        phi = unwrap(phi)/4-pi/4;
    case {'data-aided','decision-directed'},
        phi = unwrap(phi);
    case 'nthPower',
        c   = 2*pi/(pi-4*atan(1/3));
        phi = unwrap(phi)/(4*c);
    case 'recenter',
        phi = unwrap(phi)/4;
end
