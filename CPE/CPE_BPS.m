function [Aout,phi] = CPE_BPS(Ain,nSpS,QAM,CPE)

% Last Update: 25/07/2017


%% Input Parser
if ~isfield(CPE,'decisionMode')
    CPE.decisionMode = 'DD';
end
if any(strcmp(CPE.decisionMode,{'DA','data-aided','genie-aided'}))
    dataAided = true;
else
    dataAided = false;
end

%% Input Parameters
[nPol,nSamples] = size(Ain);
B = CPE.nTestPhases;                                                        % number of test phases
phiInt = CPE.angleInterval;                                                 % angle interval
nTaps = CPE.nTaps;                                                          % number of filter taps

%% If Signal is Oversampled, Perform Downsampling
if nSpS > 1
    nSamples = nSamples/nSpS;
    if isfield(CPE,'ts0')
        A_CPE = Ain(:,CPE.ts0:nSpS:end);
    else
        A_CPE = Ain(:,1:nSpS:end);
    end
else
    A_CPE = Ain;
end

%% Apply Blind Phase Search
phi = zeros(nPol,nSamples);
d = zeros(B+1,nSamples);
W = ones(1,nTaps)/nTaps;
for n = 1:nPol
    if dataAided
        Aref = CPE.txSignal(n,:);
    end
    for b = 0:B
        phiTest = (b/B-1/2)*phiInt;
        Arot = A_CPE(n,:).*exp(1j*phiTest);
        if ~dataAided
            Aref = symbol2signal(signal2symbol(Arot,QAM),QAM);
        end
        err = abs(Arot-Aref).^2;
        d(b+1,:) = conv(err,W,'same');
    end
    [~,ind] = min(d);
    phi(n,:) = ((ind-1)/B-1/2)*phiInt;
end
phi = -unwrap(phi);

%% Correct Carrier Phase
if nSpS > 1
    Aout = Ain.*exp(-1j*rectpulse(phi',nSpS)');
else
    Aout = Ain.*exp(-1j*phi);
end
