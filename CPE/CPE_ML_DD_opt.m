function [Sout,CPE] = CPE_ML_DD_opt(Sin,QAM,CPE)
%CPE_ML_DD_opt  Optimized Decision-Directed Maximum Likelihood
%               Carrier-Phase Estimation
%
%   This function implements a decision-directed (DD) maximum likelihood
%   (ML) carrier-phase estimation (CPE) algorithm with optimized number of
%   taps to minimize the symbol error-rate (SER). The number of taps is
%   optimized using the fminbnd function to minimize the SER. 
% 
%   INPUTS:
%   Sin     :=  input signal vector [nPol x nSamples]
%   QAM     :=  struct with QAM parameters. Required fields:
%               QAM.meanConstPower := average constellation power
%               QAM.symbolMapping := constellation symbol mapping
%               QAM.IQmap := IQ mapping
%               QAM.symbolIndex := symbol indices of the constellation
%   CPE     :=  struct with CPE parameters. Required fields:
%               CPE.nTaps_min := minimum number of taps for optimization
%               CPE.nTaps_max := maximum number of taps for optimization
%
%   OUTPUTS:
%   Sout    :=  output signal vector [nPol x nSamples]
%   CPE     :=  struct with CPE parameters. New output fields are:
%               CPE.nTaps_opt := optimum number of taps
%               CPE.phi := vector of estimated phase noise [nPol x nSamples]
%
%
% [1] Xiang Zhou, "An Improved Feed-Forward Carrier Recovery Algorithm for 
% Coherent Receivers With M-QAM Modulation Format", vol.22, no.14, 2010.
%
%
%   Author: Fernando Guiomar
%   Last Update: 25/06/2017

%% Input Parameters
[nPol,nSamples] = size(Sin);
H = zeros(nPol,nSamples);
nTapsMin = CPE.nTaps_min;
nTapsMax = CPE.nTaps_max;
CPE.decision = 'DD';

%% Normalize Input Signal
Sin = normalizeSignal(Sin,QAM.meanConstPower,'joint');
txSyms = signal2symbol(Sin,QAM);
Sref = symbol2signal(txSyms,QAM);
F = Sref.*conj(Sin);

%% Optimize Number of Taps
% options = optimset('Display','iter');%,'PlotFcns',@optimplotfval);
[nTaps_opt,opt_SER] = fminbnd(@(nTaps) CPE_ML_DD_minBER(Sin,txSyms,F,QAM,nTaps,H), ...
    nTapsMin,nTapsMax);%,options);

%% Apply ML-DA-CPE With Optimum Number of Taps
nTaps_opt = round(nTaps_opt);
[Sout,CPE.phi] = CPE_ML_DD(Sin,F,nTaps_opt,H);

%% Output CPE Parameters
CPE.nTaps_opt = nTaps_opt;

end


%% Auxiliar Functions for Maximum Likelihood Data-Aided CPE
function [Sout,phi] = CPE_ML_DD(Sin,F,nTaps,H)
    W = ones(1,nTaps);
    for m = 1:size(H,1)
        H(m,:) = conv(F(m,:),W,'same');
    end
    phi = atan(imag(H)./real(H));
    Sout = Sin.*exp(1j*phi);
end

function SERxy = CPE_ML_DD_minBER(Sin,txSyms,F,QAM,nTaps,H)
    nTaps = round(nTaps);
    Sout = CPE_ML_DD(Sin,F,nTaps,H);
    
    rxSyms = signal2symbol(Sout,QAM);
    SER = SER_eval(txSyms,rxSyms);
    SERxy = SER.SERxy;
end
