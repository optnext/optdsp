function [Aout,phi] = CPE_Viterbi(Ain,nSpS,QAM,CPE)

% Last Update: 27/07/2017


%% Input Parser
if strncmpi(CPE.demodQAM,'data-aided',10)
    if ~isfield(CPE,'txSignal')
        error('For data-aided QAM demodulation in V&V CPE you must provide the transmitted signal (CPE.txSignal)');
    end
end

%% Input Parameters
nTaps = CPE.nTaps;
CPE.nTaps = max(nTaps);
[nPol,nSamples] = size(Ain);

%% If Signal is Oversampled, Perform Downsampling
if nSpS > 1
    A_CPE = zeros(nPol,size(Ain,2)/nSpS);
    nSamples = nSamples/nSpS;
    if ~isfield(CPE,'ts0')
        % If there is no preset sampling instant, try to find the best
        % sampling instant blindly:
        ts0 = zeros(1,nPol);
        for n = 1:nPol
            ts0(n) = bestSamplingInstant(Ain(n,:),nSpS,QAM,'amplitude');
            A_CPE(n,:) = Ain(n,ts0(n):nSpS:end);
        end
        if sum(diff(ts0))
            warning('CPE_Viterbi.m: Two different sampling instants have been found for the two polarizations! This can be due to a wrong decision on the sampling instant...');
        end
        CPE.ts0 = ts0;
    else
        % If there is a preset sampling instant, use it for downsampling:
        A_CPE = Ain(:,CPE.ts0:nSpS:end);
    end
else
    A_CPE = Ain;
end

%% Viterbi & Viterbi Phase Estimation
[Ap,bnd] = deal(cell(1,nPol));
[phi,Ademod] = deal(zeros(nPol,nSamples));
for n = 1:nPol
    % mQAM Demodulation:
    switch CPE.demodQAM
        case {'DA','data-aided'}
            Ademod(n,:) = demodQAM_DA(A_CPE(n,:),CPE.txSignal(n,:));
        case 'data-aided (4th-power)'
            Ademod(n,:) = demodQAM_4thPower_DA(A_CPE(n,:),...
                CPE.txSignal(n,:),QAM);
        case 'decision-directed'
            Ademod(n,:) = demodQAM_DD(A_CPE(n,:),QAM);
        case 'decision-directed (4th-power)'
            Ademod(n,:) = demodQAM_4thPower_DD(A_CPE(n,:),QAM);            
        case 'QPSKpartition'
            % Ring Partitioning of QAM Constellation:
            [Ap{n},bnd{n}] = ringPartitionQAM(A_CPE(n,:),CPE,QAM);
            % Class Partitioning of QAM Constellation:
            Aclass(n) = classPartitionQAM(Ap{n},CPE);
            % 4th-Power Demodulation:
            A4thPower(n) = demodQAM_4thPower(Aclass(n),CPE);
            % Sample Selection:
            if strcmp(QAM.modFormat,'PM-8QAM')
                Ademod(n,:) = A4thPower(n).A1 + A4thPower(n).B1;
            else
                Ademod(n,:) = CPE_sampleSelection_mQAM(A4thPower(n),CPE);
            end
        otherwise
            Aclass(n).A = A_CPE(n,:);
            % 4th-Power Demodulation:
            A4thPower(n) = demodQAM_4thPower(Aclass(n),CPE);
            Ademod(n,:) = A4thPower(n).A1;
    end
    % V&V Phase Estimation:
    if numel(nTaps) == 2
        % Phase Estimation with Larger Number of Taps:
        CPE.nTaps = max(nTaps);
        phi1(n,:) = VV_phaseEstimation(Ademod(n,:),CPE,'[all QAM classes]');
        % Unwrap Phase:
        phi1(n,:) = unwrapPhase(phi1(n,:),CPE);
        % Phase Estimation with Shorter Number of Taps:
        CPE.nTaps = min(nTaps);
        phi2(n,:) = VV_phaseEstimation(Ademod(n,:),CPE,'[all QAM classes]');
        % Unwrap Phase:
        phi2(n,:) = unwrapPhase(phi2(n,:),CPE);
        % Cycle-Slip Correction:
        phi(n,:) = VV_dualStage_CS_removal(phi1(n,:),phi2(n,:));
    else
        % Phase Estimation:
        phi(n,:) = VV_phaseEstimation(Ademod(n,:),CPE,'[all QAM classes]');        
        % Unwrap Phase:
        phi(n,:) = unwrapPhase(phi(n,:),CPE);
    end
end

%% Correct Carrier Phase
if nSpS > 1
    Aout = Ain.*exp(-1j*rectpulse(phi',nSpS)');
else
    Aout = Ain.*exp(-1j*phi);
end

%% Debug Plots
if isfield(CPE,'debugPlots')
    for m = 1:length(CPE.debugPlots)
        debugPlot = CPE.debugPlots{m};
        if any(strncmpi(debugPlot,{'ringPartition','all'},3))
            ringPartitionPlot(A_CPE,Ap,bnd);
        end
        if any(strcmpi(debugPlot,{'demodQAM','all'}))
            demodQAMplot(A4thPower);
        end
        if any(strncmpi(debugPlot,{'phase','all'},3))
            phaseCPEplot(phi,phi);
        end
        if any(strcmpi(debugPlot,{'demodQAM_motion','all'}))
            A = Ademod(1,:);
            A(A==0) = NaN;
            plotSpecs.blockSize = CPE.nTaps;
            plotSpecs.advanceBlock = ceil(CPE.nTaps/4);
            scatterPlotMotion(A,plotSpecs);
        end
    end
end
