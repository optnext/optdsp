function [phi,w,A_VV] = VV_phaseEstimation(Ain,CPE,MSG)

% Last Update: 15/07/2016


%% Input Parser
if ~exist('MSG','var')
    MSG = [];
end

%% Input Parameters
nTaps           = CPE.nTaps;
ni              = ceil(nTaps/2);
nf              = floor(nTaps/2);
nSamples        = length(Ain);
[A_VV,phi,w]    = deal(zeros(1,nSamples));

%% Perform Phase Average
switch CPE.convMethod
    case {'mean','vector'},
        % ProgressBar Initialization:
        progressbar(['Viterbi-Viterbi ',MSG,': current sample (',num2str(nSamples-nTaps),' in total)']);
        for n = ni:nSamples-nf
            A = Ain(n+nf:-1:n-ni+1);
            w(n) = sum(A~=0);
            A_VV(n) = mean(A(A~=0));
            if w(n)
                A_VV(n) = mean(A(A~=0));
                phi(n) = atan2(imag(A_VV(n)),real(A_VV(n)));
            else
                phi(n) = phi(n-1);
            end
            progressbar((n-ceil(nTaps/2)+1)/(nSamples-nTaps+1));
        end
    case {'conv','filter'},
        W = ones(1,nTaps)/nTaps;
%         W(ceil(nTaps/2)) = 0;
%         A_VV = conv(Ain,W,'same');
        A_VV = nanconv(Ain,W);
%         for n = nTaps:nSamples
%             A = Ain(n:-nSpS:n-nTaps+ts0);
%             w(n) = sum(A~=0);
%             if ~w(n)
%                 A_VV(n) = A_VV(n-1);
%             end
%         end
        phi = atan2(imag(A_VV),real(A_VV));
    otherwise,
        error('Unsupported convolution method for CPE');
end

