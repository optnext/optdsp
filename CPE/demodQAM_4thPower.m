function [A] = demodQAM_4thPower(A,CPE)

% Last Update: 08/06/2017


%% Input Parameters
mQAM = CPE.mQAM;
p = CPE.p;

%% 4th Power Demodulation
if ~isempty(strfind(mQAM,'64QAM'))
    % Rotation angles definition:
    phiRotB = atan(3)-pi/4;
    phiRotC = atan(5)-pi/4;
    phiRotD = atan(5/3)-pi/4;
    phiRotE = atan(7)-pi/4;
    phiRotF = atan(7/3)-pi/4;
    phiRotG = atan(7/5)-pi/4;
    % 4th power demodulation:
    if isfield(A,'A')
        A.A1 = abs(A.A).^p.*exp(1j*angle(A.A)*4);
    end
    if isfield(A,'B')
        A.B1 = A.B.*exp(1j*phiRotB);
        A.B1 = abs(A.B1).^p.*exp(1j*angle(A.B1)*4);
        A.B2 = A.B.*exp(-1j*phiRotB);
        A.B2 = abs(A.B2).^p.*exp(1j*angle(A.B2)*4);
    end
    if isfield(A,'C')
        A.C1 = A.C.*exp(1j*phiRotC);
        A.C1 = abs(A.C1).^p.*exp(1j*angle(A.C1)*4);
        A.C2 = A.C.*exp(-1j*phiRotC);
        A.C2 = abs(A.C2).^p.*exp(1j*angle(A.C2)*4);
    end
    if isfield(A,'D')
        A.D1 = A.D.*exp(1j*phiRotD);
        A.D1 = abs(A.D1).^p.*exp(1j*angle(A.D1)*4);
        A.D2 = A.D.*exp(-1j*phiRotD);
        A.D2 = abs(A.D2).^p.*exp(1j*angle(A.D2)*4);
    end
    if isfield(A,'E')
        A.E1 = A.E.*exp(1j*phiRotE);
        A.E1 = abs(A.E1).^p.*exp(1j*angle(A.E1)*4);
        A.E2 = A.E.*exp(-1j*phiRotE);
        A.E2 = abs(A.E2).^p.*exp(1j*angle(A.E2)*4);
        A.E3 = abs(A.E).^p.*exp(1j*angle(A.E)*4);
    end
    if isfield(A,'F')
        A.F1 = A.F.*exp(1j*phiRotF);
        A.F1 = abs(A.F1).^p.*exp(1j*angle(A.F1)*4);
        A.F2 = A.F.*exp(-1j*phiRotF);
        A.F2 = abs(A.F2).^p.*exp(1j*angle(A.F2)*4);
    end
    if isfield(A,'G')
        A.G1 = A.G.*exp(1j*phiRotG);
        A.G1 = abs(A.G1).^p.*exp(1j*angle(A.G1)*4);
        A.G2 = A.G.*exp(-1j*phiRotG);
        A.G2 = abs(A.G2).^p.*exp(1j*angle(A.G2)*4);
    end
elseif ~isempty(strfind(mQAM,'32QAM'))
    % Rotation angles definition:
    phiRotB = atan(3)-pi/4;
    phiRotC = atan(5)-pi/4;
    phiRotD = atan(5/3)-pi/4;
    % 4th power demodulation:
    if isfield(A,'A')
        A.A1 = abs(A.A).^p.*exp(1j*angle(A.A)*4);
    end
    if isfield(A,'B')
        A.B1 = A.B.*exp(1j*phiRotB);
        A.B1 = abs(A.B1).^p.*exp(1j*angle(A.B1)*4);
        A.B2 = A.B.*exp(-1j*phiRotB);
        A.B2 = abs(A.B2).^p.*exp(1j*angle(A.B2)*4);
    end
    if isfield(A,'C')
        A.C1 = A.C.*exp(1j*phiRotC);
        A.C1 = abs(A.C1).^p.*exp(1j*angle(A.C1)*4);
        A.C2 = A.C.*exp(-1j*phiRotC);
        A.C2 = abs(A.C2).^p.*exp(1j*angle(A.C2)*4);
    end
    if isfield(A,'D')
        A.D1 = A.D.*exp(1j*phiRotD);
        A.D1 = abs(A.D1).^p.*exp(1j*angle(A.D1)*4);
        A.D2 = A.D.*exp(-1j*phiRotD);
        A.D2 = abs(A.D2).^p.*exp(1j*angle(A.D2)*4);
    end
elseif ~isempty(strfind(mQAM,'16QAM'))
    switch CPE.demodQAM
        case 'QPSKpartition'
            % Rotation angles definition:
            phiRotB = atan(3)-pi/4;
            % 4th Power Demod:
            if isfield(A,'A')
                A.A1 = abs(A.A).^p.*exp(1j*angle(A.A)*4);
            end
            if isfield(A,'B')
                A.B1 = A.B.*exp(1j*phiRotB);
                A.B2 = A.B.*exp(-1j*phiRotB);
                A.B1 = abs(A.B1).^p.*exp(1j*angle(A.B1)*4);
                A.B2 = abs(A.B2).^p.*exp(1j*angle(A.B2)*4);
            end
        case 'nthPower'
            S = A.A;
            % 4th Power Demod:
            S = abs(S).*exp(1j*angle(S)*4);
            % pi Rotation:
            S = S*exp(1j*pi);
            % nth Power Demod:
            c = 2*pi/(pi-4*atan(1/3));
            A.A1 = abs(S).*exp(1j*angle(S)*c);
        case 'recenter'
            S = A.A;
            % 4th Power Demod:
            S = abs(S).*exp(1j*angle(S)*4);
            % pi Rotation:
            S = S*exp(1j*pi);
            % 1/4th Power:
            S = abs(S).*exp(1j*angle(S)/4);
            % Recenter:
%                 a=(-pi+4*atan(3))/4;
%                 d=sqrt(mean(abs(S).^2)/10);
%                 c=sqrt(10)*d*cos(a);
            c = 2/3;
            S = S-c;
            % pi/4 Rotation:
            S = S*exp(1j*pi/4);
            %
            Sp = ringPartitionQAM(S,CPE,0.555555555555555);
            % 4th Power Demod:
            A.A1 = abs(S).*exp(1j*angle(S)*4);
    end
elseif ~isempty(strfind(mQAM,'8QAM'))
    if isfield(A,'A')
        A.A1 = abs(A.A).^p.*exp(1j*angle(A.A)*4);
    end
    if isfield(A,'B')
        % Rotation angles definition:
        phiRotB = pi/4;
        A.B1 = A.B.*exp(1j*phiRotB);
        A.B1 = abs(A.B1).^p.*exp(1j*angle(A.B1)*4);
    end
elseif ~isempty(strfind(mQAM,'QPSK')) || ~isempty(strfind(mQAM,'4QAM'))
    A.A1 = abs(A.A).^p.*exp(1j*angle(A.A)*4);
end

