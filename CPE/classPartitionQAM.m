function [A] = classPartitionQAM(Ap,CPE)

% Last Update: 15/01/2017


%% Input Parser
if ~isfield(CPE,'QAM_classes')
    CPE.QAM_classes = 'A';
end

%% Input Parameters
mQAM        = CPE.mQAM;
QAM_classes = CPE.QAM_classes;

%% QAM Class Partitioning
switch mQAM
    case {'QPSK','PM-QPSK','DP-QPSK'},
        A = Ap;
    case {'8QAM','PM-8QAM','DP-8QAM'},
        if ~isempty(strfind(QAM_classes,'A'))
            A.A = Ap(1,:);
        end
        if ~isempty(strfind(QAM_classes,'B'))
            A.B = Ap(2,:);
        end
    case {'16QAM','PM-16QAM','DP-16QAM'},
        if ~isempty(strfind(QAM_classes,'A'))
            A.A = Ap(1,:)+Ap(3,:);
        end
        if ~isempty(strfind(QAM_classes,'B'))
            A.B = Ap(2,:);
        end
    case {'32QAM','PM-32QAM','DP-32QAM'},
        if ~isempty(strfind(QAM_classes,'A'))
            A.A = Ap(1,:) + Ap(3,:);
        end
        if ~isempty(strfind(QAM_classes,'B'))
            A.B = Ap(2,:);
        end
        if ~isempty(strfind(QAM_classes,'C'))
            A.C = Ap(4,:);
        end
        if ~isempty(strfind(QAM_classes,'D'))
            A.D = Ap(5,:);
        end
    case {'64QAM','PM-64QAM','DP-64QAM'},
        if ~isempty(strfind(QAM_classes,'A'))
            A.A = Ap(1,:) + Ap(3,:) + Ap(9,:);
        end
        if ~isempty(strfind(QAM_classes,'B'))
            A.B = Ap(2,:);
        end
        if ~isempty(strfind(QAM_classes,'C'))
            A.C = Ap(4,:);
        end
        if ~isempty(strfind(QAM_classes,'D'))
            A.D = Ap(5,:);
        end
        if ~isempty(strfind(QAM_classes,'E'))
            A.E = Ap(6,:);
        end
        if ~isempty(strfind(QAM_classes,'F'))
            A.F = Ap(7,:);
        end
        if ~isempty(strfind(QAM_classes,'G'))
            A.G = Ap(8,:);
        end
end


