function [Srx,PARAMout] = SCM_staticEQ(Srx,PARAMin,SIG,FIBER,DSP,nthStage)

% Last Update: 16/04/2017


%% Input Parser
if isfield(DSP,['EQ',num2str(nthStage)])
    eval(['EQ = DSP.EQ',num2str(nthStage),';']);
else
    EQ_out = [];
    PARAMout = PARAMin;
    return;
end

%% Input Parameters
nSC = numel(SIG);
grid_SC = NaN(1,nSC);
if isfield(SIG,'fWDM')
    fWDM = [SIG.fWDM];
else
    fWDM = zeros(1,nSC);
end
for k = 1:nSC
    grid_SC(k) = SIG(k).fSC + fWDM(k);
end

%% Static Equalization (over each subcarrier)
for k = 1:nSC
    EQ.centerFreq = grid_SC(k) * 1e9;
    [Srx{k},PARAMout,DSP.EQ2] = staticEq(Srx{k},PARAMin,EQ,FIBER);
end

