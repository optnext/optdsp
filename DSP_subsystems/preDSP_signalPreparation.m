function [S,PARAM] = preDSP_signalPreparation(S,PARAM,DSP)

% Last Update: 16/04/2017


%% Signal Truncation
if isfield(DSP,'TRUNC_preDSP')
    [S,PARAM] = truncateSignal(S,PARAM,DSP.TRUNC_preDSP);
end

%% Resampling
if isfield(DSP,'RESAMP1')
%     [S,PARAM] = myResample(S,PARAM,DSP.RESAMP1);
    Fs_out = DSP.RESAMP1.sampRate;
    [S,PARAM] = applyResample(S,PARAM.sampRate,Fs_out);
end

%% Signal Truncation
if isfield(DSP,'TRUNC1')
    [S,PARAM] = truncateSignal(S,PARAM,DSP.TRUNC1);
elseif isfield(DSP,'TRUNC')
    [S,PARAM] = truncateSignal(S,PARAM,DSP.TRUNC);
end

