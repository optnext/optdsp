function [Srx,PARAM] = SC_postDSP(Srx,PARAM,DSP)

% Last Update: 17/07/2017


%% Remove Edge Samples
if isfield(DSP,'TRUNC_postDSP')
    [Srx,PARAM] = rmvEdgeSamples(Srx,PARAM,DSP.TRUNC_postDSP);
elseif isfield(DSP,'TRUNC')
    [Srx,PARAM] = rmvEdgeSamples(Srx,PARAM,DSP.TRUNC);
end

