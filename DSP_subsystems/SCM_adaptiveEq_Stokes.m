function [Srx,STOK_out] = SCM_adaptiveEq_Stokes(Srx,STOK)

% Last Update: 27/07/2017


global PROG;

%% Input Parser
if ~isfield(STOK,'applicationRange')
    STOK.applicationRange = 'all';
end
nSC = numel(Srx);
        
%% Adaptive Equalization
switch STOK.applicationRange
    case 'all'
        for k = 1:nSC
            PROG.MSG.adaptiveEq = ['Stokes Pol-Demux: Subcarrier ',...
                num2str(k)];
            [Srx{k},STOK_out{k}] = stokesEq(Srx{k},STOK);
        end
    case 'referenceCarrier'
        % Define Reference Carrier(s):
        if isfield(STOK,'refCarrier')
            kRef = STOK.refCarrier;
        else
            if nSC == 1
                kRef = 1;
            else
                kRef = floor(nSC/2);
            end
        end
        % Process Reference Carrier(s):
        n = 1;
        for k = kRef
            % Display Message:
            PROG.MSG.adaptiveEq = ['Stokes Pol-Demux: Subcarrier ',...
                num2str(k)];
            % Apply Adaptive Equalizer:            
            [Srx{k},STOK_out{k}] = stokesEq(Srx{k},STOK);
            % Collect Filter Taps:
            W(:,:,n) = STOK_out{k}.W;
            % Update index:
            n = n + 1;
        end
        % Apply the same taps to the other carriers:
        STOK.W = mean(W,3);
        for k = 1:numel(WDM.subCarriersDSP)
            if k~=kRef
                % Display Message:
                PROG.MSG.adaptiveEq = ['Stokes Pol-Demux: Subcarrier ',...
                    num2str(k)];
                [Srx{k},STOK_out{k}] = stokesEq(Srx{k},STOK);
            end
        end
end



