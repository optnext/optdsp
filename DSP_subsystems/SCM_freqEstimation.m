function [Srx,FE_out] = SCM_freqEstimation(Srx,Stx,PARAM,SIG,DSP)

% Last Update: 10/04/2017


%% Input Parser
if ~isfield(DSP,'FE')
    FE_out = [];
    return;
else
    FE = DSP.FE;
end
if ~isfield(FE,'applicationRange')
    FE.applicationRange = 'all';
end
nSC = numel(SIG);

%% Check for Transmitted Signal in Case of Data-Aided FE
for k = 1:nSC
    Srx_SYNC{k} = Srx{k}(:,1:SIG(k).nSpS:end);
end
SYNC.syncMethod = 'abs';
[Stx] = syncTxRx_SCM(Srx_SYNC,Stx,SYNC);

%% Frequency Mismatch Compensation
switch FE.applicationRange
    case 'all'
        for k = 1:nSC
            FE.txSignal = Stx{k};
            [Srx{k},FE_out] = myFE(Srx{k},PARAM(k),FE,SIG(k).nSpS);
        end
    case 'referenceCarrier'
        if isfield(FE,'refCarrier')
            kRef = FE.refCarrier;
        else
            if nSC == 1
                kRef = 1;
            else
                kRef = floor(nSC/2);
            end
        end
        FE.txSignal = Stx{kRef};
        [Srx{kRef},FE_out] = myFE(Srx{kRef},PARAM(kRef),FE,SIG(kRef).nSpS);
        if size(FE_out.df,2) == 1
            for n = 1:size(FE_out.df,1)
                phi_t(n,:) = 2*pi*FE_out.df(n)*PARAM(kRef).t;
            end
        else
            phi_t = 2*pi*cumsum(FE_out.df,2)*PARAM(kRef).dt;
        end
        for k = 1:nSC
            if k~=kRef
                Srx{k} = Srx{k}.*exp(-1j*repmat(phi_t,...
                    size(Srx{k},1)/size(phi_t,1),1));
            end
        end
end


