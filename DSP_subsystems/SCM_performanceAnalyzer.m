function [OUT] = SCM_performanceAnalyzer(Srx,Stx,QAM,SIG,DSP)

% Last Update: 01/09/2017


%% Input Parser
if ~isfield(DSP,'FOM')
    OUT = [];
    return;
end
nSC = numel(Srx);

%% Analyze QAM Variance
if any(strcmp('varianceQAM',DSP.FOM))
    [txSignal] = syncTxRx_SCM(Srx,Stx);
    for k = 1:nSC
        OUT.QAM{k} = QAM_varianceAnalyzer(Srx{k},txSignal{k},QAM{k});
    end
end

%% BER Evaluation
if any(strncmp('BER',DSP.FOM,3)) && isfield(DSP,'DECODER') && ...
    all(isfield(DSP.DECODER,{'txBits','rxBits'}))

    for k = 1:nSC
        txBits = DSP.DECODER(k).txBits;
        rxBits = DSP.DECODER(k).rxBits;
        if any(strcmp('BER_sym',DSP.FOM))
            options.BER_sym = true;
            OUT.BER{k} = BER_eval(txBits,rxBits,QAM{k},options);
        else
            OUT.BER{k} = BER_eval(txBits,rxBits,QAM{k});
        end
    end
end

%% SER Evaluation
if any(strncmp('SER',DSP.FOM,3)) && isfield(DSP,'DECODER') && ...
    all(isfield(DSP.DECODER,{'txSyms','rxSyms'}))

    for k = 1:nSC
        txSyms = DSP.DECODER(k).txSyms;
        rxSyms = DSP.DECODER(k).rxSyms;
        if any(strcmp('SER_sym',DSP.FOM))
            OUT.SER{k} = SER_eval(txSyms,rxSyms,QAM{k});
        else
            OUT.SER{k} = SER_eval(txSyms,rxSyms);
        end
    end
end

%% EVM Evaluation
if any(strncmp('EVM',DSP.FOM,3)) && isfield(DSP,'DECODER') && ...
    isfield(DSP.DECODER,'txSyms')

    for k = 1:nSC
        txSyms = DSP.DECODER(k).txSyms;
        if any(strcmp('EVM_t',DSP.FOM))
            OUT.EVM{k} = EVM_eval(Srx{k},txSyms,SIG(k),QAM{k},500);
        elseif any(strcmp('EVM_sym',DSP.FOM))
            OUT.EVM{k} = EVM_eval(Srx{k},txSyms,SIG(k),QAM{k});
        else
            OUT.EVM{k} = EVM_eval(Srx{k},Stx{k});
        end
    end
end

%% MI Evaluation
if any(strncmp('MI',DSP.FOM,2)) && isfield(DSP,'DECODER') && ...
    isfield(DSP.DECODER,'txSyms')

    for k = 1:nSC
        txSyms = DSP.DECODER(k).txSyms;
        M = QAM{k}.M;
        C = QAM{k}.IQmap;
        if isfield(QAM{k},'sym2bitMap')
            B = QAM{k}.sym2bitMap;
            [mi,snr,rxSyms,gmi] = MI_eval(Srx{k}.',txSyms+1,M,C,B,false);
        else
            [mi,snr,rxSyms,gmi] = MI_eval(Srx{k}.',txSyms+1,M,C);
        end
        if any(strcmp('SER_sym',DSP.FOM))
            OUT.MI{k}.SER = SER_eval(txSyms,rxSyms.'-1,QAM{k});
        else
            OUT.MI{k}.SER = SER_eval(txSyms,rxSyms.'-1);
        end
        OUT.MI{k}.SER = rmfield(OUT.MI{k}.SER,{'errPosX','errPosY'});

        OUT.MI{k}.MIx = mi(1);
        OUT.MI{k}.MIy = mi(2);
        OUT.MI{k}.MIxy = mean([mi(1) mi(2)]);
        OUT.MI{k}.SNRx_dB = snr(1);
        OUT.MI{k}.SNRy_dB = snr(2);
        OUT.MI{k}.GMIx = gmi(1);
        OUT.MI{k}.GMIy = gmi(2);
    end
end
