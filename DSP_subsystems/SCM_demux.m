function [S_SC,PARAM_SC,SIG,meanP] = SCM_demux(S_MSC,PARAM_MSC,SIG,DSP)

% Last Update: 29/08/2017

global PROG


%% Input Parameters
nSC = numel(SIG);
grid_SC = NaN(1,nSC);
if isfield(SIG,'fWDM')
    fWDM = [SIG.fWDM];
else
    fWDM = zeros(1,nSC);
end
for k = 1:nSC
    grid_SC(k) = SIG(k).fSC + fWDM(k);
end
% If the SC central frequencies are in GHz, translate into Hz:
if max(abs(grid_SC)) < 1e5
    grid_SC = grid_SC * 1e9;
end
SCM = DSP.SCM;

%% Frequency Shift each Subcarrier to Baseband
t = repmat(PARAM_MSC.t,size(S_MSC,1),1);
for k = 1:nSC
    fShift = -grid_SC(k);
    S_SC{k} = S_MSC.*exp(1j*2*pi*fShift*t);
end

%% Check for Frequency Mismatch at the Receiver (due to intradyne receiver)
if isfield(SCM,'LPF')
    if isfield(DSP,'DC')
        if iscell(DSP.DC)
            for n = 1:numel(DSP.DC)
                if isfield(DSP.DC{n},'df')
                    [SCM.LPF.F0] = deal(-DSP.DC{n}.df);
                    break;
                end
            end
        end
    end
end

%% Low-Pass Filtering
if isfield(SCM,'LPF')
    for k = 1:nSC
        S_SC{k} = myLPF(S_SC{k},SCM.LPF(k),PARAM_MSC,SIG(k));
    end
end

%% Digital Resampling
if isfield(SCM,'RESAMP')
    Fs_in = PARAM_MSC.sampRate;
    for k = 1:nSC
        Fs_out = SCM.RESAMP(k).nSpS * SIG(k).symRate;
        PROG.MSG.myResample = ['RESAMPLING: Subcarrier ',num2str(k)];
        [S_SC{k},PARAM_SC(k)] = applyResample(S_SC{k},Fs_in,Fs_out);
    end
else
    for k = 1:nSC
        PARAM_SC(k) = PARAM_MSC;
    end
end

%% Signal Truncation
if isfield(SCM,'TRUNC')
    if isnumeric(SCM.TRUNC)
        for k = 1:nSC
            [S_SC{k},PARAM_SC(k)] = truncateSignal(S_SC{k},PARAM_SC(k),...
                SCM.TRUNC(k,:));
        end
    elseif istruct(SCM.TRUNC)
        for k = 1:nSC
            [S_SC{k},PARAM_SC(k)] = truncateSignal(S_SC{k},PARAM_SC(k),...
                SCM.TRUNC(k));
        end
    end
end

%% Update Signal Parameters of Each Subcarrier
for k = 1:nSC
    SIG_out(k) = setSignalParams(SIG(k),PARAM_SC(k));
end
SIG = SIG_out;

%% Calculate Average Power per Subcarrier
for n = 1:nSC
    meanP(n) = pow2db((mean(abs(S_SC{n}(1,:)).^2)*1e3 + ...
        mean(abs(S_SC{n}(2,:)).^2)*1e3)/2);
end
