function [Srx,PARAM,SIG] = SC_matchedFilter(Srx,PARAM,SIG,DSP)

% Last Update: 01/08/2017

global PROG


%% Low-Pass Filtering
if isfield(DSP,'LPF')
    Srx = myLPF(Srx,DSP.LPF,PARAM,SIG);
end

%% Digital Resampling
if isfield(DSP,'RESAMP')
    Fs_in = PARAM.sampRate;
    Fs_out = DSP.RESAMP.nSpS * SIG.symRate;
    PROG.MSG.myResample = 'RESAMPLING';
    [Srx,PARAM] = applyResample(Srx,Fs_in,Fs_out);
end

%% Update Signal Parameters
SIG = setSignalParams(SIG,PARAM);
