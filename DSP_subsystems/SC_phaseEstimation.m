function [Srx,PARAM,CPE_out] = SC_phaseEstimation(Srx,Stx,PARAM,SIG,...
    QAM,DSP,nthStage)

% Last Update: 24/07/2017


global PROG;

%% Input Parser
if isfield(DSP,['CPE',num2str(nthStage)])
    eval(['CPE = DSP.CPE',num2str(nthStage),';']);
else
    CPE_out = [];
    return;
end

%% Check for Transmitted Signal in Case of Data-Aided CPE
if isfield(CPE,'SYNC')
    if isfield(CPE.SYNC,'applySync')
        applySync = CPE.SYNC.applySync;
    else
        applySync = true;
    end
else
    applySync = true;
end

if applySync
    if SIG.nSpS > 1
        if ~isfield(CPE,'ts0')
            for n = 1:size(Srx,1)
                ts0 = bestSamplingInstant(Srx(n,:),SIG.nSpS,QAM,'DD');
                Srx_SYNC(n,:) = Srx(n,ts0:SIG.nSpS:end);
            end
        else
            Srx_SYNC = Srx(:,CPE(1).ts0:SIG.nSpS:end);
        end
    else
        Srx_SYNC = Srx;
    end
    SYNC.method = 'complexField';
    SYNC.txSignal = Stx;
    Stx = syncTxRx_MIMO(Srx_SYNC,SYNC);
else
    nRep = floor(length(Srx)/SIG.nSpS/length(Stx));
    nHead = mod(length(Srx)/SIG.nSpS,length(Stx));
    Stx = [repmat(Stx,1,nRep) Stx(:,1:nHead)];
end
    
%% Carrier-Phase Estimation
for n = 1:numel(CPE)
    PROG.MSG.CPE = ['PHASE ESTIMATION ',num2str(nthStage)];
    CPE(n).txSignal = Stx;
    [Srx,PARAM,CPE_out(n)] = myCPE(Srx,SIG.nSpS,PARAM,QAM,CPE(n));
end
