function [Srx,PARAM] = SCM_postDSP(Srx,PARAM,DSP)

% Last Update: 14/04/2017


%% Input Parser
nSC = numel(Srx);

%% Remove Edge Samples
if isfield(DSP,'TRUNC_postDSP')
    TRUNC = DSP.TRUNC_postDSP;
    for k = 1:nSC
        [Srx{k},PARAM(k)] = rmvEdgeSamples(Srx{k},PARAM(k),TRUNC);
    end
elseif isfield(DSP,'TRUNC') && isfield(DSP.TRUNC,'rmvEdgeSamples')
    TRUNC = DSP.TRUNC.rmvEdgeSamples;
    for k = 1:nSC
        [Srx{k},PARAM(k)] = rmvEdgeSamples(Srx{k},PARAM(k),TRUNC);
    end
end

