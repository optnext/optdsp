function [DEC,Srx] = SC_rxDECODER(Srx,Stx,QAM,DSP)

% Last Update: 01/09/2017

global PROG;

%% Rx Decoder
if isfield(DSP,'DECODER')
    PROG.MSG.symDecoder = 'SYMBOL DECODER';
    [DEC,Srx] = symDecoder(Srx,Stx,QAM,DSP.DECODER);
else
    DEC = [];
end

