function [Srx,PARAM,SIG,ts0] = SC_downSample(Srx,PARAM,SIG,ts0)

% Last Update: 17/07/2017


%% Input Parser
if ~exist('ts0','var')
    ts0 = 1;
end

%% Downsampling
[Srx,PARAM,ts0] = downsample_1sps(Srx,SIG.nSpS,PARAM,ts0);
SIG = setSignalParams(SIG,PARAM);



