function [Srx,PARAM,MIMO_out] = SCM_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,...
    DSP,nthStage)

% Last Update: 08/08/2017


global PROG;

%% Input Parser
MIMO_ID = ['MIMO',num2str(nthStage)];
if isfield(DSP,MIMO_ID)
    eval(['MIMOin = DSP.MIMO',num2str(nthStage),';']);
else
    MIMO_out = [];
    return;
end
% Input parser for adaptive equalizer functions:
nSC = numel(SIG);
for k = 1:nSC
    if numel(MIMOin) == nSC
        idx = k;
    else
        idx = 1;
    end
    [MIMO(k),Srx{k}] = adaptiveEq_inputParser(MIMOin(idx),Srx{k},QAM{k});
end

%% Adaptive Equalization
if any(strcmp(MIMO(1).method(end-2:end),{'4x4','2x2','4x2'}))
    if strcmpi(MIMO(1).applicationRange,'all')
        for k = 1:nSC
            % Display Message:
            PROG.MSG.adaptiveEq = [MIMO_ID,' Equalizer (',...
                MIMO(k).method,'): Subcarrier ',num2str(k)];
            % Apply Adaptive Equalizer:
            [Srx{k},PARAM(k),MIMO_out{k}] = adaptiveEq(Srx{k},Stx{k},...
                PARAM(k),QAM{k},MIMO(k),SIG(k).nSpS);
        end
    elseif strcmpi(MIMO(1).applicationRange,'referenceCarrier')
        % Define Reference Carrier(s):
        if isfield(MIMO(1),'refCarrier')
            kRef = MIMO(1).refCarrier;
        else
            kRef = ceil(nSC/2);
        end
        % Process Reference Carrier(s):
        n = 1;
        for k = kRef
            % Display Message:
            PROG.MSG.adaptiveEq = [MIMO_ID,' Equalizer (',...
                MIMO(k).method,'): Subcarrier ',num2str(k)];
            % Apply Adaptive Equalizer:            
            [Srx{k},PARAM(k),MIMO_out{k}] = adaptiveEq(Srx{k},Stx{k},...
                PARAM(k),QAM{k},MIMO(k),SIG(k).nSpS);
            % Collect Filter Taps:
            W(:,:,n) = MIMO_out{k}.W;
            % Update index:
            n = n + 1;
        end
        % Apply the same taps to the other carriers:
        meanW = mean(W,3);
        nTaps = MIMO(k).nTaps;
        EQ_method = MIMO(k).method(end-2:end);
        TRUNC = MIMO_out{kRef(1)}.indDemux;
        for k = 1:nSC
            if k ~= kRef
                Srx{k} = adaptiveEq_freeze(Srx{k},meanW,nTaps,EQ_method);
                [Srx{k},PARAM(k)] = truncateSignal(Srx{k},PARAM(k),TRUNC);
            end
        end
    end
elseif any(strcmp(MIMO(1).method(end-2:end),{'8x8','8x4'}))
    for k = 1:nSC/2
        kk = nSC-k+1;
        % Display Message:
        PROG.MSG.adaptiveEq = [MIMO_ID,' Equalizer (',MIMO(k).method,...
            '): Subcarriers ',num2str(k),' and ',num2str(kk)];
        % Apply Adaptive Equalizer:   
        [Sout,PARAM(k),MIMO_out{k}] = adaptiveEq([Srx{k};Srx{kk}],...
            [Stx{k};Stx{kk}],PARAM(k),QAM{k},MIMO(k),SIG(k).nSpS);
        % TEMPORARY FIX (works only for FDHMF signals whose formats are 
        % symmetric between left and right carriers):
        PARAM(kk) = PARAM(k);
        % TEMPORARY FIX (works only for FDHMF signals whose formats are 
        % symmetric between left and right carriers):
        Srx{k} = Sout(1:2,:);
        Srx{kk} = Sout(3:4,:);
    end
elseif strcmp(MIMO(1).method,'none')
    MIMO_out = [];
end



