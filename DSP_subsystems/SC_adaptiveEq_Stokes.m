function [Srx,STOK] = SC_adaptiveEq_Stokes(Srx,STOK)

% Last Update: 27/07/2017


global PROG;
       
%% Adaptive Equalization
PROG.MSG.adaptiveEq = ['Stokes Pol-Demux: Subcarrier ',num2str(k)];
[Srx,STOK] = stokesEq(Srx,STOK);
