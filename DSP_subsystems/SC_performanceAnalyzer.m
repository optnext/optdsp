function [OUT] = SC_performanceAnalyzer(Srx,Stx,QAM,SIG,DSP)

% Last Update: 01/09/2017


%% Input Parser
if ~isfield(DSP,'FOM')
    OUT = [];
    return;
end

%% Analyze QAM Variance
if any(strcmp('varianceQAM',DSP.FOM))
    [txSignal] = SC_syncTxRx(Srx,Stx);
    OUT.QAM = QAM_varianceAnalyzer(Srx,txSignal,QAM);
end

%% BER Evaluation
if any(strcmp('BER',DSP.FOM)) && isfield(DSP,'DECODER') && ...
    all(isfield(DSP.DECODER,{'txBits','rxBits'}))

    txBits = DSP.DECODER.txBits;
    rxBits = DSP.DECODER.rxBits;
    if any(strcmp('BER_sym',DSP.FOM))
        options.BER_sym = true;
        OUT.BER = BER_eval(txBits,rxBits,QAM,options);
    else
        OUT.BER = BER_eval(txBits,rxBits,QAM);
    end
end

%% SER Evaluation
if any(strcmp('SER',DSP.FOM)) && isfield(DSP,'DECODER') && ...
    all(isfield(DSP.DECODER,{'txSyms','rxSyms'}))

    txSyms = DSP.DECODER.txSyms;
    rxSyms = DSP.DECODER.rxSyms;
    if any(strcmp('SER_sym',DSP.FOM))
        OUT.SER = SER_eval(txSyms,rxSyms,QAM);
    else
        OUT.SER = SER_eval(txSyms,rxSyms);
    end
end

%% EVM Evaluation
if any(strncmp('EVM',DSP.FOM,3)) && isfield(DSP,'DECODER') && ...
    isfield(DSP.DECODER,'txSyms')

    txSyms = DSP.DECODER.txSyms;
    if any(strcmp('EVM_t',DSP.FOM))
        OUT.EVM = EVM_eval(Srx,txSyms,SIG,QAM,500);
    elseif any(strcmp('EVM_sym',DSP.FOM))
        OUT.EVM = EVM_eval(Srx,txSyms,SIG,QAM);
    else
        OUT.EVM = EVM_eval(Srx,Stx);
    end
end

%% MI Evaluation
if any(strncmp('MI',DSP.FOM,2)) && isfield(DSP,'DECODER') && ...
    isfield(DSP.DECODER,'txSyms')

    txSyms = DSP.DECODER.txSyms;
    M = QAM.M;
    C = QAM.IQmap;
    if isfield(QAM,'sym2bitMap')
        B = QAM.sym2bitMap;
        [mi,snr,rxSyms,gmi] = MI_eval(Srx.',txSyms+1,M,C,B,false);
    else
        [mi,snr,rxSyms,gmi] = MI_eval(Srx.',txSyms+1,M,C);
    end
    if any(strcmp('SER_sym',DSP.FOM))
        OUT.MI.SER = SER_eval(txSyms,rxSyms.'-1,QAM);
    else
        OUT.MI.SER = SER_eval(txSyms,rxSyms.'-1);
    end
    OUT.MI.SER = rmfield(OUT.MI.SER,{'errPosX','errPosY'});

    OUT.MI.MIx = mi(1);
    OUT.MI.MIy = mi(2);
    OUT.MI.MIxy = mean([mi(1) mi(2)]);
    OUT.MI.SNRx_dB = snr(1);
    OUT.MI.SNRy_dB = snr(2);
    OUT.MI.GMIx = gmi(1);
    OUT.MI.GMIy = gmi(2);
end

