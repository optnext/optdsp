function [Srx,PARAM,SIG,ts0] = SCM_downSample(Srx,PARAM,SIG,ts0)

% Last Update: 10/04/2017


global PROG;

%% Input Parser
nSC = numel(Srx);
if ~exist('ts0','var')
    ts0 = 1;
end

%% Downsampling
for k = 1:nSC
    PROG.MSG.downSamp = ['DOWNSAMPLING: Subcarrier ',num2str(k)];
    [Srx{k},PARAM(k),ts0] = downsample_1sps(Srx{k},SIG(k).nSpS,PARAM(k),ts0);
    SIG(k) = setSignalParams(SIG(k),PARAM(k));
end
