function [S,PARAMrx,DC_out] = RX_frontEndCorrection(S,PARAMrx,DSP)

% Last Update: 17/07/2016


%% Rx Deskew
if isfield(DSP,'DESKEW')
    S = deskewRx(S,PARAMrx(1),DSP.DESKEW);
end

%% Remove DC Component(s)
if isfield(DSP,'DC')
    if iscell(DSP.DC)
        for n = 1:numel(DSP.DC)
            [S,DC_out{n}] = removeDC(S,PARAMrx(1),DSP.DC{n});
        end
    else
        [S,DC_out] = removeDC(S,PARAMrx(1),DSP.DC);
    end
else
    DC_out = [];
end

