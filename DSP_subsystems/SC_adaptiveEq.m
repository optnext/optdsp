function [Srx,PARAM,MIMO] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,...
    nthStage)

% Last Update: 08/08/2017


global PROG;

%% Input Parser
MIMO_ID = ['MIMO',num2str(nthStage)];
if isfield(DSP,MIMO_ID)
    eval(['MIMO = DSP.MIMO',num2str(nthStage),';']);
else
    MIMO = [];
    return;
end

%% Adaptive Equalization
% Display Message:
PROG.MSG.adaptiveEq = [MIMO_ID,' Equalizer (',MIMO.method,')'];

% Apply Adaptive Equalizer:
[Srx,PARAM,MIMO] = adaptiveEq(Srx,Stx,PARAM,QAM,MIMO,SIG.nSpS);
