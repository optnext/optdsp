function [Srx,PARAM,CPE_out] = SCM_phaseEstimation(Srx,Stx,PARAM,SIG,...
    QAM,DSP,nthStage)

% Last Update: 10/04/2017


global PROG;

%% Input Parser
if isfield(DSP,['CPE',num2str(nthStage)])
    eval(['CPE = DSP.CPE',num2str(nthStage),';']);
else
    CPE_out = [];
    return;
end
if isstruct(CPE)
    TMP = CPE;
    clear CPE;
    CPE{1} = TMP;
end
for n = 1:numel(CPE)
    if ~isfield(CPE{n}(1),'applicationRange')
        CPE{n}(1).applicationRange = 'all';
    end
end
nSC = numel(Srx);

%% Check for Transmitted Signal in Case of Data-Aided CPE
if SIG(1).nSpS > 1
    if ~isfield(CPE{1},'ts0')
        for k = 1:nSC
            for n = 1:size(Srx{k},1)
                ts0 = bestSamplingInstant(Srx{k}(n,:),SIG(k).nSpS,QAM{k},'DD');
                Srx_SYNC{k}(n,:) = Srx{k}(n,ts0:SIG(k).nSpS:end);
            end
        end
    else
        for k = 1:nSC
            Srx_SYNC{k} = Srx{k}(:,CPE{1}(1).ts0:SIG(k).nSpS:end);
        end
    end
else
    Srx_SYNC = Srx;
end
[Stx] = syncTxRx_SCM(Srx_SYNC,Stx);

%% Carrier-Phase Estimation
for n = 1:numel(CPE)
    switch CPE{n}(1).applicationRange
        case 'all'
            for k = 1:nSC
                if numel(CPE{n}) == nSC
                    ind_CPE = k;
                else
                    ind_CPE = 1;
                end
                PROG.MSG.CPE = ['PHASE ESTIMATION ',num2str(nthStage),...
                    ': Subcarrier ',num2str(k)];
                CPE{n}(ind_CPE).txSignal = Stx{k};
                [Srx{k},PARAM(k),CPE_out{n}(k)] = myCPE(Srx{k},...
                    SIG(k).nSpS,PARAM(k),QAM{k},CPE{n}(ind_CPE));
            end
        case 'referenceCarrier'
            if isfield(CPE{n}(1),'refCarrier')
                kRef = CPE{n}(1).refCarrier;
            else
                if nSC == 1
                    kRef = 1;
                else
                    kRef = floor(nSC/2);
                end
            end
            if numel(CPE{n}) == nSC
                ind_CPE = kRef;
            else
                ind_CPE = 1;
            end
            PROG.MSG.CPE = ['REFERENCE PHASE ESTIMATION ',...
                num2str(nthStage),': Subcarrier ',num2str(kRef)];
            CPE{n}(ind_CPE).txSignal = Stx{kRef};
            [Srx{kRef},PARAM(kRef),CPE_out{n}(ind_CPE)] = myCPE(...
                Srx{kRef},SIG(kRef).nSpS,PARAM(kRef),QAM{kRef},...
                CPE{n}(ind_CPE));
            phi = rectpulse(CPE_out{n}(ind_CPE).phi',SIG(kRef).nSpS)';
            CPE{n}(ind_CPE).convMethod = 'vector';
            phi = rmvEdgeSamplesFIR(phi,PARAM(kRef),CPE{n}(ind_CPE),...
                SIG(kRef).nSpS);
            for k = 1:nSC
                if k~=kRef
                    Srx{k} = rmvEdgeSamplesFIR(Srx{k},...
                        PARAM(k),CPE{n}(ind_CPE),SIG(kRef).nSpS);
                    Srx{k} = Srx{k}.*exp(-1j*phi);
                end
            end
        case 'none'
            CPE_out = [];
    end
end
