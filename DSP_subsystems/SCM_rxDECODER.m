function [DEC,Srx] = SCM_rxDECODER(Srx,Stx,QAM,DSP)

% Last Update: 01/09/2017


global PROG;

%% Input Parser
nSC = numel(Srx);

%% Rx Decoder
if isfield(DSP,'DECODER')
    for k = 1:nSC
        PROG.MSG.symDecoder = ['SYMBOL DECODER: Subcarrier ',num2str(k)];
        [DEC(k),Srx{k}] = symDecoder(Srx{k},Stx{k},QAM{k},DSP.DECODER);
    end
else
    DEC = [];
end

