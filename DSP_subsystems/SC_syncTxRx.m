function [Stx_SYNC] = SC_syncTxRx(Srx,Stx,SYNC)

% Last Update: 21/07/2016


%% Input Parser
if ~exist('SYNC','var')
    SYNC.method = 'complexField';
    SYNC.nPol = 2;
else
    if ~isfield(SYNC,'method')
        SYNC.method = 'complexField';
    end
    if ~isfield(SYNC,'nPol')
        SYNC.nPol = 2;
    end
end

%% Synchronize Tx and Rx Signals
SYNC.txSignal = Stx;
Stx_SYNC = syncTxRx_MIMO(Srx,SYNC);
