function [Srx,FE] = SC_freqEstimation(Srx,Stx,PARAM,SIG,DSP)

% Last Update: 20/07/2017


%% Input Parser
if ~isfield(DSP,'FE')
    FE = [];
    return;
else
    FE = DSP.FE;
end

%% Check for Transmitted Signal in Case of Data-Aided FE
if isfield(FE,'SYNC')
    if isfield(FE.SYNC,'applySync')
        applySync = FE.SYNC.applySync;
    else
        applySync = true;
    end
else
    applySync = false;
end

if applySync
    if isfield(FE.SYNC,'method')
        SYNC_MIMO.syncMethod = FE.SYNC.method;
    else
        if SIG.M <= 4
            SYNC_MIMO.syncMethod = 'complexField';
        else
            SYNC_MIMO.syncMethod = 'abs';
        end
    end
    SYNC_MIMO.txSignal = Stx;
    FE.txSignal = syncTxRx_MIMO(Srx(:,1:SIG.nSpS:end),SYNC_MIMO);
else
    nRep = floor(length(Srx)/SIG.nSpS/length(Stx));
    nHead = mod(length(Srx)/SIG.nSpS,length(Stx));
    FE.txSignal = [repmat(Stx,1,nRep) Stx(:,1:nHead)];
end

%% Frequency Mismatch Compensation
[Srx,FE] = myFE(Srx,PARAM,FE,SIG.nSpS);

