function [STOK]=presetSTK(varargin)

% Last Update: 07/09/2014


%% Input Parser
nOptionalArgs=length(varargin);
for n=1:2:nOptionalArgs
    varName=varargin{n};
    varValue=varargin{n+1};
    if any(strncmpi(varName,{'PreRunning','Pre-Running'},4))
        preR=varValue;
    elseif any(strncmpi(varName,{'Nsamp'},4))
        nSamp=varValue;
    elseif any(strncmpi(varName,{'rotation'},3))
        rot=varValue;
    elseif any(strncmpi(varName,{'invert'},3))
        invertRotation=varValue;
    elseif any(strncmpi(varName,{'muPreRun'},3))
        muPreRun=varValue;
    elseif any(strncmpi(varName,{'muDemux'},3))
        muDemux=varValue;
    elseif any(strncmpi(varName,{'PDLcomp'},4))
        PDLcomp=varValue;
    elseif any(strncmpi(varName,{'debugPlots'},5))
        debugPlots=varValue;
    end 
end

% Default Values:
if ~exist('preR','var')
    preR=1;
end
if ~exist('nSamp','var')
    nSamp=5e3;
end
if ~exist('rot','var')
    rot=-pi/4;
end
if ~exist('invertRotation','var')
    invertRotation=1;
end
if ~exist('muPreRun','var')
    muPreRun=0.1;
end
if ~exist('muDemux','var')
    muDemux=0.09;
end
if ~exist('PDLcomp','var')
    PDLcomp=[0 0 0];
%     PDLcomp=[0 0 1];
end
if ~exist('debugPlots','var')
    debugPlots={};
end

%% Assign Parameters
STOK.PreR=preR;             % 0 -> witout pre-running; 1 -> with pre-runnning;
STOK.Nsamp=nSamp;           % Samples to be considered in the pre run
STOK.rot=rot;               % rotation
STOK.Inv=invertRotation;    % inverts the rotation (-1)
STOK.muPr=muPreRun;         % step size PreRunnning
STOK.mu=muDemux;            % step size Decoding
STOK.PDLcomp=PDLcomp;       % PDL compensation
STOK.debugPlots=debugPlots; % debug plots

