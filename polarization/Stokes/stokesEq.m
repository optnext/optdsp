function [Sout,STOK] = stokesEq(Sin,STOK)

% Last Update: 07/09/2014


%% Input Parser
if isfield(STOK,'debugPlots')
    debugPlots = STOK.debugPlots;
else
    debugPlots = {};
end

%% Adpative Stokes PolDemux
[Sout,coeff,MM] = PDStokesAdapt(Sin,STOK);

%% Plots
nPlots = length(debugPlots);
if nPlots
    [S_Input,s_Input] = jones2stokesAvg(Sin);
    [S_Out3D,s_Out3D] = jones2stokesAvg(Sout);
end

for n = 1:nPlots
    if any(strcmpi(debugPlots{n},{'inputSignal','inputSignal_2D','all'}))
        figure();
        plot(Sin(1,:),'.');
        hold on
        plot(Sin(2,:),'.r');
        hold off
    end
    if any(strcmpi(debugPlots{n},{'outputSignal','outputSignal_2D','all'}))
        figure();
        plot(Sout(1,:),'.');
        hold on
        plot(Sout(2,:),'.r');
        hold off
    end
    if any(strcmpi(debugPlots{n},{'inputSignal_3D','all'}))
        figure();
        plot_sphere(mean(S_Input(1,:)));%
        hold on;
%         plot3(S_Input(2,:),S_Input(3,:),S_Input(4,:),'.k',...
%         'MarkerEdgeColor','r','MarkerSize',18)
        plot3(S_Input(2,60000:end),S_Input(3,60000:end),S_Input(4,60000:end),'.k',...
        'MarkerEdgeColor','b','MarkerSize',18)
        axis equal
        hold off
        %%vitor
        SV0=sqrt(S_Input(2,60000:end).^2+S_Input(3,60000:end).^2+S_Input(4,60000:end).^2);
        SV1=S_Input(2,60000:end);
        SV2=S_Input(3,60000:end);
        SV3=S_Input(4,60000:end);
        x=[];
        for i=1:length(SV0)
         x=[x;1,SV0(i),SV1(i)'./SV0(i)',SV2(i)'./SV0(i)',SV3(i)'./SV0(i)',1];
        end
        dlmwrite(['stokes1.prn'],x)
    end
    if any(strcmpi(debugPlots{n},{'outputSignal_3D','all'}))
        figure();
        plot_sphere(mean(S_Out3D(1,:)));%
        hold on;
%            plot3(S_Out3D(2,:),S_Out3D(3,:),S_Out3D(4,:),'.k',...
%            'MarkerEdgeColor','r','MarkerSize',18)
     plot3(S_Out3D(2,60000:end),S_Out3D(3,60000:end),S_Out3D(4,60000:end),'.k',...
       'MarkerEdgeColor','r','MarkerSize',18)
        axis equal
        hold off
        %%vitor
        SA0=sqrt(S_Input(2,60000:end).^2+S_Input(3,60000:end).^2+S_Input(4,60000:end).^2);
        SA1=S_Input(2,60000:end);
        SA2=S_Input(3,60000:end);
        SA3=S_Input(4,60000:end);
        x=[];
        for i=1:length(SA0)
         x=[x;1,SA0(i),SA1(i)'./SA0(i)',SA2(i)'./SA0(i)',SA3(i)'./SA0(i)',1];
        end
        dlmwrite(['stokes2.prn'],x)
    end
    if any(strcmpi(debugPlots{n},{'convergence','normalConvergence','all'}))
        figure();
        load SampNormH25
        plot(SampNormH(1:5000,1),SampNormH(1:5000,2:4))
    end
end

%% Output STOK Struct
STOK.coeff = coeff;
STOK.W = MM;
