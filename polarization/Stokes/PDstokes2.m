
function [E_3d,coeff]=PDstokes2(Ein,Stk3D)
%%%%%%% 3D Stokes Demultiplexing for PM-QPSK
%
% Finds the plane XCoeff*S1+YCoeff*S2+S3=CCoeff
% that best fits the Stokes vectors of the sysmbols
% After that, calculates the two unit vectors normal to the plane
% n1,2=+/1[XCoeff YCoeff 1], which can be used to obtain the inverse
% matrix of the fiber
%-----
% Sin -> input pol. multipled signal
% St3D.NsampBit -> nbr of samples to be used
% Signal.sampBit -> signal bumber of samples/bit
%-----
% E_3d -Z output pol. demultiplexed signal
% coeff -> 3D plane equation

NsampBit = Stk3D.NsampBit;
Nsamp    = Stk3D.Nsamp;



[Spdl,spdl] = jones2stokes(Ein(:,(1:min([length(Ein),Nsamp]))));

S1v = Spdl(2,:)'; % Make S1 a column vector
S2v = Spdl(3,:)'; % Make S2 a column vector
S3v = Spdl(4,:)'; % Make S3 a column vector

A=[S1v,S2v,S3v,ones(length(S1v),1)];
[U,S,V]=svd(A);
ss=diag(S);
i=find(ss==min(ss)); % find position of minimal singular value
display 'output plane fit'
coeff=V(:,min(i)) % this may be multiple
display 'Central point distance to the origin'
cm=sqrt(mean(S1v)^2+mean(S2v)^2+mean(S3v)^2)% Calculates the mean point
display 'plane distance from the origin'
cm_d=coeff(4) %distance from the origin


% [AA,BB,CC]=fit_3D_data(Spdl(2,:)',Spdl(3,:)',Spdl(4,:)', 'plane', 'on', 'on')
% 

coeff = coeff/norm(coeff(1:3),2); % Normalizes the coefficients
coeff = coeff*Stk3D.Inv;          % Inverts the rotation

% Auxiliar parameters
% dfi = atan((coeff(2)/coeff(3))^(-1));%
% dalf = 1/2*atan((coeff(1)/sqrt(coeff(2)^2+coeff(3)^2))^(-1));%
dfi = atan2(100*coeff(3),100*coeff(2));%
dalf = 1/2*atan2(sqrt(coeff(2)^2+coeff(3)^2),coeff(1));%

% SOP rotation compensation matrix
MM = [cos(dalf)*exp(1i*dfi/2) sin(dalf)*exp(-1i*dfi/2) 
     -sin(dalf)*exp(1i*dfi/2) cos(dalf)*exp(-1i*dfi/2)];

E_3d = MM*Ein; % compesates for SOP rotation

 
display 'parametros normalizados'
d=coeff(4)/(2*mean(abs(Ein(:)).^2));
cM=cm/(2*mean(abs(Ein(:)).^2));
 

%%
%----- PDL COMPENSATION SECTION
%
%% Compensates for S1
if Stk3D.PDLcomp(1) == 1 %compensation matrix (using plane)
PP = [sqrt(1+d) 0
      0         sqrt(1-d)];
else                  % no compensation
PP = [1 0
      0         1];
end

 E_3d=PP*E_3d; % compensates S1
 
 %% Compensates for S2 and S3
 
 if Stk3D.PDLcomp(2) == 1 %compensation matrix (using plane)
     
 [S_S1Comp,S1Comp]   = jones2stokes(E_3d);
 % calculation of mean s2 and s3 (after s1 comp.)
  S_mean = mean(S_S1Comp,2);
  
  %------- Comp.s2
  psi=pi/2;
  
  %rotation pi/2 matrix around s3
  U3 = [cos(psi/2) -sin(psi/2);sin(psi/2) cos(psi/2)];
  E_3d=U3*E_3d;
  
  %distance along s2
  d2=S_mean(3)/(2*mean(abs(Ein(:)).^2));
  PP2 = [sqrt(1+d2) 0
        0 sqrt(1-d2)];
  E_3d=PP2*E_3d;
  
  %rotation -pi/2 matrix around s3
  U3 = [cos(-psi/2) -sin(-psi/2);sin(-psi/2) cos(-psi/2)];
  E_3d=U3*E_3d;
 else
 end
 
 if Stk3D.PDLcomp(3) == 1 %compensation matrix (using plane)

  %------- Comp.s3
  psi=pi/2;
  
  %rotation pi/2 matrix around s3
  U2 = [cos(psi/2) 1i*sin(psi/2);1i*sin(psi/2) cos(psi/2)];
  E_3d=U2*E_3d;
  
  %distance along s2
  d3=S_mean(4)/(2*mean(abs(Ein(:)).^2));
  PP3 = [sqrt(1+d3) 0
        0 sqrt(1-d3)];
  E_3d=PP3*E_3d;
  
  %rotation -pi/2 matrix around s3
  U2 = [cos(-psi/2) 1i*sin(-psi/2);1i*sin(-psi/2) cos(-psi/2)];
  E_3d=U2*E_3d;
 else
    
 end
  %E_3d=MM*E_3d; % compesates for SOP rotation
 
 
 