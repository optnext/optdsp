
% Calculate the stokes vectors
function [S,s]=jones2stokesAvg(Eout)
%%%%%%%%%% NJ Muga 25-09-2014
% Ein -> Input Jones Space signal
%
% S -> Stokes parameters
% s -> Normalized Stokes parameters S/mean(S(1,:))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Initalizes the vector
S=zeros(4,length(Eout)); 

% Calculates the 4 Stokes parameters
S(1,:)=Eout(1,:).*conj(Eout(1,:))+Eout(2,:).*conj(Eout(2,:));
S(2,:)=Eout(1,:).*conj(Eout(1,:))-Eout(2,:).*conj(Eout(2,:));
S(3,:)=Eout(1,:).*conj(Eout(2,:))+conj(Eout(1,:)).*Eout(2,:);
S(4,:)=1i*(Eout(1,:).*conj(Eout(2,:))-conj(Eout(1,:)).*Eout(2,:));

% Calculates the 3 normalized Stokes parameters
s=S(2:end,:)./repmat(mean(S(1,:)),3,length(Eout));
%%% END OF FUCNTION %%%%%%%%%%%%%%%%%%%%%%%%%


