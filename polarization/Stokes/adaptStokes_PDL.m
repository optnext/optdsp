function [M_PDL] = adaptStokes_PDL(Ein,Eout_aux)


%% PDL Cicle -------------------------------------
% If used with Eout_aux from first run
S_Out_aux = jones2stokesAvg(Eout_aux); %Calculates the Normalized and non-normalized stokes vectors
m_S1 = mean(S_Out_aux(2,:));
m_S2 = mean(S_Out_aux(3,:));
m_S3 = mean(S_Out_aux(4,:));

% Calculates the 3 displacements in Stokes Space
M1 = -m_S1/(2*mean(abs(Ein(:)).^2));
M2 = m_S2/(2*mean(abs(Ein(:)).^2));
M3 = -m_S3/(2*mean(abs(Ein(:)).^2));

% display 'Central point distance to the origin'
% cm = sqrt(m_S1^2 + m_S2^2 + m_S3^2);
% cm_norm = cm/(2*mean(abs(Ein(:)).^2));

m11 = -((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) - (sqrt((1 + M3)) * sqrt((1 + M2))) - (sqrt((1 + M2)) * sqrt((1 - M3))) - (sqrt((1 - M2)) * sqrt((1 + M3))) - (sqrt((1 - M3)) * sqrt((1 - M2))) + (-1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (1i) * sqrt((1 - M2)) * sqrt((1 - M3))) * sqrt((1 + M1)) / 4;
m21 = -((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) + (sqrt((1 + M3)) * sqrt((1 + M2))) + (sqrt((1 + M2)) * sqrt((1 - M3))) + (1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 - M2)) * sqrt((1 - M3)) - (sqrt((1 - M2)) * sqrt((1 + M3))) - (sqrt((1 - M3)) * sqrt((1 - M2)))) * sqrt((1 + M1)) / 4;
m12 = ((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) - (sqrt((1 + M3)) * sqrt((1 + M2))) - (sqrt((1 + M2)) * sqrt((1 - M3))) + (sqrt((1 - M2)) * sqrt((1 + M3))) + (sqrt((1 - M3)) * sqrt((1 - M2))) + (1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 - M2)) * sqrt((1 - M3))) * sqrt((1 - M1)) / 4;
m22 = ((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) + (sqrt((1 + M3)) * sqrt((1 + M2))) + (sqrt((1 + M2)) * sqrt((1 - M3))) + (-1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (1i) * sqrt((1 - M2)) * sqrt((1 - M3)) + (sqrt((1 - M2)) * sqrt((1 + M3))) + (sqrt((1 - M3)) * sqrt((1 - M2)))) * sqrt((1 - M1)) / 4;

% PDL Inverse Matrix
M_PDL = [m11 m12 
         m21 m22];