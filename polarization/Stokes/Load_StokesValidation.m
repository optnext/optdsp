% Adaptive Stokes Validadtion

%% 
close all
clear all



%%  *******  3DStokes parameters (default -pi/4 and -1
Stk3D.NsampBit  = 1;       % Samples to be used in 3D
Stk3D.Nsamp     = 2^14; %2^10   % Samples to be considered in the pre run
Stk3D.rot       = -pi/4;
Stk3D.PreR      = 1; % 0 -> witout pre-running; 1 -> with pre-runnning;
Stk3D.Inv       = 1; %Inverts the rotation (-1)
Stk3D.muPr        = 0.005; %Step size PreRunnning
Stk3D.mu        = 0.001; %Step size Decoding

Stk3D.PDLcomp  = [0 0 0]; % PDL Compensation ?


%%  Load files
%load DP-QPSK_10Gbps_B2B_VPI
load DP-QPSK_10Gbps_B2B_LAB_1taps_1SpS
%load DP-64QAM_10Gbps_B2B_VPI

%% DownSampling
Nspsymb = 16;
spsymb  = 1;
InitSamp = 6;
EoutSampled = S.rxTrunc(:,InitSamp:Nspsymb/spsymb:end);


%% Plots
[S_Input,s_Input]=jones2stokesAvg(EoutSampled);       % Input signal

figure(1)
plot(EoutSampled(1,:),'.');
hold on
plot(EoutSampled(2,:),'.r');
hold off

figure(2) % 3D Plot
qq=plot_sphere(mean(S_Input(1,:)));hold on;
plot3(S_Input(2,:),S_Input(3,:),S_Input(4,:),'.k',...
'MarkerEdgeColor','r','MarkerSize',18)
axis equal
hold off
%% Adpative Stokes PolDemux
[EDemSampled3D,coeff]=PDStokesAdapt(EoutSampled,Stk3D); % Call the adative Stokes method




%% Plots
[S_Out3D,s_Out3D]=jones2stokesAvg(EDemSampled3D);       % Input signal

figure(3) %Constelations
plot(EDemSampled3D(1,:),'.');
hold on
plot(EDemSampled3D(2,:),'.r');
hold off


figure(4) % 3D Plot
qq=plot_sphere(mean(S_Out3D(1,:)));hold on;
plot3(S_Out3D(2,:),S_Out3D(3,:),S_Out3D(4,:),'.k',...
'MarkerEdgeColor','r','MarkerSize',18)
axis equal
hold off

figure(5) % Normal COnvergence
load SampNormH25
plot(SampNormH(1:5000,1),SampNormH(1:5000,2:4))
%%