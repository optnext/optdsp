
function a=plot_sphere(R)
% plots sphere with radious R
% NjMuga 2012
% R -> sphere radious

[Xs Ys Zs]=sphere(30);  % create data for sphere surface

hs1 = surf(R*Xs, R*Ys, R*Zs);    % create sphere with radious R
set(hs1,'EdgeColor','none', ...
    'FaceColor','green', ...
    'FaceAlpha','interp');
alpha(0.1)
camlight(45,45);
lighting phong
hidden off
%axis square
a=1;
