% Plot the Poincar� sphere and Stokes vectors
% N.J. Muga 2013
function a=plotStokes(s_Input)
% Check if Stokes is normalized
if min(size(s_Input))>3
    R = mean(s_Input(1,:));
    S =  s_Input((2:end),:);
else
    R = 1;
    S = s_Input;
end

%Plots the 3D data and Poincar� Sphere
plot_sphere(R);
hold on
plot3(S(1,:),S(2,:),S(3,:),'--ok',...
'MarkerEdgeColor','r')
grid on
xlabel('s_1'); ylabel('s_2'); zlabel('s_3')
title('Input Signal')
%axis([-1.1 1.1 -1.1 1.1 -1.1 1.1])
axis equal
%view(165,15)
hold off