function [E_3Dad,n_normal,MM] = PDStokesAdapt(Ein,Stk3D)
%%%%%%% Adaptive 3D Stokes Demultiplexing 
%

% Versio 2.0 18 Fev

%% Calculates the Normalized and non-normalized stokes vectors

[S_Out,s_Out]   = jones2stokesAvg(Ein); 

%% Initialization 
n = [1;0;0];   %  Initial normal vector for the same as CMA with 1 0
 %n = [0;1;0];   % Initial normal vector start with 0.7 
muPr = Stk3D.muPr; % '' Prerunning
mu = Stk3D.mu;     % Weight of each step



%% Vectors Alocation
n_Store = zeros(3,size(Ein,2));    % normal

SampNormH = zeros(size(Ein,2),8);  % inverse matrix 

%Eout = zeros(size(Ein)-Stk3D.Nsamp);           % Output Signal
kk=1; %counter
k=1; %counter


if Stk3D.PreR~=0 % First Run to find initial orientation of the normal (optional)   %1
    
    %% Adaptive Stokes Cicle
    %
    for i=1:Stk3D.Nsamp 
        
    
    % ---------------- Cicle for adaptive calculation of "n"-------------%
    S = s_Out(:,i);     % Stokes vector
    n_aux = n;          % old normal
    A = dot(S,n);       % scalar
    cr1 = cross(S,n);   % normal to the plane defined by "n" and "s"
    sp = cross(cr1,S);  % normal to "s" liyng in the plane defined by "n" and "s"
    G = muPr*abs(A)*sp;   % vector to be added to "n"
    erro(i)=norm(G);
    n = n_aux+G;                            % vector defining the new orientation of "n"
    n =n./sqrt((n(1)^2+n(2)^2+n(3).^2));    % new n
    
    %-------  Information to be used in PDL comp ---------------------------%
    % Calculation of the inverse matrix 
    normal = n*Stk3D.Inv;                                      % Inverts the rotation
    
    dfi = atan2(100*normal(3),100*normal(2));                  % Calculate the angle 1
    
    dalf = 1/2*atan2(sqrt(normal(2)^2+normal(3)^2),normal(1)); % Calculate the angle 2

    MM = [cos(dalf)*exp(1i*dfi/2) sin(dalf)*exp(-1i*dfi/2)     % Calculate the inverse matrix
          -sin(dalf)*exp(1i*dfi/2) cos(dalf)*exp(-1i*dfi/2)];
 
    Eout_aux(:,i) = MM*Ein(:,i); % Auxiliar Sgnal

    %%
        % ------------ Stores data ( "step" , "normal" , "abs(M11),abs(M12),...
    MMt1=MM';
    SampNormH1(kk,:) = [i,normal', abs(MMt1(:)')];
    kk=kk+1;
    
    end

%
   Eout_train=Eout_aux;
else 
end
 
%% PDL Cicle -------------------------------------
if Stk3D.PDLcomp(1) == 1  %0
% If used with Eout_aux from first run
    [S_Out_aux,s_Out_aux]  = jones2stokesAvg(Eout_aux(:,(.5*Stk3D.Nsamp:end))); %Calculates the Normalized and non-normalized stokes vectors
    m_S1 = mean(S_Out_aux(2,:));
    m_S2 = mean(S_Out_aux(3,:));
    m_S3 = mean(S_Out_aux(4,:));

% Calculates the 3 displacements in Stokes Space
    M1   = -m_S1/(2*mean(abs(Ein(:)).^2));
    M2   = m_S2/(2*mean(abs(Ein(:)).^2));
    M3   = -m_S3/(2*mean(abs(Ein(:)).^2));



display 'Central point distance to the origin'
cm      = sqrt(m_S1^2+m_S2^2+m_S3^2);
cm_norm = cm/(2*mean(abs(Ein(:)).^2));


    m11 =-((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) - (sqrt((1 + M3)) * sqrt((1 + M2))) - (sqrt((1 + M2)) * sqrt((1 - M3))) - (sqrt((1 - M2)) * sqrt((1 + M3))) - (sqrt((1 - M3)) * sqrt((1 - M2))) + (-1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (1i) * sqrt((1 - M2)) * sqrt((1 - M3))) * sqrt((1 + M1)) / 4;
    m21 = -((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) + (sqrt((1 + M3)) * sqrt((1 + M2))) + (sqrt((1 + M2)) * sqrt((1 - M3))) + (1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 - M2)) * sqrt((1 - M3)) - (sqrt((1 - M2)) * sqrt((1 + M3))) - (sqrt((1 - M3)) * sqrt((1 - M2)))) * sqrt((1 + M1)) / 4;
    m12 = ((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) - (sqrt((1 + M3)) * sqrt((1 + M2))) - (sqrt((1 + M2)) * sqrt((1 - M3))) + (sqrt((1 - M2)) * sqrt((1 + M3))) + (sqrt((1 - M3)) * sqrt((1 - M2))) + (1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 - M2)) * sqrt((1 - M3))) * sqrt((1 - M1)) / 4;
    m22 = ((1i) * sqrt((1 + M2)) * sqrt((1 + M3)) + (-1i) * sqrt((1 + M2)) * sqrt((1 - M3)) + (sqrt((1 + M3)) * sqrt((1 + M2))) + (sqrt((1 + M2)) * sqrt((1 - M3))) + (-1i) * sqrt((1 - M2)) * sqrt((1 + M3)) + (1i) * sqrt((1 - M2)) * sqrt((1 - M3)) + (sqrt((1 - M2)) * sqrt((1 + M3))) + (sqrt((1 - M3)) * sqrt((1 - M2)))) * sqrt((1 - M1)) / 4;

% PDL Inverse Matrix
M_PDL =[m11 m12
        m21 m22];
    
else;end;    
 dfitotal=1;
 dalftotal=1;
%% Adaptive Stokes Cicle (MAIN) 
%for i=Stk3D.Nsamp+1:size(Ein,2)   
for i = 1:size(Ein,2) 
    
    % ---------------- Cicle for adaptive calculation of "n"-------------%
    S = s_Out(:,i);     % Stokes vector
    n_aux = n;          % old normal
    A = dot(S,n);       % scalar
    cr1 = cross(S,n);   % normal to the plane defined by "n" and "s"
    sp = cross(cr1,S);  % normal to "s" liyng in the plane defined by "n" and "s"
    G = mu*abs(A)*sp;   % vector to be added to "n"
    n = n_aux+G;                            % vector defining the new orientation of "n"
    n = n./sqrt((n(1)^2+n(2)^2+n(3).^2));   % new n
    n_Store(:,i) = n;                       % stores 
    % -------------------------------------------------------------------%
    
    % ---------------- Calculation of the inverse matrix 
    normal = n*Stk3D.Inv;                                      % Inverts the rotation
    
    dfi = atan2(100*normal(3),100*normal(2));                  % Calculate the angle 1
   
    dalf = 1/2*atan2(sqrt(normal(2)^2+normal(3)^2),normal(1)); % Calculate the angle 2

    MM = [cos(dalf)*exp(1i*dfi/2) sin(dalf)*exp(-1i*dfi/2)     % Calculate the inverse matrix
         -sin(dalf)*exp(1i*dfi/2) cos(dalf)*exp(-1i*dfi/2)];
 
     dfitotal(i)=dfi;
     dalftotal(i)=dalf;
    % ---------------- Calculates the output field
    Eout(:,i) = MM*Ein(:,i);

    %----------------  Compensates for PDL
     if Stk3D.PDLcomp(1) == 1
        Eout(:,i) = M_PDL*Eout(:,i);
     else;end
 
    % ------------ Stores data ( "step" , "normal" , "abs(M11),abs(M12),...
    MMt=MM';
    SampNormH(k,:) = [i,normal', abs(MMt(:)')];
    k=k+1;
end


n_normal = SampNormH(:,2:4);
MM=SampNormH(:,5:8);
E_3Dad  = Eout;


