function [Sin,STOKES] = adaptStokes_PolDemux(Sin,STOKES)

% Last Update: 04/08/2017


%% Input Parameters
if isfield(STOKES,'W')
    n = STOKES.W;
else
    n = [1;0;0];   %  Initial normal vector for the same as CMA with 1 0
end

mu = STOKES.current.stepSize;
Inv = STOKES.Inv;
if isfield(STOKES,'M_PDL')
    compPDL = true;
    M_PDL = STOKES.M_PDL;
else
    compPDL = false;
end

nSamples = size(Sin,2);
n_Store = zeros(3,nSamples);    % normal
SampNormH = zeros(nSamples,8);  % inverse matrix 
[err,dfitotal,dalftotal] = deal(zeros(1,nSamples));

%% Calculates the Normalized and non-normalized stokes vectors
[~,s_Out] = jones2stokesAvg(Sin); 
 
%% Adaptive Stokes Cicle (MAIN) 
k = 1; %counter
for i = 1:nSamples
    % ---------------- Cicle for adaptive calculation of "n"-------------%
    S_vec = s_Out(:,i);     % Stokes vector
    n_aux = n;          % old normal
    A = dot(S_vec,n);       % scalar
    cr1 = cross(S_vec,n);   % normal to the plane defined by "n" and "s"
    sp = cross(cr1,S_vec);  % normal to "s" liyng in the plane defined by "n" and "s"
    G = mu*abs(A)*sp;   % vector to be added to "n"
    err(i) = norm(G);
    n = n_aux + G;                            % vector defining the new orientation of "n"
    n = n./sqrt((n(1)^2+n(2)^2+n(3).^2));   % new n
    n_Store(:,i) = n;                       % stores 
    % -------------------------------------------------------------------%
    
    % ---------------- Calculation of the inverse matrix 
    normal = n*Inv;                                      % Inverts the rotation
    
    dfi = atan2(100*normal(3),100*normal(2));                  % Calculate the angle 1
   
    dalf = 1/2*atan2(sqrt(normal(2)^2+normal(3)^2),normal(1)); % Calculate the angle 2

    MM = [cos(dalf)*exp(1i*dfi/2) sin(dalf)*exp(-1i*dfi/2)     % Calculate the inverse matrix
         -sin(dalf)*exp(1i*dfi/2) cos(dalf)*exp(-1i*dfi/2)];
 
    dfitotal(i) = dfi;
    dalftotal(i) = dalf;
    
    % ---------------- Calculates the output field
    Sout(:,i) = MM*Sin(:,i);

    %----------------  Compensates for PDL
    if compPDL
        Sout(:,i) = M_PDL*Sout(:,i);
    end
 
    % ------------ Stores data ( "step" , "normal" , "abs(M11),abs(M12),...
    MMt = MM';
    SampNormH(k,:) = [i,normal', abs(MMt(:)')];
    k = k+1;
end

%% Output Parameters
STOKES.W = n;
STOKES.n_normal = SampNormH(:,2:4);
STOKES.MM = SampNormH(:,5:8);
STOKES.err = err;
STOKES.dfitotal = dfitotal;
STOKES.dalftotal = dalftotal;

%% Debug Plots
if isfield(STOKES,'debugPlots')
    debugPlots = STOKES.debugPlots;
else
    debugPlots = {};
end
nPlots = length(debugPlots);
if nPlots
    [S_Input,s_Input] = jones2stokesAvg(Sin);
    [S_Out3D,s_Out3D] = jones2stokesAvg(Sout);
end

for n = 1:nPlots
    if any(strcmpi(debugPlots{n},{'inputSignal','inputSignal_2D','all'}))
        figure();
        plot(Sin(1,:),'.');
        hold on
        plot(Sin(2,:),'.r');
        hold off
    end
    if any(strcmpi(debugPlots{n},{'outputSignal','outputSignal_2D','all'}))
        figure();
        plot(Sout(1,:),'.');
        hold on
        plot(Sout(2,:),'.r');
        hold off
    end
    if any(strcmpi(debugPlots{n},{'inputSignal_3D','all'}))
        figure();
        plot_sphere(mean(S_Input(1,:)));%
        hold on;
%         plot3(S_Input(2,:),S_Input(3,:),S_Input(4,:),'.k',...
%         'MarkerEdgeColor','r','MarkerSize',18)
        plot3(S_Input(2,60000:end),S_Input(3,60000:end),S_Input(4,60000:end),'.k',...
        'MarkerEdgeColor','b','MarkerSize',18)
        axis equal
        hold off
        %%vitor
        SV0=sqrt(S_Input(2,60000:end).^2+S_Input(3,60000:end).^2+S_Input(4,60000:end).^2);
        SV1=S_Input(2,60000:end);
        SV2=S_Input(3,60000:end);
        SV3=S_Input(4,60000:end);
        x=[];
        for i=1:length(SV0)
         x=[x;1,SV0(i),SV1(i)'./SV0(i)',SV2(i)'./SV0(i)',SV3(i)'./SV0(i)',1];
        end
        dlmwrite(['stokes1.prn'],x)
    end
    if any(strcmpi(debugPlots{n},{'outputSignal_3D','all'}))
        figure();
        plot_sphere(mean(S_Out3D(1,:)));%
        hold on;
%            plot3(S_Out3D(2,:),S_Out3D(3,:),S_Out3D(4,:),'.k',...
%            'MarkerEdgeColor','r','MarkerSize',18)
     plot3(S_Out3D(2,60000:end),S_Out3D(3,60000:end),S_Out3D(4,60000:end),'.k',...
       'MarkerEdgeColor','r','MarkerSize',18)
        axis equal
        hold off
        %%vitor
        SA0=sqrt(S_Input(2,60000:end).^2+S_Input(3,60000:end).^2+S_Input(4,60000:end).^2);
        SA1=S_Input(2,60000:end);
        SA2=S_Input(3,60000:end);
        SA3=S_Input(4,60000:end);
        x=[];
        for i=1:length(SA0)
         x=[x;1,SA0(i),SA1(i)'./SA0(i)',SA2(i)'./SA0(i)',SA3(i)'./SA0(i)',1];
        end
%         dlmwrite(['stokes2.prn'],x)
    end
    if any(strcmpi(debugPlots{n},{'convergence','normalConvergence','all'}))
        figure();
        plot(SampNormH(1:5000,1),SampNormH(1:5000,2:4))
    end
end


