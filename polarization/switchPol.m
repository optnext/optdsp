function [Sout] = switchPol(Sin)

% Last Update: 17/04/2016


%% Switch Polarizations
Sout(1,:) = Sin(2,:);
Sout(2,:) = Sin(1,:);

