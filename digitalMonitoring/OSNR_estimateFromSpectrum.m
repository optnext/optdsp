function [OSNR] = OSNR_estimateFromSpectrum(Sin,PARAM,SIG,debugPlots)

% Last Update: 11/04/2017


%% Input Parser
fNoise = [-1 -0.9; 0.9 1]*PARAM.sampRate/2;
if ~exist('debugPlots','var')
    debugPlots = false;
end

%% Input Parameters
sampRate    = PARAM.sampRate;
symRate     = PARAM.symRate;
nSamples    = PARAM.nSamples;

nSC         = numel(SIG);
fSig        = NaN(1,nSC);
for k = 1:nSC
    fSig(k) = SIG(k).fSC;
end
rollOff     = SIG(1).rollOff;
nNoiseBins  = size(fNoise,1);

%% Calculate Spectrum
Sf = abs(fftshift(fft(Sin))).^2/nSamples;
% [Sf,f] = pwelch(Sin,round(nSamples/10),[],[],sampRate,'centered','psd');
% nSamples = numel(Sf);

%% Calculate Signal+Noise Power per Subcarrier
for n = 1:nSC
    indSig(n,1) = round(nSamples*(1/2 + ...
        (fSig(n)-symRate/2*(1+rollOff))/sampRate)) + 1;
    indSig(n,2) = round(nSamples*(1/2 + ...
        (fSig(n)+symRate/2*(1+rollOff))/sampRate)) + 1;
%     Psn_SCM(n) = mean(Sf(indSig(n,1):indSig(n,2))) * symRate*(1+rollOff);
    Psn_SCM(n) = sum(Sf(indSig(n,1):indSig(n,2)))/nSamples;
end

%% Calculate Noise Power
for n = 1:nNoiseBins
    indNoise(n,1) = round(nSamples*(1/2 + fNoise(n,1)/sampRate)) + 1;
    indNoise(n,2) = round(nSamples*(1/2 + fNoise(n,2)/sampRate));
    Pn_BW(n)    = abs(fNoise(n,1) - fNoise(n,2));
    Pn_bins(n)  = sum(Sf(indNoise(n,1):indNoise(n,2)));
end
% nSamples_Pn = sum(diff(indNoise')) + size(indNoise,1);
% Pn = sum(Pn_bins)/nSamples_Pn * sum(Pn_BW);
Pn = sum(Pn_bins)/nSamples;

%% Calculate OSNR per Subcarrier
Pn_Rs   = Pn * symRate/sum(Pn_BW);
Ps_Rs 	= Psn_SCM - Pn_Rs;
for n = 1:nSC
    SNR_dB(n)           = 10*log10(Ps_Rs(n)/Pn_Rs);
end

%% Calculate Average OSNR
Ps_SCM  = sum(Ps_Rs);
Pn_SCM  = Pn_Rs * nSC;
Bref    = lambda2freq(0.1*1e-9);

SNR_SCM_dB      = 10*log10(Ps_SCM/Pn_SCM);
OSNR_0p1nm_dB   = SNR_SCM_dB - 10*log10(Bref/symRate);

%% Output Struct
OSNR.SNR_Rs_dB      = SNR_dB;
OSNR.SNR_SCM_dB     = SNR_SCM_dB;
OSNR.OSNR_0p1nm_dB  = OSNR_0p1nm_dB;
OSNR.Ps_Rs          = Ps_Rs;
OSNR.Pn_Rs          = Pn_Rs;
OSNR.Ps_SCM         = Ps_SCM;
OSNR.Pn_SCM         = Pn_SCM;

%% Debug Plots
if debugPlots
    figure();
    f = PARAM.f * 1e-9;
    hPlot1 = plot(f,10*log10(Sf));
    hold on;
    for n = 1:nNoiseBins
        ind = indNoise(n,1):indNoise(n,2);
        hPlot2(n) = plot(f(ind'),10*log10(Sf(ind')));
    end
    
    for n = 1:nSC
        ind = indSig(n,1):indSig(n,2);
        hPlot3(n) = plot(f(ind'),10*log10(Sf(ind')));
    end

    % format:
    set(hPlot1,'Color',[0 0.24 0.431]);
    set(hPlot2,'Color',[0.73 0.07 0.169],'LineStyle',':');

    grid on;
    axis tight;
    xlabel('$f$ [GHz]','Interpreter','latex');

    legend('Signal','Noise Bins');
end
