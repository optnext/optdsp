function [ADC]=monitorADC(Ain)

% Last Update: 06/12/2013


%% Input Parameters
nPol=size(Ain,1);

%% Determine the Number of Quantization Levels
for n=1:nPol
    levels_I{n}=sort(unique(real(Ain(n,:))));
    nLevels_I(n)=length(levels_I{n});
    levels_Q{n}=sort(unique(imag(Ain(n,:))));
    nLevels_Q(n)=length(levels_Q{n});
end

%% Output Struct
ADC.levels_I=levels_I;
ADC.levels_Q=levels_Q;
ADC.nLevels_I=nLevels_I;
ADC.nLevels_Q=nLevels_Q;
