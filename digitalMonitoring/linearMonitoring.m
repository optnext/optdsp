function [accumulatedCD,FIBER2]=linearMonitoring(FIR,PARAM,FIBER,U)

% Last Update: 09/01/2014

global CONST;

%% Input Parameters
ht=FIR.W;
% nTaps=FIR.nTaps;
symRate=PARAM.symRate;
L=FIBER.L;
c=CONST.c*1e9;
lambda=CONST.lambda;
nFFT=512;

%% Calculate Fourier Transform of Filter Taps
f=linspace(-symRate,symRate,nFFT);

% Mexp=[];
% for n=1:nTaps
%     Mexp=[Mexp;exp(1j*2*pi*n*f/(2*symRate))];
% end
% 
% hf=ht*Mexp;
% 
% figure();
% plot(f*1e-9,20*log10(abs(hf)),'--','linewidth',2)
% grid
% xlabel('f [GHz]')
% axis([-symRate*1e-9 symRate*1e-9 -inf inf]);
% title('Transfer Function of the FIR Filters');

hf=FIR2TF(ht,nFFT,1);


%% Monitoring
for n=1:nFFT
    W=[hf(1,n) hf(2,n); hf(3,n) hf(4,n)];
    % CD Estimation:
    phi(n)=angle(sqrt(det(W)));
    HH(n)=abs(det(W));
    % PDL Estimation:
    T=W'*W;
    [V,A]=eig(T);
    PDL(n)=abs(10*log10(A(1,1)/A(2,2)));
    tmp=abs(sqrt(det(W)));
    PDL(n)=abs(10*log10(tmp));

    % PMD Estimation:
    Wue2=W./sqrt(det(W));
    u1(n)=Wue2(1,1);
    u2(n)=Wue2(2,2);
    v1(n)=Wue2(1,2);
    v2(n)=Wue2(2,1);
end

% u1T=U.Uxx;
% u2T=U.Uyy;
% v1T=U.Uxy;
% v2T=U.Uyx;



% Derive:
dw=(f(2)-f(1))*(2*pi);
u1w=diff(u1)/dw;
u2w=diff(u2)/dw;
v1w=diff(v1)/dw;
v2w=diff(v2)/dw;
DGD=2*sqrt(u1w.*u2w-v1w.*v2w);

% dw=PARAM.df*2*pi;
% dw=4.282917278511992e+05*2*pi;
% u1Tw=diff(u1T)/dw;
% u2Tw=diff(u2T)/dw;
% v1Tw=diff(v1T)/dw;
% v2Tw=diff(v2T)/dw;
% DGD_t=2*sqrt(u1Tw.*u2Tw-v1Tw.*v2Tw);

% meanDGD_t=mean(abs(DGD_t(ni:nf)))


nT=nFFT/8;
ni=nFFT/2-nT/2;
nf=nFFT/2+nT/2;

meanDGD=mean(abs(DGD(ni:nf)))


figure();
plot(abs(DGD));
hold on
plot(ni:nf,abs(DGD(ni:nf)),'--r');

phi2=unwrap(phi'*2)/2;

for n=1:nFFT
    W=[hf(1,n) hf(2,n); hf(3,n) hf(4,n)];
    
    T=W'*W;
    [V,A]=eig(T);

    % DGD Estimation
    Pinv=zeros(2,2);
    Pinv(1,1)=1./sqrt(A(1,1));
    Pinv(2,2)=1./sqrt(A(2,2));
    Pinv=V*Pinv*inv(V);
    U=W*Pinv*exp(-1j*phi2(n));

    Wue(n,1)=U(1,1);
    Wue(n,2)=U(1,2);
    Wue(n,3)=U(2,1);
    Wue(n,4)=U(2,2);
end


% Derivative of DGD matrix:
dWue=zeros(size(Wue,1)-1,size(Wue,2));
dWue(:,1)=diff(Wue(:,1))/(f(2)-f(1))/2/pi;
dWue(:,2)=diff(Wue(:,2))/(f(2)-f(1))/2/pi;
dWue(:,3)=diff(Wue(:,3))/(f(2)-f(1))/2/pi;
dWue(:,4)=diff(Wue(:,4))/(f(2)-f(1))/2/pi;

% DGD evaluation
for i=1:size(dWue,1)
    A=det([dWue(i,1) dWue(i,2);dWue(i,3) dWue(i,4)]);
    tau(i)=2*sqrt(A);
end


dgd_norm=mean(abs(tau(ni:nf)))

figure();
plot(abs(tau));
hold on;
plot(ni:nf,abs(tau(ni:nf)),'--r');


%% Plot CD Graphs
phi2=unwrap(phi'*2)/2;
CD=(phi2+flipud(phi2))/2;
offset=150;

f_quadFit=f(1+offset:end-offset);
y=CD(1+offset:end-offset)-max(CD);
quadFit=polyfit(f_quadFit'*1e-9,y,2)*1e-18;
CD_quadFit=polyval(quadFit,f_quadFit);

beta2=quadFit(1)/(2*pi^2*L);
D=-2*pi*c*beta2/lambda^2*1e12;
accumulatedCD=D*L;


figure();
CDplot=CD-CD(ceil(nFFT/2));
plot(f*1e-9,CDplot,'r','linewidth',2)
hold on
plot(f_quadFit*1e-9,CD_quadFit,'--k','linewidth',2);
grid
ylabel('2\pi^2\beta_2 f^2 L');
xlabel('f [GHz]');
text(f(350)*1e-9,max(CDplot)-0.1*(max(CDplot)-min(CDplot)),...
    ['CD_{res}:',num2str(accumulatedCD),' ps/nm'],...
    'BackgroundColor',[.7 .9 .7]);
axis([-symRate*1e-9 symRate*1e-9 -inf inf])

% figure();
% plot(f*1e-9,10*log10(HH/max(HH)),'r','linewidth',2)
% grid
% title('Transfer function of FIR filters')
% xlabel('f [GHz]')
% ylabel('|H(f)|^2 [dB]')
% axis([-symRate*1e-9 symRate*1e-9 -20 0]);


%% Plot PDL
pdl_dB=10*log10(mean(10.^(PDL(1+floor(nFFT/4):end-floor(nFFT/4))/10)));
figure
plot(f*1e-9,PDL,'linewidth',2)
text(f(350)*1e-9,max(PDL)-0.1*(max(PDL)-min(PDL)),...
    strcat('PDL = ',num2str(pdl_dB,'%3.2f'),' dB'),'fontsize',12)
grid
xlabel('f [GHz]')
ylabel('PDL [dB]')
axis([-symRate*1e-9 symRate*1e-9 -inf inf]);

%% Output Struct
FIBER2.D=FIBER.D-D;
FIBER2.beta2=-FIBER2.D*lambda^2/(2*pi*c);





