function [P] = measurePowerFreq_nBins(S,PARAM,dfBin)

% Last Update: 21/01/2017


%% Input Parameters
nSig = size(S,1);
f = PARAM.f;
nBins = size(dfBin,1);

%% Fourier Transform
for n = 1:nSig
    S(n,:) = fftshift(fft(S(n,:)));
end

%% Measure Power per Frequency Bin
P = zeros(nSig,nBins);
for n = 1:nSig
    for k = 1:nBins
        ind = (f>=dfBin(k,1)) & (f<=dfBin(k,end));
        P(n,k) = pow2db(mean(abs(S(n,ind).^2)));
    end
end
