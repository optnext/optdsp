function [Hw] = FIR2TF(W,nFFT,plotOption)

% Last Update: 18/02/2016


%% Input Parser
if ~exist('plotOption','var')
    plotOption = false;
end

%% Input Parameters
[nFilters,nTaps] = size(W);
Hw = zeros(nFilters,nFFT);

%% Calculate Transfer Function from FIR Taps
for n = 1:nFilters
    if mod(nTaps,2)
        tapsLeft    = W(n,1:floor(nTaps/2));
        centralTap  = W(n,ceil(nTaps/2));
        tapsRight   = W(n,ceil(nTaps/2)+1:end);
    end

    Ht(1) = centralTap;
    Ht(nFFT-floor(nTaps/2)+1:nFFT) = tapsLeft;
    Ht(2:ceil(nTaps/2)) = tapsRight;

    Hw(n,:) = ifftshift(fft(Ht));
end

%% Plot
if plotOption
    figure();
    plot(1:nFFT,10*log10(abs(Hw)),'linewidth',2);
    grid on;
    title('Transfer Function of the FIR Filters');
end

