function [Sout,WSS] = WSS_filter(Sin,f,WSS)
%WSS_filter     Implements a WSS optical filter
%   This function designs a WSS optical filter and then filters the input
%   signal accordingly. The analytical formulation adopted for the WSS
%   filter can be found in:
%   Cibby Pulikkaseril et al, "Spectral modeling of channel band shapes in 
%   wavelength selective switches," Opt. Express, vol. 19, no. 9, 2011.
%   https://doi.org/10.1364/OE.19.008458
%
%   INPUTS:
%   Sin     := input optical signal [nPol x nSamples]
%   f       := vector of frequencies [Hz]
%   WSS     := struct of required WSS parameters
%           WSS.B           := bandwith of the frequency slot in which the 
%                              WSS is to be placed [Hz]
%           WSS.Bw_OTF      := 3-dB bandwidth of the optical transfer 
%                              function [Hz]
%           WSS.f0          := central frequency of the WSS filter [Hz]
%                              (optional, default is WSS.f0 = 0)
%           WSS.nCascaded   := number of cascaded WSS filters (optional,
%                              default is WSS.nCascaded = 1). Note that
%                              WSS.nCascaded = 0 corresponds to switching
%                              off the WSS filter
%           WSS.debugPlots  := flag to activate the debug plots of the WSS 
%                              filter function (shows the output spectrum)
%                              (optional, default is WSS.debugPlots=false)
%
%   Alternatively, WSS_filter also accepts f to be parsed as field of a
%   PARAM struct:
%   f = PARAM.f;
%
%   OUTPUTS:
%   Sout    := output optical signal [nPol x nSamples]
%   WSS     := struct of WSS parameters, updated with field "Hf",
%              corresponding to the synthetized WSS transfer function
%
%
%   Author: Fernando Guiomar
%   Last Update: 31/08/2017

%% Input Parameters
% Signal Parameters:
if isstruct(f)
    f = f.f;
end
% WSS Parameters:
B = WSS.B;
Bw_OTF = WSS.Bw_OTF;
if isfield(WSS,'f0')
    f0 = WSS.f0;
else
    f0 = 0;
end
if ~isfield(WSS,'nCascaded')
    WSS.nCascaded = 1;
end
n = WSS.nCascaded;
sigma = Bw_OTF/(2*sqrt(2*log(2)));

%% Create WSS Transfer Function in Freq-Domain
Hf = 0.5 * sigma * sqrt(2*pi) * (erf((B/2-f+f0)/(sqrt(2)*sigma)) - ...
    erf((-B/2-f+f0)/(sqrt(2)*sigma)));
% Normalize Hf:
Hf = Hf./max(Hf);
% Cascaded WSS:
Hf = Hf.^n;
% Center Hf:
% Hf = fftshift(Hf);

%% Add Group Delay Ripple
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Under development               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nSamples = length(Sin);
% df = PARAM.sampRate;
% GDR = 4*1e-12 * rand(1,nSamples);
% phi = pi/180 * df * movsum(GDR,2)/2;
% 
% Hf_phi = exp(1j*2*pi*phi);
% Hf = Hf .* Hf_phi;
% 
% h = TF2FIR(Hf,10001,true);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Under development               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Apply WSS
for n = 1:size(Sin,1)
    Sout(n,:) = ifft(ifftshift(Hf.*fftshift(fft(Sin(n,:)))));
end

%% Output WSS
WSS.Hf = Hf;

%% Debug Plots
if WSS.debugPlots
    figure();
    Fs = 2*max(f);
    [PSD_in,F] = pwelch(Sin(1,:),1e4,[],[],Fs,'centered','psd');
    [PSD_out,F] = pwelch(Sout(1,:),1e4,[],[],Fs,'centered','psd');
    hPlot(1) = plot(F*1e-9,10*log10(PSD_in/max(PSD_in)));
    hold on;
    hPlot(2) = plot(F*1e-9,10*log10(PSD_out/max(PSD_in)));
    hPlot(3) = plot(f*1e-9,10*log10(abs(Hf).^2));
    
    minF = min(F*1e-9);
    maxF = max(F*1e-9);
    set(gca,'xtick',[fliplr(0:-12.5:minF) 12.5:12.5:maxF]);
    grid on;
    axis([-inf inf -30 5]);
    xlabel('$f$ [GHz]','Interpreter','latex');
    ylabel('Normalized PSD','Interpreter','latex');
    legend('Input Signal','Output Signal','WSS Filter','Location','best')
end
