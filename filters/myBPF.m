function [Sout,BPF] = myBPF(Sin,BPF,Fs)

% Last Update: 10/04/2017


%% Input Parameters
nSamples = size(Sin,2);

%% Rx Signal Filtering
switch BPF.type
    case 'Gaussian'
        BW = floor(BPF.fc*nSamples/Fs);
        f0 = floor(nSamples/2)-BPF.F0/(Fs/2)*nSamples/2;
        TF = exp(-log(sqrt(2)).*(([1:nSamples]-f0)./BW).^(2*BPF.order));
        for n = 1:size(Sin,1)
            Sout(n,:) = ifft(ifftshift(TF.*fftshift(fft(Sin(n,:)))));
        end
    case 'none'
        TF = ones(1,nSamples);
        Sout = Sin;
    otherwise
        error('Invalid Rx Filter!');
end

%% Output LPF Struct
BPF.TF = TF;

