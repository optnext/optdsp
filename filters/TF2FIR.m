function h = TF2FIR(Hw,nTaps,DEBUG)

% Last Update: 27/07/2017


%% Input Parser
if nargin == 2
    DEBUG = false;
end

%% Input Parameters
nFFT = length(Hw);

%% Get FIR taps from Frequency-Domain Transfer Function
Ht = ifft(fftshift(Hw));
centralTap = Ht(1);
tapsLeft = Ht(nFFT-floor(nTaps/2)+1:nFFT);
tapsRight = fliplr(tapsLeft);
if mod(nTaps,2)
    h = [tapsLeft centralTap tapsRight];
else
    h = [tapsLeft(2:end) centralTap tapsRight];
end

%% Plot
if DEBUG
    figure();
    plot(1:nTaps,10*log10(abs(h)),'-ob');
end
