function [Sout,LPF] = myLPF(Sin,LPF,PARAM,SIG)

% Last Update: 01/08/2017


%% Input Parser
if isstruct(PARAM) && isfield(PARAM,'sampRate')
    Fs = PARAM.sampRate;
elseif isnumeric(PARAM)
    Fs = PARAM;
end
if isfield(LPF,'fc')
    fc = LPF.fc;
elseif ~isfield(LPF,'fc') && isfield(LPF,'fcn')
    fc = LPF.fcn*Fs;
    LPF.fc = fc;
end
if ~isfield(LPF,'F0')
    LPF.F0 = 0;
end

%% Input Parameters
nSamples = size(Sin,2);

%% Rx Signal Filtering
switch LPF.type
    case 'user-defined'
        f = (-nSamples/2:nSamples/2-1)*(Fs/nSamples);
        LPF.TF = sqrt(10.^(LPF.TF_dB/10));
        TF = interp1(LPF.f,LPF.TF,f,'linear','extrap');
        if isfield(LPF,'stopband')
            TF(abs(f) > LPF.stopband) = 0;
        end
        Sout = zeros(size(Sin,1),nSamples);
        for n = 1:size(Sin,1)
            Sout(n,:) = ifft(ifftshift(TF.*fftshift(fft(Sin(n,:)))));
        end
        
    case 'Butter'
        f = (-nSamples/2:nSamples/2-1)*(Fs/nSamples);
        [B,A] = butter(LPF.order,fc/(Fs/2));
        Sout = zeros(size(Sin,1),nSamples);
        for n = 1:size(Sin,1)
            Sout(n,:) = filter(B,A,Sin(n,:));
        end
        TF = freqz(B,A,2*pi*f/Fs);
        
    case 'ButterAnalog'
        f = (-nSamples/2:nSamples/2-1)*(Fs/nSamples);
        [B,A] = butter(LPF.order,fc,'s');
        s = 1j*f;
        h = polyval(B,s)./polyval(A,s);
        TF = (h/max(h));
        Sout = zeros(size(Sin,1),nSamples);
        for n = 1:size(Sin,1)
            Sout(n,:) = ifft(ifftshift(TF.*fftshift(fft(Sin(n,:)))));
        end
        
    case 'Bessel'
        f = (-nSamples/2:nSamples/2-1)*(Fs/nSamples);
        [B,A] = besself(LPF.order,fc*2);
        s = 1j*f;
        h = polyval(B,s)./polyval(A,s);
        TF = h/max(h);
        Sout = zeros(size(Sin,1),nSamples);
        for n = 1:size(Sin,1)
            Sout(n,:) = ifft(ifftshift(TF.*fftshift(fft(Sin(n,:)))));
        end
        
    case 'Gaussian'
        BW = floor(fc*nSamples/Fs);
        f0 = floor(nSamples/2)-LPF.F0/(Fs/2)*nSamples/2;
        TF = exp(-log(sqrt(2)).*(([1:nSamples]-f0)./BW).^(2*LPF.order));
        for n = 1:size(Sin,1)
            Sout(n,:) = ifft(ifftshift(TF.*fftshift(fft(Sin(n,:)))));
        end
        
    case 'Rect'
        f = (-nSamples/2:nSamples/2-1)*(Fs/nSamples);
        TF = zeros(1,nSamples);
        TF((f>-fc) & (f<fc))=1;
        Sout = zeros(size(Sin,1),nSamples);
        for n = 1:size(Sin,1)
            Sout(n,:) = ifft(ifftshift(TF.*fftshift(fft(Sin(n,:)))));
        end
        
    case {'raisedCosine','raised-cosine','Raised Cosine','raised cosine','raised-cos','raisedCos'}
        f = (-nSamples/2:nSamples/2-1)*(Fs/nSamples);
        W = PARAM.symRate/2;
        a = LPF.rollOff;
        TF(abs(f)<=(1-a)*W) = 1;
        f2 = f(abs(f)>=(1-a)*W & abs(f)<=(1+a)*W);
        TF(abs(f)>(1-a)*W & abs(f)<(1+a)*W)=0.5*(1-sin(pi*(abs(f2)-W)/(2*a*W)));
        TF(abs(f)>=(1+a)*W)=0;
        Sout = ifft(ifftshift(TF.*fftshift(fft(Sin))));
    
    case {'RRC','root-raised-cosine'}
        if nargin == 4
            if isfield(SIG,'nSpS')
                nSpS_in = SIG.nSpS;
                nSpS = round(nSpS_in);
            elseif isfield(SIG,'symRate')
                nSpS_in = Fs/SIG.symRate;
                nSpS = round(nSpS_in);
            end
            if abs(nSpS - nSpS_in) > 1e-3
                nSpS = ceil(nSpS_in);
                Fs_out = nSpS * SIG.symRate;
                Sin = applyResample(Sin,PARAM.sampRate,Fs_out);
            end
        elseif isstruct(PARAM) 
            if isfield(PARAM,'nSpS')
                nSpS = round(PARAM.nSpS);
                if abs(nSpS - PARAM.nSpS) > 1e-3
                    warning('Non-integer number of samples per symbol before RRC filter. This may create some implementation penalty.');
                end
            end
        elseif isnumeric(PARAM)
            nSpS = round(PARAM);
            if nSpS > 1e6
                error('Specified number of samples per symbol is too high. Probably you are parsing the sampling-rate instead.');
            end
            if abs(nSpS - PARAM) > 1e-3
                warning('Non-integer number of samples per symbol before RRC filter. This may create some implementation penalty.');
            end
        end
        if isfield(LPF,'nTaps')
            nTaps = LPF.nTaps;
        else
            nTaps = 64 * nSpS;
        end
        W = rcosdesign(LPF.rollOff, nTaps/nSpS, nSpS, 'sqrt');
%         W = W/max(abs(W));
        W = W/sum(W);   % to guarantee unity gain at DC
        for n = 1:size(Sin,1)
            Sout(n,:) = conv(Sin(n,:),W,'same');
        end
        TF = fft(W);
        if nargin == 4
            if abs(nSpS - SIG.nSpS) > 1e-3
                Sout = applyResample(Sout,Fs_out,PARAM.sampRate);
            end
        end
        
    case 'none'
        TF = ones(1,nSamples);
        Sout = Sin;
    otherwise
        error('Invalid LPF type!');
end

%% Output LPF Struct
LPF.TF = TF;

