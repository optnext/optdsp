function [OUT,S,DSP,PARAMrxFinal] = rxDSP_MSC_OB2B_PhotonLAB_Dpre(S,PARAMrx,...
    WDM,DSP)

% Last Update: 23/02/2017

% Warning: Old version, needs update to work properly!

%% Entrance Message
entranceMsg('DSP for receiver-side SCM processing',0)
tStart = tic;

%% Pre-DSP: Signal Preparation
[S.rx,PARAMrx] = preDSP_signalPreparation(S.rx,PARAMrx,DSP);

%% Rx Front-End Correction
[S.rxFEC,PARAMrxFEC,DSP.DC] = RX_frontEndCorrection(S.rx,PARAMrx,DSP);

%% Subcarrier Demux
[S.rxSCM,PARAMrxSCM,DSP.RESAMP2] = SCM_demux(S.rxFEC,PARAMrxFEC,WDM,DSP);

TRUNC.firstSample = 1e3;
for n = 1:WDM.nSubCarriers
    A{n} = S.rxSCM{n};
    TRUNC.nSamples = length(A{n}) - 2*TRUNC.firstSample;
    S.rxSCM{n} = truncateSignal(S.rxSCM{n},PARAMrxSCM(n),TRUNC);
end

%% Remove CD Pre-Distortion
DSP.preCD.accCD = -1*DSP.preCD.accCD;
if strcmpi(DSP.MIMO1(1).method,'CMA:4x2')
    DSP.preCD.IQ = 'ReIm';
end
for n = 1:WDM.nSubCarriers
    S.rxDpre{n} = applyCD_preDistortion(A{n},PARAMrxSCM(n),DSP.preCD);
    [S.rxDpre{n},PARAMrxSCM(n)] = truncateSignal(S.rxDpre{n},PARAMrxSCM(n),TRUNC);
end

%% 1st Adaptive Equalization (for coefficient estimation)
[S.rxMIMO1,PARAMrxMIMO1,MIMO1] = SCM_adaptiveEq(S.rxDpre,S.tx,...
    PARAMrxSCM,WDM,DSP,1);

%% Estimate and Store Frequency Deviation
[S.rxFE,DSP.FE] = SCM_freqEstimation(S.rxMIMO1,S.tx,PARAMrxMIMO1,WDM,DSP);
df = DSP.FE.df;

%% Carrier-Phase Estimation (1st stage)
kRef = DSP.CPE1{1}(1).refCarrier;
[S.rxCPE1,PARAMrxCPE1,DSP.CPE1] = SCM_phaseEstimation(S.rxFE,S.tx,PARAMrxMIMO1,WDM,DSP,1);
phi = rectpulse(DSP.CPE1{1}(kRef).phi',2)';

%% Apply 1st Adaptive Equalization Estimated Before (static)
DSP.MIMO1 = rmfield(DSP.MIMO1,'train');
for n = 1:WDM.nSubCarriers
    DSP.MIMO1(n).demux.updateRate = 0;
    DSP.MIMO1(n).W = MIMO1{n}.W;
end
[S.rxMIMO1,PARAMrxMIMO1,DSP.MIMO1] = SCM_adaptiveEq(S.rxSCM,S.tx,...
    PARAMrxSCM,WDM,DSP,1);

%% Apply Frequency-Mismatch Compensation Estimated Before
% kRef = DSP.FE.refCarrier;
t = PARAMrxMIMO1.t;
dt = PARAMrxMIMO1.dt;
if size(df,2) == 1
    for n = 1:size(df,1)
        phi_t(n,:) = 2*pi*df(n)*t;
    end
else
    phi_t = 2*pi*cumsum(df,2)*dt;
end
for k = 1:numel(WDM.subCarriersDSP)
    S.rxFE{k} = S.rxMIMO1{k}.*exp(-1j*repmat(phi_t,...
        size(S.rxMIMO1{k},1)/size(phi_t,1),1));
end

%% Apply Carrier-Phase Recovery Estimated Before (1st stage)
for k = 1:numel(WDM.subCarriersDSP)
    S.rxCPE1{k} = S.rxFE{k}.*exp(-1j*phi);
end

%% Remove CD Pre-Distortion (4x2)
DSP.preCD.IQ = 'ReIm';
for k = 1:numel(WDM.subCarriersDSP)
    S.rxCPE1{k} = applyCD_preDistortion(S.rxCPE1{k},PARAMrxMIMO1(k),DSP.preCD);
end

%% 3rd Adaptive Equalization (4x2)
[S.rxMIMO3,PARAMrxMIMO3,DSP.MIMO3] = SCM_adaptiveEq(S.rxCPE1,S.tx,...
    PARAMrxCPE1,WDM,DSP,3);

%% Downsampling
[S.rx1sp,PARAMrx1sp,DSP.DOWNSAMP] = SCM_downSample(S.rxMIMO3,...
    PARAMrxMIMO3,WDM,DSP);

%% Carrier-Phase Estimation (2nd stage)
[S.rxCPE2,PARAMrxCPE2,DSP.CPE2] = SCM_phaseEstimation(S.rx1sp,...
    S.tx,PARAMrx1sp,WDM,DSP,2);

%% Post-DSP Signal Preparation
[S.rxFinal,PARAMrxFinal] = SCM_postDSP(S.rxCPE2,PARAMrxCPE2,WDM,DSP);

%% Rx Decoder
[DSP.DECODER,S.rxFinal] = SCM_rxDECODER(S.rxFinal,S.tx,PARAMrxFinal,...
    WDM,DSP);

%% Performance Analyzer
OUT = SCM_performanceAnalyzer(S.rxFinal,S.tx,PARAMrxFinal,WDM,DSP);

%% Calculate Average Power per Subcarrier
for n = 1:numel(WDM.subCarriersDSP)
    OUT.meanP(n) = pow2db((mean(abs(S.rxSCM{n}(1,:)).^2)*1e3 + ...
        mean(abs(S.rxSCM{n}(1,:)).^2)*1e3)/2);
end

%% Exit Message
myMessages(['\nDSP for SCM finished - Elapsed Time: ',...
    num2str(toc(tStart),'%2.4f\n'),' [s]'],0);
