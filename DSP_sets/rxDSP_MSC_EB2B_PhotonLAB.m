function [OUT,S,DSP,PARAMrxFinal] = rxDSP_MSC_EB2B_PhotonLAB(S,PARAMrx,...
    WDM,DSP)

% Last Update: 21/11/2016

% Warning: Old version, needs update to work properly!

%% Pre-DSP: Signal Preparation
[S.rx,PARAMrx] = preDSP_signalPreparation(S.rx,PARAMrx,DSP);

%% Rx Front-End Correction
[S.rxFEC,PARAMrxFEC,DC] = RX_frontEndCorrection(S.rx,PARAMrx,DSP);

%% Subcarrier Demux
if iscell(DC)
    DSP.RESAMP2.LPF.F0 = -DC{1}.df;
end
[S.rxSCM,PARAMrxSCM] = SCM_demux(S.rxFEC,PARAMrxFEC,WDM,DSP);

%% Adaptive Equalization
[S.rxMIMO,PARAMrxMIMO,DSP.MIMO1] = SCM_adaptiveEq(S.rxSCM,S.tx,...
    PARAMrxSCM,WDM,DSP,1);

%% Downsampling
[S.rx1sp,PARAMrx1sp,DSP.DOWNSAMP] = SCM_downSample(S.rxMIMO,...
    PARAMrxMIMO,WDM,DSP);

%% Carrier-Phase Estimation
[S.rxCPE,PARAMrxCPE,DSP.CPE] = SCM_phaseEstimation(S.rx1sp,...
    S.tx,PARAMrx1sp,WDM,DSP,1);

%% Post-DSP Signal Preparation
[S.rxFinal,PARAMrxFinal] = SCM_postDSP(S.rxCPE,PARAMrxCPE,WDM,DSP);

%% Rx Decoder
[DSP.DECODER,S.rxFinal] = SCM_rxDECODER(S.rxFinal,S.tx,PARAMrxFinal,WDM,DSP);

%% Performance Analyzer
OUT = SCM_performanceAnalyzer(S.rxFinal,S.tx,PARAMrxFinal,WDM,DSP);

%% Calculate Average Power per Subcarrier
for n = 1:numel(WDM.subCarriersDSP)
    OUT.meanP(n) = pow2db((mean(abs(S.rxSCM{n}(1,:)).^2)*1e3 + ...
        mean(abs(S.rxSCM{n}(1,:)).^2)*1e3)/2);
end

