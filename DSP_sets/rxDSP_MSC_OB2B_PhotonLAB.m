function [RES,Srx,SIG,PARAM,DSP] = rxDSP_MSC_OB2B_PhotonLAB(Srx,Stx,...
    PARAM,SIG,QAM,DSP)

% Last Update: 11/04/2017


%% Entrance Message
entranceMsg('DSP for receiver-side SCM processing',0)
tStart = tic;

%% Pre-DSP: Signal Preparation
[Srx,PARAM] = preDSP_signalPreparation(Srx,PARAM,DSP);

%% Rx Front-End Correction
[Srx,PARAM,DSP.DC] = RX_frontEndCorrection(Srx,PARAM,DSP);

%% Subcarrier Demux
[Srx,PARAM,SIG,meanP] = SCM_demux(Srx,PARAM,SIG,DSP);

%% 1st Adaptive Equalization
[Srx,PARAM,DSP.MIMO1] = SCM_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% Frequency-Mismatch Compensation
[Srx,DSP.FE] = SCM_freqEstimation(Srx,Stx,PARAM,SIG,DSP);

%% 2nd Adaptive Equalization
[Srx,PARAM,DSP.MIMO2] = SCM_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,2);

%% Carrier-Phase Estimation (1st stage)
[Srx,PARAM,DSP.CPE1] = SCM_phaseEstimation(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% 3rd Adaptive Equalization
[Srx,PARAM,DSP.MIMO3] = SCM_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,3);

%% Downsampling
[Srx,PARAM,SIG] = SCM_downSample(Srx,PARAM,SIG,1);

%% Carrier-Phase Estimation (2nd stage)
[Srx,PARAM,DSP.CPE2] = SCM_phaseEstimation(Srx,Stx,PARAM,SIG,QAM,DSP,2);

%% 4th Adaptive Equalization
[Srx,PARAM,DSP.MIMO4] = SCM_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,4);

%% Post-DSP Signal Preparation
[Srx,PARAM] = SCM_postDSP(Srx,PARAM,DSP);

%% Rx Decoder
[DSP.DECODER,Srx] = SCM_rxDECODER(Srx,Stx,QAM,DSP);

%% Performance Analyzer
RES = SCM_performanceAnalyzer(Srx,Stx,QAM,SIG,DSP);
RES.meanP = meanP;

%% Exit Messages
myMessages(['\nDSP for SCM finished - Elapsed Time: ',...
    num2str(toc(tStart),'%2.4f\n'),' [s]'],0);
