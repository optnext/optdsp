function [RES,Srx,SIG,PARAM,DSP] = rxDSP_SC_OB2B_PhotonLAB(Srx,Stx,...
    PARAM,SIG,QAM,DSP)

% Last Update: 17/07/2017


%% Entrance Message
entranceMsg('DSP for receiver-side single-carrier processing',0)
tStart = tic;

%% Pre-DSP: Signal Preparation
[Srx,PARAM] = preDSP_signalPreparation(Srx,PARAM,DSP);

%% Rx Front-End Correction
[Srx,PARAM,DSP.DC] = RX_frontEndCorrection(Srx,PARAM,DSP);

%% Resampling and Matched Filtering
[Srx,PARAM,SIG] = SC_matchedFilter(Srx,PARAM,SIG,DSP);

%% 1st Adaptive Equalization
[Srx,PARAM,DSP.MIMO1] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% Frequency-Mismatch Compensation
[Srx,DSP.FE] = SC_freqEstimation(Srx,Stx,PARAM,SIG,DSP);

%% 2nd Adaptive Equalization
[Srx,PARAM,DSP.MIMO2] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,2);

%% Carrier-Phase Estimation (1st stage)
[Srx,PARAM,DSP.CPE1] = SC_phaseEstimation(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% 3rd Adaptive Equalization
[Srx,PARAM,DSP.MIMO3] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,3);

%% Downsampling
[Srx,PARAM,SIG] = SC_downSample(Srx,PARAM,SIG,1);

%% Carrier-Phase Estimation (2nd stage)
[Srx,PARAM,DSP.CPE2] = SC_phaseEstimation(Srx,Stx,PARAM,SIG,QAM,DSP,2);

%% 4th Adaptive Equalization
[Srx,PARAM,DSP.MIMO4] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,4);

%% Post-DSP Signal Preparation
[Srx,PARAM] = SC_postDSP(Srx,PARAM,DSP);

%% Rx Decoder
[DSP.DECODER,Srx] = SC_rxDECODER(Srx,Stx,QAM,DSP);

%% Performance Analyzer
RES = SC_performanceAnalyzer(Srx,Stx,QAM,SIG,DSP);

%% Exit Messages
myMessages(['\nDSP for Single-Carrier finished - Elapsed Time: ',...
    num2str(toc(tStart),'%2.4f\n'),' [s]'],0);


