function [RES,Srx,SIG,PARAM,DSP] = rxDSP_SC_B2B_ideal(Srx,Stx,...
    PARAM,SIG,QAM,DSP)

% Last Update: 17/07/2017


%% Entrance Message
entranceMsg('DSP for receiver-side single-carrier processing',0)
tStart = tic;

%% Truncate Signal (if needed)
if isfield(DSP,'TRUNC_preDSP')
    [Srx,PARAM] = truncateSignal(Srx,PARAM,DSP.TRUNC_preDSP);
end

%% Resampling and Matched Filtering
[Srx,PARAM,SIG] = SC_matchedFilter(Srx,PARAM,SIG,DSP);

%% Adaptive Equalization
[Srx,PARAM,DSP.MIMO1] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% Frequency-Mismatch Compensation
[Srx,DSP.FE] = SC_freqEstimation(Srx,Stx,PARAM,SIG,DSP);

%% Carrier-Phase Estimation
[Srx,PARAM,DSP.CPE1] = SC_phaseEstimation(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% Downsampling
[Srx,PARAM,SIG] = SC_downSample(Srx,PARAM,SIG,1);

%% Post-DSP Signal Preparation
[Srx,PARAM] = SC_postDSP(Srx,PARAM,DSP);

%% Rx Decoder
[DSP.DECODER,Srx] = SC_rxDECODER(Srx,Stx,QAM,DSP);

%% Performance Analyzer
RES = SC_performanceAnalyzer(Srx,Stx,QAM,SIG,DSP);

%% Exit Messages
myMessages(['\nDSP for Single-Carrier finished - Elapsed Time: ',...
    num2str(toc(tStart),'%2.4f\n'),' [s]'],0);


