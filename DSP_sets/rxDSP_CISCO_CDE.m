function [RES,Srx,SIG,PARAM,DSP] = rxDSP_CISCO_CDE(Srx,Stx,PARAM,SIG,...
    QAM,DSP,Dacc,LO)

% Last Update: 07/05/2017


%% Entrance Message
entranceMsg('DSP for receiver-side processing',0)
tStart = tic;

%% Compensate Chromatic Dispersion
Srx = applyCD(Srx,PARAM,-Dacc);

%% Ideally Compensate for LO
LO.sign = -1;
LO.debug = false;
Srx = applyLO(Srx,PARAM,LO);

%% Matched Filtering
Srx = myLPF(Srx,DSP.LPF,PARAM,SIG);

%% Digital Resampling
Fs_out = DSP.RESAMP.nSpS * SIG.symRate;
[Srx,PARAM] = applyResample(Srx,PARAM.sampRate,Fs_out);
SIG = setSignalParams(SIG,PARAM);

%% Adaptive Equalization
[Srx,PARAM,DSP.MIMO1] = SC_adaptiveEq(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% Re-insert LO
LO.sign = 1;
LO.debug = false;
Srx = applyLO(Srx,PARAM,LO);

%% Frequency-Mismatch Compensation
[Srx,DSP.FE] = SC_freqEstimation(Srx,Stx,PARAM,SIG,DSP);

%% Carrier-Phase Estimation
[Srx,PARAM,DSP.CPE1] = SC_phaseEstimation(Srx,Stx,PARAM,SIG,QAM,DSP,1);

%% Downsampling
[Srx,PARAM] = downsample_1sps(Srx,SIG.nSpS,PARAM,1);
SIG = setSignalParams(SIG,PARAM);

%% Post-DSP Signal Preparation
[Srx,PARAM] = rmvEdgeSamples(Srx,PARAM,DSP.TRUNC_postDSP);

%% Rx Decoder
[DSP.DECODER,Srx] = symDecoder(Srx,Stx,QAM,DSP.DECODER);

%% Performance Analyzer
RES = SC_performanceAnalyzer(Srx,Stx,QAM,SIG,DSP);

%% Exit Messages
myMessages(['\nDSP finished - Elapsed Time: ',...
    num2str(toc(tStart),'%2.4f\n'),' [s]'],0);
