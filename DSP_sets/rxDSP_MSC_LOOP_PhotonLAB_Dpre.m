function [OUT,S,DSP,PARAMrxFinal] = rxDSP_MSC_LOOP_PhotonLAB_Dpre(S,PARAMrx,...
    WDM,DSP,FIBER)

% Last Update: 23/02/2017

% Warning: Old version, needs update to work properly!

%% Entrance Message
entranceMsg('DSP for receiver-side SCM processing',0)
tStart = tic;

%% Pre-DSP: Signal Preparation
[S.rx,PARAMrx] = preDSP_signalPreparation(S.rx,PARAMrx,DSP);

%% Rx Front-End Correction
[S.rxFEC,PARAMrxFEC,DSP.DC] = RX_frontEndCorrection(S.rx,PARAMrx,DSP);

%% Static Equalization (Full Bandwidth)
if isfield(DSP,'EQ1')
    PARAMrxEq1 = PARAMrxFEC;
    [S.rxEq1,PARAMrxEq1(1),DSP.EQ1] = staticEq(S.rxFEC,PARAMrxFEC(1),DSP.EQ1,FIBER);
    % Update Params in Case of FDHMF
    if numel(PARAMrx) > 1
        for n = 2:numel(PARAMrx)
            PARAMrxEq1(n).nBits = PARAMrxEq1(1).nBits;
            PARAMrxEq1(n).nSyms = PARAMrxEq1(1).nSyms;
            PARAMrxEq1(n).nSamples = PARAMrxEq1(1).nSamples;
            PARAMrxEq1(n).tWindow = PARAMrxEq1(1).tWindow;
            PARAMrxEq1(n).firstSample = PARAMrxEq1(1).firstSample;
            PARAMrxEq1(n).lastSample = PARAMrxEq1(1).lastSample;
            PARAMrxEq1(n).firstSym = PARAMrxEq1(1).firstSym;
            PARAMrxEq1(n).lastSym = PARAMrxEq1(1).lastSym;
            PARAMrxEq1(n).sampRate = PARAMrxEq1(1).sampRate;
            PARAMrxEq1(n).nSpS = PARAMrxEq1(1).nSpS;
            PARAMrxEq1(n).nSpB = PARAMrxEq1(1).nSpB;
            PARAMrxEq1(n).dt = PARAMrxEq1(1).dt;
            PARAMrxEq1(n).df = PARAMrxEq1(1).df;
            PARAMrxEq1(n).t = PARAMrxEq1(1).t;
            PARAMrxEq1(n).f = PARAMrxEq1(1).f;
        end
    end
else
    S.rxEq1     = S.rxFEC;
    PARAMrxEq1  = PARAMrxFEC;
end

%% Subcarrier Demux
[S.rxSCM,PARAMrxSCM] = SCM_demux(S.rxEq1,PARAMrxEq1,WDM,DSP);

%% Static Equalization (over each subcarrier)
[S.rxEq2,PARAMrxEq2] = SCM_staticEQ(S.rxSCM,PARAMrxSCM,WDM,FIBER,DSP,2);
A = S.rxEq2{1};

TRUNC.firstSample = 1e3;
TRUNC.nSamples = length(A) - 2*TRUNC.firstSample;
S.rxEq2{1} = truncateSignal(S.rxEq2{1},PARAMrxEq2,TRUNC);

%% Remove CD Pre-Distortion
DSP.preCD.accCD = -1*DSP.preCD.accCD;
if strcmpi(DSP.MIMO1.method,'CMA:4x2')
    DSP.preCD.IQ = 'ReIm';
end
S.rxDpre{1} = applyCD_preDistortion(A,PARAMrxEq2,DSP.preCD);

[S.rxDpre{1},PARAMrxEq2] = truncateSignal(S.rxDpre{1},PARAMrxEq2,TRUNC);

%% 1st Adaptive Equalization (for coefficient estimation)
[S.rxMIMO1,PARAMrxMIMO1,MIMO1] = SCM_adaptiveEq(S.rxDpre,S.tx,...
    PARAMrxEq2,WDM,DSP,1);

%% Estimate and Store Frequency Deviation
[S.rxFE,DSP.FE] = SCM_freqEstimation(S.rxMIMO1,S.tx,PARAMrxMIMO1,WDM,DSP);
df = DSP.FE.df;

%% Carrier-Phase Estimation (1st stage)
[S.rxCPE1,PARAMrxCPE1,DSP.CPE1] = SCM_phaseEstimation(S.rxFE,...
    S.tx,PARAMrxMIMO1,WDM,DSP,1);
phi = rectpulse(DSP.CPE1{1}.phi',2)';

%% Apply 1st Adaptive Equalization as Estimated Before (static)
DSP.MIMO1 = rmfield(DSP.MIMO1,'train');
DSP.MIMO1.demux.updateRate = 0;
DSP.MIMO1.W = MIMO1{1}.W;
[S.rxMIMO1,PARAMrxMIMO1,DSP.MIMO1] = SCM_adaptiveEq(S.rxEq2,S.tx,...
    PARAMrxEq2,WDM,DSP,1);

%% Apply Frequency-Mismatch Compensation Estimated Before
t = PARAMrxMIMO1.t;
S.rxFE{1}(1,:) = S.rxMIMO1{1}(1,:).*exp(-1j*2*pi*df(1).*t);
S.rxFE{1}(2,:) = S.rxMIMO1{1}(2,:).*exp(-1j*2*pi*df(2).*t);

%% Apply Carrier-Phase Recovery Estimated Before (1st stage)
S.rxCPE1{1} = S.rxFE{1}.*exp(-1j*phi);

%% Remove CD Pre-Distortion (4x2)
DSP.preCD.IQ = 'ReIm';
S.rxCPE1{1} = applyCD_preDistortion(S.rxCPE1{1},PARAMrxCPE1,DSP.preCD);

%% 3rd Adaptive Equalization (4x2)
[S.rxMIMO3,PARAMrxMIMO3,DSP.MIMO3] = SCM_adaptiveEq(S.rxCPE1,S.tx,...
    PARAMrxCPE1,WDM,DSP,3);

%% Downsampling
[S.rx1sp,PARAMrx1sp,DSP.DOWNSAMP] = SCM_downSample(S.rxMIMO3,...
    PARAMrxMIMO3,WDM,DSP);

%% Carrier-Phase Estimation (2nd stage)
[S.rxCPE2,PARAMrxCPE2,DSP.CPE2] = SCM_phaseEstimation(S.rx1sp,...
    S.tx,PARAMrx1sp,WDM,DSP,2);

%% 4th Adaptive Equalization
[S.rxMIMO4,PARAMrxMIMO4,DSP.MIMO4] = SCM_adaptiveEq(S.rxCPE2,S.tx,...
    PARAMrxCPE2,WDM,DSP,4);

%% Post-DSP Signal Preparation
[S.rxFinal,PARAMrxFinal] = SCM_postDSP(S.rxMIMO4,PARAMrxMIMO4,WDM,DSP);

%% Rx Decoder
[DSP.DECODER,S.rxFinal] = SCM_rxDECODER(S.rxFinal,S.tx,PARAMrxFinal,WDM,DSP);

%% Performance Analyzer
OUT = SCM_performanceAnalyzer(S.rxFinal,S.tx,PARAMrxFinal,WDM,DSP);

%% Calculate Average Power per Subcarrier
for n = 1:numel(WDM.subCarriersDSP)
    OUT.meanP(n) = pow2db((mean(abs(S.rxSCM{n}(1,:)).^2)*1e3 + ...
        mean(abs(S.rxSCM{n}(1,:)).^2)*1e3)/2);
end

%% Exit Message
myMessages(['\nDSP for SCM finished - Elapsed Time: ',...
    num2str(toc(tStart),'%2.4f\n'),' [s]'],0);


