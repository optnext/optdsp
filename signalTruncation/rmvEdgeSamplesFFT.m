function [S,PARAM] = rmvEdgeSamplesFFT(S,PARAM,DFT)

% Last Update: 16/04/2017


global PROG;

%% Input Parser
if ~isfield(PROG,'rmvEdgeSamples')
    flag = 1;
else
    flag = PROG.rmvEdgeSamples;
end
if ~isfield(DFT,'rmvEdgeSamples')
    DFT.rmvEdgeSamples = true;
end
if ~flag || ~DFT.rmvEdgeSamples
    return;
end

%% Input Parameters
firstSample = DFT.PARAM.overlapSize + 1;
lastSample = length(S) - DFT.PARAM.overlapSize;

%% Remove Edge Samples
S = S(:,firstSample:lastSample);

%% Update PARAM struct
PARAM.nSamples = length(S);
% PARAMnew.firstSample    = PARAMin.firstSample + firstSample - 1;
% PARAMnew.lastSample     = PARAMnew.firstSample + PARAMnew.nSamples - 1;
PARAM = setSimulationParams(PARAM);


