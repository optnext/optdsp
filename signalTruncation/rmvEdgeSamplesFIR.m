function [S,PARAM,idx] = rmvEdgeSamplesFIR(S,PARAM,FIR,nSpS)

% Last Update: 20/07/2017


global PROG;

%% Input Parser
if ~isfield(PROG,'rmvEdgeSamples')
    flag = 1;
else
    flag = PROG.rmvEdgeSamples;
end
FIR = FIR(1);
if ~isfield(FIR,'nIter')
    FIR.nIter = 1;
end
if ~isfield(FIR,'method')
    FIR.method = 'conv';
end
if ~isfield(FIR,'rmvEdgeSamples')
    FIR.rmvEdgeSamples = true;
end

if ~flag || ~FIR.rmvEdgeSamples
    idx = 1:length(S);
    return;
end

%% Input Parameters
switch FIR.method
    case {'vector','mean','distributive','angle'}
        firstSample = ceil(max(FIR.nTaps)/2)*FIR.nIter;
        lastSample  = length(S)-floor(max(FIR.nTaps)/2)*FIR.nIter;
    case {'filter','FIR'}
        firstSample = round(max(FIR.nTaps)/2)*FIR.nIter;
        lastSample  = length(S);
    otherwise
        firstSample = 1;
        lastSample  = length(S);
end
% Avoid changing the sampling instant:
if nargin == 4
    firstSample = firstSample + mod(firstSample,nSpS) + 1;
end

%% Remove Edge Samples
if nargin == 4
    remSamples = floor(mod(lastSample-firstSample+1,nSpS));
else
    remSamples = 0;
end
idx = firstSample:lastSample-remSamples;
S = S(:,idx);

%% Redefine PARAM struct
PARAM.nSamples = length(S);
% PARAM.firstSample = PARAM.firstSample+firstSample-1;
% PARAM.lastSample = PARAM.firstSample+PARAM.nSamples-1;
PARAM = setSimulationParams(PARAM);

