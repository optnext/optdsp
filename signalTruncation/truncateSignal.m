function [Aout,PARAM] = truncateSignal(Ain,PARAM,TRUNC)

% Last Update: 11/04/2017


%% Input Parser
if isnumeric(TRUNC)
    firstSample = TRUNC(1);
    lastSample = TRUNC(end);
else
    if isfield(TRUNC,'firstSample')
        firstSample = TRUNC.firstSample;
    else
        firstSample = 1;
    end
    if isfield(TRUNC,'nSamples')
        nSamples = TRUNC.nSamples;
        if isinf(nSamples)
            lastSample = PARAM.nSamples;
        else
            lastSample = firstSample + nSamples - 1;
        end
    else
        lastSample = PARAM.nSamples;
    end
end
if isinf(lastSample)
    lastSample = PARAM.nSamples;
end

%% Signal Truncation
Aout = Ain(:,firstSample:lastSample);

%% Ouput Struct
PARAM.nSamples = length(Aout);
PARAM = setSimulationParams(PARAM);

