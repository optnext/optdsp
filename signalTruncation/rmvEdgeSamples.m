function [Sout,PARAM] = rmvEdgeSamples(Sin,PARAM,edgeSamples)

% Last Update: 14/04/2017


%% Input Parser
if length(edgeSamples) == 1
    edgeSamples = rectpulse(edgeSamples,2);
end

%% Truncate Input Signal
firstSample = edgeSamples(1)+1;
lastSample = length(Sin)-edgeSamples(2);
Sout = Sin(:,firstSample:lastSample);

%% Redefine PARAM struct
PARAM.nSamples = length(Sout);
% PARAM.firstSample = firstSample;
% PARAM.lastSample = lastSample;
PARAM = setSimulationParams(PARAM);


