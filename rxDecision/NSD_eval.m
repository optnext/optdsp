function [NSD,NSD_t] = NSD_eval(Sin,Sref)

% Last Update: 01/09/2017

NSD = sum(abs(Sin-Sref).^2)/sum(abs(Sin).^2);
NSD_t = (abs(Sin-Sref).^2)./(abs(Sin).^2);

