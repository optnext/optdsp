function [OUT] = QAM_varianceAnalyzer(Sin,Stx,QAM)

% Last Update: 17/07/2016


%% Input Parameters
nPol = size(Sin,1);
M = sqrt(QAM.M);

%% Separate Analysis of Each QAM Symbol
for m = 1:nPol
    Ssyms = QAM_demod(Sin(m,:),Stx(m,:),'separate-symbols');
    for k1 = 1:M
        for k2 = 1:M
            var_r_sym{k1,k2}(m) = var(real(Ssyms{k1,k2}));
            var_i_sym{k1,k2}(m) = var(imag(Ssyms{k1,k2}));
            var_sym_FOM{k1,k2}(m) = var_i_sym{k1,k2}(m)/var_r_sym{k1,k2}(m);
        end
    end
end

%% Separate Analysis of Each QAM Ring
for m = 1:nPol
    Sring = QAM_demod(Sin(m,:),Stx(m,:),'separate-rings');
    for n = 1:numel(Sring)
        var_r_ring{n}(m) = var(real(Sring{n}));
        var_i_ring{n}(m) = var(imag(Sring{n}));
        var_ring_FOM{n}(m) = var_i_ring{n}(m)/var_r_ring{n}(m);
    end
end

%% Analysis of the Fully-Colapsed QAM
for m = 1:nPol
    Scolapsed = QAM_demod(Sin(m,:),Stx(m,:),'fully-colapsed');
    var_r(m) = var(real(Scolapsed));
    var_i(m) = var(imag(Scolapsed));
    var_FOM(m) = var_i(m)/var_r(m);
end

%% Outputs
OUT.var_r_sym = var_r_sym;
OUT.var_i_sym = var_i_sym;
OUT.var_sym_FOM = var_sym_FOM;
OUT.var_r_ring = var_r_ring;
OUT.var_i_ring = var_i_ring;
OUT.var_ring_FOM = var_ring_FOM;
OUT.var_r = var_r;
OUT.var_i = var_i;
OUT.var_FOM = var_FOM;


