function [EVM,yRef] = EVM_eval(y,txSyms,SIG,QAM,tMem)

% Last Update: 01/09/2017


%% Input Parser
if nargin == 2
    txSyms = syncSignals_2x2(y,txSyms);
    meanP = mean(mean(abs(txSyms).^2,2));
else
    if ~isfield(SIG,'firstSym')
        firstSym = 1;
        nTotalSyms = length(txSyms);
        lastSym = nTotalSyms;
    else
        firstSym = SIG.firstSym;
        lastSym = SIG.lastSym;
        nTotalSyms = SIG.nSyms;
    end
    M = QAM.M;
    IQmap = QAM.IQmap;
    meanP = QAM.meanConstPower;
    symbolInd = QAM.symbolIndex;
end
if ~exist('tMem','var')
    tMem = 0;
end

%% Input Parameters
nSyms = length(txSyms);
nPol = size(y,1);

%% Normalize Input Signal
% y = normalizeSignal(y,meanP,'joint');

%% Determine if txSyms Corresponds to the Symbols or to the Reference Signal
if numel(unique(txSyms)) <= 256 && nargin > 2
    yRef = zeros(nPol,nSyms);
    for n = 1:nPol
        if any(txSyms(n,:) < 0)
            % If there are invalid symbols, then populate yRef with "inf":
            yRef(n,:) = zeros(1,length(txSyms)) + inf;
        else
            yRef(n,:) = IQmap(symbolInd(txSyms(n,:)+1));
        end
    end
else
    yRef = txSyms;
    clear txSyms;
end

%% Calculate Overall EVM
polEVM = sqrt(mean(abs(y-yRef).^2,2)./meanP);
% polEVM_abs = sqrt(mean(abs(abs(y)-abs(yRef)).^2,2)./meanP);
% polEVM_arg = sqrt(mean(abs(angle(y)-angle(yRef)).^2,2)./pi);

y_abs = abs(y).*exp(1j*angle(yRef));
y_arg = abs(yRef).*exp(1j*angle(y));
polEVM_abs = sqrt(mean(abs(y_abs-yRef).^2,2)./meanP);
polEVM_arg = sqrt(mean(abs(y_arg-yRef).^2,2)./meanP);

%% Calculate EVM per Symbol
if nargin > 2
    if exist('txSyms','var')
        allSyms = unique(txSyms);
        [symEVM,yRefSym,symEVM_abs,symEVM_arg] = ...
            deal(zeros(nPol,numel(allSyms)) + NaN);
        for m = 1:nPol
            yPol = y(m,:);
            yPol_abs = y_abs(m,:);
            yPol_arg = y_arg(m,:);
            txSymsPol = txSyms(m,:);
            for n = 1:numel(allSyms)
                thisSym = allSyms(n);
                try
                    yRefSym(m,n) = IQmap(symbolInd(thisSym + 1));
                    ySym = yPol(txSymsPol == thisSym);
                    ySym_abs = yPol_abs(txSymsPol == thisSym);
                    ySym_arg = yPol_arg(txSymsPol == thisSym);
                    symEVM(m,n) = sqrt(mean(abs(ySym - yRefSym(m,n)).^2)./meanP);
        %             symEVM_abs(m,n) = sqrt(mean(abs(abs(ySym) - ...
        %                 abs(yRefSym)).^2,2)./meanP);
        %             symEVM_arg(m,n) = sqrt(mean(abs(unwrap(angle(ySym)) - ...
        %                 angle(yRefSym)).^2,2)./pi);
                    symEVM_abs(m,n) = sqrt(mean(abs(ySym_abs - yRefSym(m,n)).^2)./meanP);
                    symEVM_arg(m,n) = sqrt(mean(abs(ySym_arg - yRefSym(m,n)).^2)./meanP);
                catch ME
                    EVM.errorMsg_sym = ME;
                end
            end
        end
    end
end

%% Calculate SNR and BER from EVM
SNR_EVM = 10*log10(1./polEVM.^2);
if nargin>2
    BER_EVM = evm2ber(polEVM,M);
end

%% Evaluate EVM Evolution in Time, with Averaging Memory
if tMem
    polEVM_tMem = zeros(nPol,nSyms);
    polEVM_t = sqrt(abs(y-yRef).^2./abs(yRef).^2);
    W = ones(1,tMem)*1/tMem;
    for m = 1:nPol
%         for n = ceil(tMem/2):nSyms-fix(tMem/2)
%             polEVM_tMem(m,n) = mean(polEVM_t(m,n-ceil(tMem/2)+1:n+fix(tMem/2)));
%         end
        polEVM_tMem(m,:) = conv(polEVM_t(m,:),W,'same');
    end
    polEVM_tMem = [zeros(nPol,firstSym-1) polEVM_tMem ...
        zeros(nPol,nTotalSyms-lastSym)];
else
    polEVM_tMem = zeros(nPol,1);
end

%% Output EVM Struct
EVM.tMem = tMem;
EVM.EVMx = polEVM(1)*100;
EVM.EVMx_dB = 20*log10(polEVM(1));
EVM.EVMx_t = polEVM_tMem(1,:)*100;
EVM.EVMx_abs = polEVM_abs(1)*100;
EVM.EVMx_arg = polEVM_arg(1)*100;
EVM.SNRx = SNR_EVM(1);
if exist('symEVM','var')
    EVM.symEVMx = symEVM(1,:);
    EVM.symEVMx_abs = symEVM_abs(1,:);
    EVM.symEVMx_arg = symEVM_arg(1,:);
    EVM.symEVMx_IQ = yRefSym(1,:);
end
if nargin > 2
    EVM.BERx = BER_EVM(1);
end
if nPol == 2
    EVM.EVMy = polEVM(2)*100;
    EVM.EVMy_dB = 20*log10(polEVM(2));
    EVM.EVMy_t = polEVM_tMem(2,:)*100;
    EVM.SNRy = SNR_EVM(2);
    if nargin>2
        EVM.BERy = BER_EVM(2);
    end
    EVM.EVMxy = mean(polEVM)*100;
    EVM.EVMxy_dB = 20*log10(mean(polEVM));
    if exist('symEVM','var')
        EVM.symEVMy = symEVM(2,:);
        EVM.symEVMy_IQ = yRefSym(2,:);
    end
end

