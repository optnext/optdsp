function NMSE = NMSE_eval(Sin,Sref)

% Last Update: 01/09/2017

NMSE = mean((Sin-Sref).^2)/mean(Sin.^2);

