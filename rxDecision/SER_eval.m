function [SER] = SER_eval(txSyms,rxSyms,QAM)

% Last Update: 01/09/2017


%% Input Parameters
nPol = size(txSyms,1);

%% Evaluate SER
errPos = cell(1,nPol);
nSymErr = zeros(1,nPol);
polSER = zeros(1,nPol);
for n = 1:nPol
    errPos{n} = find(rxSyms(n,:)~=txSyms(n,:))';
    nSymErr(n) = length(errPos{n});
    polSER(n) = nSymErr(n)/length(txSyms);
end
totalSER = sum(nSymErr)/(nPol*length(txSyms));

%% Calculate SER Per Symbol
if exist('QAM','var')
    errSyms = cell(1,nPol);
    [nSymErrors,nSymTx] = deal(zeros(nPol,QAM.M));
    for m = 1:nPol
        errSyms{m} = txSyms(1,errPos{m});
        for n = 1:QAM.M
            nSymErrors(m,n) = sum(errSyms{m}==n-1);
            nSymTx(m,n) = sum(txSyms(m,:)==n-1);
        end
    end
    P = abs(QAM.IQmap).^2;
    allP = unique(P);
    for n = 1:length(allP)
        for m = 1:nPol
            errs = nSymErrors(m,:);
            nSym = nSymTx(m,:);
            nSymErrorsP(m,n) = sum(errs(P==allP(n)));
            nSymTxP(m,n) = sum(nSym(P==allP(n)));
        end
    end
end

%% Output SER struct
SER.errPosX = errPos{1};
SER.nSymErrX = nSymErr(1);
SER.SERx = polSER(1);
if exist('QAM','var')
    SER.SERxSym = nSymErrors(1,:)./nSymTx(1,:);
    SER.SERxSymP = nSymErrorsP(1,:)./nSymTxP(1,:);
    SER.nSymsTx_x = nSymTx(1,:);
    SER.nSymsTxP_x = nSymTxP(1,:);
end
if nPol == 2
    SER.errPosY = errPos{2};
    SER.nSymErrY = nSymErr(2);
    SER.SERy = polSER(2);
    SER.SERxy = totalSER;
    if exist('QAM','var')
        SER.SERySym = nSymErrors(2,:)./nSymTx(2,:);
        SER.SERySymP = nSymErrorsP(2,:)./nSymTxP(2,:);
        SER.nSymsTx_y = nSymTx(2,:);
        SER.nSymsTxP_y = nSymTxP(2,:);
    end
end

