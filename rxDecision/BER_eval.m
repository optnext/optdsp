function BER = BER_eval(txBits,rxBits,QAM,options)

% Last Update: 01/09/2017


%% Input Parser
if ~exist('options','var') || ~isfield(options,'saveErrPos')
    saveErrPos = true;
    BER_sym = false;
else
    saveErrPos = options.saveErrPos;
    BER_sym = option.BER_sym;
end

%% Input Parameters
M = QAM.M;
[nPol,nBits] = size(txBits);

%% Evaluate BER
errPos = cell(1,nPol);
[nBitErr,polBER] = deal(zeros(1,nPol));
polCI95 = zeros(nPol,2);
for n = 1:nPol
    if any([txBits(n,:) rxBits(n,:)] == -1)
        errPos{n} = 1:nBits;
        nBitErr(n) = nBits;
    else
        errPos{n} = find(xor(txBits(n,:),rxBits(n,:)) == 1);
        nBitErr(n) = numel(errPos{n});
    end
    [polBER(n),polCI95(n,:)] = berconfint(nBitErr(n),nBits,0.95);
end
[totalBER,CI95] = berconfint(sum(nBitErr),nPol*nBits,0.95);

%% Evaluate BER per Symbol
if BER_sym
    IQmap = QAM.IQmap;
    symbolInd = QAM.symbolIndex;
    txSyms = rectpulse(bit2sym(txBits,QAM.nBpS).',log2(M)/2).';
    allSyms = unique(txSyms);
    errPos_sym = cell(nPol,numel(allSyms));
    [nBits_sym,nBitErr_sym] = deal(zeros(nPol,numel(allSyms))+NaN);
    totalBER_sym = zeros(1,numel(allSyms))+NaN;
    CI95_sym = zeros(numel(allSyms),2)+NaN;
    [polBER_sym,polCI95_sym] = deal(cell(nPol,1));
    [polBER_sym{:}] = deal(zeros(1,numel(allSyms))+NaN);
    [polCI95_sym{:}] = deal(zeros(numel(allSyms),2)+NaN);
    try
        IQ = IQmap(symbolInd(allSyms + 1));
        for n = 1:numel(allSyms)
            thisSym = allSyms(n);
            for m = 1:nPol
                txBits_sym = txBits(m,txSyms(m,:) == thisSym);
                rxBits_sym = rxBits(m,txSyms(m,:) == thisSym);
                nBits_sym(m,n) = length(txBits_sym);
                errPos_sym{m,n} = find(xor(txBits_sym,rxBits_sym) == 1);
                nBitErr_sym(m,n) = numel(errPos_sym{m,n});
                [polBER_sym{m}(n),polCI95_sym{m}(n,:)] = ...
                    berconfint(nBitErr_sym(m,n),nBits_sym(m,n),0.95);
            end
            [totalBER_sym(n),CI95_sym(n,:)] = berconfint(...
                sum(nBitErr_sym(:,n)),nPol*sum(nBits_sym(:,n)),0.95);
        end
    catch ME
        IQ = NaN;
        BER.errorMsg_sym = ME;
    end
end

%% Calculate SNR from BER
if exist('M','var')
    SNR = ber2snr(polBER,M);
end

%% Output BER Struct
% x-pol:
if saveErrPos
    BER.errPosX = errPos{1};
end
BER.nBitsTotal = nBits;
BER.nBitErrX = nBitErr(1);
BER.BERx = polBER(1);
BER.CI95x = polCI95(1,:);
if BER_sym
    BER.nBits_sym = nBits_sym;
    BER.nBitErrX_sym = nBitErr_sym(1,:);
    BER.BERx_sym = polBER_sym{1};
    BER.CI95x_sym = polCI95_sym{1};
end
if exist('SNR','var')
    BER.SNRx = SNR(1);
end
BER.Q2x = Qfactor(BER.BERx);
% y-pol:
if nPol == 2
    if saveErrPos
        BER.errPosY = errPos{2};
    end
    BER.nBitErrY = nBitErr(2);
    BER.BERy = polBER(2);
    BER.CI95y = polCI95(2,:);
    if BER_sym
        BER.nBitErrY_sym = nBitErr_sym(2,:);
        BER.BERy_sym = polBER_sym{2};
        BER.CI95y_sym = polCI95_sym{2};
    end
    if exist('SNR','var')
        BER.SNRy = SNR(2);
    end
    BER.Q2y = Qfactor(BER.BERy);
    BER.BERxy = totalBER;
    BER.Q2xy = Qfactor(BER.BERxy);
    BER.CI95 = CI95;
    if BER_sym
        BER.BERxy_sym = totalBER_sym;
        BER.CI95_sym = CI95_sym;
        BER.BERxy_sym_IQ = IQ;
    end
end
