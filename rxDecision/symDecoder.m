function [DECODER,Srx] = symDecoder(Srx,Stx,QAM,DECODER)

% Last Update: 01/09/2017


global PROG;

%% Entrance Message
entranceMsg(PROG.MSG.symDecoder);
tic

%% Input Parser
% Check if input signal is valid. If not, return an error:
if any(any(isnan(Srx),2)) || ~all(sum(Srx,2))
    error('Error in Symbol Decoder: input signal is not valid!');
end
if nargin < 4
    DECODER.scaleFactor = 1;
    DECODER.syncMethod = 'complexField';
end
if isfield(DECODER,'scaleFactor')
    c = DECODER.scaleFactor;
else
    c = 1;
end
if ~isfield(DECODER,'syncMethod')
    DECODER.syncMethod = 'complexField';
end
if ~isfield(DECODER,'normalizeSignal')
    DECODER.normalizeSignal = true;
end

%% Normalize Rx Signal
Stx = normalizeSignal(Stx,QAM.meanConstPower,'independent');
if DECODER.normalizeSignal
    Srx = c*normalizeSignal(Srx,QAM.meanConstPower,'independent');
end

%% Synchronize Tx and Rx Signals
switch DECODER.syncMethod
    case 'complexField'
        SYNC = DECODER;
        SYNC.txSignal = Stx;
        SYNC.method = 'complexField';
        SYNC.nPol = 2;
        [Stx,SYNC.delay] = syncTxRx_MIMO(Srx,SYNC);
    case 'IQ'
%         SYNC.debugPlots = DECODER.debugPlots;
        [Stx,SYNC.delay] = syncTxRx_IQ(Stx,Srx);
end

%% Transmitted Symbols
txSyms = signal2symbol(Stx,QAM);

%% Decode Rx Symbols
rxSyms = signal2symbol(Srx,QAM);

%% Transmitted and Received Bits
if isfield(QAM,'sym2bitMap')
    DECODER.txBits = sym2bit(txSyms,QAM);
    DECODER.rxBits = sym2bit(rxSyms,QAM);
end

%% Output Decoder Struct
DECODER.SYNC = SYNC;
DECODER.txSyms = txSyms;
DECODER.rxSyms = rxSyms;

%% Elapsed Time
elapsedTime = toc;
myMessages(['Symbol Decoder - Elapsed Time: ',...
    num2str(elapsedTime,'%1.4f'),' [s]\n'],1);


