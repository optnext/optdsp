function [Sout,PARAM,ts0] = downsample_1sps(Sin,nSpS,PARAM,ts0)

% Last Update: 07/03/2017


global PROG;

%% Entrance Message
entranceMsg(PROG.MSG.downSamp);
tic

%% Input Parameters
nPol    = size(Sin,1);
if ~exist('ts0','var') || isempty(ts0)
    ts0 = 1;
end
if isnumeric(ts0) && length(ts0) ~= nPol
    ts0 = rectpulse(ts0(1),nPol);
end

%% Determine Initial Sampling Phase
if ischar(ts0)
    ts0 = bestSamplingInstant(Sin,nSpS,QAM,ts0);
end

%% Signal Sampling
if nSpS == floor(nSpS)
    for n = 1:nPol
        Sout(n,:) = Sin(n,ts0(n):nSpS:end);
    end
    t = PARAM.t(ts0(1):nSpS:end);
else
    [a,b] = rat(nSpS);
    for n = 1:nPol
        Sout(n,:) = resample(Sin(n,ts0(n):end),b,a);
    end
    t = resample(PARAM.t(ts0(1):end),b,a);
end

%% Output PARAM struct
PARAM.nSamples = length(Sout);
PARAM.sampRate = PARAM.sampRate / nSpS;
PARAM = setSimulationParams(PARAM);
PARAM.t = t;

%% Print Parameters
if PROG.showMessagesLevel >= 2
    fprintf('------------------------------------------------------------------');
    fprintf('\nDown-Sampling PARAMETERS:\n');
    fprintf('Sampling Factor (PARAMout.sampFactor):               %1.1f \n',nSpS);
    fprintf('Sampling Rate (PARAMout.sampRate):                   %1.2f [GHz]\n',PARAM.sampRate*1e-9);
    fprintf('Total Number of Samples (PARAMout.nSamples):         %1.1f \n',PARAM.nSamples);
    fprintf('------------------------------------------------------------------\n');
end

%% Elapsed Time
elapsedTime = toc;
myMessages(['Symbol Rate Downsampling - Elapsed Time: ',...
    num2str(elapsedTime,'%1.4f'),' [s]\n'],1);
