function ts0 = bestSamplingInstant(A,nSpS,QAM,mode)

% Last Update: 10/04/2017


%% Input Parser
if ~exist('mode','var')
    mode = 'std';
end

%% Input Parameters
nPol    = size(A,1);
r       = QAM.radius;
P       = QAM.meanConstPower(1);

%% Normalize the Signal
% A = normalizeSignal(A,1);
A = normalizeSignal(A,P);

%% Find the Optimum Sampling Instant
ts0 = zeros(nPol,1);
switch mode
    case 'amplitude'
        for m = 1:nPol
            for n = 1:nSpS
                Sdown   = A(m,n:nSpS:end);
                q(m,n)  = sum(abs(Sdown).^2 < 0.1*P);
            end
            [~,ts0(m)] = min(q(m,:));
        end
        
    case {'best','std'}
        q   = zeros(nPol,nSpS);
        for m = 1:nPol
            for n = 1:nSpS
                Sdown = A(m,n:nSpS:end);
                for k = 1:length(r)
                    S{k} = Sdown(abs(Sdown)<r(k)+0.1 & abs(Sdown)>r(k)-0.1);
                    qS(k) = std(abs(S{k}));
        %             qS(k) = numel(S{k});
                end
                q(m,n) = sum(qS);
            end
            [~,ts0(m)] = min(q(m,:));
        %     [~,ts0(m)] = max(q(m,:));
        end
        
    case 'DD'
        err = zeros(nPol,nSpS);
        for m = 1:nPol
            for n = 1:nSpS
                Sdown = A(m,n:nSpS:end);
                F = symbol2signal(signal2symbol(Sdown,QAM),QAM);
                err(m,n) = std(abs(abs(F).^2-abs(Sdown).^2));
            end
            [~,ts0(m)] = min(err(m,:));
        end
end

%% Show Warning if the Optimum Sampling Instant is not Common to All Pols
if length(unique(ts0)) > 1
    warning('Different sampling instants have been determined for different polarizations!');
end

