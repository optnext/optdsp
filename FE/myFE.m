function [Aout,FE] = myFE(Ain,PARAM,FE,nSpS)

% Last Update: 10/04/2017


global PROG;

%% Entrance Message
entranceMsg('FREQUENCY ESTIMATION');
tic

%% Input Parser
FE = presetFE('FE',FE);

%% Input Parameters
nPol = size(Ain,1);
t = PARAM.t;

%% Check for Pre-Defined Frequency Deviation
if FE.usePreset
    FE.df = checkFreqDeviation();
    if ~isempty(FE.df)
        FE.method = 'preset';
    end
end

%% Check for Preset Coarse Frequency Estimation
if isfield(FE,'dfCoarse')
    for n = 1:nPol
        Ain(n,:) = Ain(n,:).*exp(-1j*2*pi*FE.dfCoarse*PARAM.t);
    end
end

%% Apply Frequency Estimation
switch FE.method
    case 'spectral'
        [Aout,FE] = freqEstimation_FD(Ain,PARAM,FE,nSpS);
%     case {'time-domain','TD'}
%         [Aout,FE]   = freqEstimation_TD(Ain,SIG,QAM,FE);
    case 'preset'
        for n = 1:nPol
            if ~FE.polIndependent
                Aout(n,:) = Ain(n,:).*exp(-1j*2*pi*FE.df.*t);
            else
                Aout(n,:) = Ain(n,:).*exp(-1j*2*pi*FE.df(n,:).*t);
            end
        end
    case 'none'
        Aout = Ain;
end

%% Update FE Struct
if isfield(FE,'dfCoarse')
    FE.df = FE.df + FE.dfCoarse;
end

%% Print Parameters
if PROG.showMessagesLevel >= 2
    fprintf('------------------------------------------------------------------');
    fprintf('\nFrequency Estimation PARAMETERS:\n');
    fprintf('Frequency Shift (FE.df):                        %2.3f [MHz]\n',FE.df*1e-6);
    fprintf('Estimation Method (FE.method):                  %s \n',FE.method);
    fprintf('------------------------------------------------------------------\n');
end
elapsedTime = toc;
myMessages(['Frequency Estimation - Elapsed Time: ',...
    num2str(elapsedTime,'%1.4f'),' [s]\n'],1);

%% Save Estimated Frequency to File
if ~strcmp(FE.method,'preset')
    saveFlag = FE.saveFile;
    if saveFlag == 2
        fprintf('\nThe frequency estimation stage has determined a frequency deviation of %1.3f MHz\n',FE.df*1e-6);
        fprintf('\nYou can save this value to be used in future without repeating this calculation.');
        saveFlag = input('\nDo you want to save the FE.df value? (1:Yes \ 0:No)');
    end
    if saveFlag
        fileName = [PROG.rxDataFolder,'auxFiles\',PROG.rxDataFileName,'_freqDeviation.txt'];
        fid = fopen(fileName,'w+');
        if fid > 0
            fprintf(fid,'%f',FE.df);
            fclose(fid);
        end
    end
end
