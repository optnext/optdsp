function FE = presetFE(varargin)

% Last Update: 07/03/2017


%% Input Parser
for n = 1:2:nargin
    varName = varargin{n};
    varValue = varargin{n+1};
    if any(strncmpi(varName,{'method'},4))
        method = varValue;
    elseif any(strncmpi(varName,{'nSamples'},5))
        nSamples = varValue;
    elseif any(strncmpi(varName,{'usePreset'},6))
        usePreset = varValue;
    elseif any(strncmpi(varName,{'polIndependent'},5))
        polIndependent = varValue;
    elseif any(strncmpi(varName,{'saveFile'},5))
        saveFile = varValue;
    elseif any(strncmpi(varName,{'debugPlots'},6))
        debugPlots = varValue;
    elseif strncmpi(varName,'FE',2)
        FE_old = varValue;
    end
end

if ~exist('method','var')
    method = 'spectral';
end
if ~exist('nSamples','var')
    nSamples = 'all';
end
if ~exist('usePreset','var')
    usePreset = false;
end
if ~exist('polIndependent','var')
    polIndependent = false;
end
if ~exist('saveFile','var')
    saveFile = 0;
end
if ~exist('debugPlots','var')
    debugPlots = {};
end

%% Check for pre-defined FE
if exist('FE_old','var')
    FE = FE_old;
end

%% Set FE Parameters
% Method:
if ~exist('FE','var') || ~isfield(FE,'method')
    FE.method = method;
end
if ~isfield(FE,'nSamples')
    FE.nSamples = nSamples;
end
if ~isfield(FE,'usePreset')
    FE.usePreset = usePreset;
end
if ~isfield(FE,'polIndependent')
    FE.polIndependent = polIndependent;
end
if ~isfield(FE,'saveFile')
    FE.saveFile = saveFile;
end
if ~isfield(FE,'debugPlots')
    FE.debugPlots = debugPlots;
end




