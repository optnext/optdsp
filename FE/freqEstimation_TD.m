function [Aout,FE] = freqEstimation_TD(Ain,SIG,QAM,FE)

% Last Update: 11/05/2016

% Warning: Old version, needs update to work properly!

%%
if SIG.nSpS > 1
    ts0 = bestSamplingInstant(Ain,SIG,QAM,'std');
end
A = Ain(:,ts0:SIG.nSpS:end);

%% Input Parameters
nTaps       = FE.nTaps;
mu          = FE.stepSize;
nSamples    = length(A);
tSym        = SIG.tSym;

%% Normalization
normX = modnorm(A(1,:),'avpow',1);
normY = modnorm(A(2,:),'avpow',1);
A(1,:) = A(1,:)*normX;
A(2,:) = A(2,:)*normY;

%% Initializating vectors to improve performance
% df = ones(1,nSamples)*(1/(8*pi*tSym))*...
%     angle(sum((A(1,2:25+1).*conj(A(1,1:25))).^4) + ...
%     sum((A(2,2:25+1).*conj(A(2,1:25))).^4));
df = zeros(1,nSamples);
% tetaxy = zeros(1,nSamples);

%% Calculate Frequency Offset
for n = nTaps+2:nSamples
    df(n) = df(n-1)*(1-mu) + (mu/(8*pi*tSym))*...
        angle(sum((A(1,n-nTaps:n).*conj(A(1,n-1-nTaps:n-1))).^4) +...
        sum((A(2,n-nTaps:n).*conj(A(2,n-1-nTaps:n-1))).^4));
%     df(n) = df(n-1)*(1-mu) + (mu/(8*pi*tSym))*...
%         angle(sum((A(1,n-nTaps:n).*conj(A(1,n-1-nTaps:n-1)))) +...
%         sum((A(2,n-nTaps:n).*conj(A(2,n-1-nTaps:n-1)))));
end

%%
mu = 1e-4;
df(nTaps+1) = df(end);
for n = nTaps+2:nSamples
    df(n) = df(n-1)*(1-mu) + (mu/(8*pi*tSym))*...
        angle(sum((A(1,n-nTaps:n).*conj(A(1,n-1-nTaps:n-1))).^4) +...
        sum((A(2,n-nTaps:n).*conj(A(2,n-1-nTaps:n-1))).^4));
end

%% Remove Frequency Offset
% dTeta = 2*pi*df*tSym;
% for n = 2:nSamples
%     tetaxy(n) = mod(tetaxy(n-1)+dTeta(n),2*pi);
% %     tetaxy(n) = tetaxy(n-1)+dTeta(n);
% end
% 
% Aout2 = Ain.*exp(-1j*repmat(rectpulse(tetaxy,PARAM.nSpS),2,1));

phi_t = 2*pi*cumsum(df)*tSym;
Aout = Ain.*exp(-1j*repmat(rectpulse(phi_t,SIG.nSpS),2,1));

%%
% Aout = Ain.*exp(-1j*2*pi*repmat(rectpulse(df,PARAM.nSpS),2,1).*...
%     repmat(PARAM.t,2,1));

% Aout = Ain.*exp(-1j*2*pi*repmat(rectpulse(mean(df),PARAM.nSamples).',2,1).*...
%     repmat(PARAM.t,2,1));

% a = -1.38e8;
% Aout = Ain.*exp(-1j*2*pi*repmat(rectpulse(a,PARAM.nSamples).',2,1).*...
%     repmat(PARAM.t,2,1));

%% Output Parameters
FE.df = df;


