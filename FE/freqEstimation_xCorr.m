function [FE] = freqEstimation_xCorr(Sin,Stx,PARAM,FE)

% Last Update: 10/05/2016

% Warning: Old version, needs update to work properly!

%% Input Parameters
freqRes = FE.freqResolution;
dfMax   = FE.dfMax;
dfMin   = FE.dfMin;
dfTrial = dfMin:freqRes:dfMax;
txX     = fft(Stx(1,:));
txY     = fft(Stx(2,:));
N       = length(txX);
t       = [0:N-1]*PARAM.dt;

%%
for n = 1:numel(dfTrial)
    X = fft(Sin(1,1:N).*exp(-1j*2*pi*dfTrial(n)*t));
    Y = fft(Sin(2,1:N).*exp(-1j*2*pi*dfTrial(n)*t));
    Zx = ifft(txX.*conj(X));
    Zy = ifft(txY.*conj(Y));
    Z = abs(Zx) + abs(Zy);
%     figure(101);
%     plot(Z);
    max1(n) = max(Z);
end
[max_Df,max_Df_pos] = max(max1);
df = dfTrial(max_Df_pos);

%%
FE.df = df;

