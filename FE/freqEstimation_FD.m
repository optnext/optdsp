function [Aout,FE] = freqEstimation_FD(Ain,PARAM,FE,nSpS)

% Last Update: 10/04/2017


%% Pre-Equalization CMA
if ~isfield(FE,'zeroPad')
    FE.zeroPad = 0;
end
if FE.nSamples ~= PARAM.nSamples
    if ~isfield(FE,'blockJump')
        FE.blockJump = 64;
    end
end
if ~isfield(FE,'jointPol')
    FE.jointPol = false;
end
if ~isfield(FE,'demodQAM')
    FE.demodQAM = '4thPower';
end

%% Input Parameters
[nPol,nSamples] = size(Ain);
t               = repmat(PARAM.t,nPol,1);
nSamplesFE      = FE.nSamples;                                              % number of samples per FE block
if strcmpi(nSamplesFE,'all')
    nSamplesFE  = PARAM.nSamples;
end
zeroPad         = FE.zeroPad;                                               % zero padding (frequency-domain interpolation)
nSamplesTotal   = nSamplesFE + zeroPad;
Fs              = PARAM.sampRate;                                           % sampling rate [Hz]
fRes            = Fs/nSamplesTotal;

%% Demod QAM
switch FE.demodQAM
    case '4thPower'
        Ademod = Ain.^4;
        c = 4;
    case {'data-aided','DA'}
        Ademod(1,:) = demodQAM_DA(Ain(1,:),...
            rectpulse(FE.txSignal(1,:),nSpS));
        Ademod(2,:) = demodQAM_DA(Ain(2,:),...
            rectpulse(FE.txSignal(2,:),nSpS));
        c = 1;
end

%% Apply Frequency Estimation
if nSamplesFE == nSamples
    if FE.jointPol
        Ax_0Pad     = [Ademod(1,:) zeros(1,zeroPad)];      
        Ay_0Pad     = [Ademod(2,:) zeros(1,zeroPad)];      
        Aw          = abs(fftshift(fft(Ax_0Pad))) + ...
                      abs(fftshift(fft(Ay_0Pad)));
        [~,ind]     = max(Aw);
        df          = (ind-nSamplesTotal/2-1)*fRes/c;
    else
        Aw          = zeros(nPol,nSamplesTotal);
        [df,ind]    = deal(zeros(nPol,1));
        for n = 1:nPol
            A_0Pad      = [Ademod(n,:) zeros(1,zeroPad)];      
            Aw(n,:)     = abs(fftshift(fft(A_0Pad)));
            [~,ind(n)]  = max(Aw(n,:));
            df(n)       = (ind(n)-nSamplesTotal/2-1)*fRes/c;
        end
    end
else
    fw = FE.blockJump;
    if FE.jointPol
        df      = zeros(1,nSamples);
        ind0    = 1:nSamplesFE;
        for k = ceil(nSamplesFE/2):fw:nSamples-floor(nSamplesFE/2)
            Ax_0Pad = [Ademod(1,ind0) zeros(1,zeroPad)];
            Ay_0Pad = [Ademod(2,ind0) zeros(1,zeroPad)];
            Aw      = abs(fftshift(fft(Ax_0Pad))) + ...
                      abs(fftshift(fft(Ay_0Pad)));
            [~,ind] = max(Aw);
            df(k)   = (ind-nSamplesTotal/2-1)*fRes/c;
            ind0    = ind0 + fw;
        end
    else
        df = zeros(nPol,nSamples);
        for n = 1:nPol
            ind0 = 1:nSamplesFE;
            for k = ceil(nSamplesFE/2):fw:nSamples-floor(nSamplesFE/2)
                A_0Pad  = [Ademod(n,ind0) zeros(1,zeroPad)];
                Aw      = abs(fftshift(fft(A_0Pad)));
                [~,ind] = max(Aw);
                df(n,k) = (ind-nSamplesTotal/2-1)*fRes/c;
                ind0    = ind0 + fw;
            end
        end
    end
end

%% Remove Frequency Offset
if nSamplesFE == nSamples
    DF = repmat(df,size(t,1)/size(df,1),1);
    for n = 1:size(Ain,1)
        Aout(n,:) = Ain(n,:).*exp(-1j*2*pi*DF(n).*t(n,:));
    end
else
    for n = 1:size(df,1)
        tmp = df(n,nSamplesFE/2:fw:end-floor(nSamplesFE/2));
        tmp = [zeros(1,nSamplesFE/2-1)+tmp(1) rectpulse(tmp,fw)];
        DF(n,:) = [tmp zeros(1,nSamples-numel(tmp)) + tmp(end)];
    end
    phi_t   = 2*pi*cumsum(DF,2)*PARAM.dt;
    Aout    = Ain.*exp(-1j*repmat(phi_t,size(Ain,1)/size(phi_t,1),1));
end

%% Output Parameters
FE.df = DF;

%% Debug
if isfield(FE,'debugPlots')
    for m = 1:length(FE.debugPlots)
        switch FE.debugPlots{m}
            case {'spectrum','all'}
                if exist('Aw','var')
                    figure();
                    f = (-Fs/2:fRes:Fs/2-fRes)/4*1e-9;
                    y = 20*log10(Aw(1,:)/max(Aw(1,:)));
                    plot(f,y,'-k',f(ind(1)),...
                        y(ind(1)),'or');
                    axis([min(f) max(f) -inf 5]);
                    xlabel('f [GHz]','Interpreter','latex',...
                        'FontSize',12);
                    ylabel('$|\tilde{A}^4|$ [dB]','Interpreter','latex',...
                        'FontSize',12);
                    xText = f(ind(1))+fRes*1e-6;
                    yText = 1*y(ind(1));
                    text(xText,yText,['$\Delta f=$',...
                        num2str(df(1)*1e-6),'MHz'],...
                        'Interpreter','latex','FontSize',12);
%                     if ~FE.jointPol
%                         hold on;
%                         y = 20*log10(Aw(2,:)/max(Aw(2,:)));
%                         plot(f,y,'-b',f(ind(2)),...
%                             Aw(2,ind(2)),'or');
%                     end
                end
        end
    end
end
