function [Sout]=GramSchmidt(Sin)

% Last Update: 13/12/2013
%
% I. Fatadin, S. Savory and D. Ives, "Compensation of Quadrature Imbalance
% in an Optical QPSK Coherent Receiver", IEEE PTL, vol. 20, no. 20, 

%% Input Parameters
[nPol,nSamples]=size(Sin);

%% Orthogonalization
% Sout=zeros(nPol,nSamples);
% for n=1:nPol
%     I=real(Sin(n,:));
%     Q=imag(Sin(n,:));
%     Q_ortho=Q-sum(I.*Q)/sum(I.*I)*I;
%     Sout(n,:)=I+1j*Q_ortho;
% end

%% Orthonormalization
Sout=zeros(nPol,nSamples);
blockSize=nSamples;
% blockSize=4096;
nBlocks=ceil(nSamples/blockSize);
for n=1:nPol
    for k=1:nBlocks
        ind=(k-1)*blockSize+1:min([nSamples k*blockSize]);
        I=real(Sin(n,ind));
        P_I=mean(I.^2);
        I_ortho=I/sqrt(P_I);
        Q=imag(Sin(n,ind));
        Q2=Q-I*mean(I.*Q)/P_I;
        P_Q=mean(Q2.^2);
        Q_ortho=Q2/sqrt(P_Q);
        Sout(n,ind)=I_ortho+1j*Q_ortho;
%         initBasis.I=I_ortho;
%         initBasis.Q=Q/sqrt(P_Q);
%         newBasis.I=I_ortho;
%         newBasis.Q=Q_ortho;
    end
end

