function [Sout,DC] = removeDC(Sin,PARAM,DC)

% Last Update: 23/01/2017


global PROG;

%% Entrance Message
entranceMsg(PROG.MSG.removeDC);
tic

%% Input Parser
if ~exist('DC','var')
    DC.method = 'mean';
    DC.removeDF = false;
end
if ~isfield(DC,'method')
    if isfield(DC,'nTaps')
        DC.method = 'movingAverage';
    else
        DC.method = 'mean';
    end
end
if ~isfield(DC,'removeDF')
    DC.removeDF = false;
else
    if DC.removeDF
        if ~isfield(DC,'resetDF')
            DC.resetDF = true;
        end
    end
end
if ~isfield(DC,'maxDF')
    DC.maxDF = 1e9;
end

%% Input Parameters
if DC.removeDF
    f = PARAM.f;
    t = PARAM.t;
end
if strcmp(DC.method,'movingAverage')
    nTaps = DC.nTaps;
end

%% Remove Frequency Deviation
maxInd = find(PARAM.f>DC.maxDF,1);
minInd = PARAM.nSamples - maxInd;
if DC.removeDF

    Sf(1,:) = abs(fftshift(fft(Sin(1,:))));
    Sf(2,:) = abs(fftshift(fft(Sin(2,:))));
    [maxSf(1),ind(1)] = max(Sf(1,minInd:maxInd));
    [maxSf(2),ind(2)] = max(Sf(2,minInd:maxInd));
    [~,ind2] = max(maxSf);
    df = f(ind(ind2)+minInd-1);

    Sin(1,:) = Sin(1,:).*exp(-1j*2*pi*df*t);
    Sin(2,:) = Sin(2,:).*exp(-1j*2*pi*df*t);
    
    DC.df = df;
end

%% Remove DC component
switch DC.method
    case 'movingAverage'
        W = ones(1,nTaps)/nTaps;
        meanX   = conv(Sin(1,:),W,'same');
        meanY   = conv(Sin(2,:),W,'same');
    case 'mean'
        meanX   = mean(Sin(1,:));
        meanY   = mean(Sin(2,:));
end
Sout(1,:) = Sin(1,:) - meanX;
Sout(2,:) = Sin(2,:) - meanY;

%% Re-apply Frequency Deviation
if DC.removeDF && DC.resetDF
    Sout(1,:) = Sout(1,:).*exp(1j*2*pi*df*t);
    Sout(2,:) = Sout(2,:).*exp(1j*2*pi*df*t);
end

%% Elapsed Time
elapsedTime = toc;
myMessages(['Remove DC Component - Elapsed Time: ',...
    num2str(elapsedTime,'%1.4f'),' [s]\n'],1);

