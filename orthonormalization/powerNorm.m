function [Sout,P0Norm] = powerNorm(Sin,P0_dBm)

% Last Update: 02/01/2016


%% Determine Number of Polarizations
nSignals    = size(Sin,1);
nPol        = min([nSignals 2]);

%% Apply Power Normalization
P0_W        = db2pow(P0_dBm)*1e-3;  % normalization power [W]
P0_W_pol    = P0_W/nPol;            % power per polarization [W]
nSamples    = length(Sin);          % total number of samples
Sout        = zeros(nPol,nSamples); % output normalized field
[oldP_W,oldP_dBm,newP_W,newP_dBm] = deal(zeros(1,nPol));
for n = 1:nSignals
    oldP_W(n)   = mean(abs(Sin(n,:)).^2);
    oldP_dBm(n) = pow2db(oldP_W(n)*1e3);
    Sout(n,:)   = Sin(n,:).*sqrt(P0_W_pol)/sqrt(oldP_W(n));
    newP_W(n)   = mean(abs(Sout(n,:)).^2);
    newP_dBm(n) = pow2db(newP_W(n)*1e3);
end

%% Output Struct
P0Norm.rxP0_dBm_pol     = oldP_dBm;
P0Norm.rxP0_dBm         = pow2db(sum(oldP_W)*1e3);
P0Norm.normP0_dBm_pol   = newP_dBm;
P0Norm.normP0_dBm       = pow2db(sum(newP_W)*1e3);

