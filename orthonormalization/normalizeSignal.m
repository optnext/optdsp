function [Aout,normFactor,meanP] = normalizeSignal(Ain,normTarget,option)

% Last Update: 23/11/2014


%% Input Parser
if iscell(Ain)
    nSignals    = length(Ain);
else
    nSignals    = 1;
    tmp         = Ain;
    clear Ain;
    Ain{1}      = tmp;
end
switch nargin
    case 1,
        normTarget  = 1;
        option      = 'independent';
    case 2,
        option      = 'independent';
end

%% Normalization Factors
for n = 1:nSignals
    normFactor{n} = sqrt(mean(abs(Ain{n}).^2,2)/normTarget);
end

%% Normalize Optical Field
for n = 1:nSignals
    [nPol,nSamples] = size(Ain{n});
    if strcmp(option,'joint')
        normFactor{n} = mean(normFactor{n});
        Aout{n} = Ain{n}/normFactor{n};
    else
        Aout{n} = zeros(nPol,nSamples);
        for k = 1:nPol
            Aout{n}(k,:) = Ain{n}(k,:)./normFactor{n}(k);
        end
    end
end

%% Mean Signal Power After Normalization
for n = 1:nSignals
    meanP{n} = mean(abs(Aout{n}).^2,2);
end

%% Output Signal
if length(Aout) == 1
    Aout        = Aout{1};
    normFactor  = normFactor{1};
    meanP       = meanP{1};
end


