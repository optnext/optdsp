function [DFT] = NoOverlap_FFTconf(DFT,PARAM)

% Last Update: 03/04/2017


%% Input Parser
if ~isfield(DFT,'nFFT')
    if isfield(DFT,'nFFTblocks')
        DFT.nFFT = PARAM.nSamples/DFT.nFFTblocks;
    else
        error('For block processing, the number of samples per FFT block (nFFT) must be specified!');
    end
end

%% FFT Configuration
NO.nBlocks          = PARAM.nSamples/DFT.nFFT;
NO.nSyms            = PARAM.nSyms / NO.nBlocks;
NO.blockSize        = NO.nSyms * PARAM.nSpS;
NO.tWindow          = PARAM.tWindow / NO.nBlocks;
NO.nSamples         = NO.nSyms * PARAM.nSpS;
NO.sampRate         = PARAM.sampRate;
NO.symRate          = PARAM.symRate;
NO.f                = (-NO.nSamples/2:...
                        NO.nSamples/2-1) * (1/(NO.tWindow));

DFT.PARAM           = NO;
DFT.nTotalBlocks    = NO.nBlocks;
DFT.rmvEdgeSamples  = false;

