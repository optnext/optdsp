function [DFT] = OverlapSave_FFTconf(DFT,PARAM)

% Last Update: 16/04/2017


%% Input Parser
if ~isfield(DFT,'nFFT')
    if isfield(DFT,'nFFTblocks')
        DFT.nFFT = PARAM.nSamples/DFT.nFFTblocks;
    else
        error('For Overlap-Save block processing, the number of samples per FFT block (nFFT) must be specified!');
    end
end

%% Define Input Parameters
nSamples = PARAM.nSamples;                                                  % number of symbols
tWindow = PARAM.tWindow;                                                    % original time window [s]
sampRate = PARAM.sampRate;                                                  % original sampling rate [Hz]

%% Overlap-Save Configuration
nMainBlocks = nSamples/DFT.nFFT;
nSamples = DFT.nFFT;
nAuxBlocks = nMainBlocks-1;
nBlocks = nMainBlocks + nAuxBlocks;
overlapSize = nSamples/4;
tWindow = tWindow/nMainBlocks;
f = (-nSamples/2:nSamples/2-1)*(1/tWindow);

%% Output Struct
OS.nMainBlocks = nMainBlocks;
OS.nAuxBlocks = nAuxBlocks;
OS.nBlocks = nBlocks;
OS.nSamples = nSamples;
OS.overlapSize = overlapSize;
OS.sampRate = sampRate;
OS.tWindow = tWindow;
OS.f = f;

DFT.PARAM = OS;
DFT.nTotalBlocks = nBlocks;

