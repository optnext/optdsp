function Aout_FFT = NoOverlap_FFTsplit(Aout,DFT,MSG)

% Last Update: 17/11/2016


global PROG;

%% Input Parameters
nSamples    = DFT.PARAM.nSamples;
nBlocks     = DFT.PARAM.nBlocks;
nFFT        = DFT.nFFT;

%% Apply FFT Split
Aout_FFT = zeros(nBlocks,nFFT);
for n = 1:nBlocks
    Aout_FFT(n,:) = Aout((n-1)*nSamples+1 : n*nSamples);
end

%% Command Window Message
if PROG.showMessagesLevel >= 2
    if MSG.displayMsg
        fprintf('------------------------------------------------------------------');
        fprintf('\nFFT Split - No Overlap:\n');
        fprintf('Timestamp: %s\n',datestr(clock));
        fprintf('Splitted %1.0f samples into %1.0f FFT blocks containing %1.0f samples/block\n',length(A),nBlocks,nFFT);
        if isfield(MSG,'message')
            for n=1:length(MSG.message)
                fprintf(MSG.message{n});
            end
        end
        fprintf('------------------------------------------------------------------\n');
    end
end
