function [A] = FFTjoin(A_FFT,DFT,MSG)

% Last Update: 03/02/2014


%% Input Parser
if nargin == 2
    MSG.displayMsg = 0;
else
    MSG.displayMsg = 1;
end

%% FFT Rejoining
switch DFT.blockProc
    case {'Overlap-Save','OverlapSave','OS'},
        A = OverlapSave_FFTjoin(A_FFT,DFT.PARAM,MSG);
    case {'Overlap-Add','OverlapAdd','OA'},
        A = OverlapAdd_FFTjoin(A_FFT,DFT.PARAM,MSG);
    case {'No-Overlap','NoOverlap','none',''},
        A = NoOverlap_FFTjoin(A_FFT,DFT.PARAM,MSG);
    otherwise,
        error('Invalid DFT Block-Processing Strategy!');
end


