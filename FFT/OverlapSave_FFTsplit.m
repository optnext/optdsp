function A_FFT = OverlapSave_FFTsplit(A,DFT,MSG)

% Last Update: 16/04/2017


global PROG;

%% Input Parameters
nBlocks = DFT.PARAM.nBlocks;                                                % number of FFT blocks
nFFT    = DFT.PARAM.nSamples;                                               % number of samples per FFT block

%% Apply FFT Split
nUp     = 0;
nLow    = 0;
A_FFT   = zeros(nBlocks,nFFT);
for n = 1:nBlocks
    if mod(n,2) 
        % Upper FFT block layer:
        nUp         = nUp + 1;
        ni          = (nUp-1)*nFFT + 1;
        nf          = nUp*nFFT;
        A_FFT(n,:)  = A(ni:nf);
    elseif n<nBlocks
        % Lower FFT block layer:
        nLow        = nLow + 1;
        ni          = (nLow-1)*nFFT + nFFT/2 + 1;
        nf          = ni + nFFT - 1;
        A_FFT(n,:)  = A(ni:nf);
    end          
end

%% Command Window Message
if PROG.showMessagesLevel >= 2
    if MSG.displayMsg
        fprintf('------------------------------------------------------------------');
        fprintf('\nFFT Split - Overlap-Save:\n');
        fprintf('Timestamp: %s\n',datestr(clock));
        fprintf('Splitted %1.0f samples into %1.0f FFT blocks containing %1.0f samples/block\n',length(A),nBlocks,nFFT);
        if isfield(MSG,'message')
            for n=1:length(MSG.message)
                fprintf(MSG.message{n});
            end
        end
        fprintf('------------------------------------------------------------------\n');
    end
end
