function [A_FFT,DSP]=OverlapSaveCyclic_FFTsplit(A,DSP,PARAM)

% Last Update: 05/06/2012


%% Define Overlap-Save FFT Split Parameters
nBlocks=DSP.PARAM.nBlocks;      % number of FFT blocks
nFFT=DSP.PARAM.blockSize;       % number of samples per FFT block
tin=PARAM.t;                    % input time vector (before FFT splitting)

%% Apply FFT Split
nMain=0;
nAux=0;
A_FFT=zeros(nBlocks,nFFT);
tout=zeros(nBlocks,nFFT);
for n=1:nBlocks
    if mod(n,2)
        FFT_mainBlock=0;
    else
        FFT_mainBlock=1;
    end
    if FFT_mainBlock
        nMain=nMain+1;
        ni=(nMain-1)*nFFT+1;
        nf=nMain*nFFT;
        tout(n,:)=tin(ni:nf);
        A_FFT(n,:)=A(ni:nf);
    else
        nAux=nAux+1;
        if nAux==1
            ni=nFFT/2-1;
            nf=nFFT/2;
            t1=tin(end-ni:end);
            t2=tin(1:nf);
            tout(n,:)=[t1,t2];
            A1=A(end-ni:end);
            A2=A(1:nf);
            A_FFT(n,:)=[A1, A2];
        else
            ni=(nAux-2)*nFFT+nFFT/2+1;
            nf=ni+nFFT-1;
            tout(n,:)=tin(ni:nf);
            A_FFT(n,:)=A(ni:nf);
        end
    end          
end

%% Define Output Time Vector
DSP.PARAM.t=tout;


