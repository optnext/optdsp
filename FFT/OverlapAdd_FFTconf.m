function [DSP]=OverlapAdd_FFTconf(DSP,PARAM)

% Last Update: 10/05/2012

%% Input Arguments Validation
if ~isfield(DSP,'nFFTblocks')
    DSP.nFFTblocks=1;
    warning('OverlapAdd_Conf:nFFTblocksNotDefined',...
        'The number of FFT blocks has not been defined by the user');
    warning('OverlapAdd_Conf:nFFTblocksDefault',...
        'DSP.nFFTblocks=1 has been defined by default');
end

%% FFT Configuration
OS_FFT.nBlocks=DSP.nFFTblocks;
OS_FFT.nBits=PARAM.nBits/OS_FFT.nBlocks;
OS_FFT.nSyms=OS_FFT.nBits/log2(PARAM.M);
OS_FFT.mainBlockSize=OS_FFT.nBits*PARAM.nBitSamp;
OS_FFT.mainBlockTwindow=OS_FFT.mainBlockSize*PARAM.dt;
OS_FFT.zeroPadSize=OS_FFT.mainBlockSize/4;
OS_FFT.zeroPadTwindow=OS_FFT.zeroPadSize*PARAM.dt;
OS_FFT.totalBlockSize=OS_FFT.mainBlockSize+...
    OS_FFT.zeroPadSize;

OS_FFT.tWindow=OS_FFT.mainBlockTwindow+...
    OS_FFT.zeroPadTwindow;
OS_FFT.sampRate=PARAM.sampRate;
OS_FFT.symRate=PARAM.symRate;
OS_FFT.nTotalSamp=OS_FFT.totalBlockSize;
OS_FFT.f=(-OS_FFT.nTotalSamp/2:...
    OS_FFT.nTotalSamp/2-1)*(1/(OS_FFT.tWindow));
OS_FFT.omega=2*pi*OS_FFT.f;


DSP.nTotalBlocks=OS_FFT.nBlocks;
DSP.PARAM=OS_FFT;


