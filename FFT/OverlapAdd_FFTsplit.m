function [Aout_FFT,DSP]=OverlapAdd_FFTsplit(Aout,DSP,PARAM)

% Last Update: 12/07/11

OS_FFT=DSP.PARAM;
Aout_FFT=zeros(OS_FFT.nBlocks,OS_FFT.nTotalSamp);
OS_FFT.bitStream=zeros(OS_FFT.nBlocks,OS_FFT.nBits);
OS_FFT.t=zeros(OS_FFT.nBlocks,OS_FFT.nTotalSamp);
for n=1:OS_FFT.nBlocks-1
    ni=(n-1)*OS_FFT.mainBlockSize+1;
    nx=n*OS_FFT.mainBlockSize;
    nf=n*OS_FFT.mainBlockSize+OS_FFT.zeroPadSize;
    OS_FFT.t(n,:)=PARAM.t(ni:nf);
    Aout_FFT(n,:)=[Aout(ni:nx) zeros(1,OS_FFT.zeroPadSize)];
end

%Last FFT Block:
ni=PARAM.nTotalSamp-OS_FFT.mainBlockSize+1;
OS_FFT.t(n,:)=[PARAM.t(ni:end),...
    PARAM.t(1:OS_FFT.zeroPadSize)];
Aout_FFT(n,:)=[Aout(ni:end),...
    zeros(1,OS_FFT.zeroPadSize)];


DSP.PARAM=OS_FFT;




