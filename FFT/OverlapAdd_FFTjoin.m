function [Acomp]=OverlapAdd_FFTjoin(Acomp_FFT,PARAM)


% Last Update: 22/06/11

Acomp=[];

Acomp=Acomp_FFT(1:PARAM.mainBlockSize);
for n=2:PARAM.nBlocks
    OverlapAdd=Acomp_FFT(n-1,PARAM.mainBlockSize+1:end)+...
        Acomp_FFT(n,1:PARAM.zeroPadSize);
    Acomp=[Acomp, OverlapAdd, ...
        Acomp_FFT(n,PARAM.zeroPadSize+1:PARAM.mainBlockSize)];
end