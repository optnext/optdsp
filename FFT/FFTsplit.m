function [A_FFT,DFT] = FFTsplit(A,DFT,MSG)

% Last Update: 03/02/2014


%% Input Parser
if nargin == 2
    MSG.displayMsg = 0;
else
    MSG.displayMsg = 1;
end

%% FFT Block Processing - FFT Splitting
switch DFT.blockProc
    case {'Overlap-Save','OverlapSave','OS'},
        A_FFT = OverlapSave_FFTsplit(A,DFT,MSG);
    case {'Overlap-Add','OverlapAdd','OA'},
        A_FFT = OverlapAdd_FFTsplit(A,DFT,MSG);
    case {'No-Overlap','NoOverlap','none',''},
        A_FFT = NoOverlap_FFTsplit(A,DFT,MSG);
    otherwise,
        error('Invalid DFT Block-Processing Strategy!');
end


