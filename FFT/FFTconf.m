function [DFT] = FFTconf(DFT,PARAM)

% Last Update: 02/01/2015


%% Input Parser
if ~isfield(DFT,'blockProc')
    DFT.blockProc = 'Overlap-Save';
end

%% FFT Block Processing - Configuration
switch DFT.blockProc
    case {'Overlap-Save','OverlapSave','OS'},
        DFT = OverlapSave_FFTconf(DFT,PARAM);
    case {'Overlap-Add','OverlapAdd','OA'},
        DFT = OverlapAdd_FFTconf(DFT,PARAM);
    case {'No-Overlap','NoOverlap','none',''},
        DFT = NoOverlap_FFTconf(DFT,PARAM);
    otherwise,
        error('Invalid DFT Block-Processing Strategy!');
end

