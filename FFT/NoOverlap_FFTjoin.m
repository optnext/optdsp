function A = NoOverlap_FFTjoin(A_FFT,PARAM,MSG)

% Last Update: 17/11/2016


global PROG;

%% Input Parameters
nBlocks = PARAM.nBlocks;        % number of FFT blocks
nFFT    = PARAM.nSamples;       % number of samples per FFT block

%% Apply FFT Join
A = [];
for n = 1:nBlocks
    A = [A,A_FFT(n,:)];
end

%% Command Window Message
if PROG.showMessagesLevel >= 2
    if MSG.displayMsg
        fprintf('------------------------------------------------------------------');
        fprintf('\nFFT Join - No Overlap:\n');
        fprintf('Timestamp: %s\n',datestr(clock));
        fprintf('Joined %1.0f FFT blocks of %1.0f samples/block into a unique FFT block of %1.0f samples\n',nBlocks,nFFT,length(A));
        if isfield(MSG,'message')
            for n=1:length(MSG.message)
                fprintf(MSG.message{n});
            end
        end
        fprintf('------------------------------------------------------------------\n');
    end
end

