function A = OverlapSave_FFTjoin(A_FFT,PARAM,MSG)

% Last Update: 17/11/2016


global PROG;

%% Input Parameters
nBlocks     = PARAM.nBlocks;                                                % number of FFT blocks
nMainBlocks = PARAM.nMainBlocks;                                            % number of main FFT blocks
nFFT        = PARAM.nSamples;                                               % number of samples per FFT block
nOverlap    = PARAM.overlapSize;                                            % number of samples defined as overlap

%% Apply FFT Join
A = zeros(1,nMainBlocks*nFFT);
if nBlocks == 1
    A = A_FFT;
else
    for n = 1:nBlocks
        ni = (n-1)*nFFT/2 + nFFT/4 + 1;
        nf = ni + nFFT/2 - 1;
        A(ni:nf) = A_FFT(n,nOverlap+1:end-nOverlap);
    end
    % Avoid inter-block interference in the bit-sequence edges:
    A(1:nOverlap) = 0;
    A(end-nOverlap+1:end) = 0;
end

%% Command Window Message
if PROG.showMessagesLevel >= 2
    if MSG.displayMsg
        fprintf('------------------------------------------------------------------');
        fprintf('\nFFT Join - Overlap-Save:\n');
        fprintf('Timestamp: %s\n',datestr(clock));
        fprintf('Joined %1.0f FFT blocks of %1.0f samples/block into a unique FFT block of %1.0f samples\n',nBlocks,nFFT,length(A));
        fprintf('FFT overlap is %1.0f samples, corresponding to %1.2f %% of the FFT block-size\n',nOverlap,nOverlap/nFFT*100);
        if isfield(MSG,'message')
            for n=1:length(MSG.message)
                fprintf(MSG.message{n});
            end
        end
        fprintf('------------------------------------------------------------------\n');
    end
end

