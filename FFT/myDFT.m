function [S]=myDFT(A)



nSamples=length(A);

S=zeros(1,nSamples);
for k=1:nSamples
    for n=1:nSamples
        S(k)=S(k)+A(n)*exp(-2i*pi*(n-1)*(k-1)/nSamples);
    end
end
