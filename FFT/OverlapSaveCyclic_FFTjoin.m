function [A]=OverlapSaveCyclic_FFTjoin(A_FFT,PARAM)

% Last Update: 05/06/2012


%% Define Overlap-Save FFT Join Parameters
nBlocks=PARAM.nBlocks;          % number of FFT blocks
nMainBlocks=PARAM.nMainBlocks;  % number of main FFT blocks
nFFT=PARAM.nTotalSamp;          % number of samples per FFT block
nOverlap=PARAM.overlapSize;     % number of samples defined as overlap

%% Apply FFT Join
A=zeros(1,nMainBlocks*nFFT);
if nBlocks==1
    A=A_FFT;
else
    A_begin=A_FFT(1,nFFT/2+1:end-nOverlap);
    A_end=A_FFT(1,nOverlap+1:nFFT/2);
    A(1:length(A_begin))=A_begin;
    for n=2:nBlocks
        ni=(n-2)*nFFT/2+nFFT/4+1;
        nf=ni+nFFT/2-1;
        A(ni:nf)=A_FFT(n,nOverlap+1:end-nOverlap);
    end    
    A(end-nOverlap+1:end)=A_end;
end
