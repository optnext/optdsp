function Sout = deskewRx(Sin,f,delay_ps)

% Last Update: 27/07/2017


%% Input Parser
if isstruct(f)
    PARAM = f;
    f = PARAM.f;
end
if isstruct(delay_ps)
    DESKEW = delay_ps;
    delay_ps = DESKEW.delay_ps;
end

%% Input Parameters
% Input Signal:
Xi = real(Sin(1,:));
Xq = imag(Sin(1,:));
Yi = real(Sin(2,:));
Yq = imag(Sin(2,:));
% Simulation Parameters:
f = fftshift(f);
% Delay Parameters;
delay = delay_ps*1e-12;

%% Apply Deskew
X = fft(Xi);
H = exp(-1j*2*pi*f*delay(1));
Xi = real(ifft(X.*H));

X = fft(Xq);
H = exp(-1j*2*pi*f*delay(2));
Xq = real(ifft(X.*H));

X = fft(Yi);
H = exp(-1j*2*pi*f*delay(3));
Yi = real(ifft(X.*H));

X = fft(Yq);
H = exp(-1j*2*pi*f*delay(4));
Yq = real(ifft(X.*H));

%% Output Signal
Sout = [Xi + 1j*Xq; Yi + 1j*Yq];
