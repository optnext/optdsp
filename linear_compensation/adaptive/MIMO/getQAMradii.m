function [radius] = getQAMradii(modFormat)

% Last Update: 02/03/2017


%% Select CMA Radii
switch modFormat
    case {'QPSK','DP-QPSK','PM-QPSK','8PSK','PM-8PSK'}
        radius = 1;
    case {'8QAM','DP-8QAM','PM-8QAM','PM8QAM','DP8QAM'}
        radius = [1 sqrt(5)/5];
    case {'16QAM','DP-16QAM','PM-16QAM'}
        radius = [1 sqrt(5)/3 1/3];
    case {'32QAM','DP-32QAM','PM-32QAM'}
        radius = sqrt([34 26 18 10 2]/34);
    case {'36QAM','DP-36QAM','PM-36QAM'}
        radius = sqrt([50 34 26 18 10 2])/sqrt(50);
    case {'64QAM','DP-64QAM','PM-64QAM','PS:PM-64QAM'}
        radius = sqrt([98 74 58 50 34 26 18 10 2])/sqrt(98);
    case {'128QAM','DP-128QAM','PM-128QAM','PM128QAM','DP128QAM'}
        radius = sqrt([170 146 130 122 106 98 90 82 74 58 50 34 26 18 10 2])/sqrt(170);
    otherwise
        error('Unsupported modulation format!');
end
