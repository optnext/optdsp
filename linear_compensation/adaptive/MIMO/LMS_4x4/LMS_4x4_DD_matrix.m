function [Y,W] = LMS_4x4_DD_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,symMap,symIndex,IQmap)

% Last Update: 17/05/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(zeros(4,nSamples));
B           = complex(zeros(2,1));

%% Run CMA
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)   
    % Create a 1xN vector with all signals:
    U   = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)];
    
    % MIMO 4x4 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        A = [Y(1,n) + 1j*Y(2,n); Y(3,n) + 1j*Y(4,n)];
        
        % Slicer:
        error   = abs(IQmap - A(1));
        index   = find(error == min(error));
        B(1,1)  = IQmap(symIndex(symMap(index(1))+1));
        error   = abs(IQmap - A(2));
        index   = find(error == min(error));
        B(2,1)  = IQmap(symIndex(symMap(index(1))+1));
        
        F       = [real(B(1,:)); imag(B(1,:));...
                   real(B(2,:)); imag(B(2,:))];
        
        % Calculate error:
        err(:,n)  = F - Y(:,n);

        % Update taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
        W(:,3) = W(:,3) + mu * U' * err(3,n);
        W(:,4) = W(:,4) + mu * U' * err(4,n);
    end
    
    % Update Index:
    ind = ind + 1;
end
