function [W,err,F] = updateDD_4x4(W,Ain,Aout,MIMO)

% Last Update: 06/04/2016


%% Input Parameters
% Equalizer input signal:
Xi          = Ain(1,:);
Xq          = Ain(2,:);
Yi          = Ain(3,:);
Yq          = Ain(4,:);
% Equalizer output signal:
XiOut       = Aout(1,1);
XqOut       = Aout(2,1);
YiOut       = Aout(3,1);
YqOut       = Aout(4,1);
Xout        = XiOut + 1j*XqOut;
Yout        = YiOut + 1j*YqOut;
% MIMO parameters:
mu          = MIMO.current.stepSize;
crossPol    = MIMO.crossPol;

%% Calculate Error Function
Fx = symbol2signal(signal2symbol(Xout,MIMO.QAM),MIMO.QAM);
Fy = symbol2signal(signal2symbol(Yout,MIMO.QAM),MIMO.QAM);
errX = Fx - Xout;
errY = Fy - Yout;

%% Update Taps
W(1,:)  = W(1,:)+mu*real(errX)*Xi;
W(2,:)  = W(2,:)+mu*real(errX)*Xq;
if crossPol
    W(3,:)  = W(3,:)+mu*real(errX)*Yi;
    W(4,:)  = W(4,:)+mu*real(errX)*Yq;
end
W(5,:)  = W(5,:)+mu*imag(errX)*Xi;
W(6,:)  = W(6,:)+mu*imag(errX)*Xq;
if crossPol
    W(7,:)  = W(7,:)+mu*imag(errX)*Yi;
    W(8,:)  = W(8,:)+mu*imag(errX)*Yq;
    W(9,:)  = W(9,:)+mu*real(errY)*Xi;
    W(10,:) = W(10,:)+mu*real(errY)*Xq;
end
W(11,:) = W(11,:)+mu*real(errY)*Yi;
W(12,:) = W(12,:)+mu*real(errY)*Yq;
if crossPol
    W(13,:) = W(13,:)+mu*imag(errY)*Xi;
    W(14,:) = W(14,:)+mu*imag(errY)*Xq;
end
W(15,:) = W(15,:)+mu*imag(errY)*Yi;
W(16,:) = W(16,:)+mu*imag(errY)*Yq;

%% Output Parameters
err = [errX; errY];
F   = [Fx; Fy];
