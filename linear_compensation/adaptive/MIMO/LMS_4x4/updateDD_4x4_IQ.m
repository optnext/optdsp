function [W,err,F]=updateDD_4x4_IQ(W,Ain,Aout,MIMO)

% Last Update: 10/06/2015


%% Input Parameters
XiOut=Aout(1,1);
XqOut=Aout(2,1);
YiOut=Aout(3,1);
YqOut=Aout(4,1);

AxOut=XiOut+1j*XqOut;
AyOut=YiOut+1j*YqOut;

Xi=Ain(1,:);
Xq=Ain(2,:);
Yi=Ain(3,:);
Yq=Ain(4,:);

mu=MIMO.current.stepSize;

%% Calculate Error Function
Fx=symbol2signal(signal2symbol(AxOut,MIMO.QAM),MIMO.QAM);
Fy=symbol2signal(signal2symbol(AyOut,MIMO.QAM),MIMO.QAM);
errX=Fx-AxOut;
errY=Fy-AyOut;

%% Update Taps
W(1,:)=W(1,:)+mu*real(errX)*Xi;
W(2,:)=W(2,:)+mu*real(errX)*Xq;
W(3,:)=W(3,:)+mu*imag(errX)*Xi;
W(4,:)=W(4,:)+mu*imag(errX)*Xq;

W(5,:)=W(5,:)+mu*real(errY)*Yi;
W(6,:)=W(6,:)+mu*real(errY)*Yq;
W(7,:)=W(7,:)+mu*imag(errY)*Yi;
W(8,:)=W(8,:)+mu*imag(errY)*Yq;

%% Output Parameters
err=[errX; errY];
F=[Fx; Fy];
