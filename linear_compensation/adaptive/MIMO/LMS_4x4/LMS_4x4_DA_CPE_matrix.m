function [Y,W,Wp] = LMS_4x4_DA_CPE_matrix(X,F,W,Wp,mu,upRate,flagCPE)

% Last Update: 07/03/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(zeros(4,nSamples));
nTaps       = size(W,1)/4;

%% Run CMA
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)];
    
    % MIMO 4x4 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if rem(n,upRate) == 0
        % CPE:
        A = [Y(1,n) + 1j*Y(2,n); Y(3,n) + 1j*Y(4,n)];
        B = [F(1,n) + 1j*F(2,n); F(3,n) + 1j*F(4,n)];
        phi0 = exp(-1j*(angle(A) - angle(B)));
        Wp   = [phi0 Wp(:,1:end-1)];
        
        if flagCPE
            A       = A .* sign(sum(Wp,2));
            Y(:,n)  = [real(A(1,:)); imag(A(1,:));...
                       real(A(2,:)); imag(A(2,:))];
            % Calculate error:
            err(:,n)  = F(:,n) - Y(:,n);
        else
            B       = B .* conj(sign(sum(Wp,2)));
            B_real  = [real(B(1,:)); imag(B(1,:));...
                       real(B(2,:)); imag(B(2,:))];
            % Calculate error:
            err(:,n)  = B_real - Y(:,n);
        end
        
        % Update CMA Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
        W(:,3) = W(:,3) + mu * U' * err(3,n);
        W(:,4) = W(:,4) + mu * U' * err(4,n);
    end
    
    % Update Index:
    ind = ind + 1;
end

