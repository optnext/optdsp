function [Y,W] = LMS_4x4_DA_matrix(X,W,nTaps,mu,updateRate,updateOffset,F)

% Last Update: 17/05/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(zeros(4,nSamples));

%% Run CMA
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)];
    
    % MIMO 4x4 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % Calculate error:
        err(:,n)  = F(:,n) - Y(:,n);

        % Update CMA Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
        W(:,3) = W(:,3) + mu * U' * err(3,n);
        W(:,4) = W(:,4) + mu * U' * err(4,n);
    end
    
    W(2*nTaps+1:end,1) = 0;
    W(2*nTaps+1:end,2) = 0;
    W(1:2*nTaps,3) = 0;
    W(1:2*nTaps,4) = 0;
    
    % Update Index:
    ind = ind + 1;
end


%% Debug
itblue = [0 0.24 0.431];
itred = [0.73 0.07 0.169];
figure; 
hPlot(1) = plot(movmean(abs(err(1,:)),1000,'omitnan')); 
hold on; 
hPlot(2) = plot(movmean(abs(err(2,:)),1000,'omitnan'));

set(hPlot(1),'Color',itblue);
set(hPlot(2),'Color',itred);

xlabel('Sample Index','Interpreter','latex');
ylabel('Error','Interpreter','latex');
grid on;

xAxis = get(gca,'xAxis');
yAxis = get(gca,'yAxis');
set(xAxis,'TickLabelInterpreter','latex','FontSize',13);
set(yAxis,'TickLabelInterpreter','latex','FontSize',13);
set(gca,'GridLineStyle','--');

axis([-inf inf -inf inf]);

% Annotations:
hText = text(0.95*numel(err(1,:)),0.95,{['update every ',...
    num2str(updateRate),' samples']});
set(hText,'Interpreter','latex','HorizontalAlignment','right',...
    'VerticalAlignment','top','Color',itblue,...
    'EdgeColor',itblue,...
    'BackgroundColor','w','FontSize',12);


'x'