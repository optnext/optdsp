function [W,err] = updateNLMS_4x4(W,Ain,Aout,MIMO,tx)

% Last Update: 08/03/2016


%% Input Parameters
% Equalizer input signal:
Xi          = Ain(1,:);
Xq          = Ain(2,:);
Yi          = Ain(3,:);
Yq          = Ain(4,:);
% Equalizer output signal:
XiOut       = Aout(1,1);
XqOut       = Aout(2,1);
YiOut       = Aout(3,1);
YqOut       = Aout(4,1);
Xout        = XiOut + 1j*XqOut;
Yout        = YiOut + 1j*YqOut;
% Target Signal:
txX         = tx(1,:);
txY         = tx(2,:);
% MIMO parameters:
mu          = MIMO.current.stepSize;
crossPol    = MIMO.crossPol;

%% Calculate Error Function
errX = txX - Xout;
errY = txY - Yout;

%% Update Taps
W(1,:) = W(1,:) + mu*real(errX)*Xi/sum(abs(Xi).^2);
W(2,:) = W(2,:) + mu*real(errX)*Xq/sum(abs(Xq).^2);
if crossPol
    W(3,:) = W(3,:) + mu*real(errX)*Yi/sum(abs(Yi).^2);
    W(4,:) = W(4,:) + mu*real(errX)*Yq/sum(abs(Yq).^2);
end
W(5,:) = W(5,:) + mu*imag(errX)*Xi/sum(abs(Xi).^2);
W(6,:) = W(6,:) + mu*imag(errX)*Xq/sum(abs(Xq).^2);
if crossPol
    W(7,:)  = W(7,:)  + mu*imag(errX)*Yi/sum(abs(Yi).^2);
    W(8,:)  = W(8,:)  + mu*imag(errX)*Yq/sum(abs(Yq).^2);
    W(9,:)  = W(9,:)  + mu*real(errY)*Xi/sum(abs(Xi).^2);
    W(10,:) = W(10,:) + mu*real(errY)*Xq/sum(abs(Xq).^2);
end
W(11,:) = W(11,:) + mu*real(errY)*Yi/sum(abs(Yi).^2);
W(12,:) = W(12,:) + mu*real(errY)*Yq/sum(abs(Yq).^2);
if crossPol
    W(13,:) = W(13,:) + mu*imag(errY)*Xi/sum(abs(Xi).^2);
    W(14,:) = W(14,:) + mu*imag(errY)*Xq/sum(abs(Xq).^2);
end
W(15,:) = W(15,:) + mu*imag(errY)*Yi/sum(abs(Yi).^2);
W(16,:) = W(16,:) + mu*imag(errY)*Yq/sum(abs(Yq).^2);

%% Output Parameters
err = [errX; errY];

