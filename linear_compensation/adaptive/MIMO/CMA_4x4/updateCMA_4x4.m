function [W,err,F] = updateCMA_4x4(W,Ain,Aout,CMA,tx)

% Last Update: 16/03/2016


%% Input Parameters
% Equalizer input signal:
Xi      = Ain(1,:);
Xq      = Ain(2,:);
Yi      = Ain(3,:);
Yq      = Ain(4,:);
% Equalizer output signal:
XiOut   = Aout(1,1);
XqOut   = Aout(2,1);
YiOut   = Aout(3,1);
YqOut   = Aout(4,1);
Xout    = XiOut + 1j*XqOut;
Yout    = YiOut + 1j*YqOut;
% MIMO parameters:
nTaps           = CMA.nTaps;
mu              = CMA.current.stepSize;
updateRule      = CMA.current.updateRule;
constrainedCMA  = CMA.current.constrained;
crossPol        = CMA.crossPol;

%% Determine Target Signal
switch updateRule
    case 'LMS',
        Fx      = abs(tx(1)).^2;
        Fy      = abs(tx(2)).^2;
    case {'CMA','QPSK'},
        radius  = CMA.current.radius;
        Fx      = radius(1);
        Fy      = radius(1);
    case 'MMA',
        radius  = CMA.current.radius;
        Fx      = radius(1)/2;
        Fy      = radius(1)/2;
    case 'MMA-DD',
        F       = symbol2signal(signal2symbol(Aout,CMA.QAM),CMA.QAM);
        Fx      = real(F).^2;
        Fy      = imag(F).^2;
    case {'RDE','16QAM','64QAM','blind'},
        radius  = CMA.current.radius;
        [~,rX]  = min(abs(radius-abs(Xout)));
        Fx      = radius(rX)^2;
        [~,rY]  = min(abs(radius-abs(Yout)));
        Fy      = radius(rY)^2;
    case {'static','noUpdate'},
        err     = [0; 0];
        F       = [0; 0];
        return;
    otherwise,
        error('Unsupported CMA update rule!');
end

%% Calculate Error Function
errX = Fx - abs(Xout).^2;
errY = Fy - abs(Yout).^2;

%% Update CMA Taps
W(1,:) = W(1,:) + mu*errX*XiOut*Xi;
W(2,:) = W(2,:) + mu*errX*XiOut*Xq;
if crossPol
    W(3,:) = W(3,:) + mu*errX*XiOut*Yi;
    W(4,:) = W(4,:) + mu*errX*XiOut*Yq;
end
W(5,:) = W(5,:) + mu*errX*XqOut*Xi;
W(6,:) = W(6,:) + mu*errX*XqOut*Xq;
if crossPol
    W(7,:) = W(7,:) + mu*errX*XqOut*Yi;
    W(8,:) = W(8,:) + mu*errX*XqOut*Yq;
end

if constrainedCMA
    W(9,:)  = -fliplr(W(8,:));
    W(14,:) = -fliplr(W(3,:));
    W(13,:) = zeros(1,nTaps);
    W(10,:) = zeros(1,nTaps);
    
    W(11,:) = fliplr(W(6,:));
    W(16,:) = fliplr(W(1,:));
    W(15,:) = zeros(1,nTaps);
    W(12,:) = zeros(1,nTaps);
else
    if crossPol
        W(9,:)  = W(9,:)  + mu*errY*YiOut*Xi;
        W(10,:) = W(10,:) + mu*errY*YiOut*Xq;
    end
    W(11,:) = W(11,:) + mu*errY*YiOut*Yi;
    W(12,:) = W(12,:) + mu*errY*YiOut*Yq;
    if crossPol
        W(13,:) = W(13,:) + mu*errY*YqOut*Xi;
        W(14,:) = W(14,:) + mu*errY*YqOut*Xq;
    end
    W(15,:) = W(15,:) + mu*errY*YqOut*Yi;
    W(16,:) = W(16,:) + mu*errY*YqOut*Yq;
end

%% Output Parameters
err = [errX; errY];
F   = [Fx; Fy];
