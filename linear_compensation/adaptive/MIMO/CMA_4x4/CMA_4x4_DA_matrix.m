function [Y,W] = CMA_4x4_DA_matrix(X,W,nTaps,mu,updateRate,updateOffset,F)

% Last Update: 17/05/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(zeros(4,nSamples));

%% Run CMA
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)];
    
    % MIMO 4x4 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % Determine absolute value of output signal:
        R = [Y(1,n)^2 + Y(2,n)^2; Y(1,n)^2 + Y(2,n)^2;...
             Y(3,n)^2 + Y(4,n)^2; Y(3,n)^2 + Y(4,n)^2];

        % Calculate error:
        err(:,n)  = (F(:,n) - R).*Y(:,n);

        % Update CMA Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
        W(:,3) = W(:,3) + mu * U' * err(3,n);
        W(:,4) = W(:,4) + mu * U' * err(4,n);
    end
    
    % Update Index:
    ind = ind + 1;
end
