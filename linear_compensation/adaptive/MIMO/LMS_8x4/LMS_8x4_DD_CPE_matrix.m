function [Y,W,W_CPE] = LMS_8x4_DD_CPE_matrix(X,W,W_CPE,nTaps,mu,updateRate,...
    updateOffset,symMap,symIndex,IQmap)

% Last Update: 02/05/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(single(complex(zeros(4,nSamples))));
DD          = complex(zeros(4,1));

%% Run CMA 8x8
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)...
         X(5,ind) X(6,ind) X(7,ind) X(8,ind)];
    
    % MIMO 8x8 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % Slicer:
        phi0    = sign(mean(W_CPE,2));
        Y_slice = Y(:,n) .* phi0;
        error   = abs(IQmap - Y_slice(1));
        ind     = find(error == min(error));
        DD(1)   = IQmap(symIndex(symMap(ind(1))+1));
        error   = abs(IQmap - Y_slice(2));
        ind     = find(error == min(error));
        DD(2)   = IQmap(symIndex(symMap(ind(1))+1));
        error   = abs(IQmap - Y_slice(3));
        ind     = find(error == min(error));
        DD(3)   = IQmap(symIndex(symMap(ind(1))+1));
        error   = abs(IQmap - Y_slice(4));
        ind     = find(error == min(error));
        DD(4)   = IQmap(symIndex(symMap(ind(1))+1));
        
        % CPE:
        phi0    = exp(-1j*(angle(Y(:,n)) - angle(DD)));
        W_CPE   = [phi0 W_CPE(:,1:end-1)];
        Y(:,n)  = Y(:,n) .* sign(mean(W_CPE,2));
                
        % Calculate error:
        err(:,n)  = Y(:,n) - DD;
        
        % Update taps:
        W(:,1) = W(:,1) - mu * U' * err(1,n);
        W(:,2) = W(:,2) - mu * U' * err(2,n);
    	W(:,3) = W(:,3) - mu * U' * err(3,n);
    	W(:,4) = W(:,4) - mu * U' * err(4,n);
    end
end

