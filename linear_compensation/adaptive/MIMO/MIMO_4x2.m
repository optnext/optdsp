function [Srx,MIMO] = MIMO_4x2(Srx,Stx,QAM,MIMO)

% Last Update: 08/08/2017


%% Input Parser
if ~isempty(strfind(MIMO.method,'CMA'))
    MIMO_method = 'CMA';
elseif ~isempty(strfind(MIMO.method,'LMS'))
    MIMO_method = 'LMS';
end
updateRule = MIMO.current.updateRule;

%% Input Parameters:
% Equalizer input signal:
Xi = Srx(1,:);
Xq = Srx(2,:);
Yi = Srx(3,:);
Yq = Srx(4,:);
Srx = [Xi;Xq;Yi;Yq];
if strcmp(MIMO.precision,'single')
    Srx = single(Srx);
end
% Reference training signal:
switch MIMO_method
    case 'CMA'
        switch updateRule
            case 'DA'
                F = [abs(Stx(1,:)).^2;...
                     abs(Stx(2,:)).^2];
            case 'RDE'
                F = MIMO.current.radius;
        end
    case 'LMS'
        switch updateRule
            case 'DA'
                F = Stx;
            case 'DD'
                F = [];
        end
end
if strcmp(MIMO.precision,'single')
    F = single(F);
end

% MIMO Parameters:
nTaps = MIMO.nTaps;
mu = MIMO.current.stepSize;
upRate = MIMO.current.updateRate;
upOff = MIMO.current.updateOffset;
if MIMO.applyCPE
    nTapsCPE = MIMO.nTapsCPE;
end
if strcmp(updateRule,'DD')
    symMap = QAM.symbolMapping;
    symIndex = QAM.symbolIndex;
    IQmap = QAM.IQmap;
end

% Initialize Taps
if isfield(MIMO,'W')
    W = MIMO.W;
else
    W = complex(zeros(4*nTaps,2));
    if strcmp(MIMO_method,'CMA') || strcmp(updateRule,'DD')
        n = floor(nTaps/2) + 1;   % central tap
        W(n,1) = 1;
        W(n+nTaps,1) = 1;
        W(n+2*nTaps,2) = 1;
        W(n+3*nTaps,2) = 1;
    end
end
if MIMO.applyCPE
    if isfield(MIMO,'W_CPE')
        W_CPE = MIMO.W_CPE;
    else
        W_CPE = complex(single(ones(2,nTapsCPE)));
    end
end

if strcmp(MIMO.precision,'single')
    W = single(W);
    nTaps = single(nTaps);
    mu = single(mu);
    upRate = single(upRate);
    upOff = single(upOff);
    if MIMO.applyCPE
        W_CPE = single(W_CPE);
    end
    if strcmp(updateRule,'DD')
        symMap = single(symMap);
        symIndex = single(symIndex);
        IQmap = single(IQmap);
    end
end

%% Apply MIMO 4x2
if strcmp(MIMO_method,'CMA')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            % not implemented yet!
            [Srx,W] = CMA_4x2_DA_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_4x2_DA_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    elseif strcmp(updateRule,'RDE')
        if MIMO.mex
            % not implemented yet!
            [Srx,W] = CMA_4x2_RDE_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_4x2_RDE_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    end
elseif strcmp(MIMO_method,'LMS')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            if MIMO.applyCPE
                [Srx,W,W_CPE] = LMS_4x2_DA_CPE_matrix_mex(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,F);
                MIMO.W_CPE = W_CPE;
            else
                % not implemented yet!
                [Srx,W] = LMS_4x2_DA_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        else
            if MIMO.applyCPE
               [Srx,W,W_CPE] = LMS_4x2_DA_CPE_matrix(Srx,W,...
                   W_CPE,nTaps,mu,upRate,upOff,F);
               MIMO.W_CPE = W_CPE;
            else
                % not implemented yet!
                [Srx,W] = LMS_4x2_DA_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
            end
        end
    elseif strcmp(updateRule,'DD')
        if MIMO.mex
            if MIMO.applyCPE
                % not implemented yet!
                [Srx,W,MIMO.W_CPE] = LMS_4x2_DD_CPE_matrix_mex(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,symMap,symIndex,IQmap);
            else
                % not implemented yet!
                [Srx,W] = LMS_4x2_DD_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
            end
        else
            if MIMO.applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_4x2_DD_CPE_matrix(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,symMap,symIndex,IQmap);
            else
                % not implemented yet!
                [Srx,W] = LMS_4x2_DD_matrix(Srx,W,nTaps,mu,upRate,...
                    upOff,symMap,symIndex,IQmap);
            end
        end
    end
end

%% Output Parameters
MIMO.W = W;
MIMO.err = [];
MIMO.F = F;
