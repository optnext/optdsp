function [Y,W] = CMA_8x4_RDE_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,radius)

% Last Update: 02/05/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(complex(zeros(4,nSamples)));
F           = single(zeros(4,1));

%% Run CMA 8x8
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)...
         X(5,ind) X(6,ind) X(7,ind) X(8,ind)];
    
    % MIMO 8x4 (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % Determine the closest radius:
        R       = abs(Y(:,n)).^2;
        [~,r]   = min(abs(radius - sqrt(R(1))));
        F(1)    = radius(r)^2;
        [~,r]   = min(abs(radius - sqrt(R(2))));
        F(2)    = radius(r)^2;
        [~,r]   = min(abs(radius - sqrt(R(3))));
        F(3)    = radius(r)^2;
        [~,r]   = min(abs(radius - sqrt(R(4))));
        F(4)    = radius(r)^2;

        % Calculate error:
        err(:,n) = (F - R).*Y(:,n);
        
        % Update CMA taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    	W(:,3) = W(:,3) + mu * U' * err(3,n);
    	W(:,4) = W(:,4) + mu * U' * err(4,n);
    end
end
