function [Y,W] = CMA_8x4_DA_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,F)

% Last Update: 02/05/2016


%% Input Parameters
nSamples = single(length(X));
[Y,err] = deal(single(complex(zeros(4,nSamples))));

%% Run CMA 8x8
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)...
         X(5,ind) X(6,ind) X(7,ind) X(8,ind)];
    
    % MIMO 8x4 (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)        
        % Calculate error:
        err(:,n)  = (F(:,n) - abs(Y(:,n)).^2).*Y(:,n);
        
        % Update CMA taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    	W(:,3) = W(:,3) + mu * U' * err(3,n);
    	W(:,4) = W(:,4) + mu * U' * err(4,n);
    end
end
