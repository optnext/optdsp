function [W,err,F] = updateCMA_8x4(W,Ain,Aout,MIMO,tx)

% Last Update: 20/04/2016


%% Input Parameters
% Equalizer input signal:
Xi1     = Ain(1,:);
Xq1     = Ain(2,:);
Xi2     = Ain(3,:);
Xq2     = Ain(4,:);
Yi1     = Ain(5,:);
Yq1     = Ain(6,:);
Yi2     = Ain(7,:);
Yq2     = Ain(8,:);
% Equalizer output signal:
Xout1   = Aout(1,1);
Xout2   = Aout(2,1);
Yout1   = Aout(3,1);
Yout2   = Aout(4,1);
% MIMO parameters:
% nTaps       = MIMO.nTaps;
mu          = MIMO.current.stepSize;
updateRule  = MIMO.current.updateRule;

%% Determine Target Signal
switch updateRule
    case 'LMS',
        Fx1     = abs(tx(1)).^2;
        Fy1     = abs(tx(2)).^2;
        Fx2     = abs(tx(3)).^2;
        Fy2     = abs(tx(4)).^2;
    case {'CMA','QPSK'},
        radius  = MIMO.current.radius;
        [Fx1,Fy1,Fx2,Fy2] = deal(radius(1));
    case 'MMA',
        radius  = MIMO.current.radius;
        [Fx1,Fy1,Fx2,Fy2] = deal(radius(1)/2);
    case {'RDE','16QAM','64QAM','blind'},
        radius  = MIMO.current.radius;
        [~,rX1] = min(abs(radius-abs(Xout1)));
        Fx1     = radius(rX1)^2;
        [~,rY1] = min(abs(radius-abs(Yout1)));
        Fy1     = radius(rY1)^2;
        [~,rX2] = min(abs(radius-abs(Xout2)));
        Fx2     = radius(rX2)^2;
        [~,rY2] = min(abs(radius-abs(Yout2)));
        Fy2     = radius(rY2)^2;
    case {'static','noUpdate'},
        [F,err] = deal([0; 0; 0; 0]);
        return;
    otherwise,
        error('Unsupported CMA update rule!');
end

%% Calculate Error Function
errX1   = Fx1 - abs(Xout1)^2;
errX2   = Fx2 - abs(Xout2)^2;
errY1   = Fy1 - abs(Yout1)^2;
errY2   = Fy2 - abs(Yout2)^2;

%% Update Taps
W(1,:)  = W(1,:) + mu*errX1*Xout1*conj(Xi1);
W(2,:)  = W(2,:) + mu*errX1*Xout1*conj(Xq1);
W(3,:)  = W(3,:) + mu*errX1*Xout1*conj(Yi1);
W(4,:)  = W(4,:) + mu*errX1*Xout1*conj(Yq1);
W(5,:)  = W(5,:) + mu*errX1*Xout1*conj(Xi2);
W(6,:)  = W(6,:) + mu*errX1*Xout1*conj(Xq2);
W(7,:)  = W(7,:) + mu*errX1*Xout1*conj(Yi2);
W(8,:)  = W(8,:) + mu*errX1*Xout1*conj(Yq2);

W(9,:)  = W(9,:)  + mu*errY1*Yout1*conj(Xi1);
W(10,:) = W(10,:) + mu*errY1*Yout1*conj(Xq1);
W(11,:) = W(11,:) + mu*errY1*Yout1*conj(Yi1);
W(12,:) = W(12,:) + mu*errY1*Yout1*conj(Yq1);
W(13,:) = W(13,:) + mu*errY1*Yout1*conj(Xi2);
W(14,:) = W(14,:) + mu*errY1*Yout1*conj(Xq2);
W(15,:) = W(15,:) + mu*errY1*Yout1*conj(Yi2);
W(16,:) = W(16,:) + mu*errY1*Yout1*conj(Yq2);

W(17,:) = W(17,:) + mu*errX2*Xout2*conj(Xi1);
W(18,:) = W(18,:) + mu*errX2*Xout2*conj(Xq1);
W(19,:) = W(19,:) + mu*errX2*Xout2*conj(Yi1);
W(20,:) = W(20,:) + mu*errX2*Xout2*conj(Yq1);
W(21,:) = W(21,:) + mu*errX2*Xout2*conj(Xi2);
W(22,:) = W(22,:) + mu*errX2*Xout2*conj(Xq2);
W(23,:) = W(23,:) + mu*errX2*Xout2*conj(Yi2);
W(24,:) = W(24,:) + mu*errX2*Xout2*conj(Yq2);

W(25,:) = W(25,:) + mu*errY2*Yout2*conj(Xi1);
W(26,:) = W(26,:) + mu*errY2*Yout2*conj(Xq1);
W(27,:) = W(27,:) + mu*errY2*Yout2*conj(Yi1);
W(28,:) = W(28,:) + mu*errY2*Yout2*conj(Yq1);
W(29,:) = W(29,:) + mu*errY2*Yout2*conj(Xi2);
W(30,:) = W(30,:) + mu*errY2*Yout2*conj(Xq2);
W(31,:) = W(31,:) + mu*errY2*Yout2*conj(Yi2);
W(32,:) = W(32,:) + mu*errY2*Yout2*conj(Yq2);

%% Output Parameters
err     = [errX1; errY1; errX2; errY2];
F       = [Fx1; Fy1; Fx2; Fy2];
