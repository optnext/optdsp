function [Srx,MIMO] = MIMO_4x4(Srx,Stx,QAM,MIMO)

% Last Update: 08/08/2017


%% Input Parser
if ~isempty(strfind(MIMO.method,'CMA'))
    MIMO_method = 'CMA';
elseif ~isempty(strfind(MIMO.method,'LMS'))
    MIMO_method = 'LMS';
end
updateRule = MIMO.current.updateRule;

%% Input Parameters:
% Equalizer input signal:
Xi = real(Srx(1,:));
Xq = imag(Srx(1,:));
Yi = real(Srx(2,:));
Yq = imag(Srx(2,:));
Srx = [Xi;Xq;Yi;Yq];
if strcmp(MIMO.precision,'single')
    Srx = single(Srx);
end
% Reference training signal:
switch MIMO_method
    case 'CMA'
        switch updateRule
            case 'DA'
                F = [abs(Stx(1,:)).^2;...
                     abs(Stx(1,:)).^2;...
                     abs(Stx(2,:)).^2;...
                     abs(Stx(2,:)).^2];
            case 'RDE'
                F = MIMO.current.radius;
        end
    case 'LMS'
        switch updateRule
            case 'DA'
                F = [real(Stx(1,:));...
                     imag(Stx(1,:));...
                     real(Stx(2,:));...
                     imag(Stx(2,:))];
            case 'DD'
                F = [];
        end
end
if strcmp(MIMO.precision,'single')
    F = single(F);
end

% MIMO Parameters:
nTaps = MIMO.nTaps;
mu = MIMO.current.stepSize;
upRate = MIMO.current.updateRate;
upOff = MIMO.current.updateOffset;
if isfield(MIMO,'nTapsCPE')
    nTapsCPE = MIMO.nTapsCPE;
    applyCPE = logical(nTapsCPE);
else
    applyCPE = false;
end
if strcmp(updateRule,'DD')
    symMap = QAM.symbolMapping;
    symIndex = QAM.symbolIndex;
    IQmap = QAM.IQmap;
end

% Initialize Taps:
if isfield(MIMO,'W')
    W = MIMO.W;
else
    W = complex(zeros(4*nTaps,4));
    if strcmp(MIMO_method,'CMA') || strcmp(updateRule,'DD')
        n = floor(nTaps/2) + 1;   % central tap
        W(n,1) = 1;
        W(n+nTaps,2) = 1;
        W(n+2*nTaps,3) = 1;
        W(n+3*nTaps,4) = 1;
    end
end
if applyCPE
    if isfield(MIMO,'W_CPE')
        W_CPE = MIMO.W_CPE;
    else
        W_CPE = complex(single(ones(2,nTapsCPE)));
    end
end

if strcmp(MIMO.precision,'single')
    W = single(W);
    nTaps = single(nTaps);
    mu = single(mu);
    upRate = single(upRate);
    upOff = single(upOff);
    if applyCPE
        W_CPE = single(W_CPE);
    end
    if strcmp(updateRule,'DD')
        symMap = single(symMap);
        symIndex = single(symIndex);
        IQmap = single(IQmap);
    end
end

%% Apply MIMO 4x4
if strcmp(MIMO_method,'CMA')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            [Srx,W] = CMA_4x4_DA_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_4x4_DA_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    elseif strcmp(updateRule,'RDE')
        if MIMO.mex
            [Srx,W] = CMA_4x4_RDE_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_4x4_RDE_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    end
elseif strcmp(MIMO_method,'LMS')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            if applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_4x4_DA_CPE_matrix_mex(...
                    Srx,W,W_CPE,nTaps,mu,upRate,upOff,F);
            else
                [Srx,W] = LMS_4x4_DA_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        else
            if applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_4x4_DA_CPE_matrix(Srx,...
                    F,W,W_CPE,mu,upRate,0);
            else
                [Srx,W] = LMS_4x4_DA_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
            end
        end
    elseif strcmp(updateRule,'DD')
        if MIMO.mex
            if applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_4x4_DD_CPE_matrix_mex(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,symMap,symIndex,IQmap);
            else
                [Srx,W] = LMS_4x4_DD_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
            end
        else
            if applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_4x4_DD_CPE_matrix(Srx,...
                    W,W_CPE,nTaps,mu,upRate,upOff,symMap,...
                    symIndex,IQmap);
            else
                [Srx,W] = LMS_4x4_DD_matrix(Srx,W,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
            end
        end
    end
end

%% Output Parameters
Srx = [Srx(1,:) + 1j*Srx(2,:); Srx(3,:) + 1j*Srx(4,:)];
MIMO.W = W;
MIMO.err = [];
MIMO.F = F;

