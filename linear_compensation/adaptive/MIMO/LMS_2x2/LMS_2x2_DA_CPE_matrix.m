function [Y,W,W_CPE] = LMS_2x2_DA_CPE_matrix(X,W,W_CPE,nTaps,mu,...
    updateRate,updateOffset,F)

% Last Update: 14/06/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(complex(zeros(2,nSamples)));

%% Run CMA
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind)];
    
    % MIMO 2x2 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % CPE:
        A = Y(:,n);
        B = F(:,n);
        phi0    = exp(-1j*(angle(A) - angle(B)));
        W_CPE   = [phi0 W_CPE(:,1:end-1)];
        A       = A .* sign(mean(W_CPE,2));
        Y(:,n)  = A;

        % Calculate error:
        err(:,n)  = F(:,n) - Y(:,n);
%         err(:,n)  = F(:,n) - A;

        % Update CMA Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    end
    
    % Update Index:
    ind = ind + 1;
end
