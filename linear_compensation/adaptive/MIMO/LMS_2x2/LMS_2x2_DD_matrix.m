function [Y,W] = LMS_2x2_DD_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,symMap,symIndex,IQmap)

% Last Update: 14/06/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(complex(zeros(2,nSamples)));
F           = complex(zeros(2,1));

%% Run CMA
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)   
    % Create a 1xN vector with all signals:
    U   = [X(1,ind) X(2,ind)];
    
    % MIMO 2x2 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        A = Y(:,n);
        
        % Slicer:
        error   = abs(IQmap - A(1));
        index   = find(error == min(error));
        F(1,1)  = IQmap(symIndex(symMap(index(1))+1));
        error   = abs(IQmap - A(2));
        index   = find(error == min(error));
        F(2,1)  = IQmap(symIndex(symMap(index(1))+1));
        
        % Calculate error:
        err(:,n)  = F - Y(:,n);

        % Update CMA Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    end
    
    % Update Index:
    ind = ind + 1;
end
