function [Y,W,err] = LMS_2x2_DA(X,F,W,nTaps,mu,upRate,upOff,xPol,nBits)

% Last Update: 08/08/2016


%% Input Parameters
if isinf(nBits)
    quantizeTaps = false;
    nLevels = single(0);
else
    quantizeTaps = true;
    nLevels = 2^(nBits-1);
end
nSamples = single(length(X));
[Y,err] = deal(complex(NaN(2,nSamples)));

%% Run LMS 2x2
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind)];
    
    % MIMO 2x2 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+upOff,upRate) == 0)
        % Calculate error:
        err(:,n) = F(:,n) - Y(:,n);
        
        % Switch-off pol-demux:
        if ~xPol
            W(nTaps+1:end,1) = 0;
            W(1:nTaps,2) = 0;
        end
        
        % Update MIMO Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
        
        % Quantize Taps:
        if quantizeTaps
            maxIQ = max([max(max(abs(real(W)))) max(max(abs(imag(W))))]);
            W = W / maxIQ;
            W = round(W*nLevels)/nLevels * maxIQ;
        end
    end
        
    % Update Index:
    ind = ind + 1;
end
