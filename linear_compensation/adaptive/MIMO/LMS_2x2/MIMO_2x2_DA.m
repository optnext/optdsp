function [Y,W,err] = MIMO_2x2_DA(X,F,method,upRule,W,W_CPE,nTaps,mu,...
    upRate,upOff,xPol,nBits)

% Last Update: 08/08/2016


%% Input Parameters
if isinf(nBits)
    quantizeTaps = false;
    nLevels = single(0);
else
    quantizeTaps = true;
    nLevels = 2^(nBits-1);
end
nSamples = single(length(X));
[Y,err] = deal(complex(NaN(2,nSamples)));
if isempty(W_CPE)
    phaseTracker = false;
else
    phaseTracker = true;
end

%% Run MIMO 2x2
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind)];
    
    % MIMO 2x2 (as vector multiplication):
    Y(:,n) = (U * W).';
    A = Y(:,n);
    
    % Update MIMO Taps:
    if (rem(n+upOff,upRate) == 0)
        % Choose reference signal:
        if strcmp(upRule,'DA')
            Fref = F(:,n);
        elseif strcmp(upRule,'DD')
            % Slicer:
            error = abs(F - A(1));
            idx = find(error == min(error));
            Fref(1,1) = F(idx(1));
            error = abs(F - A(2));
            idx = find(error == min(error));
            Fref(2,1) = F(idx(1));
        elseif strcmp(upRule,'RDE')
            % Determine the closest radius:
            [~,rX] = min(abs(F - abs(A(1))));
            Fref(1,1) = radius(rX)^2;
            [~,rY] = min(abs(F - abs(A(2))));
            Fref(2,1) = radius(rY)^2;
        end

        % CPE:
        if phaseTracker
            phi0 = exp(-1j*(angle(A) - angle(Fref)));
            W_CPE = [phi0 W_CPE(:,1:end-1)];
            phi = sign(mean(W_CPE,2));
            A = A .* phi;
        end
        
        % Calculate error:
        if strcmp(method,'LMS')
            err(:,n) = Fref - A;
        elseif strcmp(method,'CMA')
            err(:,n) = (Fref - abs(A).^2).*A;
        end
                
        % Update MIMO Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
        
        % Switch-off pol-demux:
        if ~xPol
            W(nTaps+1:end,1) = 0;
            W(1:nTaps,2) = 0;
        end
        
        % Quantize Taps:
        if quantizeTaps
            maxIQ = max([max(max(abs(real(W)))) max(max(abs(imag(W))))]);
            W = W / maxIQ;
            W = round(W*nLevels)/nLevels * maxIQ;
        end
    end
        
    % Update Index:
    ind = ind + 1;
end
