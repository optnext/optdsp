function [Y,W] = CMA_4x2_RDE_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,radius)

% Last Update: 17/05/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(complex(zeros(2,nSamples)));
F           = single(zeros(2,1));

%% Run CMA 4x2
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)];
    
    % MIMO 8x4 (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % Determine the closest radius:
        R       = abs(Y(:,n)).^2;
        [~,r]   = min(abs(radius - sqrt(R(1))));
        F(1)    = radius(r)^2;
        [~,r]   = min(abs(radius - sqrt(R(2))));
        F(2)    = radius(r)^2;

        % Calculate error:
        err(:,n) = (F - R).*Y(:,n);
        
        % Update CMA taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    end
    
    % Update Index:
    ind = ind + 1;
end
