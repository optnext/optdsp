function [W,err,F] = updateCMA_4x2(W,Ain,Aout,CMA,tx)

% Last Update: 23/02/2016


%% Input Parameters
% Equalizer input signal:
Xi      = Ain(1,:);
Xq      = Ain(2,:);
Yi      = Ain(3,:);
Yq      = Ain(4,:);
% Equalizer output signal:
Xout    = Aout(1,:);
Yout    = Aout(2,:);
% MIMO parameters:
mu              = CMA.current.stepSize;
updateRule      = CMA.current.updateRule;
crossPol        = CMA.crossPol;

%% Determine Target Signal
switch updateRule
    case 'LMS',
        Fx      = abs(tx(1)).^2;
        Fy      = abs(tx(2)).^2;
    case {'CMA','QPSK'},
        radius  = CMA.current.radius;
        Fx      = radius(1);
        Fy      = radius(1);
    case 'MMA',
        radius  = CMA.current.radius;
        Fx      = radius(1)/2;
        Fy      = radius(1)/2;
    case 'MMA-DD',
        F       = symbol2signal(signal2symbol(Aout,CMA.QAM),CMA.QAM);
        Fx      = real(F).^2;
        Fy      = imag(F).^2;
    case 'RDE',
        radius  = CMA.current.radius;
        [~,rX]  = min(abs(radius-abs(Xout)));
        Fx      = radius(rX)^2;
        [~,rY]  = min(abs(radius-abs(Yout)));
        Fy      = radius(rY)^2;
    case {'static','noUpdate'},
        err     = [0; 0];
        F       = [0; 0];
        return;
    otherwise,
        error('Unsupported CMA update rule!');
end

%% Calculate Error Function
errX = Fx - abs(Xout).^2;
errY = Fy - abs(Yout).^2;

%% Update CMA Taps
% W(1,:) = W(1,:) + mu*errX*conj(Xout)*Xi;
% W(2,:) = W(2,:) + mu*errX*conj(Xout)*Xq;
% W(3,:) = W(3,:) + mu*errX*conj(Xout)*Yi;
% W(4,:) = W(4,:) + mu*errX*conj(Xout)*Yq;
% 
% W(5,:) = W(5,:) + mu*errY*conj(Yout)*Xi;
% W(6,:) = W(6,:) + mu*errY*conj(Yout)*Xq;
% W(7,:) = W(7,:) + mu*errY*conj(Yout)*Yi;
% W(8,:) = W(8,:) + mu*errY*conj(Yout)*Yq;

W(1,:) = W(1,:) + mu*errX*Xout*conj(Xi);
W(2,:) = W(2,:) + mu*errX*Xout*conj(Xq);
if crossPol
    W(3,:) = W(3,:) + mu*errX*Xout*conj(Yi);
    W(4,:) = W(4,:) + mu*errX*Xout*conj(Yq);
    W(5,:) = W(5,:) + mu*errY*Yout*conj(Xi);
    W(6,:) = W(6,:) + mu*errY*Yout*conj(Xq);
end
W(7,:) = W(7,:) + mu*errY*Yout*conj(Yi);
W(8,:) = W(8,:) + mu*errY*Yout*conj(Yq);

% W(1,:) = W(1,:) + mu*errX*Xout*(Xi);
% W(2,:) = W(2,:) + mu*errX*Xout*(Xq);
% W(3,:) = W(3,:) + mu*errX*Xout*(Yi);
% W(4,:) = W(4,:) + mu*errX*Xout*(Yq);
% 
% W(5,:) = W(5,:) + mu*errY*Yout*(Xi);
% W(6,:) = W(6,:) + mu*errY*Yout*(Xq);
% W(7,:) = W(7,:) + mu*errY*Yout*(Yi);
% W(8,:) = W(8,:) + mu*errY*Yout*(Yq);

%% Output Parameters
err = [errX; errY];
F   = [Fx; Fy];
