function [Srx,MIMO] = MIMO_8x8(Srx,Stx,QAM,MIMO)

% Last Update: 19/08/2017


%% Input Parser
if ~isempty(strfind(MIMO.method,'CMA'))
    MIMO_method = 'CMA';
elseif ~isempty(strfind(MIMO.method,'LMS'))
    MIMO_method = 'LMS';
end
updateRule = MIMO.current.updateRule;

%% Input Parameters:
% Check format of input signal:
if ~isreal(Srx)
    TMP = zeros(2*size(Srx,1),size(Srx,2));
    for k = 1:size(Srx,1)
        TMP(2*k-1:2*k,:) = [real(Srx(k,:));
                            imag(Srx(k,:))];
    end
    Srx = TMP;
end
if strcmp(MIMO.precision,'single')
    Srx = single(Srx);
end
% Reference training signal:
switch MIMO_method
    case 'CMA'
        switch updateRule
            case 'DA'
                F = [abs(Stx(1,:)).^2;...
                      abs(Stx(1,:)).^2;...
                      abs(Stx(2,:)).^2;...
                      abs(Stx(2,:)).^2;...
                      abs(Stx(3,:)).^2;...
                      abs(Stx(3,:)).^2;...
                      abs(Stx(4,:)).^2;...
                      abs(Stx(4,:)).^2];
            case 'RDE'
                F = MIMO.current.radius;
        end
    case 'LMS'
        switch updateRule
            case 'DA'
                F = [real(Stx(1,:));...
                     imag(Stx(1,:));...
                     real(Stx(2,:));...
                     imag(Stx(2,:));...
                     real(Stx(3,:));...
                     imag(Stx(3,:));...
                     real(Stx(4,:));...
                     imag(Stx(4,:))];
            case 'DD'
                F = QAM.IQmap;
        end
end
if strcmp(MIMO.precision,'single')
    F = single(F);
end

% MIMO Parameters:
nTaps = MIMO.nTaps;
mu = MIMO.current.stepSize;
upRate = MIMO.current.updateRate;
upOff = MIMO.current.updateOffset;
if isfield(MIMO,'applyPolDemux')
    xPol = MIMO.applyPolDemux;
else
    xPol = true;
end
if isfield(MIMO,'nBits_taps')
    nBits = MIMO.nBits_taps;
else
    nBits = Inf;
end
if isfield(MIMO.current,'DEBUG')
    DEBUG = MIMO.current.DEBUG;
else
    DEBUG = false;
end
if isfield(MIMO,'nTapsCPE')
    nTapsCPE = MIMO.nTapsCPE;
else
    nTapsCPE = 0;
    MIMO.nTapsCPE = 0;
end
if strcmp(updateRule,'DD')
    symMap = QAM.symbolMapping;
    symIndex = QAM.symbolIndex;
    IQmap = QAM.IQmap;
end

% Initialize Taps:
if isfield(MIMO,'W')
    W = MIMO.W;
else
    W = zeros(8*nTaps,8);
    if strncmp(MIMO_method,'CMA',3) || strcmp(updateRule,'DD')
        n = floor(nTaps/2) + 1;   % central tap
        W(n,1) = 1;
        W(n+nTaps,2) = 1;
        W(n+2*nTaps,3) = 1;
        W(n+3*nTaps,4) = 1;
        W(n+4*nTaps,5) = 1;
        W(n+5*nTaps,6) = 1;
        W(n+6*nTaps,7) = 1;
        W(n+7*nTaps,8) = 1;
    end
end
if isfield(MIMO,'W_CPE')
    W_CPE = MIMO.W_CPE;
else
    W_CPE = complex(single(ones(4,nTapsCPE)));
end

if strcmp(MIMO.precision,'single')
    W = single(W);
    nTaps = single(nTaps);
    mu = single(mu);
    upRate = single(upRate);
    upOff = single(upOff);
    nBits = single(nBits);
    if MIMO.applyCPE
        nTapsCPE = single(nTapsCPE);
        W_CPE = single(W_CPE);
    end
    if strcmp(updateRule,'DD')
        symMap = single(symMap);
        symIndex = single(symIndex);
        IQmap = single(IQmap);
    end
end

%% Apply MIMO 8x8
if strcmp(MIMO_method,'CMA')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            [Srx,W] = CMA_8x8_DA_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_8x8_DA_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    elseif strcmp(updateRule,'RDE')
        if MIMO.mex
            [Srx,W] = CMA_8x8_RDE_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W,err] = CMA_8x8_RDE_matrix(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    end
elseif strcmp(MIMO_method,'LMS')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            if nTapsCPE
                [Srx,W] = LMS_8x8_DA_CPE_matrix_mex(Srx,W,nTaps,...
                    nTapsCPE,mu,upRate,upOff,F);
            else
                [Srx,W] = LMS_8x8_DA_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        else
            if nTapsCPE
                [Srx,W] = LMS_8x8_DA_CPE_matrix(Srx,W,...
                    nTaps,nTapsCPE,mu,upRate,upOff,F);
            else
                [Srx,W] = LMS_8x8_DA_matrix(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        end
    elseif strcmp(updateRule,'DD')
        if MIMO.mex
            if nTapsCPE
                [Srx,W] = LMS_8x8_DD_CPE_matrix_mex(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,symMap,symIndex,IQmap);
            else
                [Srx,W] = LMS_8x8_DD_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
           end
        else
            if nTapsCPE
                [Srx,W] = LMS_8x8_DD_CPE_matrix(Srx,W,W_CPE,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
            else
                [Srx,W] = LMS_8x8_DD_matrix(Srx,W,nTaps,mu,upRate,...
                    upOff,symMap,symIndex,IQmap);
            end
        end
    end
end

%% Debug
if DEBUG
    itblue = [0 0.24 0.431];
    itred = [0.73 0.07 0.169];
    figure;
    hold on;
    for n = 1:size(err,1)
        hPlot(n) = plot(movmean(abs(err(n,:)),1000,'omitnan')); 
    end

%     set(hPlot(1),'Color',itblue);
%     set(hPlot(2),'Color',itred);

    xlabel('Sample Index','Interpreter','latex');
    ylabel('Error','Interpreter','latex');
    grid on;

    xAxis = get(gca,'xAxis');
    yAxis = get(gca,'yAxis');
    set(xAxis,'TickLabelInterpreter','latex','FontSize',13);
    set(yAxis,'TickLabelInterpreter','latex','FontSize',13);
    set(gca,'GridLineStyle','--');

%     axis([-inf inf 0.2 1]);

    % Annotations:
    hText = text(0.95*numel(err(1,:)),0.95,{['update every ',...
        num2str(upRate),' samples']});
    set(hText,'Interpreter','latex','HorizontalAlignment','right',...
        'VerticalAlignment','top','Color',itblue,...
        'EdgeColor',itblue,...
        'BackgroundColor','w','FontSize',12);
end

%% Output Parameters
Srx = [Srx(1,:) + 1j*Srx(2,:); Srx(3,:) + 1j*Srx(4,:);...
        Srx(5,:) + 1j*Srx(6,:); Srx(7,:) + 1j*Srx(8,:)];
MIMO.W = W;
MIMO.err = [];
MIMO.F = F;
