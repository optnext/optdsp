function [Y,W,err] = MIMO_NxN_all(X,F,method,upRule,W,W_CPE,nTaps,mu,...
    upRate,upOff,xPol,nBits)

% Last Update: 08/08/2016


%% Input Parameters
N = size(X,1);
iscomplex = ~isreal(X);
if isinf(nBits)
    quantizeTaps = false;
    nLevels = single(0);
else
    quantizeTaps = true;
    nLevels = 2^(nBits-1);
end
nSamples = single(length(X));
if iscomplex
    [Y,err] = deal(complex(NaN(N,nSamples)));
else
    [Y,err] = deal(NaN(N,nSamples));
end
if isempty(W_CPE)
    phaseTracker = false;
else
    phaseTracker = true;
end

%% Run MIMO NxN
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    for k = 1:N
        U((k-1)*nTaps + 1:k*nTaps) = X(k,ind);
    end
    
    % MIMO NxN (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+upOff,upRate) == 0)
        % Format equalized signal:
        thisY = Y(:,n);
        if strcmp(method,'CMA')
            R = abs(real2complex(thisY,N));
        end
        
        % Choose reference signal:
        if strcmp(upRule,'DA')
            Fref = F(:,n);
        elseif strcmp(upRule,'DD')
            % Slicer:
            A = real2complex(thisY,N);
            for k = 1:size(A,1)
                error = abs(F - A(k));
                idx = find(error == min(error));
                Fref(k,1) = F(idx(1));
            end
        elseif strcmp(upRule,'RDE')
            % Determine the closest radius:
            for k = 1:size(R,1)
                [~,r] = min(abs(F - R(k)));
                Fref(k,1) = F(r)^2;
            end
        end

        % Phase Tracker:
        if phaseTracker
            B = real2complex(Fref,N);
            A = real2complex(thisY,N);
            phi0 = exp(-1j*(angle(A) - angle(B)));
            W_CPE = [phi0 W_CPE(:,1:end-1)];
            phi = sign(mean(W_CPE,2));
            thisY = A .* phi;
        end
        
        % Format signals before error calculation:
        if iscomplex
            Yeq = real2complex(thisY,N);
        else
            Yeq = complex2real(thisY,N);
        end
        
        % Calculate error:
        if strcmp(method,'LMS')
            if iscomplex
                Ytx = real2complex(Fref,N);
            else
                Ytx = complex2real(Fref,N);
            end
            err(:,n) = Ytx - Yeq;
        elseif strcmp(method,'CMA')
            Ytx = complex2real(Fref,N);
            Rtx = complex2real(R,N).^2;
            err(:,n) = (Ytx - Rtx).*Yeq;
        end
                
        % Update MIMO Taps:
        for k = 1:N
            W(:,k) = W(:,k) + mu * U' * err(k,n);
        end
        
        % Switch-off pol-demux:
        if ~xPol
            if N == 2
                W(nTaps+1:end,1) = 0;
                W(1:nTaps,2) = 0;
            elseif N == 4
                W(2*nTaps+1:end,1) = 0;
                W(2*nTaps+1:end,2) = 0;
                W(1:2*nTaps,3) = 0;
                W(1:2*nTaps,4) = 0;
            elseif N == 8
                W(2*nTaps+1:4*nTaps,1) = 0;
                W(6*nTaps+1:8*nTaps,1) = 0;
                W(2*nTaps+1:4*nTaps,2) = 0;
                W(6*nTaps+1:8*nTaps,2) = 0;
                W(1:2*nTaps,3) = 0;
                W(4*nTaps+1:6*nTaps,3) = 0;
                W(1:2*nTaps,4) = 0;
                W(4*nTaps+1:6*nTaps,4) = 0;
                W(2*nTaps+1:4*nTaps,5) = 0;
                W(6*nTaps+1:8*nTaps,5) = 0;
                W(2*nTaps+1:4*nTaps,6) = 0;
                W(6*nTaps+1:8*nTaps,6) = 0;
                W(1:2*nTaps,7) = 0;
                W(4*nTaps+1:6*nTaps,7) = 0;
                W(1:2*nTaps,8) = 0;
                W(4*nTaps+1:6*nTaps,8) = 0;
            end
        end
        
        % Quantize Taps:
        if quantizeTaps
            maxIQ = max([max(max(abs(real(W)))) max(max(abs(imag(W))))]);
            W = W / maxIQ;
            W = round(W*nLevels)/nLevels * maxIQ;
        end
    end
        
    % Update Index:
    ind = ind + 1;
end

end

%% Helper Functions
function OUT = real2complex(IN,N)
    if isreal(IN)
        for k = 1:N/2
            OUT(k,1) = IN(2*k-1) + 1j*IN(2*k);
        end
    else                
        OUT = IN;
    end
end


function OUT = complex2real(IN,N)
    if isreal(IN)
        if size(IN,1) == N
            OUT = IN;
        else
            for k = 1:N/2
                OUT(2*k-1,1) = IN(k);
                OUT(2*k,1) = IN(k);
            end
        end
    else
        for k = 1:N/2
            OUT(2*k-1,1) = real(IN(k));
            OUT(2*k,1) = imag(IN(k));
        end
    end
end