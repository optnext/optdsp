function [W,err,F] = updateCMA_8x8(W,Ain,Aout,MIMO,tx)

% Last Update: 06/04/2016


%% Input Parameters
% Equalizer input signal:
Xi1     = Ain(1,:);
Xq1     = Ain(2,:);
Xi2     = Ain(3,:);
Xq2     = Ain(4,:);
Yi1     = Ain(5,:);
Yq1     = Ain(6,:);
Yi2     = Ain(7,:);
Yq2     = Ain(8,:);
% Equalizer output signal:
Xi1Out  = Aout(1,1);
Xq1Out  = Aout(2,1);
Xi2Out  = Aout(3,1);
Xq2Out  = Aout(4,1);
Yi1Out  = Aout(5,1);
Yq1Out  = Aout(6,1);
Yi2Out  = Aout(7,1);
Yq2Out  = Aout(8,1);
X1out   = Xi1Out + 1j*Xq1Out;
X2out   = Xi2Out + 1j*Xq2Out;
Y1out   = Yi1Out + 1j*Yq1Out;
Y2out   = Yi2Out + 1j*Yq2Out;
% MIMO parameters:
% nTaps       = MIMO.nTaps;
mu          = MIMO.current.stepSize;
updateRule  = MIMO.current.updateRule;

%% Determine Target Signal
switch updateRule
    case 'LMS',
        Fx1     = abs(tx(1)).^2;
        Fy1     = abs(tx(2)).^2;
        Fx2     = abs(tx(3)).^2;
        Fy2     = abs(tx(4)).^2;
    case {'CMA','QPSK'},
        radius  = MIMO.current.radius;
        [Fx1,Fy1,Fx2,Fy2] = deal(radius(1));
    case 'MMA',
        radius  = MIMO.current.radius;
        [Fx1,Fy1,Fx2,Fy2] = deal(radius(1)/2);
    case {'RDE','16QAM','64QAM','blind'},
        radius  = MIMO.current.radius;
        [~,rX1] = min(abs(radius-abs(X1out)));
        Fx1     = radius(rX1)^2;
        [~,rY1] = min(abs(radius-abs(Y1out)));
        Fy1     = radius(rY1)^2;
        [~,rX2] = min(abs(radius-abs(X2out)));
        Fx2     = radius(rX2)^2;
        [~,rY2] = min(abs(radius-abs(Y2out)));
        Fy2     = radius(rY2)^2;
    case {'static','noUpdate'},
        [F,err] = deal([0; 0; 0; 0]);
        return;
    otherwise,
        error('Unsupported CMA update rule!');
end

%% Calculate Error Function
errX1   = Fx1 - abs(X1out)^2;
errX2   = Fx2 - abs(X2out)^2;
errY1   = Fy1 - abs(Y1out)^2;
errY2   = Fy2 - abs(Y2out)^2;

%% Update Taps
W(1,:)  = W(1,:) + mu*errX1*Xi1Out*Xi1;
W(2,:)  = W(2,:) + mu*errX1*Xi1Out*Xq1;
W(3,:)  = W(3,:) + mu*errX1*Xi1Out*Yi1;
W(4,:)  = W(4,:) + mu*errX1*Xi1Out*Yq1;
W(5,:)  = W(5,:) + mu*errX1*Xi1Out*Xi2;
W(6,:)  = W(6,:) + mu*errX1*Xi1Out*Xq2;
W(7,:)  = W(7,:) + mu*errX1*Xi1Out*Yi2;
W(8,:)  = W(8,:) + mu*errX1*Xi1Out*Yq2;

W(9,:)  = W(9,:)  + mu*errX1*Xq1Out*Xi1;
W(10,:) = W(10,:) + mu*errX1*Xq1Out*Xq1;
W(11,:) = W(11,:) + mu*errX1*Xq1Out*Yi1;
W(12,:) = W(12,:) + mu*errX1*Xq1Out*Yq1;
W(13,:) = W(13,:) + mu*errX1*Xq1Out*Xi2;
W(14,:) = W(14,:) + mu*errX1*Xq1Out*Xq2;
W(15,:) = W(15,:) + mu*errX1*Xq1Out*Yi2;
W(16,:) = W(16,:) + mu*errX1*Xq1Out*Yq2;

W(17,:) = W(17,:) + mu*errY1*Yi1Out*Xi1;
W(18,:) = W(18,:) + mu*errY1*Yi1Out*Xq1;
W(19,:) = W(19,:) + mu*errY1*Yi1Out*Yi1;
W(20,:) = W(20,:) + mu*errY1*Yi1Out*Yq1;
W(21,:) = W(21,:) + mu*errY1*Yi1Out*Xi2;
W(22,:) = W(22,:) + mu*errY1*Yi1Out*Xq2;
W(23,:) = W(23,:) + mu*errY1*Yi1Out*Yi2;
W(24,:) = W(24,:) + mu*errY1*Yi1Out*Yq2;

W(25,:) = W(25,:) + mu*errY1*Yq1Out*Xi1;
W(26,:) = W(26,:) + mu*errY1*Yq1Out*Xq1;
W(27,:) = W(27,:) + mu*errY1*Yq1Out*Yi1;
W(28,:) = W(28,:) + mu*errY1*Yq1Out*Yq1;
W(29,:) = W(29,:) + mu*errY1*Yq1Out*Xi2;
W(30,:) = W(30,:) + mu*errY1*Yq1Out*Xq2;
W(31,:) = W(31,:) + mu*errY1*Yq1Out*Yi2;
W(32,:) = W(32,:) + mu*errY1*Yq1Out*Yq2;

W(33,:) = W(33,:) + mu*errX2*Xi2Out*Xi1;
W(34,:) = W(34,:) + mu*errX2*Xi2Out*Xq1;
W(35,:) = W(35,:) + mu*errX2*Xi2Out*Yi1;
W(36,:) = W(36,:) + mu*errX2*Xi2Out*Yq1;
W(37,:) = W(37,:) + mu*errX2*Xi2Out*Xi2;
W(38,:) = W(38,:) + mu*errX2*Xi2Out*Xq2;
W(39,:) = W(39,:) + mu*errX2*Xi2Out*Yi2;
W(40,:) = W(40,:) + mu*errX2*Xi2Out*Yq2;

W(41,:) = W(41,:) + mu*errX2*Xq2Out*Xi1;
W(42,:) = W(42,:) + mu*errX2*Xq2Out*Xq1;
W(43,:) = W(43,:) + mu*errX2*Xq2Out*Yi1;
W(44,:) = W(44,:) + mu*errX2*Xq2Out*Yq1;
W(45,:) = W(45,:) + mu*errX2*Xq2Out*Xi2;
W(46,:) = W(46,:) + mu*errX2*Xq2Out*Xq2;
W(47,:) = W(47,:) + mu*errX2*Xq2Out*Yi2;
W(48,:) = W(48,:) + mu*errX2*Xq2Out*Yq2;

W(49,:) = W(49,:) + mu*errY2*Yi2Out*Xi1;
W(50,:) = W(50,:) + mu*errY2*Yi2Out*Xq1;
W(51,:) = W(51,:) + mu*errY2*Yi2Out*Yi1;
W(52,:) = W(52,:) + mu*errY2*Yi2Out*Yq1;
W(53,:) = W(53,:) + mu*errY2*Yi2Out*Xi2;
W(54,:) = W(54,:) + mu*errY2*Yi2Out*Xq2;
W(55,:) = W(55,:) + mu*errY2*Yi2Out*Yi2;
W(56,:) = W(56,:) + mu*errY2*Yi2Out*Yq2;

W(57,:) = W(57,:) + mu*errY2*Yq2Out*Xi1;
W(58,:) = W(58,:) + mu*errY2*Yq2Out*Xq1;
W(59,:) = W(59,:) + mu*errY2*Yq2Out*Yi1;
W(60,:) = W(60,:) + mu*errY2*Yq2Out*Yq1;
W(61,:) = W(61,:) + mu*errY2*Yq2Out*Xi2;
W(62,:) = W(62,:) + mu*errY2*Yq2Out*Xq2;
W(63,:) = W(63,:) + mu*errY2*Yq2Out*Yi2;
W(64,:) = W(64,:) + mu*errY2*Yq2Out*Yq2;

%% Output Parameters
err     = [errX1; errY1; errX2; errY2];
F       = [Fx1; Fy1; Fx2; Fy2];
