function [Y,W,err] = CMA_8x8_RDE_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,radius)

% Last Update: 08/08/2016


%% Input Parameters
nSamples = single(length(X));
[Y,err] = deal(single(NaN(8,nSamples)));
F = single(zeros(8,1));

%% Run CMA 8x8
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)...
         X(5,ind) X(6,ind) X(7,ind) X(8,ind)];
    
    % MIMO 8x8 (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % Determine absolute value of output signal:
        R = [Y(1,n)^2 + Y(2,n)^2; Y(1,n)^2 + Y(2,n)^2;...
             Y(3,n)^2 + Y(4,n)^2; Y(3,n)^2 + Y(4,n)^2;...
             Y(5,n)^2 + Y(6,n)^2; Y(5,n)^2 + Y(6,n)^2;...
             Y(7,n)^2 + Y(8,n)^2; Y(7,n)^2 + Y(8,n)^2];
        
        % Determine the closest radius:
        [~,r]       = min(abs(radius - sqrt(R(1))));
        F(1:2,1)    = radius(r)^2;
        [~,r]       = min(abs(radius - sqrt(R(3))));
        F(3:4,1)    = radius(r)^2;
        [~,r]       = min(abs(radius - sqrt(R(5))));
        F(5:6,1)    = radius(r)^2;
        [~,r]       = min(abs(radius - sqrt(R(7))));
        F(7:8,1)    = radius(r)^2;
        
        % Calculate error:
        err(:,n)  = (F - R).*Y(:,n);
        
        % Update CMA taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    	W(:,3) = W(:,3) + mu * U' * err(3,n);
    	W(:,4) = W(:,4) + mu * U' * err(4,n);
        W(:,5) = W(:,5) + mu * U' * err(5,n);
        W(:,6) = W(:,6) + mu * U' * err(6,n);
    	W(:,7) = W(:,7) + mu * U' * err(7,n);
    	W(:,8) = W(:,8) + mu * U' * err(8,n);
    end
end
