function [Srx,MIMO] = MIMO_8x4(Srx,Stx,QAM,MIMO)

% Last Update: 08/08/2017


%% Input Parser
if ~isempty(strfind(MIMO.method,'CMA'))
    MIMO_method = 'CMA';
elseif ~isempty(strfind(MIMO.method,'LMS'))
    MIMO_method = 'LMS';
end
updateRule = MIMO.current.updateRule;

%% Input Parameters:
% Equalizer input signal:
Xi1 = Srx(1,:);
Xq1 = Srx(2,:);
Yi1 = Srx(3,:);
Yq1 = Srx(4,:);
Xi2 = Srx(5,:);
Xq2 = Srx(6,:);
Yi2 = Srx(7,:);
Yq2 = Srx(8,:);
Srx = [Xi1;Xq1;Yi1;Yq1;Xi2;Xq2;Yi2;Yq2];
if strcmp(MIMO.precision,'single')
    Srx = single(Srx);
end
% Reference training signal:
switch MIMO_method
    case 'CMA'
        switch updateRule
            case 'DA'
                F = [abs(Stx(1,:)).^2;...
                     abs(Stx(2,:)).^2;...
                     abs(Stx(3,:)).^2;...
                     abs(Stx(4,:)).^2];
            case 'RDE'
                F = MIMO.current.radius;
        end
    case 'LMS'
        switch updateRule
            case 'DA'
                F = Stx;
            case 'DD'
                F = [];
        end
end
if strcmp(MIMO.precision,'single')
    F = single(F);
end

% MIMO Parameters:
nTaps = MIMO.nTaps;
mu = MIMO.current.stepSize;
upRate = MIMO.current.updateRate;
upOff = MIMO.current.updateOffset;
if MIMO.applyCPE
    nTapsCPE = MIMO.nTapsCPE;
end
if strcmp(updateRule,'DD')
    symMap = QAM.symbolMapping;
    symIndex = QAM.symbolIndex;
    IQmap = QAM.IQmap;
end

% Initialize Taps
if isfield(MIMO,'W')
    W = MIMO.W;
else
    W = complex(zeros(8*nTaps,4));
    if strcmp(MIMO_method,'CMA') || strcmp(updateRule,'DD')
        n = floor(nTaps/2) + 1;   % central tap
        W(n,1) = 1;
        W(n+nTaps,1) = 1;
        W(n+2*nTaps,2) = 1;
        W(n+3*nTaps,2) = 1;
        W(n+4*nTaps,3) = 1;
        W(n+5*nTaps,3) = 1;
        W(n+6*nTaps,4) = 1;
        W(n+7*nTaps,4) = 1;
    end
end
if MIMO.applyCPE
    if isfield(MIMO,'W_CPE')
        W_CPE = MIMO.W_CPE;
    else
        W_CPE = complex(single(ones(4,nTapsCPE)));
    end
end

if strcmp(MIMO.precision,'single')
    W = single(W);
    nTaps = single(nTaps);
    mu = single(mu);
    upRate = single(upRate);
    upOff = single(upOff);
    if MIMO.applyCPE
        nTapsCPE = single(nTapsCPE);
        W_CPE = single(W_CPE);
    end
    if strcmp(updateRule,'DD')
        symMap = single(symMap);
        symIndex = single(symIndex);
        IQmap = single(IQmap);
    end
end

%% Apply MIMO 8x8
if strcmp(MIMO_method,'CMA')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            [Srx,W] = CMA_8x4_DA_matrix_mex(Srx,W,nTaps,mu,...
                upRate,upOff,F);
        else
            [Srx,W] = CMA_8x4_DA_matrix(Srx,W,nTaps,mu,...
                upRate,upOff,F);
        end
    elseif strcmp(updateRule,'RDE')
        if MIMO.mex
            [Srx,W] = CMA_8x4_RDE_matrix_mex(Srx,W,nTaps,mu,...
                upRate,upOff,F);
        else
            [Srx,W] = CMA_8x4_RDE_matrix(Srx,W,nTaps,mu,...
                upRate,upOff,F);
        end
    end
elseif strcmp(MIMO_method,'LMS')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            if MIMO.applyCPE
                [Srx,W,W_CPE] = LMS_8x4_DA_CPE_matrix_mex(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,F);
                MIMO.W_CPE = W_CPE;
            else
                % not implemented yet
                [Srx,W] = LMS_8x4_DA_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        else
            if MIMO.applyCPE
               [Srx,W,W_CPE] = LMS_8x4_DA_CPE_matrix(Srx,W,...
                   W_CPE,nTaps,mu,upRate,upOff,F);
               MIMO.W_CPE = W_CPE;
            else
                % not implemented yet
                [Srx,W] = LMS_8x4_DA_matrix(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        end
    elseif strcmp(updateRule,'DD')
        if MIMO.mex
            if MIMO.applyCPE
                [Srx,W,W_CPE] = LMS_8x4_DD_CPE_matrix_mex(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,symMap,symIndex,...
                    IQmap);
                MIMO.W_CPE = W_CPE;
%                 [Aout,W] = LMS_8x8_DD_CPE_DA_matrix_mex(Ain,W,...
%                     nTaps,nTapsCPE,mu,upRate,upOff,symMap,...
%                     symIndex,IQmap,F);
            else
                % not implemented yet:
                [Srx,W] = LMS_8x4_DD_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,F);
            end
        else
            if MIMO.applyCPE
                [Srx,W,W_CPE] = LMS_8x4_DD_CPE_matrix(Srx,W,...
                    W_CPE,nTaps,mu,upRate,upOff,symMap,symIndex,...
                    IQmap);
                MIMO.W_CPE = W_CPE;
%                 [Aout,W] = LMS_8x4_DD_CPE_DA_matrix(Ain,W,nTaps,...
%                     nTapsCPE,mu,upRate,upOff,symMap,symIndex,IQmap,F);
            else
                % not implemented yet
                [Srx,W] = LMS_8x4_DD_matrix(Srx,W,nTaps,mu,upRate,...
                    upOff,symMap,symIndex,IQmap);
            end
        end
    end
end

%% Output Parameters
MIMO.W = W;
MIMO.err = [];
MIMO.F = F;
