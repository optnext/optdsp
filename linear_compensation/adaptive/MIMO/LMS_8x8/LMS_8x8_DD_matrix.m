function [Y,W] = LMS_8x8_DD_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,symMap,symIndex,IQmap)

% Last Update: 02/04/2017


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(single(zeros(8,nSamples)));
B           = complex(zeros(4,1));

%% Run CMA 8x8
% Determine initial indices:
taps = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)   
    % Create a 1xN vector with all signals:
    U = [X(1,taps) X(2,taps) X(3,taps) X(4,taps)...
         X(5,taps) X(6,taps) X(7,taps) X(8,taps)];
    
    % MIMO 8x8 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        A = [Y(1,n) + 1j*Y(2,n); Y(3,n) + 1j*Y(4,n); ...
             Y(5,n) + 1j*Y(6,n); Y(7,n) + 1j*Y(8,n)];
        
        % Slicer:
        error   = abs(IQmap - A(1));
        idx     = find(error == min(error));
        B(1,1)  = IQmap(symIndex(symMap(idx(1))+1));
        error   = abs(IQmap - A(2));
        idx     = find(error == min(error));
        B(2,1)  = IQmap(symIndex(symMap(idx(1))+1));
        error   = abs(IQmap - A(3));
        idx     = find(error == min(error));
        B(3,1)  = IQmap(symIndex(symMap(idx(1))+1));
        error   = abs(IQmap - A(4));
        idx     = find(error == min(error));
        B(4,1)  = IQmap(symIndex(symMap(idx(1))+1));
        
        F       = [real(B(1,:)); imag(B(1,:));...
                   real(B(2,:)); imag(B(2,:));...
                   real(B(3,:)); imag(B(3,:));...
                   real(B(4,:)); imag(B(4,:))];
        
        % Calculate error:
        err(:,n)  = Y(:,n) - F;
        
        % Update taps:
        W(:,1) = W(:,1) - mu * U' * err(1,n);
        W(:,2) = W(:,2) - mu * U' * err(2,n);
    	W(:,3) = W(:,3) - mu * U' * err(3,n);
    	W(:,4) = W(:,4) - mu * U' * err(4,n);
        W(:,5) = W(:,5) - mu * U' * err(5,n);
        W(:,6) = W(:,6) - mu * U' * err(6,n);
    	W(:,7) = W(:,7) - mu * U' * err(7,n);
    	W(:,8) = W(:,8) - mu * U' * err(8,n);
    end
    
    % Update Index:
    taps = taps + 1;
end