function [Y,W] = LMS_8x8_DA_matrix(X,W,nTaps,mu,updateRate,...
    updateOffset,F)

% Last Update: 27/04/2016


%% Input Parameters
nSamples = length(X);
[Y,err] = deal(zeros(8,nSamples));

%% Run CMA 8x8
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)...
         X(5,ind) X(6,ind) X(7,ind) X(8,ind)];
    
    % MIMO 8x8 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)        
        % Calculate error:
        err(:,n)  = Y(:,n) - F(:,n);
        
        % Update taps:
        W(:,1) = W(:,1) - mu * U' * err(1,n);
        W(:,2) = W(:,2) - mu * U' * err(2,n);
    	W(:,3) = W(:,3) - mu * U' * err(3,n);
    	W(:,4) = W(:,4) - mu * U' * err(4,n);
        W(:,5) = W(:,5) - mu * U' * err(5,n);
        W(:,6) = W(:,6) - mu * U' * err(6,n);
    	W(:,7) = W(:,7) - mu * U' * err(7,n);
    	W(:,8) = W(:,8) - mu * U' * err(8,n);
    end
end