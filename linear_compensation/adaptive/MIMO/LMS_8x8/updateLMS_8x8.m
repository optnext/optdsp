function [W,err] = updateLMS_8x8(W,Ain,Aout,MIMO,tx)

% Last Update: 06/04/2016


%% Input Parameters
% Equalizer input signal:
Xi1     = Ain(1,:);
Xq1     = Ain(2,:);
Xi2     = Ain(3,:);
Xq2     = Ain(4,:);
Yi1     = Ain(5,:);
Yq1     = Ain(6,:);
Yi2     = Ain(7,:);
Yq2     = Ain(8,:);
% Equalizer output signal:
Xi1Out  = Aout(1,1);
Xq1Out  = Aout(2,1);
Xi2Out  = Aout(3,1);
Xq2Out  = Aout(4,1);
Yi1Out  = Aout(5,1);
Yq1Out  = Aout(6,1);
Yi2Out  = Aout(7,1);
Yq2Out  = Aout(8,1);
X1out   = Xi1Out + 1j*Xq1Out;
X2out   = Xi2Out + 1j*Xq2Out;
Y1out   = Yi1Out + 1j*Yq1Out;
Y2out   = Yi2Out + 1j*Yq2Out;
% Target Signal:
txX1    = tx(1,:);
txY1    = tx(2,:);
txX2    = tx(3,:);
txY2    = tx(4,:);
% MIMO parameters:
mu      = MIMO.current.stepSize;

%% Calculate Error Function
errX1   = txX1 - X1out;
errX2   = txX2 - X2out;
errY1   = txY1 - Y1out;
errY2   = txY2 - Y2out;

%% Update Taps
W(1,:)  = W(1,:) + mu*real(errX1)*Xi1;
W(2,:)  = W(2,:) + mu*real(errX1)*Xq1;
W(3,:)  = W(3,:) + mu*real(errX1)*Yi1;
W(4,:)  = W(4,:) + mu*real(errX1)*Yq1;
W(5,:)  = W(5,:) + mu*real(errX1)*Xi2;
W(6,:)  = W(6,:) + mu*real(errX1)*Xq2;
W(7,:)  = W(7,:) + mu*real(errX1)*Yi2;
W(8,:)  = W(8,:) + mu*real(errX1)*Yq2;

W(9,:)  = W(9,:)  + mu*imag(errX1)*Xi1;
W(10,:) = W(10,:) + mu*imag(errX1)*Xq1;
W(11,:) = W(11,:) + mu*imag(errX1)*Yi1;
W(12,:) = W(12,:) + mu*imag(errX1)*Yq1;
W(13,:) = W(13,:) + mu*imag(errX1)*Xi2;
W(14,:) = W(14,:) + mu*imag(errX1)*Xq2;
W(15,:) = W(15,:) + mu*imag(errX1)*Yi2;
W(16,:) = W(16,:) + mu*imag(errX1)*Yq2;

W(17,:) = W(17,:) + mu*real(errY1)*Xi1;
W(18,:) = W(18,:) + mu*real(errY1)*Xq1;
W(19,:) = W(19,:) + mu*real(errY1)*Yi1;
W(20,:) = W(20,:) + mu*real(errY1)*Yq1;
W(21,:) = W(21,:) + mu*real(errY1)*Xi2;
W(22,:) = W(22,:) + mu*real(errY1)*Xq2;
W(23,:) = W(23,:) + mu*real(errY1)*Yi2;
W(24,:) = W(24,:) + mu*real(errY1)*Yq2;

W(25,:) = W(25,:) + mu*imag(errY1)*Xi1;
W(26,:) = W(26,:) + mu*imag(errY1)*Xq1;
W(27,:) = W(27,:) + mu*imag(errY1)*Yi1;
W(28,:) = W(28,:) + mu*imag(errY1)*Yq1;
W(29,:) = W(29,:) + mu*imag(errY1)*Xi2;
W(30,:) = W(30,:) + mu*imag(errY1)*Xq2;
W(31,:) = W(31,:) + mu*imag(errY1)*Yi2;
W(32,:) = W(32,:) + mu*imag(errY1)*Yq2;

W(33,:) = W(33,:) + mu*real(errX2)*Xi1;
W(34,:) = W(34,:) + mu*real(errX2)*Xq1;
W(35,:) = W(35,:) + mu*real(errX2)*Yi1;
W(36,:) = W(36,:) + mu*real(errX2)*Yq1;
W(37,:) = W(37,:) + mu*real(errX2)*Xi2;
W(38,:) = W(38,:) + mu*real(errX2)*Xq2;
W(39,:) = W(39,:) + mu*real(errX2)*Yi2;
W(40,:) = W(40,:) + mu*real(errX2)*Yq2;

W(41,:) = W(41,:) + mu*imag(errX2)*Xi1;
W(42,:) = W(42,:) + mu*imag(errX2)*Xq1;
W(43,:) = W(43,:) + mu*imag(errX2)*Yi1;
W(44,:) = W(44,:) + mu*imag(errX2)*Yq1;
W(45,:) = W(45,:) + mu*imag(errX2)*Xi2;
W(46,:) = W(46,:) + mu*imag(errX2)*Xq2;
W(47,:) = W(47,:) + mu*imag(errX2)*Yi2;
W(48,:) = W(48,:) + mu*imag(errX2)*Yq2;

W(49,:) = W(49,:) + mu*real(errY2)*Xi1;
W(50,:) = W(50,:) + mu*real(errY2)*Xq1;
W(51,:) = W(51,:) + mu*real(errY2)*Yi1;
W(52,:) = W(52,:) + mu*real(errY2)*Yq1;
W(53,:) = W(53,:) + mu*real(errY2)*Xi2;
W(54,:) = W(54,:) + mu*real(errY2)*Xq2;
W(55,:) = W(55,:) + mu*real(errY2)*Yi2;
W(56,:) = W(56,:) + mu*real(errY2)*Yq2;

W(57,:) = W(57,:) + mu*imag(errY2)*Xi1;
W(58,:) = W(58,:) + mu*imag(errY2)*Xq1;
W(59,:) = W(59,:) + mu*imag(errY2)*Yi1;
W(60,:) = W(60,:) + mu*imag(errY2)*Yq1;
W(61,:) = W(61,:) + mu*imag(errY2)*Xi2;
W(62,:) = W(62,:) + mu*imag(errY2)*Xq2;
W(63,:) = W(63,:) + mu*imag(errY2)*Yi2;
W(64,:) = W(64,:) + mu*imag(errY2)*Yq2;

%% Output Parameters
err     = [errX1; errY1; errX2; errY2];

