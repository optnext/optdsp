function [Y,W] = LMS_8x8_DD_CPE_DA_matrix(X,W,nTaps,nTapsCPE,mu,updateRate,...
    updateOffset,symMap,symIndex,IQmap,F)

% Last Update: 27/04/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(single(zeros(8,nSamples)));
allPhi      = complex(single(ones(4,nTapsCPE)));
B           = complex(single(zeros(4,1)));

%% Run CMA 8x8
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)...
         X(5,ind) X(6,ind) X(7,ind) X(8,ind)];
    
    % MIMO 8x8 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        A = [Y(1,n) + 1j*Y(2,n); Y(3,n) + 1j*Y(4,n); ...
             Y(5,n) + 1j*Y(6,n); Y(7,n) + 1j*Y(8,n)];
        FF = [F(1,n) + 1j*F(2,n); F(3,n) + 1j*F(4,n); ...
              F(5,n) + 1j*F(6,n); F(7,n) + 1j*F(8,n)];
               
        % CPE:
        phi     = exp(-1j*(angle(A) - angle(FF)));
        allPhi  = [phi allPhi(:,1:end-1)];
        phi     = sign(mean(allPhi,2));
        A       = A .* phi;
        Y(:,n)  = [real(A(1,:)); imag(A(1,:));...
                   real(A(2,:)); imag(A(2,:));...
                   real(A(3,:)); imag(A(3,:));...
                   real(A(4,:)); imag(A(4,:))];

        % Slicer:
        error   = abs(IQmap - A(1));
        ind     = find(error == min(error));
        B(1,1)  = IQmap(symIndex(symMap(ind(1))+1));
        error   = abs(IQmap - A(2));
        ind     = find(error == min(error));
        B(2,1)  = IQmap(symIndex(symMap(ind(1))+1));
        error   = abs(IQmap - A(3));
        ind     = find(error == min(error));
        B(3,1)  = IQmap(symIndex(symMap(ind(1))+1));
        error   = abs(IQmap - A(4));
        ind     = find(error == min(error));
        B(4,1)  = IQmap(symIndex(symMap(ind(1))+1));
        
        DD       = [real(B(1,:)); imag(B(1,:));...
                    real(B(2,:)); imag(B(2,:));...
                    real(B(3,:)); imag(B(3,:));...
                    real(B(4,:)); imag(B(4,:))];

        % Calculate error:
        err(:,n)  = Y(:,n) - DD;
        
        % Update taps:
        W(:,1) = W(:,1) - mu * U' * err(1,n);
        W(:,2) = W(:,2) - mu * U' * err(2,n);
    	W(:,3) = W(:,3) - mu * U' * err(3,n);
    	W(:,4) = W(:,4) - mu * U' * err(4,n);
        W(:,5) = W(:,5) - mu * U' * err(5,n);
        W(:,6) = W(:,6) - mu * U' * err(6,n);
    	W(:,7) = W(:,7) - mu * U' * err(7,n);
    	W(:,8) = W(:,8) - mu * U' * err(8,n);
    end
end

