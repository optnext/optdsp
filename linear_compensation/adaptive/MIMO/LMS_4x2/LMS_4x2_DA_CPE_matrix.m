function [Y,W,W_CPE] = LMS_4x2_DA_CPE_matrix(X,W,W_CPE,nTaps,mu,...
    updateRate,updateOffset,F)

% Last Update: 30/06/2016


%% Input Parameters
nSamples    = single(length(X));
[Y,err]     = deal(complex(zeros(2,nSamples)));

%% Run LMS 4x2
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind) X(3,ind) X(4,ind)];
    
    % MIMO 4x2 (as vector multiplication):
    Y(:,n) = (U * W).';

    % Update MIMO Taps:
    if (rem(n+updateOffset,updateRate) == 0)
        % CPE:
        phi0    = exp(-1j*(angle(Y(:,n)) - angle(F(:,n))));
        W_CPE   = [phi0 W_CPE(:,1:end-1)];
        Y(:,n)  = Y(:,n) .* sign(mean(W_CPE,2));

        % Calculate error:
        err(:,n) = F(:,n) - Y(:,n);

        % Update LMS Taps:
        W(:,1)  = W(:,1) + mu * U' * err(1,n);
        W(:,2)  = W(:,2) + mu * U' * err(2,n);
    end
    
    % Update Index:
    ind = ind + 1;
end
