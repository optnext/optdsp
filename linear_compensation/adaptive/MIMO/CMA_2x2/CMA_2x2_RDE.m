function [Y,W] = CMA_2x2_RDE(X,W,nTaps,mu,upRate,upOff,radius)

% Last Update: 08/08/2016


%% Input Parameters
nSamples = length(X);
[Y,err] = deal(complex(zeros(2,nSamples)));
F = complex(zeros(2,1));

%% Run CMA 2x2
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Determine indices:
    ind = n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
    
    % Create a 1xN vector with all signals:
    U = [X(1,ind) X(2,ind)];
    
    % MIMO 2x2 (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+upOff,upRate) == 0)
        % Determine the closest radius:
        [~,rX] = min(abs(radius - abs(Y(1,n))));
        F(1,1) = radius(rX)^2;
        [~,rY] = min(abs(radius - abs(Y(2,n))));
        F(2,1) = radius(rY)^2;
        % Calculate error:
        err(:,n) = (F - abs(Y(:,n)).^2).*Y(:,n);
        % Update CMA Taps:
        W(:,1) = W(:,1) + mu * U' * err(1,n);
        W(:,2) = W(:,2) + mu * U' * err(2,n);
    end
end

