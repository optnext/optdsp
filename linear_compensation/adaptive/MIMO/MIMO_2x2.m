function [Srx,MIMO] = MIMO_2x2(Srx,Stx,QAM,MIMO)

% Last Update: 08/08/2017


%% Input Parser
if ~isempty(strfind(MIMO.method,'CMA'))
    MIMO_method = 'CMA';
elseif ~isempty(strfind(MIMO.method,'LMS'))
    MIMO_method = 'LMS';
elseif ~isempty(strfind(MIMO.method,'Stokes'))
    MIMO_method = 'Stokes';
end
updateRule = MIMO.current.updateRule;

%% Input Parameters
% Equalizer input signal:
if strcmp(MIMO.precision,'single')
    Srx = single(Srx);
end
% Reference training signal:
switch MIMO_method
    case 'CMA'
        switch updateRule
            case 'DA'
                F = abs(Stx).^2;
            case 'RDE'
                F = MIMO.current.radius;
        end
    case 'LMS'
        switch updateRule
            case 'DA'
                F = Stx;
            case 'DD'
                F = QAM.IQmap;
        end
end
if strcmp(MIMO.precision,'single')
    F = single(F);
end

% MIMO Parameters:
nTaps = MIMO.nTaps;
mu = MIMO.current.stepSize;
upRate = MIMO.current.updateRate;
upOff = MIMO.current.updateOffset;
if isfield(MIMO,'applyPolDemux')
    xPol = MIMO.applyPolDemux;
else
    xPol = true;
end
if isfield(MIMO,'nBits_taps')
    nBits = MIMO.nBits_taps;
else
    nBits = Inf;
end
if isfield(MIMO.current,'DEBUG')
    DEBUG = MIMO.current.DEBUG;
else
    DEBUG = false;
end

if isfield(MIMO,'nTapsCPE')
    nTapsCPE = MIMO.nTapsCPE;
else
    nTapsCPE = 0;
    MIMO.nTapsCPE = 0;
end
if strcmp(updateRule,'DD')
    symMap = QAM.symbolMapping;
    symIndex = QAM.symbolIndex;
    IQmap = QAM.IQmap;
end

% Initialize Taps:
if isfield(MIMO,'W')
    W = MIMO.W;
else
    W = complex(zeros(2*nTaps,2));
    if strncmp(MIMO_method,'CMA',3) || strcmp(updateRule,'DD')
        n = floor(nTaps/2) + 1;   % central tap
        W(n,1) = 1;
        W(end-n+1,2) = 1;
    end
end
if isfield(MIMO,'W_CPE')
    W_CPE = MIMO.W_CPE;
else
    W_CPE = complex(single(ones(2,nTapsCPE)));
end
if strcmp(MIMO.precision,'single')
    W = single(W);
    nTaps = single(nTaps);
    mu = single(mu);
    upRate = single(upRate);
    upOff = single(upOff);
    nBits = single(nBits);
    if MIMO.applyCPE
        W_CPE = single(W_CPE);
    end
    if strcmp(updateRule,'DD')
        symMap = single(symMap);
        symIndex = single(symIndex);
        IQmap = single(IQmap);
    end
end

%% Apply MIMO 2x2
if strcmp(MIMO_method,'CMA')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            [Srx,W] = CMA_2x2_DA_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_2x2_DA(Srx,F,W,nTaps,mu,upRate,upOff,xPol,nBits);
        end
    elseif strcmp(updateRule,'RDE')
        if MIMO.mex
            [Srx,W] = CMA_2x2_RDE_matrix_mex(Srx,W,nTaps,mu,upRate,upOff,F);
        else
            [Srx,W] = CMA_2x2_RDE(Srx,W,nTaps,mu,upRate,upOff,F);
        end
    end
elseif strcmp(MIMO_method,'LMS')
    if strcmp(updateRule,'DA')
        if MIMO.mex
            if MIMO.applyCPE
                % Not Implemented Yet!
                [Srx,W,MIMO.W_CPE] = LMS_2x2_DA_CPE_matrix_mex(...
                    Srx,W,W_CPE,nTaps,mu,upRate,upOff,F);
            else
                [Srx,W] = LMS_2x2_DA_mex(Srx,F,W,nTaps,mu,...
                    upRate,upOff,xPol,nBits);
            end
        else
            if MIMO.applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_2x2_DA_CPE_matrix(Srx,...
                    W,W_CPE,nTaps,mu,upRate,upOff,F);
            else
                [Srx,W,err] = LMS_2x2_DA(Srx,F,W,nTaps,mu,upRate,...
                    upOff,xPol,nBits);
            end
        end
    elseif strcmp(updateRule,'DD')
        if MIMO.mex
            if MIMO.applyCPE
                % Not Implemented Yet!
                [Srx,W,MIMO.W_CPE] = LMS_2x2_DD_CPE_matrix_mex(...
                    Srx,W,W_CPE,nTaps,mu,upRate,upOff,symMap,...
                    symIndex,IQmap);
            else
                % Not Implemented Yet!
                [Srx,W] = LMS_2x2_DD_matrix_mex(Srx,W,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
            end
        else
            if MIMO.applyCPE
                [Srx,W,MIMO.W_CPE] = LMS_2x2_DD_CPE_matrix(Srx,W,W_CPE,...
                    nTaps,mu,upRate,upOff,symMap,symIndex,IQmap);
            else
                [Srx,W] = LMS_2x2_DD_matrix(Srx,W,nTaps,mu,...
                    upRate,upOff,symMap,symIndex,IQmap);
            end
        end
    end
elseif strcmp(MIMO_method,'STOKES')
    [Srx,MIMO] = stokesEq(Srx,MIMO);
end

%% Debug
if DEBUG
    itblue = [0 0.24 0.431];
    itred = [0.73 0.07 0.169];
    figure; 
    hPlot(1) = plot(movmean(abs(err(1,:)),1000,'omitnan')); 
    hold on; 
    hPlot(2) = plot(movmean(abs(err(2,:)),1000,'omitnan'));

    set(hPlot(1),'Color',itblue);
    set(hPlot(2),'Color',itred);

    xlabel('Sample Index','Interpreter','latex');
    ylabel('Error','Interpreter','latex');
    grid on;

    xAxis = get(gca,'xAxis');
    yAxis = get(gca,'yAxis');
    set(xAxis,'TickLabelInterpreter','latex','FontSize',13);
    set(yAxis,'TickLabelInterpreter','latex','FontSize',13);
    set(gca,'GridLineStyle','--');

    axis([-inf inf 0.2 1]);

    % Annotations:
    hText = text(0.95*numel(err(1,:)),0.95,{['update every ',...
        num2str(updateRate),' samples']});
    set(hText,'Interpreter','latex','HorizontalAlignment','right',...
        'VerticalAlignment','top','Color',itblue,...
        'EdgeColor',itblue,...
        'BackgroundColor','w','FontSize',12);
end

%% Output Parameters
MIMO.W = W;
MIMO.err = [];
MIMO.F = F;

