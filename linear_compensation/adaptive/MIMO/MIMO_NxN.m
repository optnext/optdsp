function [Srx,MIMO] = MIMO_NxN(Srx,Stx,QAM,MIMO)

% Last Update: 23/08/2017


%% Input Parser
% Check MIMO method:
if ~isempty(strfind(MIMO.method,'CMA'))
    MIMO_method = 'CMA';
elseif ~isempty(strfind(MIMO.method,'LMS'))
    MIMO_method = 'LMS';
end
% Check if MIMO is real-valued:
isReal = MIMO.isReal;
% Check format of input signal:
if isReal
    TMP = zeros(2*size(Srx,1),size(Srx,2));
    for k = 1:size(Srx,1)
        TMP(2*k-1:2*k,:) = [real(Srx(k,:));
                            imag(Srx(k,:))];
    end
    Srx = TMP;
    clear TMP;
end
% Get MIMO Size:
N = size(Srx,1);

%% Set MIMO Parameters
nTaps = MIMO.nTaps;
mu = MIMO.current.stepSize;
upRate = MIMO.current.updateRate;
upOff = MIMO.current.updateOffset;
xPol = MIMO.applyPolDemux;
nBits = MIMO.nBits_taps;
nTapsCPE = MIMO.nTapsCPE;
updateRule = MIMO.current.updateRule;
DEBUG = MIMO.current.DEBUG;

%% Set Reference Signal and Constellation
switch MIMO_method
    case 'CMA'
        switch updateRule
            case 'DA'
                if isReal
                    for k = 1:N/2
                        F(2*k-1:2*k,:) = [abs(Stx(k,:)).^2;...
                                          abs(Stx(k,:)).^2];
                    end
                else
                    F = abs(Stx).^2;
                end
                C = NaN;
            case 'RDE'
                F = NaN;
                C = MIMO.current.radius;
        end
    case 'LMS'
        switch updateRule
            case 'DA'
                if isReal
                    for k = 1:N/2
                        F(2*k-1:2*k,:) = [real(Stx(k,:));...
                                          imag(Stx(k,:))];
                    end
                else
                    F = Stx;
                end
                C = NaN;
            case 'DD'
                C = QAM.IQmap.';
                F = NaN;
        end
end

%% Initialize Filter Taps
if isfield(MIMO,'W')
    W = MIMO.W;
else
    W = zeros(N*nTaps,N);
    if strcmp(MIMO_method,'CMA') || strcmp(updateRule,'DD')
        n = floor(nTaps/2) + 1; % central tap
        for k = 1:N
            W(n+(k-1)*nTaps,k) = 1;
        end
    end
end
% Set mask for filter taps:
if isfield(MIMO,'W_mask')
    W_mask = MIMO.W_mask;
else
    W_mask = ones(N*nTaps,N);
end
if ~xPol
    W_mask = PolDemux_switchOff(W_mask,N,nTaps);
end
% Taps for phase tracker:
if isfield(MIMO,'W_CPE')
    W_CPE = MIMO.W_CPE;
else
    if isReal
        nCPE = N/2;
    else
        nCPE = N;
    end
    W_CPE = complex(single(ones(nCPE,nTapsCPE)));
end

%% Set Numeric Precision
if strcmp(MIMO.precision,'single')
    Srx = single(Srx);
    nTaps = single(nTaps);
    W = single(W);
    W_mask = single(W_mask);
    W_CPE = single(W_CPE);
    mu = single(mu);
    upRate = single(upRate);
    upOff = single(upOff);
    nBits = single(nBits);
    F = single(F);
    C = single(C);
end

%% Apply MIMO NxN
if isReal
    if MIMO.mex
        [Srx,W,err] = MIMO_NxN_real_mex(Srx,F,C,MIMO_method,updateRule,...
            W,W_mask,W_CPE,nTaps,mu,upRate,upOff,nBits);
    else
        [Srx,W,err] = MIMO_NxN_real(Srx,F,C,MIMO_method,updateRule,...
            W,W_mask,W_CPE,nTaps,mu,upRate,upOff,nBits);
    end
    for k = 1:N/2
        TMP(k,:) = Srx(2*k-1,:) + 1j*Srx(2*k,:);
    end
    Srx = TMP;
else
    if MIMO.mex
        [Srx,W,err] = MIMO_NxN_complex_mex(Srx,F,C,MIMO_method,...
            updateRule,W,W_mask,W_CPE,nTaps,mu,upRate,upOff,nBits);
    else
        [Srx,W,err] = MIMO_NxN_complex(Srx,F,C,MIMO_method,updateRule,...
            W,W_mask,W_CPE,nTaps,mu,upRate,upOff,nBits);
    end
end

%% Debug Plots
if DEBUG
    itblue = [0 0.24 0.431];
    itred = [0.73 0.07 0.169];
    figure;
    hold on;
    for n = 1:size(err,1)
        hPlot(n) = plot(movmean(abs(err(n,:)),1000,'omitnan')); 
    end

%     set(hPlot(1),'Color',itblue);
%     set(hPlot(2),'Color',itred);

    xlabel('Sample Index','Interpreter','latex');
    ylabel('Error','Interpreter','latex');
    grid on;

    xAxis = get(gca,'xAxis');
    yAxis = get(gca,'yAxis');
    set(xAxis,'TickLabelInterpreter','latex','FontSize',13);
    set(yAxis,'TickLabelInterpreter','latex','FontSize',13);
    set(gca,'GridLineStyle','--');

%     axis([-inf inf 0.2 1]);

    % Annotations:
    hText = text(0.95*numel(err(1,:)),0.95,{['update every ',...
        num2str(upRate),' samples']});
    set(hText,'Interpreter','latex','HorizontalAlignment','right',...
        'VerticalAlignment','top','Color',itblue,...
        'EdgeColor',itblue,...
        'BackgroundColor','w','FontSize',12);
end

%% Output Parameters
MIMO.W = W;
MIMO.err = [];
MIMO.F = F;

end

%% Helper Functions
% Switch-off polarization demultiplexing:
function W = PolDemux_switchOff(W,N,nTaps)
    if N == 2
        W(nTaps+1:end,1) = 0;
        W(1:nTaps,2) = 0;
    elseif N == 4
        W(2*nTaps+1:end,1) = 0;
        W(2*nTaps+1:end,2) = 0;
        W(1:2*nTaps,3) = 0;
        W(1:2*nTaps,4) = 0;
    elseif N == 8
        W(2*nTaps+1:4*nTaps,1) = 0;
        W(6*nTaps+1:8*nTaps,1) = 0;
        W(2*nTaps+1:4*nTaps,2) = 0;
        W(6*nTaps+1:8*nTaps,2) = 0;
        W(1:2*nTaps,3) = 0;
        W(4*nTaps+1:6*nTaps,3) = 0;
        W(1:2*nTaps,4) = 0;
        W(4*nTaps+1:6*nTaps,4) = 0;
        W(2*nTaps+1:4*nTaps,5) = 0;
        W(6*nTaps+1:8*nTaps,5) = 0;
        W(2*nTaps+1:4*nTaps,6) = 0;
        W(6*nTaps+1:8*nTaps,6) = 0;
        W(1:2*nTaps,7) = 0;
        W(4*nTaps+1:6*nTaps,7) = 0;
        W(1:2*nTaps,8) = 0;
        W(4*nTaps+1:6*nTaps,8) = 0;
    end
end

