function [Y,W,err] = MIMO_NxN_complex(X,F,C,method,upRule,W,W_mask,...
    W_CPE,nTaps,mu,upRate,upOff,nBits)

% Last Update: 23/08/2016


%% Input Parameters
% Check for quantized taps:
if isinf(nBits)
    quantizedTaps = false;
    nLevels = single(0);
else
    quantizedTaps = true;
    nLevels = 2^(nBits-1);
end
% Check for phase tracker:
if isempty(W_CPE)
    phaseTracker = false;
else
    phaseTracker = true;
end
% Define method:
[isCMA,isLMS] = deal(false);
if strcmp(method,'CMA')
    isCMA = true;
end
if strcmp(method,'LMS')
    isLMS = true;
end
% Define update rule:
[isDA,isDD,isRDE] = deal(false);
if strcmp(upRule,'DA')
    isDA = true;
end
if strcmp(upRule,'DD')
    isDD = true;
end
if strcmp(upRule,'RDE')
    isRDE = true;
end
% Check for mask on filter taps
if any(any(W_mask - 1))
    W_mask_apply = true;
else
    W_mask_apply = false;
end
% Initialize variables:
nSamples = single(length(X));
N = single(size(X,1));
[Y,err] = deal(single(complex(NaN(N,nSamples))));
U = single(complex(zeros(1,N*nTaps)));
Fref = single(complex(zeros(N,1)));

%% Run MIMO NxN
% Determine initial indices:
ind = nTaps:-1:1;
for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
    % Create a 1xN vector with all signals:
    for k = 1:N
        U((k-1)*nTaps + 1:k*nTaps) = X(k,ind);
    end
    
    % MIMO NxN (as vector multiplication):
    Y(:,n) = (U * W).';
    
    % Update MIMO Taps:
    if (rem(n+upOff,upRate) == 0)
        % Format equalized signal:
        thisY = Y(:,n);
        if isCMA
            R = abs(thisY);
        else
            R = single(0);
        end
        
        % Choose reference signal:
        if isDA
            Fref = F(:,n);
        elseif isDD
            % Slicer:
            for k = 1:N
                [~,idx] = min(abs(C - thisY(k)));
                Fref(k,1) = C(idx);
            end
        elseif isRDE
            % Determine the closest radius:
            for k = 1:N
                [~,r] = min(abs(C - R(k)));
                Fref(k,1) = C(r)^2;
            end
        end

        % Phase Tracker:
        if phaseTracker && ~isCMA
            thisY = phaseTracking(thisY,Fref,W_CPE);
        end
                
        % Calculate error:
        if isLMS
            err(:,n) = Fref - thisY;
        elseif isCMA
            err(:,n) = (Fref - R.^2).*thisY;
        end
                
        % Update MIMO Taps:
        for k = 1:N
            W(:,k) = W(:,k) + mu * U' * err(k,n);
        end
        
        % Apply mask over filter taps:
        if W_mask_apply
            W = W.*W_mask;
        end
        
        % Quantize Taps:
        if quantizedTaps
            W = quantizeTaps(W,nLevels);
        end
    end
        
    % Update Index:
    ind = ind + 1;
end

end

%% Helper Functions
% Quantize Filter Taps:
function W = quantizeTaps(W,nLevels)
    maxIQ = max([max(max(abs(real(W)))) max(max(abs(imag(W))))]);
    W = W / maxIQ;
    W = round(W*nLevels)/nLevels * maxIQ;
end

% Use a Phase Tracker:
function thisY = phaseTracking(thisY,Fref,W_CPE)
    phi0 = exp(-1j*(angle(thisY) - angle(Fref)));
    W_CPE = [phi0 W_CPE(:,1:end-1)];
    phi = sign(mean(W_CPE,2));
    thisY = thisY .* phi;
end
