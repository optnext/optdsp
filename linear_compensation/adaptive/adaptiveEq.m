function [Srx,PARAM,EQ] = adaptiveEq(Srx,Stx,PARAM,QAM,EQ,nSpS)

% Last Update: 21/08/2017


global PROG;

%% Input Parser
% Return if EQ method is "none" or nTaps=0:
if strcmpi(EQ.method,'none') | (isfield(EQ,'nTaps') && EQ.nTaps == 0)
    return
end
EQ = adaptiveEq_inputParser(EQ,Srx,QAM);
if numel(strfind(EQ.method,'LMS')) || numel(strfind(EQ.method,'CMA'))
    EQ = adaptiveFIR_inputParser(EQ,QAM,nSpS);
end
if isfield(EQ,'nTaps')
    nTaps = EQ.nTaps;
else
    nTaps = 0;
end

%% Entrance Message
entranceMsg(PROG.MSG.adaptiveEq);
tic

%% Synchronize Tx and Rx Signals
Stx = adaptiveEq_syncTxRx(Srx,Stx,EQ,nSpS);

%% Signal Normalization
if numel(EQ.normTarget) == 1
    Srx = normalizeSignal(Srx,EQ.normTarget,EQ.normMethod);
    Stx = normalizeSignal(Stx,EQ.normTarget,EQ.normMethod);
elseif numel(EQ.normTarget) == 2
    Srx(1,:) = normalizeSignal(Srx(1,:),EQ.normTarget(1),EQ.normMethod);
    Srx(2,:) = normalizeSignal(Srx(2,:),EQ.normTarget(2),EQ.normMethod);
    Stx(1,:) = normalizeSignal(Stx(1,:),EQ.normTarget(1),EQ.normMethod);
    Stx(2,:) = normalizeSignal(Stx(2,:),EQ.normTarget(2),EQ.normMethod);
end    

%% Check for Pre-Defined FIR Taps
if EQ.usePresetTaps && ~isfield(EQ,'W')
    EQ = loadFIRtaps(EQ);
end

%% MIMO Training
nf = 0;
if isfield(EQ,'train')
    % Check for number of serial stages:
    if isfield(EQ.train,'nStages')
        nStages = EQ.train(1).nStages;
    else
        nStages = 1;
    end
    for n = 1:nStages
        % Set current stage to training:
        EQ.current = EQ.train(n);
        EQ.current.stage = 'train';
        EQ_tmp = EQ;
        if isfield(EQ,'nTaps')
            EQ_tmp.nTaps = EQ.nTaps(n);
        end
        % Select signal indices for training:
        ni = nf + 1;
        nf = ni + EQ.current.nSamples - 1;
        indT = ni:nf;
        if n > 1 && strcmp(EQ.current.multiStageStrategy,'goBack')
            indT = 1:EQ_tmp.current.nSamples;
        end
        % Select adaptive equalizer function:
        if ~isempty(strfind(EQ.method,'Stokes'))
            [Stmp,EQ_tmp] = adaptStokes_PolDemux(Srx(:,indT),EQ_tmp);
        elseif ~isempty(strfind(EQ.method,'2x2')) || ...
               ~isempty(strfind(EQ.method,'4x4')) || ...
               ~isempty(strfind(EQ.method,'8x8'))
            [~,EQ_tmp] = MIMO_NxN(Srx(:,indT),Stx(:,indT),QAM,EQ_tmp);
%         elseif ~isempty(strfind(EQ.method,'2x2'))
%             [~,EQ_tmp] = MIMO_2x2(Srx(:,indT),Stx(:,indT),QAM,EQ_tmp);
%         elseif ~isempty(strfind(EQ.method,'4x4'))
%             [~,EQ_tmp] = MIMO_4x4(Srx(:,indT),Stx(:,indT),QAM,EQ_tmp);
%         elseif ~isempty(strfind(EQ.method,'8x8'))
%             [~,EQ_tmp] = MIMO_8x8(Srx(:,indT),Stx(:,indT),QAM,EQ_tmp);
        elseif ~isempty(strfind(EQ.method,'4x2'))
            [~,EQ_tmp] = MIMO_4x2(Srx(:,indT),Stx(:,indT),QAM,EQ_tmp);
        elseif ~isempty(strfind(EQ.method,'8x4'))
            [~,EQ_tmp] = MIMO_8x4(Srx(:,indT),Stx(:,indT),QAM,EQ_tmp);
        else
            error('Invalid adaptive equalizer submethod!');
        end
        % Parse equalizer parameters back to the EQ struct:
        EQ = EQ_tmp;
        if nTaps
            EQ.nTaps = nTaps;
        end
        EQ.train(n).W = EQ_tmp.W;
        EQ.train(n).err = EQ_tmp.err;
        if isfield(EQ_tmp,'F')
            EQ.train(n).F = EQ_tmp.F;
        end
        EQ.train(n).index = [ni nf];
    end
    % Remove current field from EQ struct:
    EQ = rmfield(EQ,'current');
end

%% MIMO Demux
if isfield(EQ,'demux')
    % If RDE is the update rule, check if DA has been used in training:
    if isfield(EQ,'train') && isfield(EQ.demux,'updateRule')
        if strcmpi(EQ.demux.updateRule,'RDE') && ...
                strcmpi(EQ.train(end).updateRule,'DA') && ...
                isfield(EQ.train(end),'F')
            radius = sort(sqrt(unique(EQ.train(end).F)),'ascend');
            EQ.demux.radius = [radius(diff(radius)>1e-2); radius(end)].';
        end
    end
    % Check for number of serial stages:
    if isfield(EQ.demux,'nStages')
        nStages = EQ.demux(1).nStages;
    else
        nStages = 1;
    end
    for n = 1:nStages
        % Set current stage to demux:
        EQ.current = EQ.demux(n);
        EQ.current.stage = 'demux';
        EQ_tmp = EQ;
        if isfield(EQ,'nTaps')
            nTaps = EQ.nTaps(end);
            EQ_tmp.nTaps = nTaps;
        else
            nTaps = 0;
        end
        % Select signal indices for demux:
        if strcmp(EQ.train2demux,'goBack') || ~isfield(EQ,'train')
            i1 = 1;
        else
            i1 = indT(end) - nTaps - mod(indT(end),nSpS) + ...
                mod(nTaps,nSpS) + 1;
        end
        indD = i1:length(Srx);
        % Synchronize reference signal:
        if isfield(EQ_tmp,'txSignal')
            EQ_tmp.txSignal = EQ_tmp.txSignal(:,indD);
        end
        % Select adaptive equalizer function:
        if ~isempty(strfind(EQ.method,'Stokes'))
            if EQ.PDLcomp(1) == 1
                EQ_tmp.M_PDL = adaptStokes_PDL(Srx,Stmp);
            end
            [Srx,EQ_tmp] = adaptStokes_PolDemux(Srx(:,indD),EQ_tmp);
        elseif ~isempty(strfind(EQ.method,'2x2')) || ...
               ~isempty(strfind(EQ.method,'4x4')) || ...
               ~isempty(strfind(EQ.method,'8x8'))
            [Srx,EQ_tmp] = MIMO_NxN(Srx(:,indD),Stx(:,indD),QAM,EQ_tmp);
%         elseif ~isempty(strfind(EQ.method,'2x2'))
%             [Srx,EQ_tmp] = MIMO_2x2(Srx(:,indD),Stx(:,indD),QAM,EQ_tmp);
%         elseif ~isempty(strfind(EQ.method,'4x4'))
%             [Srx,EQ_tmp] = MIMO_4x4(Srx(:,indD),Stx(:,indD),QAM,EQ_tmp);
%         elseif ~isempty(strfind(EQ.method,'8x8'))
%             [Srx,EQ_tmp] = MIMO_8x8(Srx(:,indD),Stx(:,indD),QAM,EQ_tmp);
        elseif ~isempty(strfind(EQ.method,'4x2'))
            [Srx,EQ_tmp] = MIMO_4x2(Srx(:,indD),Stx(:,indD),QAM,EQ_tmp);
        elseif ~isempty(strfind(EQ.method,'8x4'))
            [Srx,EQ_tmp] = MIMO_8x4(Srx(:,indD),Stx(:,indD),QAM,EQ_tmp);
        else
            error('Invalid adaptive equalizer submethod!');
        end
        % Parse equalizer parameters back to the EQ struct:
        EQ = EQ_tmp;
        if nTaps
            EQ.nTaps = nTaps;
        end
        EQ.demux(n).W = EQ_tmp.W;
        EQ.demux(n).err = EQ_tmp.err;
        if isfield(EQ_tmp,'F')
            EQ.demux(n).F = EQ_tmp.F;
        end
    end
    % Remove current field from EQ struct:
    EQ = rmfield(EQ,'current');
end

%% Save Tap Weights
if EQ.saveTaps
    saveFIRtaps(EQ);
end

%% Remove Edge Samples and Update PARAM Struct
PARAM.nSamples = length(Srx);
PARAM = setSimulationParams(PARAM);

if ~isempty(nTaps)
    FIR.method = 'vector';
    FIR.nTaps = nTaps;
    [Srx,PARAM,idx] = rmvEdgeSamplesFIR(Srx,PARAM,FIR,nSpS);
    indD = indD(idx);
end

%% Save signal indices used for demux:
EQ.indDemux = [indD(1) indD(end)];

%% Print Parameters
if PROG.showMessagesLevel >= 2
    fprintf('------------------------------------------------------------------');
    fprintf('\nPolarization Demux PARAMETERS:\n');
    fprintf('EQ method (EQ.method):                             %s \n',EQ.method);
    if ~isempty(nTaps)
        fprintf('EQ number of taps (EQ.nTaps):                      %1.0f \n',EQ.nTaps);
    end
    if isfield(EQ,'train')
        fprintf('EQ number of training samples (EQ.train.nSamples): %5.0f \n',EQ.train.nSamples);
        fprintf('EQ step-size for training (EQ.train.stepSize):     %1.2e \n',EQ.train.stepSize);
    end
    if isfield(EQ,'demux')
        fprintf('EQ step-size for demux (EQ.demux.stepSize):        %1.2e \n',EQ.demux.stepSize);
    end
    fprintf('------------------------------------------------------------------\n');
end

%% Elapsed Time
elapsedTime = toc;
myMessages(['Adaptive Equalization - Elapsed Time: ',...
    num2str(elapsedTime,'%1.4f'),' [s]\n'],1);

