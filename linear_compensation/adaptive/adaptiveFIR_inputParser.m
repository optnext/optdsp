function EQ = adaptiveFIR_inputParser(EQ,QAM,nSpS)

% Last Update: 23/08/2017


%% Set Default Training Parameters
if isfield(EQ,'train')
    if ~isfield(EQ.train,'updateRate')
        EQ.train.updateRate = nSpS;
    end
    if ~isfield(EQ.train,'updateOffset')
        EQ.train.updateOffset = nSpS - 1;
    end
    if ~isfield(EQ.train,'multiStageStrategy')
        EQ.train.multiStageStrategy = 'goBack';
    end
    if ~isfield(EQ.train,'nStages')
        EQ.train.nStages = length(EQ.train.stepSize);
    end
    if ~isfield(EQ.train,'DEBUG')
        EQ.train.DEBUG = false;
    end
    nStages = EQ.train.nStages;
    if numel(strfind(EQ.method,'CMA'))
        if ~isfield(EQ.train,'constrained')
            EQ.train.constrained = false;
        end
        if ~isfield(EQ.train,'radiusFormat')
            for n = 1:nStages
                EQ.train.radiusFormat{n} = QAM.modFormat;
            end
        end
    end        
    TMP = EQ.train(1);
    if length(EQ.nTaps) == 1
        TMP.nTaps = repmat(EQ.nTaps,nStages,1);
    else
        TMP.nTaps = EQ.nTaps;
    end
    if length(TMP.nSamples) == 1
        TMP.nSamples = repmat(TMP.nSamples,nStages,1);
    end
    if length(TMP.percentageTrain) == 1
        TMP.percentageTrain = repmat(TMP.percentageTrain,nStages,1);
    end
    if length(TMP.stepSize) == 1
        TMP.stepSize = repmat(TMP.stepSize,nStages,1);
    end
    if length(TMP.updateRate) == 1
        TMP.updateRate = repmat(TMP.updateRate,nStages,1);
    end
    if length(TMP.updateOffset) == 1
        TMP.updateOffset = repmat(TMP.updateOffset,nStages,1);
    end
    if length(TMP.DEBUG) == 1
        TMP.DEBUG = repmat(TMP.DEBUG,nStages,1);
    end
    if ~iscell(TMP.updateRule)
        updateRule = TMP.updateRule;
        TMP = rmfield(TMP,'updateRule');
        for n = 1:nStages
            TMP.updateRule{n} = updateRule;
        end
    end
    TMP.multiStageStrategy = EQ.train(1).multiStageStrategy;
    for n = 1:nStages
        EQ.nTaps(n) = TMP.nTaps(n);
        EQ.train(n).nSamples = TMP.nSamples(n);
        EQ.train(n).percentageTrain = TMP.percentageTrain(n);
        EQ.train(n).stepSize = TMP.stepSize(n);
        EQ.train(n).updateRate = TMP.updateRate(n);
        EQ.train(n).updateOffset = TMP.updateOffset(n);
        EQ.train(n).nStages = TMP.nStages;
        EQ.train(n).updateRule = TMP.updateRule{n};
        EQ.train(n).multiStageStrategy = TMP.multiStageStrategy;
        EQ.train(n).DEBUG = TMP.DEBUG(n);
    end
    if numel(strfind(EQ.method,'CMA'))
        if length(TMP.constrained) == 1
            constrained = TMP.constrained;
            for n = 1:nStages
                TMP.constrained(n) = constrained;
            end
        end
        if isfield(TMP,'radiusFormat') && ~iscell(TMP.radiusFormat)
            radiusFormat = TMP.radiusFormat;
            TMP = rmfield(TMP,'radiusFormat');
            for n = 1:nStages
                TMP.radiusFormat{n} = radiusFormat;
            end
        end
        for n = 1:nStages
            EQ.train(n).constrained = TMP.constrained(n);
            if isfield(TMP,'radiusFormat') && n<=length(TMP.radiusFormat)
                EQ.train(n).radiusFormat = TMP.radiusFormat{n};
                EQ.train(n).radius = getQAMradii(EQ.train(n).radiusFormat);
            end
        end
    end
end

%% Set Default Demux Parameters
if isfield(EQ,'demux')
    if ~isfield(EQ.demux,'updateRate')
        EQ.demux.updateRate = nSpS;
    end
    if ~isfield(EQ.demux,'updateOffset')
        EQ.demux.updateOffset = nSpS - 1;
    end
    if ~isfield(EQ.demux,'nStages')
        EQ.demux.nStages = length(EQ.demux.stepSize);
    end
    if ~isfield(EQ.demux,'DEBUG')
        EQ.demux.DEBUG = false;
    end
    nStages = EQ.demux.nStages;
    if numel(strfind(EQ.method,'CMA'))
        if ~isfield(EQ.demux,'constrained')
            EQ.demux.constrained = false;
        end
        if ~isfield(EQ.demux,'radiusFormat')
            EQ.demux.radiusFormat = QAM.modFormat;
        end
    end
    TMP = EQ.demux(1);
    if length(TMP.stepSize) == 1
        TMP.stepSize = repmat(TMP.stepSize,nStages,1);
    end
    if length(TMP.updateRate) == 1
        TMP.updateRate = repmat(TMP.updateRate,nStages,1);
    end
    if length(TMP.updateOffset) == 1
        TMP.updateOffset = repmat(TMP.updateOffset,nStages,1);
    end
    if length(TMP.DEBUG) == 1
        TMP.DEBUG = repmat(TMP.DEBUG,nStages,1);
    end
    if ~iscell(TMP.updateRule)
        updateRule = TMP.updateRule;
        TMP = rmfield(TMP,'updateRule');
        for n = 1:nStages
            TMP.updateRule{n} = updateRule;
        end
    end
    for n = 1:nStages
        EQ.demux(n).stepSize = TMP.stepSize(n);
        EQ.demux(n).updateRate = TMP.updateRate(n);
        EQ.demux(n).updateOffset = TMP.updateOffset(n);
        EQ.demux(n).nStages = TMP.nStages;
        EQ.demux(n).updateRule = TMP.updateRule{n};
        EQ.demux(n).DEBUG = TMP.DEBUG(n);
    end
    if numel(strfind(EQ.method,'CMA'))
        if length(TMP.constrained) == 1
            constrained = TMP.constrained;
            for n = 1:nStages
                TMP.constrained(n) = constrained;
            end
        end
        if isfield(TMP,'radiusFormat') && ~iscell(TMP.radiusFormat)
            radiusFormat = TMP.radiusFormat;
            TMP = rmfield(TMP,'radiusFormat');
            for n = 1:nStages
                TMP.radiusFormat{n} = radiusFormat;
            end
        end
        for n = 1:nStages
            EQ.demux(n).constrained = TMP.constrained(n);
            if isfield(TMP,'radiusFormat')
                EQ.demux(n).radiusFormat = TMP.radiusFormat{n};
                EQ.demux(n).radius = getQAMradii(EQ.demux(n).radiusFormat);
            end
        end
    end 
end


