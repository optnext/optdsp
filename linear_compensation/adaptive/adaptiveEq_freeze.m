function [S] = adaptiveEq_freeze(S,W,nTaps,EQ_method)

% Last Update: 03/04/2017


%% Apply Equalizer
switch EQ_method
    case '2x2'
        Wxx = W(1:nTaps,1);
        Wyx = W(nTaps+1:end,1);
        Wxy = W(1:nTaps,2);
        Wyy = W(nTaps+1:end,2);
        X = S(1,:);
        Y = S(2,:);

        Xout = conv(X,Wxx,'same') + conv(Y,Wyx,'same');
        Yout = conv(Y,Wyy,'same') + conv(X,Wxy,'same');
        S = [Xout; Yout];
    otherwise
        error('Not implemented yet!');
end
