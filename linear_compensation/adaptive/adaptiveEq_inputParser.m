function [EQ,Srx] = adaptiveEq_inputParser(EQ,Srx,QAM)

% Last Update: 23/08/2017


%% Check Input Signal
nSamples = size(Srx,2);

%% Check if Signal is in the Correct Format
if any(strcmp(EQ.method(end-2:end),{'4x2','8x4'}))
    if size(Srx,1) == 2
        Srx = [real(Srx(1,:));
              1j*imag(Srx(1,:));
              real(Srx(2,:));
              1j*imag(Srx(2,:))];
    end
end

%% Set Default General Parameters
% Set default normalization target and method:
if ~isfield(EQ,'normTarget')
    EQ.normTarget = QAM.meanConstPower;
end
if ~isfield(EQ,'normMethod')
    EQ.normMethod = 'joint';
end
% Set default flag for real-valued equalizer:
if ~isfield(EQ,'isReal')
    if any(strcmp(EQ.method(end-2:end),{'4x4','8x8'}))
        EQ.isReal = true;
    else
        EQ.isReal = false;
    end
end
% Set default pol-demux flag:
if ~isfield(EQ,'applyPolDemux')
    EQ.applyPolDemux = true;
end
% Set default precision for filter taps:
if ~isfield(EQ,'nBits_taps')
    EQ.nBits_taps = Inf;
end
% Set default number of taps for phase tracker:
if ~isfield(EQ,'nTapsCPE')
    EQ.nTapsCPE = 0;
end
% Set default strategy for switching between training and demux (continue /
% goBack):
if ~isfield(EQ,'train2demux')
    EQ.train2demux = 'continue';
end
% Set default application range (in case of subcarrier mux):
if ~isfield(EQ,'applicationRange')
    EQ.applicationRange = 'all';
end
% Set default precision:
if ~isfield(EQ,'precision')
    EQ.precision = 'double';
end
% Set default .mex option:
if ~isfield(EQ,'mex')
    EQ.mex = false;
end
if EQ.mex
    EQ.precision = 'single';
end
% Set default debug plots:
if ~isfield(EQ,'debugPlots')
    EQ.debugPlots = {};
end
% Set default preset taps usage:
if ~isfield(EQ,'usePresetTaps')
    EQ.usePresetTaps = false;
end
% Set default saveTaps flag:
if ~isfield(EQ,'saveTaps')
    EQ.saveTaps = false;
end
if EQ.saveTaps
    % Set default saveTaps flag:
    if ~isfield(EQ,'overwriteTaps')
        EQ.overwriteTaps = 2;
    end
end

%% Set Default Training Parameters
if isfield(EQ,'train')
    % Default update rule for training:
    if isfield(EQ.train,'updateRule')
        switch EQ.train.updateRule
            case {'DA','data-aided','LMS'}
                EQ.train.updateRule = 'DA';
            case {'DD','decision-directed'}
                EQ.train.updateRule = 'DD';
            case {'blind','QPSK','CMA','RDE','radius-directed'}
                EQ.train.updateRule = 'RDE';
        end
    end
    % Default number of samples for training stage:
    if ~isfield(EQ.train,'nSamples')
        if isfield(EQ.train,'percentageTrain')
            EQ.train.nSamples = round(EQ.train.percentageTrain*nSamples);
        else
            EQ.train.nSamples = 0.5*nSamples;
            EQ.train.percentageTrain = 0.5;
        end
    elseif EQ.train.nSamples > nSamples
        warning('The number of required training samples is higher than the total number of samples. The total number of samples will be used instead.');
        EQ.train.nSamples = nSamples;
        EQ.train.percentageTrain = 1;
    else
        EQ.train.percentageTrain = EQ.train.nSamples/nSamples;
    end
    % Default step-size for training:
    if ~isfield(EQ.train,'stepSize')
        EQ.train.stepSize = 1e-3;
    end
end

%% Set Default Demux Parameters
if isfield(EQ,'demux')
    % Default update rule for training:
    if isfield(EQ.demux,'updateRule')
        switch EQ.demux.updateRule
            case {'DA','data-aided','LMS'}
                EQ.demux.updateRule = 'DA';
            case {'DD','decision-directed'}
                EQ.demux.updateRule = 'DD';
            case {'blind','QPSK','CMA','RDE','radius-directed'}
                EQ.demux.updateRule = 'RDE';
        end
    end
    % Default step-size for demux:
    if ~isfield(EQ.demux,'stepSize')
        EQ.demux.stepSize = 1e-4;
    end
end

%% Set Default Synchronization Parameters
if ~isfield(EQ,'SYNC') || ~isfield(EQ.SYNC,'applySync')
    if isfield(EQ,'train') && isfield(EQ.train,'updateRule')
        applySyncTrain = strcmp('DA',EQ.train.updateRule);
    else
        applySyncTrain = false;
    end
    if isfield(EQ,'demux') && isfield(EQ.demux,'updateRule')
        applySyncDemux = strcmp('DA',EQ.demux.updateRule);
    else
        applySyncDemux = false;
    end
    EQ.SYNC.applySync = applySyncTrain || applySyncDemux;
end
if ~isfield(EQ.SYNC,'method')
    if any([numel(strfind(QAM.modFormat,'QPSK')) ...
            numel(strfind(QAM.modFormat,'4QAM')) ...
            numel(strfind(QAM.modFormat,'BPSK'))])
        EQ.SYNC.method = 'complexField';
    else
        EQ.SYNC.method = 'abs';
    end
end
    

