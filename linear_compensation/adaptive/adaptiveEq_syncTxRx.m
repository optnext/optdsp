function Stx = adaptiveEq_syncTxRx(Srx,Stx,MIMO,nSpS)

% Last Update: 08/08/2017


%% Check if Sync is Active
if ~MIMO.SYNC.applySync
    % Repeat Stx up to the length of Srx:
    nRep = floor(length(Srx)/length(Stx));
    nHead = mod(length(Srx),length(Stx));
    Stx = [repmat(Stx,1,nRep) Stx(:,1:nHead)];
    return;
end

%% Apply Synchronization Between Tx and Rx Signals
if any(strcmp(MIMO.method(end-2:end),{'4x4','2x2','4x2'}))
    % Upsample Tx Signal:
    TX = rectpulse(Stx.',nSpS).';
    % Synchronize Tx and Rx Signals:
    if strcmp(MIMO.method(end-2:end),'4x2')
        TMP = [Srx(1,:) + 1j*Srx(2,:);...
               Srx(3,:) + 1j*Srx(4,:)];
        [Stx,MIMO.SYNC] = syncSignals_2x2(TMP,TX,MIMO.SYNC);
    else
        [Stx,MIMO.SYNC] = syncSignals_2x2(Srx,TX,MIMO.SYNC);
    end
elseif any(strcmp(MIMO.method(end-2:end),{'8x8','8x4'}))
    % Upsample Tx Signal:
    TX1 = rectpulse(Stx(1:2,:).',nSpS).';
    TX2 = rectpulse(Stx(3:4,:).',nSpS).';
    % Synchronize Tx and Rx Signals:
    if strcmp(MIMO.method(end-2:end),'8x4')
        TMP = [Srx(1,:) + 1j*Srx(2,:);...
               Srx(3,:) + 1j*Srx(4,:)];
        [Stx_sync(1:2,:),MIMO.SYNC_out{1}] = ...
            syncSignals_2x2(TMP,TX1,MIMO.SYNC);
        TMP = [Srx(5,:) + 1j*Srx(6,:);...
               Srx(7,:) + 1j*Srx(8,:)];
        [Stx_sync(3:4,:),MIMO.SYNC_out{2}] = ...
            syncSignals_2x2(TMP,TX2,MIMO.SYNC);
    else
        [Stx_sync(1:2,:),MIMO.SYNC_out{1}] = ...
            syncSignals_2x2(Srx(1:2,:),TX1,MIMO.SYNC);
        [Stx_sync(3:4,:),MIMO.SYNC_out{2}] = ...
            syncSignals_2x2(Srx(3:4,:),TX2,MIMO.SYNC);
    end
    Stx = Stx_sync;
end
