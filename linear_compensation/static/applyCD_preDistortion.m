function [S] = applyCD_preDistortion(S,PARAM,preCD,FIBER)

% Last Update: 23/02/2017


global CONST;

%% Input Parameters
if isfield(CONST,'lambda')
    lambda = CONST.lambda;                                                  % wavelength [nm]
else
    lambda = 1550;
end
if isfield(CONST,'c')
    c = CONST.c * 1e-3;                                                     % light speed in vacuum [nm/ps]
else
    c = 2.9979e5;
end

if isfield(preCD,'accCD')
    Dpre = preCD.accCD;
else
    if exist('FIBER','var')
        if isfield(FIBER,'D')
            D = FIBER.D;
        elseif isfield(FIBER,'beta2')
            D = -2*pi*c*FIBER.beta2/lambda^2*1e12;
        else
            error('Please specify either FIBER.D or FIBER.beta2');
        end
        if isfield(FIBER,'L')
            L = FIBER.L;
        else
            error('Please specify the total link length, FIBER.L');
        end
        Dpre = D * L;
    end
end

if ~isfield(preCD,'DFT')
    preCD.DFT.blockProc = 'NoOverlap';
    preCD.DFT.nFFT = PARAM.nSamples;
end
if ~isfield(preCD,'IQ')
    preCD.IQ = 'complex';
end

%% Configure DFT Parameters
DFT = FFTconf(preCD.DFT,PARAM);
nBlocksFFT = DFT.nTotalBlocks;
f = fftshift(DFT.PARAM.f') * 1e-12;                                         % frequency [1/ps]

%% Apply CD Pre-Distortion
CD_TF = repmat(exp(-1i*pi*Dpre*lambda^2/c*f.^2),1,nBlocksFFT);

% FFT Split:
[Sx_FFT,DFT] = FFTsplit(S(1,:),DFT);
[Sy_FFT,DFT] = FFTsplit(S(2,:),DFT);
Sx_FFT = Sx_FFT.';
Sy_FFT = Sy_FFT.';

% Frequency-Domain Multiplification by CD Transfer Function:
if strcmpi(preCD.IQ,'complex')
    Sx_FFT = ifft(fft(Sx_FFT) .* CD_TF);
    Sy_FFT = ifft(fft(Sy_FFT) .* CD_TF);
elseif strcmpi(preCD.IQ,'ReIm')
    Xi = real(Sx_FFT);
    Xq = imag(Sx_FFT);
    Yi = real(Sy_FFT);
    Yq = imag(Sy_FFT);
    Xi = ifft(fft(Xi) .* CD_TF);
    Xq = ifft(fft(Xq) .* CD_TF);
    Yi = ifft(fft(Yi) .* CD_TF);
    Yq = ifft(fft(Yq) .* CD_TF);
end

% FFT Join:
if strcmpi(preCD.IQ,'complex')
    S(1,:) = FFTjoin(Sx_FFT.',DFT);
    S(2,:) = FFTjoin(Sy_FFT.',DFT);
elseif strcmpi(preCD.IQ,'ReIm')
    Xi = FFTjoin(Xi.',DFT);
    Xq = FFTjoin(Xq.',DFT);
    Yi = FFTjoin(Yi.',DFT);
    Yq = FFTjoin(Yq.',DFT);
    
    S = [Xi; 1j*Xq; Yi; 1j*Yq];
end

