function [Sout,PARAMout] = FD_CDE_LAB(Sin,PARAMin,FIBER,MIMO,w0)

% Last Update: 28/06/2016


%% Input Parser
if ~exist('w0','var')
    w0 = 0;
end

%% 
TF = exp(-1j*0.5*FIBER.beta2*(fftshift(PARAMin.omega) - w0).^2*FIBER.L);
switch MIMO.method
    case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
        Xi(1,:) = fft(ifft(real(Sin(1,:))).*TF);
        Xi(2,:) = fft(ifft(real(Sin(2,:))).*TF);
        Xq(1,:) = 1j*fft(ifft(imag(Sin(1,:))).*TF);
        Xq(2,:) = 1j*fft(ifft(imag(Sin(2,:))).*TF);
        Sout = [Xi(1,:);...
                Xq(1,:);...
                Xi(2,:);...
                Xq(2,:)];
    otherwise,
        Sout(1,:) = ifft(fft(Sin(1,:)).*TF);
        Sout(2,:) = ifft(fft(Sin(2,:)).*TF);
end
PARAMout = PARAMin;

