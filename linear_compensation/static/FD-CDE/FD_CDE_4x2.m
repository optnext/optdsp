function [A_EQ,CDE] = FD_CDE_4x2(A,FIBER,CDE)

% Last Update: 18/04/2016


%% Input Parser
if ~FIBER.nSpans
    A_EQ = A;
    return;
end
if ~isfield(CDE,'nStepsPerFFT')
    CDE.nStepsPerFFT        = 1;
end
if ~isfield(CDE,'nSpansPerStep')
    if ~isfield(CDE,'nIter')
        CDE.nSpansPerStep   = 1;
    else
        CDE.nSpansPerStep   = FIBER.nSpans/CDE.nIter;
    end
elseif strcmp(CDE.nSpansPerStep,'max')
    CDE.nSpansPerStep       = FIBER.nSpans;
end
if ~isfield(CDE,'nPol')
    CDE.nPol                = size(A,1);
end
if ~isfield(CDE,'WDM')
    CDE.WDM.nChannels       = 1;
    CDE.WDM.chSpacing       = 0;
    CDE.WDM.channelsBP      = 1;
end
if ~isfield(CDE,'relativeBW')
    CDE.relativeBW          = 1;
end
if ~isfield(CDE,'centerFreq')
    CDE.centerFreq          = 0;
end

%% Configure FD-CDE
CDE.FIBER       = FIBER;
CDE             = FD_CDE_config(CDE);

%% Input Parameters
% Signal Parameters:
nPol            = CDE.nPol;                      % number of polarization components
% CDE Parameters:
nStepsPerFFT    = CDE.nStepsPerFFT;      % number of steps per FFT
nIter           = CDE.nIter;                    % total number of CDE iterations
ni              = CDE.index(1);
nf              = CDE.index(2);
TF              = CDE.TF;                          % CDE transfer function
dz              = CDE.dz;                          % CDE spatial-step [km]
if length(dz) > 1
    nIter       = nIter - 1;
end
% FFT parameters:
DFT             = CDE.DFT;
nBlocks         = DFT.nTotalBlocks;           % total number of FFT block (upper + lower FFT layers)
nFFT            = CDE.DFT.nFFT;
% WDM parameters:
WDMch           = CDE.WDM.channelsBP;           % indexes of WDM channels to be equalized
nWDMch_CDE      = length(WDMch);           % number of WDM channels to be equalized

%% Auxiliary Variables
zeroPadding1    = zeros(1,ni-1);
zeroPadding2    = zeros(1,nFFT-nf);

%% FD-CDE
for k = 1:nWDMch_CDE
    for kk = 1:nPol
        mm = nPol*WDMch(k)-nPol+kk;
        % ProgressBar Initialization:
        nIterTotal = nIter*nBlocks;
        progressbar(['Total Number of Iterations (',num2str(nIterTotal),' in total)'],['Current FD-CDE Transfer Functions (',num2str(nIter),' in total)'],['Current FFT Block (',num2str(nBlocks),' in total)']);
        % FFT split for the first fiber span (mandatory):
        [A_FFT,DFT] = FFTsplit(A(mm,:),DFT);
        % FD-CDE Transfer Functions:
        for n = 1:nIter
            % FFT Split:
            if n > 1 && ~mod(n,nStepsPerFFT)
                [A_FFT,DFT] = FFTsplit(A(mm,:),DFT);
            end
            % Block Processing:
            for m = 1:nBlocks
                % FFT:
                A_w_I = ifft(real(A_FFT(m,:)));
                A_w_Q = ifft(imag(A_FFT(m,:)));
                % Adjust BW to CDE Settings:
                A_w_I = A_w_I(ni:nf);
                A_w_Q = A_w_Q(ni:nf);
                % CDE:
                A_CD_w_I = A_w_I.*fftshift(TF);
                A_CD_w_Q = A_w_Q.*fftshift(TF);
                % Zero Padding:
                A_CD_w_I = [zeroPadding1 A_CD_w_I zeroPadding2];
                A_CD_w_Q = [zeroPadding1 A_CD_w_Q zeroPadding2];
                % IFFT:
                A_FFT_I(m,:) = fft(A_CD_w_I);
                A_FFT_Q(m,:) = fft(A_CD_w_Q);
                % ProgressBar:
                iter = (n-1)*nBlocks+m;
                progressbar(iter/nIterTotal,(n-1)/nIter,m/nBlocks);
            end
            % FFT Join:
            if ~mod(n,nStepsPerFFT)
                A_EQ(2*mm-1,:) = FFTjoin(A_FFT_I,DFT);
                A_EQ(2*mm,:) = 1j*FFTjoin(A_FFT_Q,DFT);
            end
        end
    end
end

