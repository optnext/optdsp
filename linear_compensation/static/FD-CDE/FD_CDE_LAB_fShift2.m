function [Sout,PARAMout] = FD_CDE_LAB_fShift2(Sin,PARAMin,FIBER,MIMO,w0)

% Last Update: 22/07/2016


%% Input Parser
if ~exist('w0','var')
    w0 = zeros(1,FIBER.nSpans/4);
end

%%
switch MIMO.method
    case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
        Xi = real(Sin(1,:));
        Xq = imag(Sin(1,:));
        Yi = real(Sin(2,:));
        Yq = imag(Sin(2,:));
    otherwise,
        X = Sin(1,:);
        Y = Sin(2,:);
end

%% 
TF = exp(-1j*0.5*FIBER.beta2*(fftshift(PARAMin.omega)).^2*FIBER.Lspan*4);
t = PARAMin.t;
for n = 1:FIBER.nSpans/4
    switch MIMO.method
        case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
            Xi = Xi.*exp(1j*w0(n)*t);
            Xq = Xq.*exp(1j*w0(n)*t);
            Yi = Yi.*exp(1j*w0(n)*t);
            Yq = Yq.*exp(1j*w0(n)*t);
            Xi = fft(ifft(Xi).*TF);
            Xq = fft(ifft(Xq).*TF);
            Yi = fft(ifft(Yi).*TF);
            Yq = fft(ifft(Yq).*TF);
        otherwise,
            X = X.*exp(1j*w0(n)*t);
            Y = Y.*exp(1j*w0(n)*t);
            X = ifft(fft(X).*TF);
            Y = ifft(fft(Y).*TF);
    end
end

%% Output Parameters
switch MIMO.method
    case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
        Sout = [Xi; 1j*Xq; Yi; 1j*Yq];
    otherwise,
        Sout = [X; Y];
end
PARAMout = PARAMin;

