function [A_EQ,CDE] = FD_CDE(A,FIBER,CDE)

% Last Update: 03/04/2017


%% Input Parser
if ~FIBER.nSpans
    A_EQ = A;
    return;
end
if ~isfield(CDE,'nPol')
    CDE.nPol = size(A,1);
end
if ~isfield(CDE,'WDM')
    CDE.WDM.nChannels       = 1;
    CDE.WDM.chSpacing       = 0;
    CDE.WDM.channelsBP      = 1;
end
if ~isfield(CDE,'centerFreq')
    CDE.centerFreq          = 0;
end

%% Configure FD-CDE
CDE.FIBER       = FIBER;
CDE             = FD_CDE_config(CDE);

%% Input Parameters
% Signal Parameters:
nPol            = CDE.nPol;                      % number of polarization components
% CDE Parameters:
nIter           = CDE.nIter;                    % total number of CDE iterations
TF              = CDE.TF;                          % CDE transfer function
% FFT parameters:
DFT             = CDE.DFT;
nBlocks         = DFT.nTotalBlocks;           % total number of FFT block (upper + lower FFT layers)
% WDM parameters:
WDMch           = CDE.WDM.channelsBP;           % indexes of WDM channels to be equalized
nWDMch_CDE      = length(WDMch);           % number of WDM channels to be equalized

%% FD-CDE
for k = 1:nWDMch_CDE
    for kk = 1:nPol
        mm = nPol*WDMch(k)-nPol+kk;
        % ProgressBar Initialization:
        nIterTotal = nIter*nBlocks;
        progressbar(['Total Number of Iterations (',num2str(nIterTotal),' in total)'],['Current FD-CDE Transfer Functions (',num2str(nIter),' in total)'],['Current FFT Block (',num2str(nBlocks),' in total)']);
        % FFT split for the first fiber span (mandatory):
        [A_FFT,DFT] = FFTsplit(A(mm,:),DFT);
        % FD-CDE Transfer Functions:
        for n = 1:nIter
            % FFT Split:
            if n > 1
                [A_FFT,DFT] = FFTsplit(A(mm,:),DFT);
            end
            % Block Processing:
            for m = 1:nBlocks
                % FFT:
                A_w = ifftshift(ifft(A_FFT(m,:)));
                % CDE:
                A_CD_w = A_w.*TF(n,:);
                % IFFT:
                A_FFT(m,:) = fft(fftshift(A_CD_w));
                % ProgressBar:
                iter = (n-1)*nBlocks+m;
                progressbar(iter/nIterTotal,(n-1)/nIter,m/nBlocks);
            end
            % FFT Join:
            A(mm,:) = FFTjoin(A_FFT,DFT);
        end
    end
end

%% Output Signal
A_EQ = A;

