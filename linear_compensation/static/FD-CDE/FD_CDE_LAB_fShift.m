function [Sout,PARAMout] = FD_CDE_LAB_fShift(Sin,PARAMin,FIBER,MIMO,w0)

% Last Update: 22/07/2016


%% Input Parser
if ~exist('w0','var')
    w0 = zeros(1,FIBER.nSpans/4);
end

%%
switch MIMO.method
    case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
        Xi = real(Sin(1,:));
        Xq = imag(Sin(1,:));
        Yi = real(Sin(2,:));
        Yq = imag(Sin(2,:));
    otherwise,
        X = Sin(1,:);
        Y = Sin(2,:);
end

%% 
FIBER.beta2=0.8*FIBER.beta2;
for n = 1:FIBER.nSpans/4
    TF = exp(-1j*0.5*FIBER.beta2*(fftshift(PARAMin.omega) - w0(n)).^2*FIBER.Lspan*4);
    switch MIMO.method
        case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
            Xi = fft(ifft(Xi).*TF);
            Xq = fft(ifft(Xq).*TF);
            Yi = fft(ifft(Yi).*TF);
            Yq = fft(ifft(Yq).*TF);
        otherwise,
            X = ifft(fft(X).*TF);
            Y = ifft(fft(Y).*TF);
    end
end

%% Output Parameters
switch MIMO.method
    case {'LMS:4x2','LMS_4x2','CMA:4x2','CMA:8x4','CMA_8x4'},
        Sout = [Xi; 1j*Xq; Yi; 1j*Yq];
    otherwise,
        Sout = [X; Y];
end
PARAMout = PARAMin;

