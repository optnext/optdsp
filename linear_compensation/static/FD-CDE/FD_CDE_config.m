function [CDE] = FD_CDE_config(CDE)

% Last Update: 03/04/2017


%% Input Parameters
% Fiber Parameters:
FIBER           = CDE.FIBER;
B2              = FIBER.beta2;
L               = FIBER.L;
% DFT Parameters:
nFFT            = CDE.DFT.nFFT;
w               = 2*pi*CDE.DFT.PARAM.f;
% CDE Parameters:
if isfield(CDE,'dz')
    CDE.nIter   = numel(CDE.dz);
elseif isfield(CDE,'nIter')
    CDE.dz      = repmat(L/CDE.nIter,1,L/CDE.nIter);
else
    CDE.nIter   = 1;
    CDE.dz      = L;
end
dz              = CDE.dz;
nWDMch          = length(CDE.WDM.channelsBP);
wc              = 2*pi*CDE.centerFreq;

%% Calculate D Operator
if nWDMch > 1
    % Determine the frequency grid for each WDM channel:
    if mod(nWDMch,2)
        dwn = -floor(nWDMch/2):floor(nWDMch/2);
    else
        dwn = -(nWDMch-1)/2:(nWDMch-1)/2;
    end
    % Define the D operator for each WDM channel:
    D = zeros(nWDMch,nFFT);
    for n = 1:nWDMch
        wD      = w + dwn(n)*WDMdf*2*pi;
        D(n,:)  = -1j*0.5*B2*wD.^2;
    end
else
    D = -1j*0.5*B2*(w-wc).^2;
%     D = -1j*0.5*B2*w.^2;
end

%% Calculate Transfer Function
TF = exp(D*dz);

%% Time-Domain Approximations
% T=2e-11;
% nTaps=2*floor(pi*abs(B2)*dz(1)/T^2)+1;
% Ht=ifft(fftshift(TF));
% centralTap=Ht(1);
% tapsLeft=Ht(nFFT-floor(nTaps/2)+1:nFFT);
% tapsRight=fliplr(tapsLeft);
% if mod(nTaps,2)
%     W=[tapsLeft centralTap tapsRight];
% else
%     W=[tapsLeft(2:end) centralTap tapsRight];
% end
% 
% maxTap=max(max([real(W) imag(W)]));
% W=round(W*2/maxTap);
% 
% centralTap=W(ceil(nTaps/2));
% tapsRight=W(ceil(nTaps/2)+1:end);
% tapsLeft=W(1:ceil(nTaps/2)-1);
% W2=[centralTap tapsRight zeros(1,nFFT-2*length(tapsRight)-1) tapsLeft];
% TF=fftshift(fft(W2));

%% Output Struct
CDE.D   = D;
CDE.TF  = TF;

%% Debug
if isfield(CDE,'debugPlots')
    for m = 1:length(CDE.debugPlots)
        switch CDE.debugPlots{m}
            case 'transferFunction'
                transferFunctionPlot(TF,w/(2*pi));
        end
    end
end
