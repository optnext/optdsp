function [A_CD,CDE] = TD_CDE(Ain,CDE,FIBER)

% Last Update: 31/03/2017


%% Input Parser
if ~isfield(CDE,'nIter')
    if isfield(CDE,'dzNorm')
        CDE.nIter = numel(CDE.dzNorm);
    end
else
    CDE.nIter = 1;
end
if ~isfield(CDE,'FIR')
    CDE.FIR.setTaps = 'iFFT';
    CDE.FIR.method = 'conv';
    CDE.FIR.qBits = Inf;
end
if ~isfield(CDE.FIR,'method')
    CDE.FIR.method = 'conv';
end

%% Get FIR Coefficients
if ~isfield(CDE,'W')
    beta2 = FIBER.beta2;
    for n = 1:CDE.nIter
        L = CDE.dzNorm(n) * FIBER.L;
        [W(n,:),CDE.FIR] = CDE_getFIRtaps(CDE.FIR,CDE.T,L,beta2);
    end
    CDE.FIR.W = W;
else
    if CDE.FIR.nTaps ~= length(CDE.FIR.W)
        warning('The number of CDE taps (CDE.FIR.nTaps) has been redefined to %d in order to agree with the preset filter taps (CDE.FIR.W)!"',length(CDE.FIR.W));
    end
    CDE.FIR.nTaps = length(CDE.FIR.W);
end

%% Input Parameters
nIter = CDE.nIter;

%% Apply FIR Filter
% ProgressBar Initialization:
progressbar(['Iteration number (',num2str(nIter),' in total)']);
for k = 1:nIter
    A_CD = CDE_FIR(Ain,W(k,:),CDE.FIR.method);
    Ain = A_CD;
        
    % ProgressBar:
    progressbar(k/nIter);
end
