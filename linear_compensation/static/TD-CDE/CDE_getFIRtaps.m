function [W,FIR] = CDE_getFIRtaps(FIR,T,L,B2)

% Last Update: 31/03/2017


%% Input Parser
% if ~isfield(CDE,'dz')
%     CDE.dz = 1 / CDE.nIter;
% end
if ~isfield(FIR,'setTaps')
    FIR.setTaps = 'IFFT';
end
if ~isfield(FIR,'qBits')
    FIR.qBits = Inf;
end

% if isfield(CDE,'corFIR')
%     if ~isfield(CDE.corFIR,'active')
%         CDE.corFIR.active   = false;
%     elseif CDE.corFIR.active
%         if ~isfield(CDE.corFIR,'nTF')
%             CDE.corFIR.nTF  = 1;
%         end
%     end
% else
%     CDE.corFIR.active   = false;
%     CDE.corFIR.nTF      = 0;
% end

%% Theorethic Number of Taps
nTapsTheorethic = 2*floor(pi*abs(B2)*L/T^2) + 1;
if ~isfield(FIR,'nTaps')
    if isfield(FIR,'nTapsFactor')
        nTaps = FIR.nTapsFactor*nTapsTheorethic;
    else
        nTaps = nTapsTheorethic;
    end
else
    nTaps = FIR.nTaps;
end

%% Calculate FIR Taps
switch FIR.setTaps
    case 'Savory'
        % Reference: S. J. Savory, "Digital filters for coherent optical receivers", 
        % Opt. Express, OSA, 2008, 16, 804-817.
        k = -floor(nTaps/2):ceil(nTaps/2)-1;
        W = sqrt(-1j*T^2/(2*pi*B2*L))*exp(1j*T^2*k.^2/(2*B2*L));
        W = W./sqrt(sum(abs(W).^2));
    case 'Ip'
        W = zeros(1,nTaps);
        for n = 1:nTaps
            h = 0;
            for k = -nTaps/2:nTaps/2-1
                h = h + exp(-1j*2*pi/nTaps*(pi*B2*L*k^2/(nTaps*T^2)-...
                    k*(n-nTaps/2-1)));
            end
            W(n) = h;
        end
        W = W/nTaps;
    case {'iFFT','IFFT'}
        nFFT    = 2^ceil(log2(nTaps));
        w       = 2*pi*(-nFFT/2:nFFT/2-1)*1/(T*nFFT);
        Hw      = exp(-1j*B2*w.^2*L/2);
        Ht      = ifft(fftshift(Hw));
        centralTap  = Ht(1);
        tapsLeft    = Ht(nFFT-floor(nTaps/2)+1:nFFT);
        tapsRight   = fliplr(tapsLeft);
        if mod(nTaps,2)
            W = [tapsLeft centralTap tapsRight];
        else
            W = [tapsLeft(2:end) centralTap tapsRight];
        end
    otherwise
        error('Unsuported CDE-FIR method!');
end

%% Quantization of FIR Coefficients
nBits = FIR.qBits;
if nBits < Inf
    nLevels = 2^(nBits-1);
    noiseA = 0*max(real(W))/nLevels;
    noise = rand(1,numel(W))*noiseA - noiseA/2;
    W = W + noise;
    W = quantizeSignal(W,nBits);
end

% 
% %% Normalize Taps
% if CDE.normalizeTaps
%     W = W/max(max(abs(real(W))),max(abs(imag(W))));
% end
% 
% %% Shift-and-Add Decomposition
% if CDE.integerTaps
%     W_original  = W;
%     maxTap      = max(max([real(W) imag(W)]));
%     W           = round(W*CDE.maxInteger/maxTap);
%     [realW_shifts,realW_adds,realW_sign] = power2decompose(real(W));
%     [imagW_shifts,imagW_adds,imagW_sign] = power2decompose(imag(W));
%     [realPlusImagW_shifts,realPlusImagW_adds,realPlusImagW_sign] = power2decompose(real(W)+imag(W));
%     [imagMinusRealW_shifts,imagMinusRealW_adds,imagMinusRealW_sign] = power2decompose(imag(W)-real(W));
% %     for n=1:length(CDE.W)
% %         CDE.realW_CSD(n,:)=csdigit(abs(real(CDE.W(n))),8,0);
% %     end
% 
%     symW = W(1:ceil(nTaps/2));
% 
%     realW_unique = unique(real(symW));
%     imagW_unique = unique(imag(symW));
%     maxInt = CDE.maxInteger;
%     
%     realW_uniqueAll = -maxInt:maxInt;
%     imagW_uniqueAll = realW_uniqueAll;
%     
%     for n = 1:length(realW_uniqueAll)
%         if ~isempty(find(realW_unique==realW_uniqueAll(n),1))
%             realW_ind{n} = find(real(symW)==realW_uniqueAll(n));
%             realW_ind{n}(end+1) = 1;
%         else
%             realW_ind{n} = [1 2];
%         end
%     end
%     for n = 1:length(imagW_uniqueAll)
%         if ~isempty(find(imagW_unique==imagW_uniqueAll(n),1))
%             imagW_ind{n} = find(imag(symW)==imagW_uniqueAll(n));
%             imagW_ind{n}(end+1) = 1;
%         else
%             imagW_ind{n} = [1 2];
%         end
%     end    
% %     for n=1:length(realW_unique)
% %         realW_ind{n}=find(real(W)==realW_unique(n));
% %     end    
% %     for n=1:length(imagW_unique)
% %         imagW_ind{n}=find(imag(W)==imagW_unique(n));
% %     end
% 
%     if CDE.preserveGain
%         W = W/max(max([real(W) imag(W)]))*maxTap;
%     end
% 
% end
% 
% %% Correction Filter
% if CDE.corFIR.active
%     Ht_target=Ht;
%     Ht_target(ceil(nTaps/2)+1:end-floor(nTaps/2))=0;
%     Hw_target=ifftshift(fft(Ht_target));
% 
%     centralTap=W(ceil(nTaps/2));
%     tapsRight=W(ceil(nTaps/2)-1:-1:1);
%     tapsLeft=fliplr(tapsRight);
%     Ht2=[centralTap tapsRight zeros(1,nFFT-nTaps) tapsLeft];
% 
%     Hw2=ifftshift(fft(Ht2));
% 
%     % HwCor=Hw_target./Hw2;
%     nTF=CDE.corFIR.nTF;
%     HwCor=(Hw_target.^nTF)./(Hw2.^nTF);
% 
%     HtCor=ifft(fftshift(HwCor));
%     centralTap=HtCor(1);
%     tapsLeft=HtCor(nFFT-floor(nTaps/2)+1:nFFT);
%     tapsRight=fliplr(tapsLeft);
%     W_cor=[tapsLeft centralTap tapsRight];
%     
%     CDE.corFIR.nTaps=nTaps;
%     CDE.corFIR.W=W_cor;
%     CDE.corFIR.convMethod=CDE.convMethod;
% end
% 
% % if CDE.integerTaps
% %     maxInteger=16;
% %     W_corOriginal=W_cor;
% %     centralTap=W_cor((nTaps+1)/2);
% %     W_cor((nTaps+1)/2)=0;
% %     maxTap=max(max([real(W_cor) imag(W_cor)]));
% %     W_cor=round(W_cor*maxInteger/maxTap);
% %     if CDE.preserveGain
% %         W_cor=W_cor/max(max([real(W_cor) imag(W_cor)]))*maxTap;
% %         W_cor((nTaps+1)/2)=centralTap;
% %     else
% %         W_cor((nTaps+1)/2)=centralTap*CDE.maxInteger/maxTap;
% %     end
% %     CDE.W_corOriginal=W_corOriginal;
% % end
% 
% %% Prune Taps
% % tolZero=6e-3;
% % realW=real(W);
% % imagW=imag(W);
% % realW(abs(realW)<tolZero)=0;
% % imagW(abs(imagW)<tolZero)=0;
% % 
% % tolMax=6e-3;
% % maxRealW=max(realW);
% % minRealW=min(realW);
% % realW(realW>maxRealW-tolMax)=maxRealW;
% % realW(realW<minRealW+tolMax)=minRealW;
% % maxImagW=max(imagW);
% % minImagW=min(imagW);
% % imagW(imagW>maxImagW-tolMax)=maxImagW;
% % imagW(imagW<minImagW+tolMax)=minImagW;
% % 
% % W=realW+1j*imagW;
% 
% %% Decimate FIR
% % centralTap=ceil(nTaps/2);
% % W(centralTap+1:2:centralTap+10)=0;
% % W(centralTap+1:-2:centralTap-10)=0;

%% Outputs
FIR.W               = W;
FIR.nTaps           = nTaps;
FIR.nTapsTheorethic = nTapsTheorethic;
% if CDE.integerTaps
%     CDE.shiftAdd.W_original             = W_original;
%     CDE.shiftAdd.realW_shifts           = realW_shifts;
%     CDE.shiftAdd.realW_adds             = realW_adds;
%     CDE.shiftAdd.realW_sign             = realW_sign;
%     CDE.shiftAdd.imagW_shifts           = imagW_shifts;
%     CDE.shiftAdd.imagW_adds             = imagW_adds;
%     CDE.shiftAdd.imagW_sign             = imagW_sign;
%     CDE.shiftAdd.realPlusImagW_shifts   = realPlusImagW_shifts;
%     CDE.shiftAdd.realPlusImagW_adds     = realPlusImagW_adds;
%     CDE.shiftAdd.realPlusImagW_sign     = realPlusImagW_sign;
%     CDE.shiftAdd.imagMinusRealW_shifts  = imagMinusRealW_shifts;
%     CDE.shiftAdd.imagMinusRealW_adds    = imagMinusRealW_adds;
%     CDE.shiftAdd.imagMinusRealW_sign    = imagMinusRealW_sign;
%     CDE.shiftAdd.realW_unique           = realW_unique;
%     CDE.shiftAdd.imagW_unique           = imagW_unique;
%     CDE.shiftAdd.realW_ind              = realW_ind;
%     CDE.shiftAdd.imagW_ind              = imagW_ind;
% end
if any(strcmp(FIR.setTaps,{'iFFT','IFFT'}))
    FIR.FFT.w       = w;
    FIR.FFT.Hw      = Hw;
    FIR.FFT.nFFT    = nFFT;
end

%% Debug
if isfield(FIR,'debugPlots')
    for m = 1:length(FIR.debugPlots)
        switch FIR.debugPlots{m}
            case 'filterTaps'
                if exist('Hw','var')
                    FIRtapsPlot(FIR);
                else
                    FIRtapsPlot(W);
                end
            case 'zero-pole'
                FIRzeroPolePlot(FIR);
        end
    end
end

