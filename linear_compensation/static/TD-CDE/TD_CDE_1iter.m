function [Aout]=TD_CDE_1iter(Ain,W)

% Last Update: 19/12/2014


%% Input Parser
if isstruct(W)
    convMethod=W.convMethod;
    if strcmp(convMethod,'distributive')
        realW_ind=W.shiftAdd.realW_ind;
        imagW_ind=W.shiftAdd.imagW_ind;
    end
    W=W.W;
else
    convMethod='filter';
end

%% Input Parameters
nTaps=length(W);
[nPol,nSamples]=size(Ain);

%% Apply FIR Filter
Aout=zeros(nPol,nSamples);
switch convMethod,
    case 'vector',
        for n=ceil(nTaps/2):nSamples-floor(nTaps/2)
            for m=1:nPol
                Aout(m,n)=Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)*W.';
            end
        end
    case 'filter',
        for m=1:nPol
            Aout(m,:)=filter(W,1,Ain(m,:));
        end
    case 'conv',
        for m=1:nPol
            Aout(m,:)=conv(Ain(m,:),W,'same');
        end
    case 'distributive',
        for n=ceil(nTaps/2):nSamples-floor(nTaps/2)
            for m=1:nPol
                A=fliplr(Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1));
                for k=1:length(realW_ind)
                    S_real(k)=sum(A(realW_ind{k}))*real(W(realW_ind{k}(1)));
                end
                for k=1:length(imagW_ind)
                    S_imag(k)=sum(A(imagW_ind{k}))*imag(W(imagW_ind{k}(1)));
                end
                Aout(m,n)=sum(S_real)+1j*sum(S_imag);
            end
        end
    case 'angle',
%         angW=atan(imag(W)./real(W));
        angW=atan2(imag(W),real(W));
        absW=abs(W);
        absW_correct=2./absW;
%         newW=absW.*exp(1j*angW);
%         newW=exp(1j*angW);
%         newW=absW_correct.*absW.*exp(1j*angW);
        newW=(absW_correct.*absW).^2.*exp(1j*angW);
        
        for n=ceil(nTaps/2):nSamples-floor(nTaps/2)
            for m=1:nPol
%                 Aout(m,n)=Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)*exp(1j*angW.');
                Aout(m,n)=Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)*newW.';
            end
        end
        
        
%         Ax_ang=atan2(imag(Ain(1,:)),real(Ain(1,:)));
%         Ax_abs=abs(Ain(1,:));
%         Ay_ang=atan2(imag(Ain(2,:)),real(Ain(2,:)));
%         Ay_abs=abs(Ain(2,:));
% 
% %         maxTap=max(Ax_abs);
% %         Ax_abs=round(Ax_abs*8/maxTap);
% %         maxTap=max(Ay_abs);
% %         Ay_abs=round(Ay_abs*8/maxTap);
% 
%         
%         Ax_out=zeros(1,nSamples);
%         Ay_out=zeros(1,nSamples);
%         for n=ceil(nTaps/2):nSamples-floor(nTaps/2)
% %             Ax_out(n)=sum(Ax_ang(n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)+angW);
% %             Ay_out(n)=sum(Ay_ang(n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)+angW);
% 
%             ind=n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1;
%             Aout(1,n)=sum(Ax_abs(ind).*exp(1j*(Ax_ang(ind)+angW)));
%             Aout(2,n)=sum(Ay_abs(ind).*exp(1j*(Ay_ang(ind)+angW)));
% %             Aout(1,n)=sum(Ax_abs(ind)).*sum(exp(1j*(Ax_ang(ind)+angW)));
% %             Aout(2,n)=sum(Ay_abs(ind)).*sum(exp(1j*(Ay_ang(ind)+angW)));
% %             Aout(1,n)=sum(Ax_abs(ind)).*sum(exp(1j*(Ax_ang(ind)+angW)));
% %             Aout(2,n)=sum(Ay_abs(ind)).*exp(1j*sum((Ay_ang(ind)+angW)));
%         end
% %         Aout(1,:)=Ax_abs.*exp(1j*Ax_out);
% %         Aout(2,:)=Ay_abs.*exp(1j*Ay_out);
%         
% %         for n=ceil(nTaps/2):nSamples-floor(nTaps/2)
% %             for m=1:nPol
% %                 Aout_abs(m,n)=Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)*exp(1j*angW.');
% %             end
% %         end        
            
    otherwise,
        error('Invalid TD-CDE version!');
end

