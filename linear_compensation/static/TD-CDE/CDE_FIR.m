function [Aout] = CDE_FIR(Ain,W,method)

% Last Update: 31/03/2017


%% Input Parameters
nTaps = length(W);
[nPol,nSamples] = size(Ain);

%% Apply FIR Filter
Aout = zeros(nPol,nSamples);
switch method
    case 'vector'
        for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
            for m = 1:nPol
                Aout(m,n) = Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1)*W.';
            end
        end
    case 'filter'
        for m = 1:nPol
            Aout(m,:) = filter(W,1,Ain(m,:));
        end
    case 'conv'
        for m = 1:nPol
            Aout(m,:) = conv(Ain(m,:),W,'same');
        end
    case 'distributive'
        realW_ind = FIR.shiftAdd.realW_ind;
        imagW_ind = FIR.shiftAdd.imagW_ind;
        for n = ceil(nTaps/2):nSamples-floor(nTaps/2)
            for m = 1:nPol
                A = fliplr(Ain(m,n+floor(nTaps/2):-1:n-ceil(nTaps/2)+1));
                for k = 1:length(realW_ind)
                    S_real(k) = sum(A(realW_ind{k}))*real(W(realW_ind{k}(1)));
                end
                for k = 1:length(imagW_ind)
                    S_imag(k) = sum(A(imagW_ind{k}))*imag(W(imagW_ind{k}(1)));
                end
                Aout(m,n) = sum(S_real)+1j*sum(S_imag);
            end
        end
    otherwise
        error('Invalid TD-CDE version!');
end

