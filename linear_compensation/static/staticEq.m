function [S,PARAM,EQout] = staticEq(S,PARAM,EQin,FIBER)

% Last Update: 16/04/2017


global PROG;

%% Entrance Message
entranceMsg(PROG.MSG.staticEq);
tic

%% Compensation of the Received Signal
for n = 1:length(EQin)
    EQ = EQin(n);
    switch EQ.method
        case {'BP-SSF','BP-SSFM','DBP-SSF','DBP-SSFM'}
            EQ.DFT      = FFTconf(EQ.DFT,PARAM);
            S       	= powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = BP_SSF(S,FIBER,EQ);
            [S,PARAM]   = rmvEdgeSamplesFFT(S,PARAM,EQ.DFT);
        case {'BP-SSFIR','DBP-SSFIR'}
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = BP_SSFIR(S,EQ,FIBER,PARAM);
            [S,PARAM]   = rmvEdgeSamplesFIR(S,PARAM,EQ.FIR);
        case 'BP-VSTF'
            EQ.DFT      = FFTconf(EQ.DFT,PARAM);
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = BP_VSTF(S,FIBER,EQ);
            [S,PARAM]   = rmvEdgeSamplesFFT(S,PARAM,EQ.DFT);
        case 'FD-VSNE'
            EQ.DFT      = FFTconf(EQ.DFT,PARAM);
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = FD_VSNE(S,FIBER,EQ);
            [S,PARAM]   = rmvEdgeSamplesFFT(S,PARAM,EQ.DFT);            
        case 'FD-CDE'
            EQ.DFT      = FFTconf(EQ.DFT,PARAM);
            [S,EQ]      = FD_CDE(S,FIBER,EQ);
            [S,PARAM]   = rmvEdgeSamplesFFT(S,PARAM,EQ.DFT);
        case 'FD-CDE:4x2'
            EQ.DFT      = FFTconf(EQ.DFT,PARAM);
            [S,EQ]      = FD_CDE_4x2(S,FIBER,EQ);
            [S,PARAM]   = rmvEdgeSamplesFFT(S,PARAM,EQ.DFT);
        case 'TD-CDE'
            EQ.T        = PARAM.dt;
            if isfield(EQin,'P0_dBm')
                S       = powerNorm(S,EQin.P0_dBm);
            end
            [S,EQ]      = TD_CDE(S,EQ,FIBER);
            [S,PARAM]   = rmvEdgeSamplesFIR(S,PARAM,EQ.FIR);
        case 'TD-CDE+iXPM'
            EQ.T        = PARAM.dt;
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = TD_CDE_iXPM_2pol(S,EQ,FIBER,PARAM);
            [S,PARAM]   = rmvEdgeSamplesFIR(S,PARAM,EQ);
        case 'TD-VSNE'
            EQ.T        = PARAM.dt;
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = TD_VSNE_Manakov(S,EQ,FIBER);
            [S,PARAM]   = rmvEdgeSamplesFIR(S,PARAM,EQ);
        case 'W-VSNE'
            EQ.T        = PARAM.dt;
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]        = W_VSNE_Manakov(S,EQ,FIBER);
        case 'W-VSNE:FD-CDE'
            EQ.T        = PARAM.dt;
            EQ.DFT      = FFTconf(EQ.DFT,PARAM);
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = W_VSNE_Manakov_FD_CDE(S,EQ,FIBER);
            [S,PARAM]   = rmvEdgeSamplesFFT(S,PARAM,EQ.DFT);
        case 'TD-NLE'
            EQ.T        = PARAM.dt;
            S           = S{1};
            Acd         = S{2};
            S           = TD_NLE(S,Acd,EQ,FIBER);
        case 'BP-PSSFIR'
            EQ.T        = PARAM.dt;
            S           = powerNorm(S,EQin.P0_dBm);
            [S,EQ]      = BP_P_SSFIR_b(S,EQ,FIBER);
            [S,PARAM]   = rmvEdgeSamplesFIR(S,PARAM,EQ);
        case 'none'
        otherwise, error('Invalid Compensation Method!');
    end
    EQout(n) = EQ;
end

%% Elapsed Time
elapsedTime = toc;
myMessages(['Signal Compensation - Elapsed Time: ',...
    num2str(elapsedTime,'%1.4f'),' [s]\n'],1);

